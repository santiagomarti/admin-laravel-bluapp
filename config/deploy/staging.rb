set :default_stage, "staging"
set :application, "WebAdminDev"
set :repository,  "git@bitbucket.org:bagdo/ibeaconwebadmin.git"
set :deploy_to, "/home/admin/dev/public_html/#{application}"

set :scm, :git

set :user, "admin"
set :use_sudo, false
set :branch, "master"

role :web, "ib.dev.vs.cl"
role :app, "ib.dev.vs.cl"
role :db,  "ib.dev.vs.cl", :primary => true

set :keep_releases, 5


set :format, :pretty
set :log_level, :debug
set :pty, true

namespace :deploy do

   task :update do
      transaction do
         update_code # built-in function
         prepare_laravel_storage
         composer_install
         prepare_artisan
         create_symlink # built-in function
         symlink_upload
         composer_update
         migrate
      end
   end

   task :migrate do
      transaction do
         run "cd #{current_release} && php artisan migrate --env=stage; true"
      end
   end

   task :composer_install do
      transaction do
         run "cd #{current_release} && composer install --no-dev --quiet; true"
      end
   end

   task :composer_update do
      transaction do
         run "cd #{current_release} && composer update; true"
      end
   end

   task :prepare_artisan do
      transaction do
         run "chmod u+x #{current_release}/artisan"
      end
   end

   task :prepare_laravel_storage do
      transaction do
         run "mkdir -p #{current_release}/app/storage/cache"
         run "mkdir -p #{current_release}/app/storage/logs"
         run "mkdir -p #{current_release}/app/storage/meta"
         run "mkdir -p #{current_release}/app/storage/sessions"
         run "mkdir -p #{current_release}/app/storage/views"
      end
   end

   task :symlink_upload do
    run "mkdir -p #{shared_path}/public;true"
    run "mkdir -p #{shared_path}/public/avatars;true"
    run "mkdir -p #{shared_path}/public/avatars/mp4;true"
    run "mkdir -p #{shared_path}/public/avatars/ads;true"
    run "mkdir -p #{shared_path}/public/avatars/companies;true"
    run "mkdir -p #{shared_path}/public/avatars/locals;true"
    run "chmod -R 777 #{shared_path}/public;true"
    run "ln -nfs #{shared_path}/public/avatars #{current_release}/public/avatars"
    run "cp #{current_release}/app/database/seeds/avatars/images* #{shared_path}/public/avatars #{current_release}/public/avatars/ads/.;true"
    run "cp #{current_release}/app//database/seeds/avatars/logo* #{shared_path}/public/avatars #{current_release}/public/avatars/companies/.;true"
  end

   task :restart do
      transaction do
         run "chmod -R g+w #{releases_path}/#{release_name}"
         run "chmod -R 777 #{current_release}/app/storage/cache"
         run "chmod -R 777 #{current_release}/app/storage/logs"
         run "chmod -R 777 #{current_release}/app/storage/meta"
         run "chmod -R 777 #{current_release}/app/storage/sessions"
         run "chmod -R 777 #{current_release}/app/storage/views"
      end
   end

   task :tail do
     trap("INT") { puts 'Interupted'; exit 0; }
     run "tail -f #{current_release}/app/storage/logs/log-apache2handler-2014-05-08.txt" do |channel, stream, data|
       puts  # for an extra line break before the host name
       puts "#{channel[:host]}: #{data}"
       break if stream == :err
     end
  end


end