var FormDropzone = function () {

    return {
        //main function to initiate the module
        init: function () {  

            Dropzone.options.myDropzone = {
                url: "/account/dropzone",
                maxFiles: 1,
                maxfilesexceeded: function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                },
                acceptedFiles: ".jpg,.png,.jpeg",
                dictInvalidFileType: "Invalid file type",
                thumbnailWidth: null,
                thumbnailHeight: null,
                init: function() {
                    this.on("addedfile", function(file) {
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-danger'>Remove file</button>");
                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function(e) {
                          // Make sure the button click doesn't submit the form:
                          e.preventDefault();
                          e.stopPropagation();

                          // Remove the file preview.
                          _this.removeFile(file);
                          // If you want to the delete the file on the server as well,
                          // you can do the AJAX request here.
                          $.ajax({
                              type: "delete",
                              url : "/account/dropzone/remove",
                              success : function(){
                                console.log('delete');
                              },
                              error : function(){
                                console.log('error');
                              }
                          },"json");
                        });

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });
                }            
            }
        }
    };
}();