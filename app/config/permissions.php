<?php

return array(

	// 'Global' => array(
	// 	array(
	// 		'permission' => 'superuser',
	// 		'label'      => 'Super User',
	// 	),
	// ),

	'Admin' => array(
		array(
			'permission' => 'admin',
			'label'      => 'Admin Rights',
		),
	),

	'Company' => array(
		array(
			'permission' => 'subadmin',
			'label'      => 'Company  Rights',
		),
	),

	'Operator' => array(
		array(
			'permission' => 'operator',
			'label'      => 'Company Operator',
		),
	),

	'ManageAdminUser' => array(
		array(
			'permission' => 'manage_users_for_admin',
			'label'      => 'Manage Admin User and Company(Company) User',
		),
	),

	'ManageCompanyUser' => array(
		array(
			'permission' => 'manage_users_for_company',
			'label'      => 'Manage Company(Company) User and Operator User',
		),
	),
	'Company' => array(
		array(
			'permission' => 'manage_companies',
			'label'      => 'Manage Companies',
		),
	),

	'Beacons' => array(
		array(
			'permission' => 'manage_beacons',
			'label'      => 'Manage Beacons',
		),
	),

	'ModifyBeacon' => array(
		array(
			'permission' => 'only_modify_beacon',
			'label'      => 'Modify Beacon only for Company User',
		),
	),
	'Zones' => array(
		array(
			'permission' => 'manage_zones',
			'label'      => 'Manage Zones',
		),
	),

	'Campaigns' => array(
		array(
			'permission' => 'manage_campaigns',
			'label'      => 'Manage Campaigns',
		),
	),

);
