<?php

return array(

	'siteName' => 'BlueApp',
	'siteOwner' => 'magno',
	'privateAccessToken' => 'more talent less ego',
	'headerAccessToken' => 'X-IBEACONS-ACESSTOKEN',
	'headerLocale' => 'X-IBEACONS-LOCALE',
	'localeEnum' => array('es','en'),
	'siteVersion' => '1.0.3',
	'tag' => '1.0.1', //use on bitbucket.org
	'timeZone' => 4,
	'beaconsIDs' => array('B9407F30-F5F8-466E-AFF9-25556B57FE6D',
						  'F7826DA6-4FA2-4E98-8024-BC5B71E0893E')

);