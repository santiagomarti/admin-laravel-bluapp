<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{

        $users = array(

        	// Users

	        array('id'              	=> 		'1',
	            'email'                 => 		'admin@blueapp.cl',
	            'password'              => 		'$2y$10$tflaNKnEmVQR8vzI2hLXaO6LPvqFbdoM82Ng97I1Nv8iJd4UmRkXC',
	            'permissions'           => 		'{"admin":1,"users":1}',
	            'activated'             => 		'1',
	            'activation_code'       => 		NULL,
	            'activated_at'          => 		NULL,
	            'last_login'            => 		NULL,
	            'persist_code'          => 		NULL,
	            'reset_password_code'   => 		NULL,
	            'first_name'            => 		'Admin',
	            'last_name'             => 		'Admin',
	            'created_at'            => 		'2014-01-06 15:09:14',
	            'updated_at'            => 		'2014-01-27 15:47:05',
	            'deleted_at'            => 		NULL,
	            'website'               => 		NULL,
	            'country'               => 		NULL,
	            'gravatar'              => 		NULL),

            array('id'             		=> 		'2',
	            'email'                 => 		'sa@blueapp.cl',
	            'password'              => 		'$2y$10$tflaNKnEmVQR8vzI2hLXaO6LPvqFbdoM82Ng97I1Nv8iJd4UmRkXC',
	            'permissions'           => 		'{"superuser":-1}',
	            'activated'             => 		'1',
	            'activation_code'       => 		NULL,
	            'activated_at'          => 		NULL,
	            'last_login'            => 		NULL,
	            'persist_code'          => 		NULL,
	            'reset_password_code'   => 		NULL,
	            'first_name'            => 		'Subadmin Blueapp',
	            'last_name'             => 		'Subadmin Blueapp',
	            'created_at'            => 		'2014-01-06 15:09:15',
	            'updated_at'            => 		'2014-01-06 19:25:44',
	            'deleted_at'            => 		NULL,
	            'website'               => 		NULL,
	            'country'               => 		NULL,
	            'gravatar'              => 		NULL),

            array('id'             		=> 		'3',
	            'email'                 => 		'op@blueapp.cl',
	            'password'              => 		'$2y$10$tflaNKnEmVQR8vzI2hLXaO6LPvqFbdoM82Ng97I1Nv8iJd4UmRkXC',
	            'permissions'           => 		'{"superuser":-1}',
	            'activated'             => 		'1',
	            'activation_code'       => 		NULL,
	            'activated_at'          => 		NULL,
	            'last_login'            => 		'2014-01-27 15:30:35',
	            'persist_code'          => 		NULL,
	            'reset_password_code'   => 		NULL,
	            'first_name'            => 		'Operator Blueapp',
	            'last_name'             => 		'Operator Blueapp',
	            'created_at'            => 		'2014-01-06 19:28:10',
	            'updated_at'            => 		'2014-01-27 15:30:35',
	            'deleted_at'            => 		NULL,
	            'website'               => 		NULL,
	            'country'               => 		NULL,
	            'gravatar'              => 		NULL),

		);

		// Uncomment the below to run the seeder,

		 DB::table('users')->insert($users);

		// //update data
		//DB::table('users')->where('id', 1)->update(array('cover' => 'avatars/somejpeg.png', 'extrafiels' => '1111'));
		
	}

}


