<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		DB::table('throttle')->truncate();

		DB::table('users_groups')->truncate();
		DB::table('users_locals')->truncate();
		DB::table('users')->truncate();
		DB::table('groups')->truncate();

		DB::table('companies')->truncate();

		DB::table('users_companies')->truncate();

		DB::table('locals')->truncate();
		DB::table('ads')->truncate();
		DB::table('ibeacons')->truncate();

		$this->call('CompaniesTableSeeder');
		$this->call('GroupsTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('UsersGroupsTableSeeder');
		$this->call('UsersCompaniesTableSeeder');
		$this->call('LocalsTableSeeder');
		$this->call('IbeaconsTableSeeder');
		$this->call('AdsTableSeeder');
		$this->call('UsersLocalsTableSeeder');

		$this->command->info("Magno says: Welcome to the Kwiki mart!");
	}

}
