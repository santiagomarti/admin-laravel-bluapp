<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AdsTableSeeder extends Seeder {

	public function run()
	{
		Ad::create(array(
          'id'         => 1,
          'title'      => 'test',
          'body'       => 'lorem ipsu',
          'cover'      => 'ads/images1@2x.png',
          'url'        => 'avatars/example1cb.png',
          'type'       => 'image',
          'ibeacon_id' => 1));

		Ad::create(array(
          'id'         => 2,
          'title'      => 'test 2',
          'body'       => 'Life happens over coffee',
          'cover'      => 'ads/images2@2x.png',
          'url'        => 'avatars/example2cb.png',
          'type'       => 'image',
          'promote'    => TRUE,
          'msg_enter'    => 'Free Coffee for All',
          'ibeacon_id' => 1));

		// Ad::create(array(
  //         'id'         => 3,
  //         'title'      => 'test 3',
  //         'body'       => 'Lorem lipsu 10',
  //         'cover'      => 'avatars/example1e.png',
  //         'url'        => 'avatars/example1eb.png',
  //         'type'       => 'image',
  //         'ibeacon_id' => 2));

		// Ad::create(array(
  //         'id'         => 4,
  //         'title'      => 'test4',
  //         'body'       => 'lorem ipsu 4',
  //         'cover'      => 'avatars/example2e.png',
  //         'url'        => 'avatars/example2eb.png',
  //         'type'       => 'image',
  //         'ibeacon_id' => 2));

		// Ad::create(array(
  //         'id'         => 5,
  //         'title'      => 'Photography',
  //         'body'       => 'Vivir mejor conectado',
  //         'cover'      => 'avatars/example3e.png',
  //         'url'        => 'http://acidfilez.wix.com/test',
  //         'type'       => 'html',
  //         'promote'    => TRUE,
  //         'msg_exit'   => 'Thank you come back soon',
  //         'ibeacon_id' => 2));

		// Ad::create(array(
  //         'id'         => 6,
  //         'title'      => 'Portabilidad',
  //         'body'       => 'lorem ipsu 6',
  //         'cover'      => 'avatars/example4e.png',
  //         'url'        => 'avatars/mp4/portabilidad.m4v',
  //         'type'       => 'video',
  //         'ibeacon_id' => 2));

  //         Ad::create(array(
  //         'id'         => 7,
  //         'title'      => 'Sony',
  //         'body'       => 'Lorem',
  //         'cover'      => 'avatars/Example1s.png',
  //         'url'        => 'avatars/Example1s.png',
  //         'type'       => 'image',
  //         'ibeacon_id' => 3));

  //         Ad::create(array(
  //         'id'         => 8,
  //         'title'      => 'Macan S',
  //         'body'       => 'Lorem Lorem Macan S',
  //         'cover'      => 'avatars/macans.png',
  //         'url'        => 'avatars/macans.png',
  //         'type'       => 'image',
  //         'ibeacon_id' => 4));

  //         Ad::create(array(
  //         'id'         => 9,
  //         'title'      => 'Macan S2',
  //         'body'       => 'Lorem Lorem Macan S2',
  //         'cover'      => 'avatars/macans.png',
  //         'url'        => 'avatars/macans.png',
  //         'type'       => 'image',
  //         'ibeacon_id' => 5));

	}

}