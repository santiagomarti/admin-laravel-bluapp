<?php

class UsersGroupsTableSeeder extends Seeder {

	public function run()
	{

		$users_groups = array(
            array('user_id' => '1','group_id'   => '1'),      //Admin
            array('user_id' => '2','group_id'   => '2'),      //SubAdmin
            array('user_id' => '3','group_id'   => '4')       //Operator
		);


		// Uncomment the below to run the seeder
		DB::table('users_groups')->insert($users_groups);
	}

}
