<?php

class UsersCompaniesTableSeeder extends Seeder {

	public function run()
	{

		$users_companies = array(
		//BlueApp
            array('user_id' => '2','company_id' => '1'),    //SubAdmin
            array('user_id' => '3','company_id' => '1')     //Operator

            );


		// Uncomment the below to run the seeder
		DB::table('users_companies')->insert($users_companies);
	}

}
