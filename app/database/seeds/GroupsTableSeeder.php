<?php

class GroupsTableSeeder extends Seeder {

	public function run()
	{

		$groups = array(
            array('id'    => '1',
                'name'    => 'Admin',
            'permissions' => '{"admin":1,"users":1}',
            'created_at'  => '2014-01-06 15:09:14',
            'updated_at'  => '2014-01-06 15:09:14'),

            array('id'    => '2',
                'name'    => 'SubAdmin',
            'permissions' => '{"subadmin":1}',
            'created_at'  => '2014-01-07 17:39:15',
            'updated_at'  => '2014-01-07 17:39:15'),

            array('id'    => '4',
                'name'    => 'Operator',
            'permissions' => '{"operator":1}',
            'created_at'  => '2014-01-08 18:02:39',
            'updated_at'  => '2014-01-08 18:02:39')
		);


		// Uncomment the below to run the seeder
		DB::table('groups')->insert($groups);
	}

}
