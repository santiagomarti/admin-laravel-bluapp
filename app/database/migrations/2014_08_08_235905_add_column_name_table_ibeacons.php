<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnNameTableIbeacons extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ibeacons', function(Blueprint $table) {
			$table->text('name');
			$table->integer('company_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ibeacons', function(Blueprint $table) {
			$table->dropColumn('name');
			$table->dropColumn('company_id');
		});
	}

}
