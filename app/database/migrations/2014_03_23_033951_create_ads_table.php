<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ads', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('body');
			$table->string('cover');
			$table->string('url');
			$table->integer('ibeacon_id');
			$table->enum('type', array('image', 'video', 'sound', 'html'));
			$table->boolean('active')->default(TRUE);
			$table->boolean('promote')->default(FALSE);
			$table->string('msg_enter')->default('');
			$table->string('msg_exit')->default('');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ads');
	}

}
