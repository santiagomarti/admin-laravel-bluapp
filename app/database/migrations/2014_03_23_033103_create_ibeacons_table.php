<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIbeaconsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ibeacons', function(Blueprint $table) {
			$table->increments('id');
			$table->string('peripheralID');
			$table->string('proximityUUID');
			$table->integer('major');
			$table->integer('minor');
			$table->integer('rssi');
			$table->enum('proximity' , array('immediate','near','far','unknown'))->default('immediate');
			$table->integer('local_id');
			$table->string('cover');
			$table->boolean('active')->default(TRUE);
			$table->boolean('enter')->default(FALSE);
			$table->boolean('exit')->default(FALSE);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ibeacons');
	}

}
