<?php

class Tracking extends Eloquent {

    /**
     * Indicates if the model should soft delete.
     *
     * @var bool
     */
	protected $table = 'tracking';
	
    protected $fillable = array('id','name','company_id', 'cover', 'active', 'description', 'ibeacons', 'company', 'ads');

    public function campaign()
    {
        return $this->belongsTo('Campaign','idcamp');
    }

}