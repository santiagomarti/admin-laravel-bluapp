<?php
class Company extends Eloquent {
    public static $headerSort = ['id', 'name', 'description', 'active', ''];

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	protected $fillable = array('id', 'name', 'active', 'description', 'cover');
	protected $visible = array('id', 'name', 'active', 'ibeacons', 'locals', 'ads', 'cover', 'description');
	const ACTIVE = 1;
	const UNACTIVE = 0;
    /**
	 * Check if the Company is activated.
	 *
	 * @return bool
	 */
	public function isActive()
	{
		return (bool) $this->active;
	}

 	/**
	 * The user companies pivot table name.
	 *
	 * @var string
	 */
	protected static $userCompaniesPivot = 'users_companies';
	protected static $userModel2 = 'User';

	/**
	 * Returns the relationship between users and companies.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users($withTrashed, $onlyTrashed)
	{

		$users = $this->belongsToMany(static::$userModel2, static::$userCompaniesPivot);

		// Do we want to include the deleted users?
		if ($withTrashed)
		{
			$users = $users->withTrashed();
		}
		else if ($onlyTrashed)
		{
			$users = $users->onlyTrashed();
		}

		return $users;
	}

	/**
	 * Returns the company's ID.
	 *
	 * @return  mixed
	 */
	public function getId()
	{
		return $this->getKey();
	}

	public function ibeacons()
	{

		//return $this->hasManyThrough('Ibeacon','Local')->hasManyThrough('Local','Company');
	    //return $this->hasMany('Ibeacon');
		//return $this->hasManyThrough('Ibeacon', 'Local')->hasManyThrough('Local', 'Company');
	}

	public function locals()
	{
	    return $this->hasMany('Local');
	}

	public function ads()
    {
    	return $this->hasMany('Local')->hasMany('Ibeacon')->hasMany('Ad');
        //return $this->hasManyThrough('Ad', 'Local')->hasManyThrough('Local', 'Ibeacon');
    }
}