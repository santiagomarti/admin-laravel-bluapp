<?php

use Cartalyst\Sentry\Groups\Eloquent\Group as SentryGroupModel;

class Group extends SentryGroupModel {
    public static $headerSort = ['id', 'name', 'number_user', 'created_at', ''];

    // additional helper relation for the count
    public function usersCount()
    {
        return $this->belongsToMany('User', 'users_groups')
            ->selectRaw('count(*) as number_user')
            ->groupBy('group_id');
    }

    // accessor for easier fetching the count
    public function getUsersCountAttribute()
    {
        if ( ! array_key_exists('usersCount', $this->relations)) $this->load('usersCount');

        $related = $this->getRelation('usersCount')->first();

        return ($related) ? $related->number_user : 0;
    }

}
