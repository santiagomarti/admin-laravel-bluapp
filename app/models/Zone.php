<?php

class Zone extends Eloquent {
    public static $headerSort = ['id', 'name', 'description', 'active'];


    /**
     * Indicates if the model should soft delete.
     *
     * @var bool
     */
    protected $softDelete = true;

    protected $fillable = array('id', 'name', 'cover', 'active', 'description', 'companies_id');

    /**
     * Set table
     * @var bool
     */
    protected $table = 'zones';

    /**
     * The user companies pivot table name.
     *
     * @var string
     */
   

    /**
     * Returns the locals's ID.
     *
     * @return  mixed
     */
    public function getId()
    {
        return $this->getKey();
    }

    /**
     * Check if the Company is activated.
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->active;
    }

    public function ibeacons()
    {
        return $this->hasMany('Ibeacon', 'zones_id');
    }

    public function company()
    {
        return $this->belongsTo('Company');
    }

    /**
     * Check if the company is activated.
     *
     * @return bool
     */
    public function isCompanyActive()
    {
        return (bool) $this->company()->first()->isActive();
    }
}