<?php

class Contact extends Eloquent {

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	protected $visible = array('id', 'contact_id', 'user_id', 'status', 'created_at', 'updated_at', 'alias', 'invitation', 'order', 'contact', 'request', 'onsite_created_at', 'onsite_ended_at', 'ended_at' );

 	protected $fillable = array('contact_id', 'user_id', 'status', 'alias', 'invitation', 'order', 'onsite_created_at', 'onsite_ended_at', 'ended_at');

	/**
	 * Deletes a contact and all the associated comments.
	 *
	 * @return bool
	 */
	public function delete()
	{
		//Delete in cascade here ..
		// Delete zone
		return parent::delete();
	}

	//returns the user that this contact belogns to.
	public function user()
    {
        return $this->belongsTo('User');
    }

    //return contact (user info)
    public function contact()
    {
        return $this->hasOne('User', 'id', 'contact_id');
    }

    //return request (user info)
    public function request()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }

}
