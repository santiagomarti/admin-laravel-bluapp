<?php

class Access extends Eloquent {


	protected $visible = array(
		'id',
		'user_id',
		'device_id',
		'access_token',
		'public_token',
		'created_at'
	);


 	protected $fillable = array('user_id', 'access_token', 'public_token', 'device_id');

	/**
	 * Deletes
	 *
	 * @return bool
	 */
	public function delete()
	{
		//Delete in cascade here ..
		// Delete the useraccess post
		return parent::delete();
	}


}
