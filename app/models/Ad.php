<?php

class Ad extends Eloquent {

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	protected $visible = array('id', 'active', 'bookshelf', 'description', 'title', 'body', 'cover', 'url', 'ibeacon_id', 'type', 'promote', 'created_at', 'updated_at', 'ibeacon' ,'msg_enter', 'msg_exit', 'ttl_enter', 'ttl_exit' );
	protected $fillable = array('id', 'active', 'bookshelf', 'description', 'title', 'body', 'cover', 'url', 'ibeacon_id', 'type', 'promote', 'created_at', 'updated_at','msg_enter', 'msg_exit', 'ttl_enter', 'ttl_exit');


	public function ibeacon()
    {
        return $this->belongsTo('Ibeacon');
    }

    /**
     * Check if the Company is activated.
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->active;
    }

    /**
     * Check if the Ads will be display on the bookshelf
     *
     * @return bool
     */
    public function isBookshelf()
    {
        return (bool) $this->bookshelf;
    }

    /**
     * Check if the Company is activated.
     *
     * @return bool
     */
    public function isPromote()
    {
        return (bool) $this->promote;
    }

    /**
     * Check if the company is activated.
     *
     * @return bool
     */
    public function isCompanyActive()
    {
    	return true;
    }

}