<?php

class Campaign extends Eloquent {

    public static $headerSort = ['title', 'msg_enter', 'type', 'begin_date', 'active'];


	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;
    /**
     * Setting table.
     */
    protected $table = 'campaigns';
	protected $visible = array('id', 'active', 'bookshelf', 'description', 'title', 'body', 'cover', 'url', 'type', 'promote', 'created_at', 'updated_at', 'ibeacon' ,'msg_enter', 'msg_exit', 'ttl_enter', 'ttl_exit','covertype' );
	protected $fillable = array('id', 'active', 'bookshelf', 'description', 'title', 'body', 'cover', 'url', 'ibeacons_id', 'type', 'promote', 'created_at', 'updated_at','msg_enter', 'msg_exit', 'ttl_enter', 'ttl_exit','covertype','begin_date','end_date');

    public static $types = ['image','html-url','video'];

    public function ibeacon()
    {
        return $this->belongsTo('Ibeacon','ibeacons_id');
    }
    public function zone()
    {
        return $this->belongsTo('Zone','zones_id');
    }

    public function trackings()
    {
        return $this->hasMany('Tracking','idcamp');
    }

    /**
     * Check if the Company is activated.
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->active;
    }

    /**
     * Check if the Ads will be display on the bookshelf
     *
     * @return bool
     */
    public function isBookshelf()
    {
        return (bool) $this->bookshelf;
    }
    /**
     * Check if the Company is activated.
     *
     * @return bool
     */
    public function isPromote()
    {
        return (bool) $this->promote;
    }
}