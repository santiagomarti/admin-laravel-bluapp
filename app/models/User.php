<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;

class User extends SentryUserModel {
	const USER_ADMIN = 1;
	const USER_SUBADMIN = 2;
	const USER_OPERATOR = 3;

    public static $headerSort = ['id', 'first_name', 'last_name', 'email', 'activated', 'roles', 'companies_id', 'created_at'];
    protected $table = 'users';
	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Returns the user full name, it simply concatenates
	 * the user first and last name.
	 *
	 * @return string
	 */
	public function getFullNameAttribute()
	{
		return "{$this->first_name} {$this->last_name}";
	}

	/**
	 * Returns the user full name, it simply concatenates
	 * the user first and last name.
	 *
	 * @return string
	 */
	public function fullName()
	{
		return "{$this->first_name} {$this->last_name}";
	}

	/**
	 * Returns the user full name, it simply concatenates
	 * the user first and last name.
	 *
	 * @return string
	 */
	public function can()
	{
		return $this->id == 1;
	}


	/**
	 * Returns the user Gravatar image url.
	 *
	 * @return string
	 */
	public function gravatar()
	{
		// Generate the Gravatar hash
		$gravatar = md5(strtolower(trim($this->gravatar)));

		// Return the Gravatar url
		return "//gravatar.org/avatar/{$gravatar}";
	}

	public function panicEvents()
    {
        return $this->hasMany('PanicEvent');
    }

	/**
	 * Returns the user contacts.
	 *
	 * @return string
	 */
	public function contacts()
    {
        return $this->hasMany('Contact');
    }


    /**
	 * Returns the user Gravatar image url.
	 *
	 * @return string
	 */
	public function coverPhoto()
	{
		// Return the photo url
		return '/'. $this->cover;
	}

	/**
	 * The user companies;
	 *
	 * @var array
	 */
	protected $userCompanies;


	/**
	 * The user companies pivot table name and model
	 *
	 * @var string
	 */
	protected static $userCompaniesPivot = 'users_companies';
	protected static $companyModel = 'Company';


	/**
	 * Returns an array of companies which the given
	 * user belongs to.
	 *
	 * @return array
	 */
	public function getCompanies()
	{
		if ( ! $this->userCompanies)
		{
			$this->userCompanies = $this->company()->get();
		}

		return $this->userCompanies;
	}

	/**
	 * Returns the relationship between users and companies.
	 *
	 * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function companies()
	{
		return $this->belongsToMany(static::$companyModel, static::$userCompaniesPivot);
		//return $this->hasOne('Company');
	}

	/**
	 * Returns the relationship between users and companies.
	 *
	 * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function company()
	{
		return $this->belongsTo('Company', 'companies_id');
	}

	/**
	 * Adds the user to the given company.
	 *
	 * @param  Company
	 * @return bool
	 */
	public function addCompany(Company $company)
	{
		if ( ! $this->inCompany($company))
		{
			$this->companies()->attach($company);
			$this->userCompanies = null;
		}

		return true;
	}

	/**
	 * See if the user is in the given company.
	 *
	 * @param Company
	 * @return bool
	 */
	public function inCompany(Company $company)
	{
		foreach ($this->getCompanies() as $_company)
		{
			if ($_company->getId() == $company->getId())
			{
				return true;
			}
		}

		return false;
	}


	/**
	 * Removes the user from the given company.
	 *
	 * @param Company
	 * @return bool
	 */
	public function removeCompany(Company $company)
	{
		if ($this->inCompany($company))
		{
			$this->companies()->detach($company);
			$this->userCompanies = null;
		}

		return true;
	}


	/**
	 * Check if the user has any company is activated.
	 *
	 * @return bool
	 */
	public function isCompanyActive()
	{

		if ($this->companies()->whereRaw('active = 1')->count() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}





	/**
	 * The user locals;
	 *
	 * @var array
	 */
	protected $userLocals;


	/**
	 * The user locals pivot table name and model
	 *
	 * @var string
	 */
	protected static $userLocalsPivot = 'users_locals';
	protected static $localModel = 'Local';


	/**
	 * Returns an array of locals which the given
	 * user belongs to.
	 *
	 * @return array
	 */
	public function getLocals()
	{
		if ( ! $this->userLocals)
		{
			$this->userLocals = $this->locals()->get();
		}

		return $this->userLocals;
	}

	/**
	 * Returns the relationship between users and locals.
	 *
	 * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function locals()
	{
		return $this->belongsToMany(static::$localModel, static::$userLocalsPivot);
	}

	/**
	 * Adds the user to the given company.
	 *
	 * @param  Local
	 * @return bool
	 */
	public function addLocal(Local $company)
	{
		if ( ! $this->inLocal($company))
		{
			$this->locals()->attach($company);
			$this->userLocals = null;
		}

		return true;
	}

	/**
	 * See if the user is in the given company.
	 *
	 * @param Local
	 * @return bool
	 */
	public function inLocal(Local $company)
	{
		foreach ($this->getLocals() as $_company)
		{
			if ($_company->getId() == $company->getId())
			{
				return true;
			}
		}

		return false;
	}


	/**
	 * Removes the user from the given company.
	 *
	 * @param Local
	 * @return bool
	 */
	public function removeLocal(Local $company)
	{
		if ($this->inLocal($company))
		{
			$this->locals()->detach($company);
			$this->userLocals = null;
		}

		return true;
	}


	/**
	 * Check if the user has any company is activated.
	 *
	 * @return bool
	 */
	public function isLocalActive()
	{

		if ($this->locals()->whereRaw('active = 1')->count() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
