<?php
class Ibeacon extends Eloquent {
    public static $headerSort = ['name', 'peripheralID', 'proximity', 'enter', 'exit', 'active', 'zones_id'];

    /**
     * Indicates if the model should soft delete.
     *
     * @var bool
     */
    protected $softDelete = true;

	protected $fillable = array('id','name','description','mac_address', 'peripheralID', 'proximityUUID','proximity','major','minor','rssi', 'enter', 'exit', 'companies_id', 'users_id', 'zones_id');
	protected $visible =  array('id','name','description','mac_address', 'peripheralID', 'proximityUUID','proximity','major','minor','rssi','local_id', 'enter', 'exit', 'local','ads', 'companies_id', 'localName');

    /**
     * Set table
     * @var bool
     */
    protected $table = 'beacons';
    

    public function user()
    {
        return $this->belongsTo('User', 'users_id');
    }


    public function ads()
    {
        return $this->hasMany('Ad');
    }

    /**
     * Check if the company is activated.
     *
     * @return bool
     */
    public function isCompanyActive()
    {
        if ($this->company_id == 0)
        {
            return false;
        }

        return (bool) $this->company()->first()->isActive();
    }

    /**
     * Check if the beacons is activated.
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->active;
    }

    /**
     * Check if the beacon has enter.
     *
     * @return bool
     */
    public function isEnter()
    {
        return (bool) $this->enter;
    }

    /**
     * Check if the beacon has exit.
     *
     * @return bool
     */
    public function isExit()
    {
        return (bool) $this->exit;
    }
}