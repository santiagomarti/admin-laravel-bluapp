<?php

class Local extends Eloquent {

    /**
     * Indicates if the model should soft delete.
     *
     * @var bool
     */
    protected $softDelete = true;

	protected $visible = array('id','name','company_id', 'company', 'cover', 'active', 'description', 'ads');
    protected $fillable = array('id','name','company_id', 'cover', 'active', 'description', 'ibeacons', 'company', 'ads');


    /**
     * The user companies pivot table name.
     *
     * @var string
     */
    protected static $userLocalsPivot = 'users_locals';
    protected static $userModel2l = 'User';


    /**
     * Returns the relationship between users and companies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users($withTrashed, $onlyTrashed)
    {

        $users = $this->belongsToMany(static::$userModel2l, static::$userLocalsPivot);

        // Do we want to include the deleted users?
        if ($withTrashed)
        {
            $users = $users->withTrashed();
        }
        else if ($onlyTrashed)
        {
            $users = $users->onlyTrashed();
        }

        return $users;
    }


    /**
     * Returns the locals's ID.
     *
     * @return  mixed
     */
    public function getId()
    {
        return $this->getKey();
    }

    /**
     * Check if the Company is activated.
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->active;
    }

    public function ibeacons()
    {
        return $this->hasMany('Ibeacon');
    }

    public function company()
    {
        return $this->belongsTo('Company');
    }

    /**
     * Check if the company is activated.
     *
     * @return bool
     */
    public function isCompanyActive()
    {
        return (bool) $this->company()->first()->isActive();
    }


    /**
     * Get all ads through beacons and locals
     *
     * @return bool
     */
    public function ads()
    {
        return $this->hasManyThrough('Ad', 'Ibeacon');
    }
}