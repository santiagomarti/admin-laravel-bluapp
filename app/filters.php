<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	HelperAuthToken::setApplicationLocaleUsingHeader();
});


App::after(function($request, $response)
{
	//Log::info('response:'. $response->getContent());
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	// Check if the user is logged in
	if ( ! Sentry::check())
	{
		// Store the current uri in the session
		Session::put('loginRedirect', Request::url());

		// Redirect to the login page
		return Redirect::route('signin');
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| Admin authentication filter.
|--------------------------------------------------------------------------
|
| This filter does the same as the 'auth' filter but it checks if the user
| has 'admin' privileges.
|
*/

Route::filter('admin-auth', function()
{
	// Check if the user is logged in
	if ( ! Sentry::check())
	{
		// Store the current uri in the session
		Session::put('loginRedirect', Request::url());

		// Redirect to the login page
		return Redirect::route('signin');
	}

 	// // Check if the user has access to the admin page
	// if ( ! (Sentry::getUser()->hasAccess('admin'))
	// {
	// 	// Show the insufficient permissions page
	// 	return App::abort(403);
	// }

	// Check if the user has access to the admin page
	if ( ! (Sentry::getUser()->hasAnyAccess(array('admin', 'operator', 'subadmin'))))
	{
		// Show the insufficient permissions page
		return App::abort(403);
	}
});


//This method will check if the user is in the correct group, else returns it to admin
//more groups can be sent in the value separated by character '-'
//http://laravelsnippets.com/snippets/sentry-route-filters
Route::filter('inGroup', function($route, $request, $value)
{
	
	try
		{
			$user = Sentry::getUser();
			if (is_null($user)) {
				Sentry::logout();

				// Redirect to the users page
				return Redirect::route('home')->with('error', 'Ooops, seems we lost your session.');
			}
			
			$groups = preg_split("[-]", $value);

			foreach ($groups as  $groupKey)
			{
				$group = Sentry::findGroupByName($groupKey);

				if($user->inGroup($group))
				 {
				 	//Ok we found one group, everything is safe, return
				 	return;
				 }
			}

			return Redirect::route('admin')->withErrors(array(Lang::get('admin/users/message.noaccess')));
		}
	catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	{
		return Redirect::route('admin')->withErrors(array(Lang::get('admin/users/message.notfound')));
	}
	catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
	{
		 return Redirect::route('admin')->withErrors(array(Lang::get('admin/groups/message.group_not_found')));
	}
});

//This method will check if the user is in the correct permissions, else returns it to admin
//more permissions can be sent in the value separated by character '-'
//http://laravelsnippets.com/snippets/sentry-route-filters
Route::filter('inPermission', function($route, $request, $value)
{
	try
		{
			$user = Sentry::getUser();
			if (is_null($user)) {
				Sentry::logout();
				// Redirect to the users page
				return Redirect::route('home')->with('error', 'Ooops, seems we lost your session.');
			}
			$permissions = preg_split("[-]", $value);
			if ($user->hasAnyAccess($permissions) ) {
				//Ok we found one permissions, everything is safe, return
				return;
			}
			return Redirect::route('admin')->withErrors(array(Lang::get('admin/users/message.noaccess')));
		}
	catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	{
		return Redirect::route('admin')->withErrors(array(Lang::get('admin/users/message.notfound')));
	}
	catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
	{
		 return Redirect::route('admin')->withErrors(array(Lang::get('admin/groups/message.group_not_found')));
	}
});


//This method will check if the user has a company activated.
Route::filter('company-active', function()
{
	return;
	try
		{
			$user = Sentry::getUser();


			if 	(is_null($user))
			{
				Sentry::logout();

				// Redirect to the users page
				return Redirect::route('home')->with('error', 'Session Expired');
			}

			if ($user->isCompanyActivated() == false) {

				Sentry::logout();

				// Redirect to the users page
				return Redirect::route('home')->with('error', 'Your company has been disabled');

			}
		}
	catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	{
		return Redirect::route('home')->with('error', Lang::get('admin/users/message.notfound'));
	}

});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	// if (Session::token() != Input::get('_token'))
	// {
	// 	throw new Illuminate\Session\TokenMismatchException;
	// }
});


