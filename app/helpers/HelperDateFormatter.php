<?php
class HelperDateFormatter {

    public static function format($date, $format) 
    {
    	$date = new DateTime($date);
    	return date_format($date,$format);
    }

    public static function isEmpty($date) 
    {
    	if ( is_null($date) || $date === '0000-00-00 00:00:00') {
    		return true;
    	} 
    	else 
    	{
    		return false;
    	}
    }
}