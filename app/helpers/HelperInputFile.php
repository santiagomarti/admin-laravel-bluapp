<?php
class HelperInputFile {

    public static function getMimeType($field)
    {
            //Log::info('mime field:' . $field);
            $field = Input::get($field,'data:none;base64,none');
            $fieldExplode =  explode(";", $field);
            $mimeType = str_replace('data:', '', $fieldExplode[0]);
            $mimeType = str_replace('image/', '.', $mimeType);

            if ($mimeType === 'none')
            {
                return '.missing';
            }

            return $mimeType;
    }

    public static function getDataBase64($field)
    {
            //Log::info('mime field:' . $field);
    	    $field = Input::get($field,'data:none;base64,none');
            $data =  explode(",", $field);


            if ($data[1] === 'none') {
                return null;
            }

            return $data[1];
    }

    public static function writeFile($destination, $base64Data)
    {
           file_put_contents($destination, base64_decode($base64Data));

           return file_exists ($destination);
    }
}