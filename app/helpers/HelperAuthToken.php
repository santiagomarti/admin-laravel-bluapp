<?php
class HelperAuthToken {

	//Clever Method to check header and database token
	//Checks if token exists, it will return a response error, or the user ID
    public static function authorizeHeaderToken() 
    {
        // // //check accesstoken
		 $headerAccesToken = Request::header(Config::get('settings.headerAccessToken'));

		 //Check if header token is sent
		 if (is_null($headerAccesToken)) 
		 {
		 	return Response::json(array('success' => false,
		  	'code' => -1,		 		
		 	'error' => '401 Unauthorized, No Token'), 401);
		 }

		 //return row with the access_token, if doesnt exist it will return null
		 $dbAccessToken = DB::table('accesses')
		 					->where('access_token', $headerAccesToken)
		 					->first();

		 //Informs that the token isnt in the database
		 if (is_null($dbAccessToken)) 
		 {
		  	return Response::json(array('success' => false,		 		
		  	'error' => '401 Unauthorized, No Token In Stack',
		  	'code' => -2,
		  	), 401);
		 }

		 return $dbAccessToken->user_id;  //ists okey just send null
    }


    //Clever Method to check header and sets the application locale, sets default to "es"
    public static function setApplicationLocaleUsingHeader() 
    {
        	//Check headers, if the language is set to english, or spanish, if not default is spanish
		$headerLocale = Request::header(Config::get('settings.headerLocale'));

		if (in_array($headerLocale, Config::get('settings.localeEnum'))) {
	        Session::put('locale', $headerLocale);
	        App::setLocale(Session::get('locale'));
		} 
		else 
		{
			Session::put('locale', 'es');
			App::setLocale(Session::get('locale'));
		}
    }
}