<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Register all the admin routes.
|
*/
Route::group(array('prefix' => 'admin'), function()
{

	# User Management
	//Route::group(array('prefix' => 'users','before' => 'inPermission:manage_admin_users-manage_company_users'), function()
	Route::group(array('prefix' => 'users','before' => 'inGroup:Admin'), function()
	{
		Route::get('/', array('as' => 'users', 'uses' => 'Controllers\Admin\UsersController@getIndex'));

		Route::post('ajax', 'Controllers\Api\UsersController@ajax');
		
		Route::get('create', array('as' => 'create/user', 'uses' => 'Controllers\Admin\UsersController@getCreate'));
		Route::post('create', 'Controllers\Admin\UsersController@postCreate');
		Route::get('{userId}/edit', array('as' => 'update/user', 'uses' => 'Controllers\Admin\UsersController@getEdit'));
		Route::post('{userId}/edit', 'Controllers\Admin\UsersController@postEdit');
		Route::post('validate', array('as' => 'validate/user', 'uses' => 'Controllers\Admin\UsersController@getEmailDuplicate'));
		Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'Controllers\Admin\UsersController@getDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'Controllers\Admin\UsersController@getRestore'));

		Route::delete('{userId}/delete', array('as' => 'deleteAjax/user', 'uses' => 'Controllers\Api\UsersController@delete'));
		Route::post('{userId}/restore', array('as' => 'restoreAjax/user', 'uses' => 'Controllers\Api\UsersController@restore'));
	});

	# User Company
	Route::group(array('prefix' => 'companyusers','before' => 'inGroup:Company'), function()
	{
		Route::get('/', array('as' => 'companyusers', 'uses' => 'Controllers\Company\UsersController@getIndex'));

		Route::post('ajax', 'Controllers\Api\CompanyUsersController@ajax');

		Route::get('create', array('as' => 'create/companyuser', 'uses' => 'Controllers\Company\UsersController@getCreate'));
		Route::post('create', 'Controllers\Company\UsersController@postCreate');
		Route::get('{userId}/edit', array('as' => 'update/companyuser', 'uses' => 'Controllers\Company\UsersController@getEdit'));
		Route::post('{userId}/edit', 'Controllers\Company\UsersController@postEdit');
		Route::post('validate', array('as' => 'validate/companyuser', 'uses' => 'Controllers\Company\UsersController@getEmailDuplicate'));
		Route::get('{userId}/delete', array('as' => 'delete/companyuser', 'uses' => 'Controllers\Company\UsersController@getDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/companyuser', 'uses' => 'Controllers\Company\UsersController@getRestore'));

		Route::delete('{userId}/delete', array('as' => 'deleteAjax/companyuser', 'uses' => 'Controllers\Api\CompanyUsersController@delete'));
		Route::post('{userId}/restore', array('as' => 'restoreAjax/companyuser', 'uses' => 'Controllers\Api\CompanyUsersController@restore'));
	});

	# Group Management
	Route::group(array('prefix' => 'groups','before' => 'inGroup:Admin'), function()
	{
		Route::get('/', array('as' => 'groups2', 'uses' => 'Controllers\Admin\GroupsController@groups2'));
		Route::get('/groups2', array('as' => 'groups', 'uses' => 'Controllers\Admin\GroupsController@groups2'));
		Route::post('ajax', 'Controllers\Api\GroupsController@ajax');
		
		Route::get('/demo', array('as' => 'demo', 'uses' => 'Controllers\Admin\GroupsController@demo'));

		Route::post('ajaxdemo', 'Controllers\Api\GroupsController@ajaxdemo');


		Route::get('create', array('as' => 'create/group', 'uses' => 'Controllers\Admin\GroupsController@getCreate'));
		Route::post('create', 'Controllers\Admin\GroupsController@postCreate');
		Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'Controllers\Admin\GroupsController@getEdit'));
		Route::post('{groupId}/edit', 'Controllers\Admin\GroupsController@postEdit');
		Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'Controllers\Admin\GroupsController@getDelete'));

		Route::delete('{groupId}/delete', array('as' => 'deleteAjax/group', 'uses' => 'Controllers\Api\GroupsController@delete'));

		Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'Controllers\Admin\GroupsController@getRestore'));
	});

	# Company Management
	Route::group(array('prefix' => 'companies','before' => 'inGroup:Admin'), function()
	{
		Route::get('/', array('as' => 'companies', 'uses' => 'CompaniesController@getIndex'));

		Route::post('ajax', array('as' => 'companiesAjax', 'uses' => 'Controllers\Api\CompaniesController@ajax'));

		Route::get('create', array('as' => 'create/company', 'uses' => 'CompaniesController@getCreate'));
		Route::post('create', 'CompaniesController@postCreate');

		Route::get('service', 'CompaniesController@getService');

		Route::post('cover-company', array('as' => 'cover-company', 'uses' => 'CompaniesController@postEditCover'));

		Route::get('{companyId}/edit', array('as' => 'update/company', 'uses' => 'CompaniesController@getEdit'));
		Route::post('{companyId}/edit', 'CompaniesController@postEdit');
		Route::get('{companyId}/delete', array('as' => 'delete/company', 'uses' => 'CompaniesController@getDelete'));

		Route::delete('{companyId}/delete', array('as' => 'deleteAjax/company', 'uses' => 'Controllers\Api\CompaniesController@delete'));
		
		Route::post('{companyId}/restore', array('as' => 'restoreAjax/company', 'uses' => 'Controllers\Api\CompaniesController@restore'));
		
		Route::get('{companyId}/restore', array('as' => 'restore/company', 'uses' => 'CompaniesController@getRestore'));
	});

	# Local Management
	Route::group(array('prefix' => 'zones','before' => 'inGroup:Company-Operator'), function()
	{
		Route::get('/', array('as' => 'zones', 'uses' => 'ZonesController@getIndex'));

		Route::post('ajax', array('as' => 'zonesAjax', 'uses' => 'Controllers\Api\ZonesController@ajax'));

		Route::get('create', array('as' => 'create/zone', 'uses' => 'ZonesController@getCreate'));
		Route::post('create', 'ZonesController@postCreate');

		Route::post('cover-zone', array('as' => 'cover-zone', 'uses' => 'ZonesController@postEditCover'));

		Route::get('{localId}/edit', array('as' => 'update/zone', 'uses' => 'ZonesController@getEdit'));
		Route::post('{localId}/edit', 'ZonesController@postEdit');
		Route::get('{localId}/delete', array('as' => 'delete/zone', 'uses' => 'ZonesController@getDelete'));
		Route::get('{localId}/restore', array('as' => 'restore/zone', 'uses' => 'ZonesController@getRestore'));

		Route::delete('{localId}/delete', array('as' => 'deleteAjax/zone', 'uses' => 'Controllers\Api\ZonesController@delete'));
		Route::post('{localId}/restore', array('as' => 'restoreAjax/zone', 'uses' => 'Controllers\Api\ZonesController@restore'));
	});

	# iBeacon Management
	Route::group(array('prefix' => 'ibeacons', 'before' => 'inPermission:manage_beacons'), function()
	{
		Route::get('/', array('as' => 'ibeacons', 'uses' => 'IbeaconsController@getIndex'));

		Route::post('ajax', array('as' => 'ibeaconsAjax', 'uses' => 'Controllers\Api\IbeaconsController@ajax'));

		Route::get('create', array('as' => 'create/ibeacon', 'uses' => 'IbeaconsController@getCreate'));
		Route::post('create', 'IbeaconsController@postCreate');

		Route::post('cover-ibeacon', array('as' => 'cover-ibeacon', 'uses' => 'IbeaconsController@postEditCover'));

		Route::get('{ibeaconId}/edit', array('as' => 'update/ibeacon', 'uses' => 'IbeaconsController@getEdit'));
		Route::post('{ibeaconId}/edit', 'IbeaconsController@postEdit');
		Route::get('{ibeaconId}/delete', array('as' => 'delete/ibeacon', 'uses' => 'IbeaconsController@getDelete'));
		Route::get('{ibeaconId}/restore', array('as' => 'restore/ibeacon', 'uses' => 'IbeaconsController@getRestore'));

		Route::delete('{ibeaconId}/delete', array('as' => 'deleteAjax/ibeacon', 'uses' => 'Controllers\Api\IbeaconsController@delete'));
		Route::post('{ibeaconId}/restore', array('as' => 'restoreAjax/ibeacon', 'uses' => 'Controllers\Api\IbeaconsController@restore'));
	});

	# iBeacon company
	Route::group(array('prefix' => 'companyibeacons', 'before' => 'inGroup:Company'), function()
	{
		Route::get('/', array('as' => 'companyibeacons', 'uses' => 'Controllers\Company\IbeaconsController@getIndex'));

		Route::post('ajax', array('as' => 'companyibeaconsAjax', 'uses' => 'Controllers\Api\CompanyIbeaconsController@ajax'));

		Route::post('cover-ibeacon', array('as' => 'cover-ibeacon', 'uses' => 'Controllers\Company\IbeaconsController@postEditCover'));
		Route::get('{ibeaconId}/edit', array('as' => 'update/companyibeacon', 'uses' => 'Controllers\Company\IbeaconsController@getEdit'));
		Route::post('{ibeaconId}/edit', 'Controllers\Company\IbeaconsController@postEdit');
		
	});


	# Operator Management
	Route::group(array('prefix' => 'operators','before' => 'inGroup:Company'), function()
	{
		Route::get('/', array('as' => 'operators', 'uses' => 'OperatorsController@getIndex'));
		Route::get('create', array('as' => 'create/operator', 'uses' => 'OperatorsController@getCreate'));
		Route::post('create', 'OperatorsController@postCreate');

		Route::post('cover-operator', array('as' => 'cover-operator', 'uses' => 'OperatorsController@postEditCover'));

		Route::get('{operatorId}/edit', array('as' => 'update/operator', 'uses' => 'OperatorsController@getEdit'));
		Route::post('{operatorId}/edit', 'OperatorsController@postEdit');
		Route::get('{operatorId}/delete', array('as' => 'delete/operator', 'uses' => 'OperatorsController@getDelete'));
		Route::get('{operatorId}/restore', array('as' => 'restore/operator', 'uses' => 'OperatorsController@getRestore'));
	});

	# Campaigns Management
	Route::group(array('prefix' => 'campaigns','before' => 'inPermission:manage_campaigns'), function()
	{
		Route::get('/', array('as' => 'campaigns', 'uses' => 'CampaignsController@getIndex'));

		Route::post('ajax', array('as' => 'campaignsAjax', 'uses' => 'Controllers\Api\CampaignsController@ajax'));

		Route::get('create', array('as' => 'create/campaign', 'uses' => 'CampaignsController@getCreate'));
		Route::post('create', 'CampaignsController@postCreate');

		Route::post('dropzone', 'CampaignsController@dropzone');
		Route::delete('dropzone/remove', 'CampaignsController@removeDropzone');

		Route::post('cover-campaign', array('as' => 'cover-campaign', 'uses' => 'CampaignsController@postEditCover'));
		Route::post('cover-campaign-type', array('as' => 'cover-campaign-type', 'uses' => 'CampaignsController@postEditCoverType'));

		Route::get('{adId}/edit', array('as' => 'update/campaign', 'uses' => 'CampaignsController@getEdit'));
		Route::post('{adId}/edit', 'CampaignsController@postEdit');
		Route::get('{adId}/delete', array('as' => 'delete/campaign', 'uses' => 'CampaignsController@getDelete'));
		Route::get('{adId}/restore', array('as' => 'restore/campaign', 'uses' => 'CampaignsController@getRestore'));

		Route::delete('{campaignId}/delete', array('as' => 'deleteAjax/campaign', 'uses' => 'Controllers\Api\CampaignsController@delete'));
		
		Route::post('{campaignId}/restore', array('as' => 'restoreAjax/campaign', 'uses' => 'Controllers\Api\CampaignsController@restore'));
	});

	# Panels Management
	Route::group(array('prefix' => 'panels','before' => 'inGroup:Company-Operator'), function()
	{
		Route::get('/', array('as' => 'panels', 'uses' => 'PanelsController@getIndex'));
		Route::post('beacons', array('as' => 'panels_beacons', 'uses' => 'PanelsController@getBeacons'));
		Route::post('campaigns', array('as' => 'panels_campaigns', 'uses' => 'PanelsController@getCampaigns'));
	});


	# Ads Management
	Route::group(array('prefix' => 'ads','before' => 'inGroup:Operator'), function()
	{
		Route::get('/', array('as' => 'ads', 'uses' => 'AdsController@getIndex'));
		Route::get('create', array('as' => 'create/ad', 'uses' => 'AdsController@getCreate'));
		Route::post('create', 'AdsController@postCreate');

		Route::post('cover-ad', array('as' => 'cover-ad', 'uses' => 'AdsController@postEditCover'));
		Route::post('cover-ad-type', array('as' => 'cover-ad-type', 'uses' => 'AdsController@postEditCoverType'));

		Route::get('{adId}/edit', array('as' => 'update/ad', 'uses' => 'AdsController@getEdit'));
		Route::post('{adId}/edit', 'AdsController@postEdit');
		Route::get('{adId}/delete', array('as' => 'delete/ad', 'uses' => 'AdsController@getDelete'));
		Route::get('{adId}/restore', array('as' => 'restore/ad', 'uses' => 'AdsController@getRestore'));
	});

	# Dashboard
	# Group Management
	Route::group(array('before' => 'inGroup:Company-Operator'), function()
	{
		# Dashboard
		Route::get('report', array('as' => 'admin.report', 'uses' => 'Controllers\Admin\DashboardController@report'));
	});
	Route::get('/', array('as' => 'admin', 'uses' => 'Controllers\Admin\DashboardController@getIndex'));

	

});

/*
|--------------------------------------------------------------------------
| Authentication and Authorization Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'auth'), function()
{
	# Login
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');

	# Register
	Route::get('signup', array('as' => 'signup', 'uses' => 'AuthController@getSignup'));
	Route::post('signup', 'AuthController@postSignup');

	# Account Activation
	Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));

	# Forgot Password
	Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@getForgotPassword'));
	Route::post('forgot-password', 'AuthController@postForgotPassword');

	# Forgot Password Confirmation
	Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));
});

/*
|--------------------------------------------------------------------------
| Account Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'account'), function()
{

	# Account Dashboard
	Route::get('/', array('as' => 'account', 'uses' => 'Controllers\Account\DashboardController@getIndex'));

	# Profile
	Route::get('profile', array('as' => 'profile', 'uses' => 'Controllers\Account\ProfileController@getIndex'));
	Route::post('profile', 'Controllers\Account\ProfileController@postIndex');
	# Avatar
	Route::get('avatar', array('as' => 'avatar', 'uses' => 'Controllers\Account\ProfileController@getAvatar'));
	Route::post('avatar', 'Controllers\Account\ProfileController@postAvatar');

	Route::post('dropzone', 'Controllers\Account\ProfileController@dropzone');
	Route::delete('dropzone/remove', 'Controllers\Account\ProfileController@removeDropzone');

	# Change Password
	Route::get('change-password', array('as' => 'change-password', 'uses' => 'Controllers\Account\ChangePasswordController@getIndex'));
	Route::post('change-password', 'Controllers\Account\ChangePasswordController@postIndex');

	# Change Email
	Route::get('change-email', array('as' => 'change-email', 'uses' => 'Controllers\Account\ChangeEmailController@getIndex'));
	Route::post('change-email', 'Controllers\Account\ChangeEmailController@postIndex');

	#Delete Session
	Route::get('sessionFlush', array('as' => 'sessionFlush', 'uses' => 'Controllers\Account\ProfileController@deleteSession'));

});


/*
|--------------------------------------------------------------------------
| AccessToken Routes
|--------------------------------------------------------------------------
|
| Method that will check if the header token has been sent
|
*/
Route::filter('authtoken', function()
{
		//Checks if token exists, it will return a response error, or the user ID
		$resposeOrUserID = HelperAuthToken::authorizeHeaderToken();

		if (is_numeric($resposeOrUserID) == true)
		{
			//Log::info('=============================> AUTOLOGGED <=======================================');
			//AccessToken Found, and its Ok, autologgin
			//Find the user using the user, this user is returned by access-token
		    //Log the user in
	    	Sentry::login(Sentry::findUserById($resposeOrUserID), false);
		}
		else
		{
			return $resposeOrUserID;
		}
});
Route::group(array('prefix' => 'api'), function()
{
	Route::post('getdatacampaign', array('as' => 'api.getdatacampaign', 'uses' => 'AjaxController@getdatacampaign'));
	Route::post('getdatabeacon', array('as' => 'api.getdatabeacon', 'uses' => 'AjaxController@getdatabeacon'));
	Route::post('getmetricab', array('as' => 'api.getmetricab', 'uses' => 'AjaxController@getmetricab'));
	Route::post('getmetricaa', array('as' => 'api.getmetricaa', 'uses' => 'AjaxController@getmetricaa'));
	Route::post('getmetricae', array('as' => 'api.getmetricae', 'uses' => 'AjaxController@getmetricae'));
});
/*
|--------------------------------------------------------------------------
| WebAPI Routes protected by AccessToken Routes
|--------------------------------------------------------------------------
|
| All the protected webapi methods will pass throug the authtoken
|
*/
Route::group(array('before' => 'authtoken'), function()
{
	Route::group(array('prefix' => 'apiv1.1'), function()
	{

	});
});


/*
|--------------------------------------------------------------------------
| WebAPI Routes publics
|--------------------------------------------------------------------------
|
| Login and csrf-token can be asked without need to be loged in
|
*/
Route::group(array('prefix' => 'apiv1.1'), function()
{
		# WepApi Login, this will add the accestoken inside the server.
		Route::post('login', array('as' => 'apiv1.1/login', 'uses' => 'ServicesController@postSignin'));

		# Tokens
		Route::get('csrf-token', array('as' => 'apiv1.1/csrf-token', 'uses' => 'ServicesController@getCsrfToken'));

		#Post User Singin
		Route::post('user-signup', array('as' => 'apiv1.1/user-signup', 'uses' => 'ServicesController@postSignup'));

		#Post User Forgot Password
		Route::post('user-forgot', array('as' => 'apiv1.1/user-forgot', 'uses' => 'ServicesController@postForgotPassword'));

		#Post User Forgot Password
		Route::post('user-forgot-info', array('as' => 'apiv1.1/user-forgot-info', 'uses' => 'ServicesController@postUserInfoReset'));

		#Post User Forgot Password
		Route::post('user-forgot-confirm', array('as' => 'apiv1.1/user-forgot-confirm', 'uses' => 'ServicesController@postForgotPasswordConfirm'));

		#Get User Activate
		Route::get('user-activate/{activationCode}', array('as' => 'apiv1.1/user-activate', 'uses' => 'ServicesController@getActivate'));

		#Get Beacon by uuidd, major, minor
		Route::get('beacon/{proximityUUID}/major/{major}/minor/{minor}/ads', 'ServicesController@getAdByBeacon');

		#Get Promotions By Beacon by uuidd, major, minor
		Route::get('beacon/{proximityUUID}/major/{major}/minor/{minor}/promoted-ads', 'ServicesController@getPromotedAdsByBeacon');

		#Get Promotions By Beacon by uuidd, major, minor
		Route::get('beacons/promoted-ads', array('as' => 'apiv1.1/beacons/promoted-ads', 'uses' => 'ServicesController@getPromotedAds'));

		#Get HTML EMBEDED Promotions By Ad ID
		Route::get('ad/{id}/show', 'ServicesController@getAd');
});




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('about-us', function()
// {
// 	//
// 	return View::make('frontend/about-us');
// });

// Route::get('contact-us', array('as' => 'contact-us', 'uses' => 'ContactUsController@getIndex'));
// Route::post('contact-us', 'ContactUsController@postIndex');

// Route::get('blog/{postSlug}', array('as' => 'view-post', 'uses' => 'BlogController@getView'));
// Route::post('blog/{postSlug}', 'BlogController@postView');

//Route::get('/', array('as' => 'home', 'uses' => 'BlogController@getIndex'));
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@getIndex'));

Route::get('sdk', array('uses' => 'SdksController@getIndex'));

Route::group(array('prefix' => 'document'), function()
{
	
	Route::get('/users', array('as' => 'document.user', 'uses' => 'Controllers\Admin\UsersController@document'));
	Route::get('/beacons', array('as' => 'document.beacon', 'uses' => 'IbeaconsController@document'));
	Route::get('/companies', array('as' => 'document.company', 'uses' => 'CompaniesController@document'));
	Route::get('/zones', array('as' => 'document.zone', 'uses' => 'ZonesController@document'));
	Route::get('/campaigns', array('as' => 'document.campaign', 'uses' => 'CampaignsController@document'));
});
