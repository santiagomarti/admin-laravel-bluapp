<?php
class PanelsController extends AdminController {

	/**
	 * Show a list of all the locals.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();
		// Get the zones information
		if (Sentry::getUser()->hasAccess('subadmin')) {
			$zones = Zone::where('companies_id', $profile->company->id)->get();
			$users = User::where('companies_id', $profile->company->id)->get();
		} else if(Sentry::getUser()->hasAccess('operator')){
			$zones = Zone::whereHas('ibeacons',  function($q) use($profile) {
			    $q->where('users_id', $profile->id)->where('companies_id', $profile->company->id);
			})->get();
		}
		// Show the page
		return View::make('backend/panels/index', compact('zones', 'profile' , 'users'));
	}

	/**
	 * Filter beacon by zone and operator.
	 *
	 * @return json
	 */
	public function getBeacons()
	{
		// Get the beacons information
		$beacons = Ibeacon::where('companies_id', Sentry::getUser()->companies_id);
		if (Input::get('zoneid')!=='All') {
			$beacons = $beacons->where('zones_id', Input::get('zoneid'));
		}
		if (Input::get('userid')!=='All') {
			$beacons = $beacons->where('users_id', Input::get('userid'));
		}
		$beacons = $beacons->get();
		// Response
		return Response::json($beacons);
	}

	/**
	 * Filter campaigns by beacon.
	 *
	 * @return json
	 */
	public function getCampaigns()
	{
		// Get the campaigns information
		$companies_id = \Sentry::getUser()->companies_id;
		$campaigns = Campaign::whereHas('ibeacon',  function($q) use($companies_id) {
		    $q->where('companies_id', $companies_id);
		});
		$campaigns = $campaigns->where('ibeacons_id', Input::get('beaconid'));
		$campaigns = $campaigns->get();
		// Response
		return Response::json($campaigns);
	}

}
