<?php

class ZonesController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'name'       => 'required'
	);

	/**
	 * Show a list of all the zones.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();
		$zones = Zone::where('companies_id', $profile->company->id);
		if (Input::get('withTrashed')) {
			$zones = $zones->withTrashed();
		}else if (Input::get('onlyTrashed')) {
			$zones = $zones->onlyTrashed();
		}

		$zones = $zones->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));
		//create tab
		//if ((Sentry::getUser()->hasAccess('subadmin')))
		//{
		//	$companies = User::find(Sentry::getUser()->id)->getCompanies();
		//}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}else $companies = User::find(Sentry::getUser()->id)->getCompanies();

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			if ($companies->count() == 0)
			{
				$selectedCompany = -1;
			}
			else
			{
				$selectedCompany = $companies->first()->id;
			}
		}

		//end create tab
		// Show the page
		return View::make('backend/locals/index', compact('zones','companies', 'selectedCompany', 'profile'));
	}

	/**
	 * Zone create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Get the user information
		$profile = Sentry::getUser();
		//if ((Sentry::getUser()->hasAccess('subadmin')))
		//{
		//	$companies = User::find(Sentry::getUser()->id)->getCompanies();
		//}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}else $companies = User::find(Sentry::getUser()->id)->getCompanies();

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			if ($companies->count() == 0)
			{
				$selectedCompany = -1;
			}
			else
			{
				$selectedCompany = $companies->first()->id;
			}
		}

		return View::make('backend/locals/create', compact('companies', 'selectedCompany', 'profile'));
	}

	/**
	 * Zone create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		//If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			Session::flash('active', Input::has('active') ? true : false);
			// Ooops.. something went wrong
			return Redirect::route('create/zone')->withInput()->withErrors($validator);
		}

		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token','_token');
            if (Input::has('active')) {
            	$inputs['active']     = 1;
            } else {
            	$inputs['active']     = 0;
            }
			$inputs['companies_id'] = Sentry::getUser()->company->id;
			// Was the local created?
			if ($local = Zone::create($inputs))
			{
				// Setting sessions
				if (Session::has('number'))
				{
				    $number = Session::get('number') + 1;
				}else $number = 1;

				Session::put('number', $number);
				
				if (Session::has('alertNotification')) {
					$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New zone created. </span></a></li>';
				} else {
					$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New zone created. </span></a></li>';
				}
				
				Session::put('alertNotification', $li);
				// Prepare the success message
				$success = Lang::get('admin/locals/message.success.create');

				// Redirect to the zone page
				return Redirect::back()->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/locals/message.error.create');
		}

		// Redirect to the local creation page
		return Redirect::route('create/zone')->withInput()->with('error', $error);
	}

	/**
	 * Zone update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();
		$local = Zone::find($id);
		if (is_null($local)) {
			// Ooops.. something went wrong
			return Redirect::route('zones')->withInput()->withErrors(array('Zone not found'));
		}
		if ($local->name=='Unassigned') {
			// Ooops.. something went wrong
			return Redirect::route('zones')->withInput()->withErrors(array('Can not edit or delete this zone'));
		}
		//if ((Sentry::getUser()->hasAccess('subadmin')))
		//{
		//	$companies = User::find(Sentry::getUser()->id)->getCompanies();
		//}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}else $companies = User::find(Sentry::getUser()->id)->getCompanies();

		if ($companies->count() == 0) {
			// Ooops.. something went wrong
			return Redirect::route('zones')->withInput()->withErrors(array('Zone has no company'));
		}

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			$selectedCompany = $companies->first()->id;
		}

		if (empty($local->cover))
		{
			$local->cover = 'pacman.png';
		}

		$urlChange = route('update/zone', $id) . '?selectedCompany=';

		// Show the page
		return View::make('backend/locals/edit', compact('local','companies', 'selectedCompany', 'urlChange', 'profile'));
	}

	/**
	 * Zone update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		try
		{
			// Get the event information
			$local = Zone::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/locals/message.local_not_found', compact('id'));

			// Redirect to the local management page
			return Redirect::route('zones')->with('error', $error);
		}

		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Update the local
			$local->name  = Input::get('name');
			$local->description   = Input::get('description');
            if (Input::has('active')) {
            	$local->active     = 1;
            } else {
            	$local->active     = 0;
            }

			// Was the local updated?
			if ($local->save())
			{
				//Log::info('polygon save');
				// Prepare the success message
				$success = Lang::get('admin/locals/message.success.update');

				// Redirect to the local page
				//return Redirect::route('update/local', $id)->with('success', $success);
				return Redirect::route('zones')->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/locals/message.error.update');
		}

		// Redirect to the local creation page
		return Redirect::route('update/zone', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given Zone.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		
		try
		{
			// Get local information
			$local = Zone::find($id);
			if (is_null($local)) {
				// Ooops.. something went wrong
				return Redirect::route('zones')->withInput()->withErrors(array('Zone not found'));
			}
			if ($local->name=='Unassigned') {
				// Ooops.. something went wrong
				return Redirect::route('zones')->withInput()->withErrors(array('Can not edit or delete this zone'));
			}

			$campaigns = Campaign::whereHas('ibeacon',  function($q) use($id) {
			    $q->where('zones_id', $id);
			});
			if (empty($campaign)) {

				// Delete the local
				$local->delete();

				// Prepare the success message
				$success = Lang::get('admin/locals/message.success.delete');

				// Redirect to the local management page
				return Redirect::route('zones')->with('success', $success);
			}else {

				// Prepare the error message
				$error = 'Can’t Delete a Zone assigned to a Campaign.';

				// Redirect to the local management page
				return Redirect::route('zones')->with('error', $error);
			}
		}
		catch (Exceptions $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/locals/message.local_not_found', compact('id' ));

			// Redirect to the local management page
			return Redirect::route('zones')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted Zone.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get local information
			$local = Zone::withTrashed()->find($id);

			// Restore the local
			$local->restore();

			// Prepare the success message
			$success = Lang::get('admin/locals/message.success.restored');

			// Redirect to the local management page
			return Redirect::route('zones')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/locals/message.local_not_found', compact('id'));

			// Redirect to the local management page
			return Redirect::route('zones')->with('error', $error);
		}
	}



	/**
	 * Zone update cover
	 *
	 * @return Edit
	 */
	public function postEditCover()
	{
		try
		{
			$mimeType = HelperInputFile::getMimeType('cover');
			$base64Data = HelperInputFile::getDataBase64('cover');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'cover_' . Input::get('local_id','nolocal') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/locals/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						Log::info('bla');

						$local_id = Input::get('local_id');

						// // Get the user information
						$local = Company::find($local_id);
						$local->cover = 'locals/' . $finalName;

						Log::info('cover:' . $local->cover);

						$local->save();

						Log::info('adssad');
					}
					catch (UserNotFoundException $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing local'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/locals/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}

	public function document()
	{
		DocumentController::document('ZonesController');
	}
}
