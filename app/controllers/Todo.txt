Todo.txt
Hola Magno,
como hablamos la vez pasada en tu casa, a continuación te describo los upgrades que que necesitamos hacer al sistema:

1. Incluir un plugin de "navegador" real dentro de la aplicación que permita abrir formatos HTML optimos, navegar por ellos (atras y adelante), etc Esto está dirigido a que podamos llamar a espacios Web de terceros, donde puedan navegar de verdad, ya que actualmente si se vá a una pagina no se puede navegar, etc...sin salir de la app.

2. Luego controlar rutinas de experiencia de usuario que no tire mas de dos veces la info de un beacon por día para que no sea Spam si el usuario pasa varias veces por el mismo sitio como puede pasar por la universidad.

3. Back-End: ahora necesitamos como hablamos, poder ofrecer un mantenedor de la configuración de la lógica del negocio de la siguiente manera:

a) Acceso Bluapp: donde podamos administrar las cuentas, esto es: a1) crear el user y pass de cuenta, a2) dar de alta sus beacon, a3) ver estado y poder bloquear o eliminar.

b) Acceso Cuenta: dirigido al acceso con user/pass creado antes, donde ya el usuario puede ver los beacon que tiene disponibles, para poder a partir de ahí configurar el resto de valores que permiten relacionar la información.

Esto sería Magno, dinos lo antes posible cual sería el monto y los tiempos de trabajo.

Quedamos atentos, muchas gracias.