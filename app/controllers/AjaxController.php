<?php

use Carbon\Carbon;

class AjaxController extends AdminController {

	/**
	 * Show a list of all the campaigns.
	 *
	 * @param id => id of Campaign
	 * @return View
	 */
	public function getdatacampaign()
	{
		$idcamp = Input::get('id');
		if($idcamp != '') {
			$totalSentPush = DB::select('select count(idcamp) as count from Tracking where showinfo = 0 and idcamp = ?', array($idcamp) );

			$totalReadedPush = DB::select('select count(idcamp) as count from Tracking where showinfo = 1  and idcamp = ?', array($idcamp));

			$uniqueReadedPush = DB::select('select count(distinct deviceid) as count from Tracking where showinfo = 1 and idcamp = ?', array($idcamp));
		}else {
			$totalSentPush = DB::select('select count(idcamp) as count from Tracking where showinfo = 0');
			$totalReadedPush = DB::select('select count(idcamp) as count from Tracking where showinfo = 1');

			$uniqueReadedPush = DB::select('select count(distinct deviceid) as count from Tracking where showinfo = 1');
		}

		$result = array(
			'totalSentPush' => $totalSentPush[0]->count,
			'totalReadedPush' => $totalReadedPush[0]->count,
			'uniqueReadedPush' => $uniqueReadedPush[0]->count
		);
		
		//return Response::json(['data'=>'data']);
		return Response::json($result);
	}

	public function getmetricaa(){
		$minor = Input::get('minor');
		$major = Input::get('major');
		$start = DateTime::createFromFormat('d F Y - H:i', Input::get('start'));
		$end   = DateTime::createFromFormat('d F Y - H:i', Input::get('end'));
		$totalReads = 0;
		$acumulated = 0;
		$devices = DB::select('select distinct deviceid from TrackingRange where updated_at < ? and updated_at > ? and major = ? and minor = ? ', array($end, $start, $major, $minor));
		$data = NULL;
		$algo = NULL;

		foreach($devices as $device){
			$interactions = DB::select('select enter, updated_at from TrackingRange where deviceid =  ? and updated_at < ? and updated_at > ? and major = ? and minor = ? order by updated_at ', array($device->deviceid, $end, $start, $major, $minor));
			$inside = FALSE;
			foreach($interactions as $inter){
				if($inter->enter == 1 && $inside == FALSE){ //Estaba afuera y nos encontramos con una entrada. Guardamos el tiempo de entrada y cambiamos $inside
					$lastSeenDate = $inter->updated_at;
					$inside = TRUE;
					$data[0] = $lastSeenDate;
					$algo = $interactions;
				} elseif($inter->enter == 0 && $inside == TRUE){ //Estaba adentro pero nos encontramos con una salida. Tomamos el tiempo adentro, sumamos una nueva medida y cambiamos $inside
					$totalReads++;
					$acumulated = $acumulated + (strtotime($inter->updated_at) - strtotime($lastSeenDate));
					$inside = FALSE;
					$data[1] = $inter->updated_at;
					$data[2] = strtotime($inter->updated_at) - strtotime($lastSeenDate);
				}
			}
			$inside = FALSE;
			$lastSeenDate = NULL;			
		}
		if($totalReads > 0){
			$average = $acumulated / $totalReads;
			return Response::json(array($average));
		} else return Response::json(array('nodata'));
	}

	public function getmetricab(){
		$minor = Input::get('minor');
		$major = Input::get('major');
		$profile = Sentry::getUser();
		$today = Carbon::now();
		$one_week_ago = Carbon::now()->subWeeks(1);
		$two_week_ago = Carbon::now()->subWeeks(2);
		$three_week_ago = Carbon::now()->subWeeks(3);
		$four_week_ago = Carbon::now()->subWeeks(4);
		$five_week_ago = Carbon::now()->subWeeks(5);
		$six_week_ago = Carbon::now()->subWeeks(6);
		$seven_week_ago = Carbon::now()->subWeeks(7);
		$eight_week_ago = Carbon::now()->subWeeks(8);
		$nine_week_ago = Carbon::now()->subWeeks(9);
		$ten_week_ago = Carbon::now()->subWeeks(10);
		//Métrica b:  Cantidad de gente que entró al rango: Buscamos dentro de las últimas 10 semanas, para beacons de la compañía, 
		//			  la cantidad de gente distinta por día que entró al rango de cada beacon.
		$bCompanyBeacons = Ibeacon::where('companies_id', $profile->companies_id)->lists('id', 'name', 'major', 'minor', 'proximityUUID');
		$bResults = array();
		$bFirst = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < NOW() and updated_at > ?',array($major, $minor, $one_week_ago));
		$bResults[1] = $bFirst[0]->count;

		$bSecond = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $one_week_ago, $two_week_ago));
		$bResults[2] = $bSecond[0]->count;

		$bThird = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $two_week_ago, $three_week_ago));
		$bResults[3] = $bThird[0]->count;

		$bFourth = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $three_week_ago, $four_week_ago));
		$bResults[4] = $bFourth[0]->count;

		$bFifth = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $four_week_ago, $five_week_ago));
		$bResults[5] = $bFifth[0]->count;

		$bSixth = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $five_week_ago, $six_week_ago));
		$bResults[6] = $bSixth[0]->count;

		$bSeventh = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $six_week_ago, $seven_week_ago));
		$bResults[7] = $bSeventh[0]->count;

		$bEigth = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $seven_week_ago, $eight_week_ago));
		$bResults[8] = $bEigth[0]->count;

		$bNinth = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $eight_week_ago, $nine_week_ago));
		$bResults[9] = $bNinth[0]->count;

		$bTenth = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 1 and 
			major = ? and minor = ? and updated_at < ? and updated_at > ?',array($major, $minor, $nine_week_ago, $ten_week_ago));
		$bResults[10] = $bTenth[0]->count;
		$bDates = array($one_week_ago->toDateString(), $two_week_ago->toDateString(), $three_week_ago->toDateString(), $four_week_ago->toDateString(), 
			$five_week_ago->toDateString(), $six_week_ago->toDateString(), $seven_week_ago->toDateString(), $eight_week_ago->toDateString()
			, $nine_week_ago->toDateString(), $ten_week_ago->toDateString());
		$result = array($bResults, $bDates);
		return Response::json($result);
	}

	public function getmetricae(){
		$id = Input::get('id');
		$start = DateTime::createFromFormat('d F Y - H:i', Input::get('start'));
		$end   = DateTime::createFromFormat('d F Y - H:i', Input::get('end'));
		
		if($start > $end){
			return Response::json(array('error'));
		}
		$timesSent = DB::select('select count(*) as count from Tracking where idcamp = ? and updated_at < ? and updated_at > ? and showinfo = 0', array($id, $end, $start));
		$timesOpened = DB::select('select count(*) as count from Tracking where idcamp = ? and updated_at < ? and updated_at > ? and showinfo = 1', array($id, $end, $start));
		return Response::json(array($timesSent[0]->count, $timesOpened[0]->count));
	}
	/**
	 * Show a list of all the campaigns.
	 *
	 * @param proximityUUID => proximityUUID of Beacon
	 * @return Json
	 */
	public function getdatabeacon()
	{
		
		$profile = Sentry::getUser();
		
		$proximityUUID = Input::get('proximityUUID');
		if($proximityUUID != '') {
			$totatCellphone = DB::select('select count(deviceid) as count from TrackingRange where enter = 0 and proximityUUID = ?', array($proximityUUID) );
			$uniqueCellphone = DB::select('select count(deviceid) as count from TrackingRange where enter = 0 and proximityUUID = ?', array($proximityUUID));

		}else {
			$totatCellphone = DB::select('select count(deviceid) as count from TrackingRange where enter = 0');
			$uniqueCellphone = DB::select('select count(distinct deviceid) as count from TrackingRange where enter = 0');
		}

		$result = array(
			'totatCellphone' => $totatCellphone[0]->count,
			'uniqueCellphone' => $uniqueCellphone[0]->count
		);
		return Response::json($result);
	}
}
