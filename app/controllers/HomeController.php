<?php
/** 
* A test class Kun
*
* @param  foo bar
* @return baz
*/
class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}


	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Show the page
		return Redirect::route('signin');
	}

	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function document()
	{
		$rc = new ReflectionClass('HomeController');
		echo '<pre>'; echo  ($rc->getDocComment());
		exit;
	}

}