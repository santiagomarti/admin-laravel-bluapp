<?php

class LocalsController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'name'       => 'required'
	);

	/**
	 * Show a list of all the locals.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		if (Input::get('withTrashed')) {
			$zones = Zone::withTrashed();
		}else if (Input::get('onlyTrashed')) {
			$zones = Zone::onlyTrashed();
		} else{
			$zones = Zone::whereRaw('1=1');
		}

		$zones = $zones->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));

		// Show the page
		return View::make('backend/locals/index', compact('zones'));
	}

	/**
	 * User create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		if ((Sentry::getUser()->hasAccess('subadmin')))
		{
			$companies = User::find(Sentry::getUser()->id)->getCompanies();
		}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			if ($companies->count() == 0)
			{
				$selectedCompany = -1;
			}
			else
			{
				$selectedCompany = $companies->first()->id;
			}
		}

		return View::make('backend/locals/create', compact('companies', 'selectedCompany'));
	}

	/**
	 * User create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		//If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token','_token');

			// Was the local created?
			if ($local = Zone::create($inputs))
			{
				// Prepare the success message
				$success = Lang::get('admin/locals/message.success.create');

				// Redirect to the new local page
				//return Redirect::route('update/local', $local->id)->with('success', $success);
				return Redirect::route('locals')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/locals/message.error.create');

			// Redirect to the local creation page
			return Redirect::route('create/local')->with('error', $error);
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/locals/message.error.create');
		}

		// Redirect to the local creation page
		return Redirect::route('create/local')->withInput()->with('error', $error);
	}

	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		$local = Zone::find($id);

		if ((Sentry::getUser()->hasAccess('subadmin')))
		{
			$companies = User::find(Sentry::getUser()->id)->getCompanies();
		}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}

		if ($companies->count() == 0) {
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors(array('Zone has no company'));
		}

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			$selectedCompany = $companies->first()->id;
		}

		if (empty($local->cover))
		{
			$local->cover = 'pacman.png';
		}

		$urlChange = route('update/local', $id) . '?selectedCompany=';

		// Show the page
		return View::make('backend/locals/edit', compact('local','companies', 'selectedCompany', 'urlChange'));
	}

	/**
	 * User update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		try
		{
			// Get the event information
			$local = Zone::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/locals/message.local_not_found', compact('id'));

			// Redirect to the local management page
			return Redirect::route('locals')->with('error', $error);
		}

		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Update the local
			$local->name  = Input::get('name');
			$local->description   = Input::get('description');
			$local->active   = Input::get('activated', $local->active);

			$local->company_id  = Input::get('company_id', $local->company_id);

			// Was the local updated?
			if ($local->save())
			{
				//Log::info('polygon save');
				// Prepare the success message
				$success = Lang::get('admin/locals/message.success.update');

				// Redirect to the local page
				//return Redirect::route('update/local', $id)->with('success', $success);
				return Redirect::route('locals')->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/locals/message.error.update');
		}

		// Redirect to the local creation page
		return Redirect::route('update/local', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given local.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get local information
			$local = Zone::find($id);

			// Delete the local
			$local->delete();

			// Prepare the success message
			$success = Lang::get('admin/locals/message.success.delete');

			// Redirect to the local management page
			return Redirect::route('locals')->with('success', $success);
		}
		catch (Exceptions $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/locals/message.local_not_found', compact('id' ));

			// Redirect to the local management page
			return Redirect::route('locals')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted local.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	/**
	 * Restore a deleted local.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get local information
			$local = Zone::withTrashed()->find($id);

			// Restore the local
			$local->restore();

			// Prepare the success message
			$success = Lang::get('admin/locals/message.success.restored');

			// Redirect to the local management page
			return Redirect::route('locals')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/locals/message.local_not_found', compact('id'));

			// Redirect to the local management page
			return Redirect::route('locals')->with('error', $error);
		}
	}



	/**
	 * Zone update cover
	 *
	 * @return Edit
	 */
	public function postEditCover()
	{
		try
		{
			$mimeType = HelperInputFile::getMimeType('cover');
			$base64Data = HelperInputFile::getDataBase64('cover');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'cover_' . Input::get('local_id','nolocal') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/locals/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						Log::info('bla');

						$local_id = Input::get('local_id');

						// // Get the user information
						$local = Company::find($local_id);
						$local->cover = 'locals/' . $finalName;

						Log::info('cover:' . $local->cover);

						$local->save();

						Log::info('adssad');
					}
					catch (UserNotFoundException $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing local'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/locals/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}
}
