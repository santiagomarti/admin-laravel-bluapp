<?php



class CompaniesController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'name'       => 'required',
	);

	/**
	 * Show a list of all the companies.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();

		// Grab all the companies
		//$companies = Company::paginate(10);
		$companies = Company::whereRaw('1=1');

			// Do we want to include the deleted companies?
		if (Input::get('withTrashed'))
		{
			$companies = $companies->withTrashed()->paginate(10)->appends(array(
				'withTrashed' => Input::get('withTrashed'),
			));
		}
		else if (Input::get('onlyTrashed'))
		{
			$companies = $companies->onlyTrashed()->paginate(10)->appends(array(
				'onlyTrashed' => Input::get('onlyTrashed'),
			));
		}else $companies = $companies->paginate(10);

		// Show the page
		return View::make('backend/companies/index', compact('companies', 'profile'));
	}
	/**
	 * Company create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Get the user information
		$profile = Sentry::getUser();
		return View::make('backend/companies/create', compact('profile'));
	}

	/**
	 * Company create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		$validator = Validator::make(Input::all(), $this->validationRules);

		//If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			Session::flash('activated', Input::get('activated') ? true : false);
			// Ooops.. something went wrong
			return Redirect::route('create/company')->withInput()->withErrors($validator);
		}
		if (Input::hasFile('cover'))
		{ 
			$file = Input::file('cover');
			//If validation fails, we'll exit the operation now.
			if ($this->checkImageType($file))
			{
				// Ooops.. something went wrong
				return Redirect::route('create/company')->withInput()->withErrors('Invalid file type uploade');
			}else {
				$cover = $this->uploadAvatar($file);
			}
			
		}else {
			$cover = 'companies/no-image.jpg';
		}
		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token','_token');
			$inputs['cover'] = $cover;
            if (Input::has('activated')) {
            	$inputs['activated'] = 1;
            } else {
            	$inputs['activated'] = 0;
            }
			// Was the company created?
			if ($company = Company::create($inputs))
			{
				//Auto create Unassigned for this Company
				$local = Zone::create([
	                'name' => 'Unassigned',
	                'active' => 1,
	                'description' => 'The default zone',
	                'companies_id' => $company->id
	            ]);
				// Setting sessions
				if (Session::has('number'))
				{
				    $number = Session::get('number') + 1;
				}else $number = 1;

				Session::put('number', $number);
				
				if (Session::has('alertNotification')) {
					$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New company created. </span></a></li>';
				} else {
					$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New company created. </span></a></li>';
				}
				
				Session::put('alertNotification', $li);
				// Prepare the success message
				$success = Lang::get('admin/companies/message.success.create');

				//after saving the company, store the image,
				//replacing cover input data.

				Request::merge(
						array('company_id' => $company->id,
							'cover' => Input::get('cover64')
				));

				if (Input::get('cover64','')!=='') {
					$this->postEditCover();
				}

				// Redirect to the new company page
				//return Redirect::route('update/company', $company->id)->with('success', $success);
				return Redirect::route('companies')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/companies/message.error.create');

			// Redirect to the company creation page
			return Redirect::route('create/company')->with('error', $error);
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/companies/message.error.create');
		}

		// Redirect to the company creation page
		return Redirect::route('create/company')->withInput()->with('error', $error);
	}

	
	/**
	 * Upload Image for company.
	 *
	 * @return Redirect
	 */
	public function uploadAvatar($file){
	 	//$file  = Input::file('cover');
		$extension = $file->getClientOriginalExtension();
		$name = $file->getClientOriginalName();
		$filename        = strtolower(preg_replace("/[^\w]+/", "-",str_replace('.'.$extension,'',$name))). '_'.uniqid().'.'.$extension;
		$destinationPath = public_path('avatars/companies/');
		$cover = Input::file('cover')->move($destinationPath, $filename );
		$cover = 'companies/'.$filename;
		return $cover;
	}   
	/**
	 * Delete an image for company.
	 *
	 * @return Redirect
	 */
	public function deleteAvatar($paths)
	{
		$paths = is_array($paths) ? $paths : func_get_args();
		$success = true;
		foreach ($paths as $path) { if ( ! @unlink($path)) $success = false; }
		return $success;
	}
	/**
	 * Check Image Type for Company
	 *
	 * @return Redirect
	 */		  
	public function checkImageType($file)
	{
		$mimeallow = array('image/jpg', 'image/jpeg', 'image/png');
		$mimeupload = $file->getMimeType();
		//If validation fails, we'll exit the operation now.
		if (!in_array($mimeupload, $mimeallow))
		{
			return true;
		}else {
			return false;
		}
	}		
	/**
	 * Company update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();
		// Get the event information
		$company = Company::find($id);
		if (empty($company->cover))
		{
			$company->cover = 'companies/no-image.jpg';
		}
		// Show the page
		return View::make('backend/companies/edit', compact('company', 'profile'));
	}

	/**
	 * Company update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		Log::info('postEdit all:' . json_encode(Input::all()));

		try
		{
			// Get the event information
			$company = Company::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/companies/message.company_not_found', compact('id'));

			// Redirect to the company management page
			return Redirect::route('companies')->with('error', $error);
		}

		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Update the company
			$company->name  = Input::get('name');
			$company->description   = Input::get('description');
            if (Input::has('activated')) {
            	$company->active = 1;
            } else {
            	$company->active = 0;
            }
			if (Input::hasFile('cover'))
			{
				$file = Input::file('cover');
				//If validation fails, we'll exit the operation now.
				if ($this->checkImageType($file))
				{
					// Ooops.. something went wrong
					return Redirect::route('update/company', $company->id)->withInput()->withErrors('Invalid file type uploade');
				}else {
					if ($company->cover !== 'companies/no-image.jpg') {
						$path = public_path('avatars/').$company->cover;
						$this->deleteAvatar($path);
					}
					$company->cover = $this->uploadAvatar($file);
				}

			}

			// Was the company updated?
			if ($company->save())
			{
				//Log::info('polygon save');
				// Prepare the success message
				$success = Lang::get('admin/companies/message.success.update');

				// Redirect to the company page
				//return Redirect::route('update/company', $id)->with('success', $success);
				return Redirect::route('companies')->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/companies/message.error.update');
		}

		// Redirect to the company creation page
		return Redirect::route('update/company', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given company.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get company information
			$company = Company::find($id);

			// Delete the company
			$company->delete();

			// Prepare the success message
			$success = Lang::get('admin/companies/message.success.delete');

			// Redirect to the company management page
			return Redirect::route('companies')->with('success', $success);
		}
		catch (Exceptions $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/companies/message.company_not_found', compact('id' ));

			// Redirect to the company management page
			return Redirect::route('companies')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted company.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get company information
			$company = Company::withTrashed()->find($id);

			// Restore the company
			$company->restore();

			// Prepare the success message
			$success = Lang::get('admin/companies/message.success.restored');

			// Redirect to the company management page
			return Redirect::route('companies')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/companies/message.company_not_found', compact('id'));

			// Redirect to the company management page
			return Redirect::route('companies')->with('error', $error);
		}
	}


	/**
	 * Company show.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getShow($id = null)
	{
		$company = Company::find($id);

		//Digest json inside path, send it to overlay
		$paths = json_decode($company->path);
		$company['overlays'] = $paths;
		$company['event_lat'] =  Input::get('lat');
		$company['event_lng'] =  Input::get('lng');

		////Log::info('companies:' . $company);

		// Show the page
		return View::make('backend/companies/show', compact('company'));
	}

	/**
	 * Company update cover
	 *
	 * @return Edit
	 */
	public function postEditCover()
	{
		try
		{
			Log::info('cover');

			$mimeType = HelperInputFile::getMimeType('cover');
			$base64Data = HelperInputFile::getDataBase64('cover');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'cover_' . Input::get('company_id','nocompany') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/companies/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						$company_id = Input::get('company_id', null);
						$company = Company::find($company_id);
						$company->cover = 'companies/' . $finalName;
						$company->save();
					}
					catch (Exception $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing comany'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/companies/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}



	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getService()
	{
		$response = App::make('ServicesController')->getPromotedAds();

		$json = $this->indent($response->getContent());

		// Show the page
		return View::make('backend/companies/show', compact('json'));
	}

	/**
	 * Indents a flat JSON string to make it more human-readable.
	 *
	 * @param string $json The original JSON string to process.
	 *
	 * @return string Indented version of the original JSON string.
	 */
	private function indent($json) {

	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}
	public function document()
	{
		DocumentController::document('CompaniesController');
	}

}
