<?php

class IbeaconsController extends AdminController {
	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
        'name'              => 'required',
        'peripheralID'      => 'required',
        'proximityUUID'     => 'required',
        'users_id'     		=> 'required',
        'major'             => 'required|numeric',
        'minor'             => 'required|numeric',
	);
	/**
	 * Show a list of all the beacons.
	 *
	 * @return View
	 */
	public function getIndex() {	
		// Get the user information
		$profile = Sentry::getUser();

		$ibeacons = Ibeacon::whereRaw('1=1');

		if (Input::get('withTrashed')) {
			$ibeacons = $ibeacons->withTrashed()->paginate(10)->appends(array(
				'withTrashed' => Input::get('withTrashed'),
			));
		}
		else if (Input::get('onlyTrashed')) {
			$ibeacons = $ibeacons->onlyTrashed()->paginate(10)->appends(array(
				'onlyTrashed' => Input::get('onlyTrashed'),
			));
		}else {
			$ibeacons = $ibeacons->paginate(10);
		}
		// Get users to prepare for creating
		//$users = $this->getUsers();
		$locals = Zone::all();
		$companies = Company::all();
		return View::make('backend/ibeacons/index', compact('ibeacons', 'users', 'profile', 'locals', 'companies'));
	}	
	/**
	 * Ibeacon create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Get the user information
		$profile = Sentry::getUser();
		// Get users to prepare for creating
		//$users = $this->getUsers();
		$locals = Zone::all();
		$companies = Company::all();
		return View::make('backend/ibeacons/create',  compact('users', 'profile', 'locals', 'companies'));
	}
	/**
	 * Get User for creating beacon.
	 * @return users lists
	 */
	public function getUsers()
	{
		//$users = User::where('roles', '!=', User::USER_ADMIN)->where('companies_id', Sentry::getUser()->companies_id)->get();
		//return $users;
		//@foreach ($users as $user)
		//	<option value="{{ $user['id'] }}" {{(Session::get('users_id')['0']==$user['id'])?'selected':''}}>{{ $user['first_name'] }}</option>
		//@endforeach
		$locals = Zone::where('companies_id', Input::get('companyid'))->get()->toArray();
		$users = User::where('companies_id', Input::get('companyid'))->get()->toArray();
	    return Response::json(array('users'=>$users,'zones'=>$locals));
	}
	/**
	 * Ibeacon create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate() {
		if (Request::ajax())
			{
			    return $this->getUsers();
			}
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);
		if ($validator->fails()){
			Session::flash('activated', Input::get('activated') ? true : false);
			Session::flash('enter', Input::get('enter') ? true : false);
			Session::flash('exit', Input::get('exit') ? true : false);
			Session::flash('zones_id', Input::get('zones_id') ? Input::get('zones_id') : '');
			Session::flash('proximity', Input::get('proximity') ? Input::get('proximity') : '');
			Session::flash('users_id', Input::get('users_id') ? Input::get('users_id') : '');
			Session::flash('companies_id', Input::get('companies_id') ? Input::get('companies_id') : '');
			return Redirect::route('create/ibeacon')->withInput()->withErrors($validator);
		}

		try {
			// Get the inputs, with some exceptions
			$inputs = Input::except(array('csrf_token','_token'));
			if ($inputs['users_id'] === '0') {
				$inputs['users_id'] = Sentry::getUser()->id;
			}
			if (!$inputs['companies_id']) {
				$inputs['companies_id'] = Sentry::getUser()->company->id;
			}
			if (Input::has('activated')) {
				$inputs['activated'] = 1;
			} else {
				$inputs['activated'] = 0;
			}
            if (Input::has('enter')) {
            	$inputs['enter']     = 1;
            } else {
            	$inputs['enter']     = 0;
            }
            $inputs['exit']     = 0;
            
            if(Ibeacon::where('minor', Input::get('minor'))->where('major', Input::get('major'))->count() > 0){
            	return Redirect::route('create/ibeacon')->withInput()->withErrors('Beacon already exists.');
            }
			// Was the ibeacon created?
			if ($ibeacon = Ibeacon::create($inputs)){
				$ibeacon->save();
				// Setting sessions
				if (Session::has('number'))
				{
				    $number = Session::get('number') + 1;
				}else $number = 1;

				Session::put('number', $number);
				
				if (Session::has('alertNotification')) {
					$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New ibeacon created. </span></a></li>';
				} else {
					$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New ibeacon created. </span></a></li>';
				}
				
				Session::put('alertNotification', $li);
				// Prepare the success message
				$success = Lang::get('admin/ibeacons/message.success.create');
				// Redirect to the new ibeacon page
				return Redirect::route('ibeacons')->with('success', $success);
			}
			// Prepare the error message
			$error = Lang::get('admin/ibeacons/message.error.create');
			// Redirect to the ibeacon creation page
			return Redirect::route('create/ibeacon')->with('error', $error);
		}
		catch (Exeption $e) {
			$error = Lang::get('admin/ibeacons/message.error.create');
		}
		// Redirect to the ibeacon creation page
		return Redirect::route('create/ibeacon')->withInput()->with('error', $error);
	}

	/**
	 * Ibeacon update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();
		$ibeacon = Ibeacon::find($id);

		$companies = Company::all();
		$selectedCompany = $ibeacon->companies_id;
		$users = User::where('roles', '!=', User::USER_ADMIN)->where('companies_id', $selectedCompany)->get();
		$selectedUser = $ibeacon->users_id;
		$locals = Zone::where('companies_id', $selectedCompany)->get();
		$selectedlocals = $ibeacon->zones_id;
		if (empty($ibeacon->cover))
		{
			$ibeacon->cover = '../pacman.png';
		}
		// Show the page
		return View::make('backend/ibeacons/edit', compact('ibeacon', 'users','selectedUser', 'profile', 'locals', 'selectedlocals', 'companies', 'selectedCompany'));
	}
	/**
	 * Ibeacon update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null) 
	{
		if (Request::ajax())
			{
			    return $this->getUsers();
			}
		try {
			// Get the event information
			$ibeacon = Ibeacon::find($id);
		}
		catch (Exeption $e) {
			// Prepare the error message
			$error = Lang::get('admin/ibeacons/message.ibeacon_not_found', compact('id'));
			// Redirect to the ibeacon management page
			return Redirect::route('beacons')->with('error', $error);
		}
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);
		// If validation fails, we'll exit the operation now.
		if ($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}
		try {
			// Update the ibeacon
			$inputs = Input::except('activated', 'enter', 'exit');
			$ibeacon->fill($inputs);
			if (Input::has('activated')) {
				$ibeacon->active = 1;
			} else {
				$ibeacon->active = 0;
			}
			if (Input::has('enter')) {
				$ibeacon->enter = 1;
			} else {
				$ibeacon->enter = 0;
			}
			if (Input::has('exit')) {
				$ibeacon->exit = 1;
			} else {
				$ibeacon->exit = 0;
			}
			// Was the ibeacon updated?
			if ($ibeacon->save()) {
				// Prepare the success message
				$success = Lang::get('admin/ibeacons/message.success.update');

				// Redirect to the ibeacon page
				return Redirect::route('ibeacons')->with('success', $success);
			}
		} catch (Exeption $e) {
			$error = Lang::get('admin/ibeacons/message.error.update');
		}
		// Redirect to the ibeacon creation page
		return Redirect::route('update/ibeacon', $id)->withInput()->with('error', $error);
	}
	/**
	 * Delete the given ibeacon.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try {
			// Get ibeacon information
			$ibeacon = Ibeacon::find($id);
			// Delete the ibeacon
			$ibeacon->delete();
			// Prepare the success message
			$success = Lang::get('admin/ibeacons/message.success.delete');
			// Redirect to the ibeacon management page
			return Redirect::route('ibeacons')->with('success', $success);
		}catch (Exceptions $e) {
			$error = Lang::get('admin/ibeacons/message.ibeacon_not_found', compact('id' ));
			return Redirect::route('ibeacons')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted ibeacon.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try {
			// Restore
			$ibeacon = Ibeacon::withTrashed()->find($id);
			$ibeacon->restore();
			// Prepare the success message
			$success = Lang::get('admin/ibeacons/message.success.restored');
			// Redirect to the ibeacon management page
			return Redirect::route('ibeacons')->with('success', $success);
		} catch (UserNotFoundException $e) {
			// Prepare the error message
			$error = Lang::get('admin/ibeacons/message.ibeacon_not_found', compact('id'));
			// Redirect to the ibeacon management page
			return Redirect::route('ibeacons')->with('error', $error);
		}
	}

	public function document()
	{
		DocumentController::document('IbeaconsController');
	}
}