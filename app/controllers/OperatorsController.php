<?php

class OperatorsController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'name'       => 'required'
	);


	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRulesUsers = array(
		'locals'       => 'required',
		'first_name'       => 'required|min:3',
		'last_name'        => 'required|min:3',
		'email'            => 'required|email|unique:users,email',
		'password'         => 'required|between:3,32',
		'password_confirm' => 'required|between:3,32|same:password',
	);

	/**
	 * Show a list of all the operators.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		if ((Sentry::getUser()->hasAccess('subadmin')))
		{
			$companies = User::find(Sentry::getUser()->id)->getCompanies();
		}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}

		$selectedCompany = Input::get('selectedCompany', null);

		$operators = [];

		if (is_null($selectedCompany))
		{
			if ($companies->count() == 0)
			{
				$operators = [];
			}
			else
			{
				$selectedCompany = $companies->first()->id;
				$operators = User::all();

				$operators = $operators->filter(function($operator) use ($selectedCompany)
				{
					$user = User::find($operator->id);

					//Filter user
				    return $user->inCompany(Company::find($selectedCompany)) &&
				    		$user->hasAccess('operator');
				});
			}
		}
		else
		{
				if ($companies->count() == 0)
				{
					$operators = [];
				}
				else
				{
					$operators = User::all();

					$operators = $operators->filter(function($operator) use ($selectedCompany)
					{
						$user = User::find($operator->id);

						//Filter user
					    return $user->inCompany(Company::find($selectedCompany)) &&
					    		$user->hasAccess('operator');
					});
				}

		}

		// Show the page
		return View::make('backend/operators/index', compact('operators', 'companies', 'selectedCompany'));
	}

	/**
	 * User create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		if ((Sentry::getUser()->hasAccess('subadmin')))
		{
			$companies = User::find(Sentry::getUser()->id)->getCompanies();
		}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			if ($companies->count() == 0)
			{
				$locals = [];
			}
			else
			{
				$selectedCompany = $companies->first()->id;
				$locals = Local::where('company_id', '=', $selectedCompany)->get();
			}
		}

		return View::make('backend/operators/create', compact('companies', 'selectedCompany', 'locals') );
	}

	/**
	 * User create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRulesUsers);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token', 'password_confirm', 'groups', 'company_id', 'locals');

			$companySelected = Company::find(Input::get('company_id'));

			if (is_null($companySelected)) {
				// Ooops.. something went wrong
				return Redirect::back()->withInput()->withErrors(array('Operator has no company'));
			}

			// Was the user created?
			if ($operator = Sentry::getUserProvider()->create($inputs))
			{

				$group = Sentry::findGroupByName('operator');

				$operator->addGroup($group);

				$operator->permissions = array(
			        'superuser' => -1
			    );

				$operator->addCompany($companySelected);

				// Get the current user locals
				$userLocals = $operator->locals()->lists('local_id', 'local_id');

				// Get the selected locals
				$selectedLocals = Input::get('locals', array());

				$localsToAdd    = array_diff($selectedLocals, $userLocals);
				$localsToRemove = array_diff($userLocals, $selectedLocals);

				// Assign the user to locals
				foreach ($localsToAdd as $localId)
				{
					$local = Local::find($localId);

					$added = $operator->addLocal($local);

				}

				// Remove the user from locals
				foreach ($localsToRemove as $localId)
				{
					$local = Local::find($localId);

					$operator->removeLocal($local);
				}

				// Prepare the success message
				$success = Lang::get('admin/users/message.success.create');

				// Redirect to the new user page
				//return Redirect::route('update/operator', $operator->id)->with('success', $success);
				return Redirect::route('operators')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/users/message.error.create');

			// Redirect to the user creation page
			return Redirect::route('create/operator')->with('error', $error);
		}
		catch (LoginRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_login_required');
		}
		catch (PasswordRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_password_required');
		}
		catch (UserExistsException $e)
		{
			$error = Lang::get('admin/users/message.user_exists');
		}

		// Redirect to the user creation page
		return Redirect::route('create/operator')->withInput()->with('error', $error);
	}

	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		$operator = User::find($id);

		if ((Sentry::getUser()->hasAccess('subadmin')))
		{
			$companies = User::find(Sentry::getUser()->id)->getCompanies();
		}
		if ((Sentry::getUser()->hasAccess('admin')))
		{
			$companies = Company::where('active','=',1)->get();
		}

		$selectedCompany = Input::get('selectedCompany', null);

		if (is_null($selectedCompany))
		{
			$selectedCompany = $companies->first()->id;
		}

		$locals = Local::where('company_id', '=', $selectedCompany)->get();

		$userLocals = $operator->getLocals();

		$urlChange = route('update/operator', $id) . '?selectedCompany=';

		// Show the page
		return View::make('backend/operators/edit', compact('operator','companies', 'selectedCompany', 'locals', 'userLocals', 'urlChange'));
	}

	/**
	 * User update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		try
		{
			// Get the event information
			$operator = User::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/operators/message.operator_not_found', compact('id'));

			// Redirect to the operator management page
			return Redirect::route('operators')->with('error', $error);
		}

		$this->validationRulesUsers['email'] = "required|email|unique:users,email,{$operator->email},email";

		// Do we want to update the user password?
		if ( ! $password = Input::get('password') )
		{
			unset($this->validationRulesUsers['password']);
			unset($this->validationRulesUsers['password_confirm']);
		}
		else
		{
			$operator->password   = Input::get('password');
		}

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRulesUsers);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Update the operator
			$operator->first_name  = Input::get('first_name');
			$operator->last_name   = Input::get('last_name');
			$operator->activated   = Input::get('activated', $operator->activated);
			$operator->email   = Input::get('email');

			// Was the operator updated?
			if ($operator->save())
			{

				// Get the current user locals
				$userLocals = $operator->locals()->lists('local_id', 'local_id');

				// Get the selected locals
				$selectedLocals = Input::get('locals', array());

				$localsToAdd    = array_diff($selectedLocals, $userLocals);
				$localsToRemove = array_diff($userLocals, $selectedLocals);

				// Assign the user to locals
				foreach ($localsToAdd as $localId)
				{
					$local = Local::find($localId);

					$added = $operator->addLocal($local);

				}

				// Remove the user from locals
				foreach ($localsToRemove as $localId)
				{
					$local = Local::find($localId);

					$operator->removeLocal($local);
				}


				//Log::info('polygon save');
				// Prepare the success message
				$success = Lang::get('admin/operators/message.success.update');

				// Redirect to the operator page
				//return Redirect::route('update/operator', $id)->with('success', $success);
				return Redirect::route('operators')->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/operators/message.error.update');
		}

		// Redirect to the operator creation page
		return Redirect::route('update/operator', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given operator.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get operator information
			$operator = User::find($id);

			// Delete the operator
			$operator->delete();

			// Prepare the success message
			$success = Lang::get('admin/operators/message.success.delete');

			// Redirect to the operator management page
			return Redirect::route('operators')->with('success', $success);
		}
		catch (Exceptions $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/operators/message.operator_not_found', compact('id' ));

			// Redirect to the operator management page
			return Redirect::route('operators')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted operator.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	/**
	 * Restore a deleted operator.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get operator information
			$operator = User::withTrashed()->find($id);

			// Restore the operator
			$operator->restore();

			// Prepare the success message
			$success = Lang::get('admin/operators/message.success.restored');

			// Redirect to the operator management page
			return Redirect::route('operators')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/operators/message.operator_not_found', compact('id'));

			// Redirect to the operator management page
			return Redirect::route('operators')->with('error', $error);
		}
	}

}
