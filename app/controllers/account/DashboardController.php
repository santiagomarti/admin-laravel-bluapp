<?php namespace Controllers\Account;

use AuthorizedController;
use Redirect;
use Sentry;

class DashboardController extends AuthorizedController {

	/**
	 * Redirect to the profile page.
	 *
	 * @return Redirect
	 */
	public function getIndex()
	{
		// Grab the user
		$profile = Sentry::getUser();
		// Redirect to the profile page
		//return Redirect::route('profile');
		return \View::make('backend/dashboard', compact('profile'));

	}

}
