<?php namespace Controllers\Account;

use AuthorizedController;
use Input;
use Redirect;
use Sentry;
use Validator;
use View;
use \Company;
use Response;
use Session;

class ProfileController extends AuthorizedController {
	/**
	 * User profile page.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();
		// Get this user groups
		$userGroups = $profile->groups()->lists('name');
		// Get this user company
		$usercompany = $profile->getCompanies()->lists('name');
		// Get all the available groups
		$groups = Sentry::getGroupProvider()->createModel();
		
		// Filter group by id
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
	        $groups = $groups->where('id', '!=', \User::USER_OPERATOR)->get();
	    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
	    {
	        $groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();
	    }else $groups = $groups->get();

		$companies = Company::lists('name' ,'id');

		// Show the page
		//return View::make('frontend/account/profile', compact('user'));
		return View::make('backend/account/profile', compact('profile', 'usercompany', 'userGroups', 'groups', 'companies'));
	}

	/**
	 * User profile form processing page.
	 *
	 * @return Redirect
	 */
	public function postIndex()
	{
		// Grab the user
		$profile = Sentry::getUser();
		$profile->first_name =  Input::get('first_name');
		$profile->last_name  =  Input::get('last_name');
		//Bloqueamos todo esto para que un usuario no pueda cambiar ni su grupo ni compañía
		/*$profile->companies_id  =  Input::get('companies_id');
		// Get the current user groups
		$userGroups = $profile->groups()->lists('group_id', 'group_id');

		// Get the selected groups
		$selectedGroups = Input::get('groups', array());

		// Groups comparison between the groups the user currently
		// have and the groups the user wish to have.
		$groupsToAdd    = array_diff($selectedGroups, $userGroups);
		$groupsToRemove = array_diff($userGroups, $selectedGroups);

		// Assign the user to groups
		foreach ($groupsToAdd as $groupId)
		{
			$group = Sentry::getGroupProvider()->findById($groupId);

			$profile->addGroup($group);
		}

		// Remove the user from groups
		foreach ($groupsToRemove as $groupId)
		{
			$group = Sentry::getGroupProvider()->findById($groupId);

			$profile->removeGroup($group);
		}
		if (is_null($profile->gravatar)) {
		 	$profile->gravatar = 'users_avatars/default-user-avatar.png';
		 }*/ 
		$profile->save();
		// Redirect to the settings page
		return Redirect::route('profile')->with('success', 'Account successfully updated');
	}
	public function getAvatar()
	{
		// Get the user information
		$profile = Sentry::getUser();
		if (is_null($profile->gravatar)) {
		 	$profile->gravatar = 'users_avatars/default-user-avatar.png';
		 }

		// Show the page
		return View::make('backend/account/avatar', compact('profile'));
	}
	public function postAvatar()
	{
		// Grab the user
		$profile = Sentry::getUser();
		if (Session::has('dropzoneAvatar')) {
			if ($profile->gravatar !== 'users_avatars/default-user-avatar.png') {
				$path = public_path('avatars/').$profile->gravatar;
				$this->deleteAvatar($path);
			}
			$profile->gravatar = Session::get('dropzoneAvatar');
			Session::forget('dropzoneAvatar');
			$profile->save();
		}

		// Redirect to the settings page
		return Redirect::route('profile')->with('success', 'Avatar successfully updated');
	}

	public function dropzone(){
		if (Session::has('dropzoneAvatar')) {
			$this->removeDropzone();
		}
		$file = Input::file('file');
		//If validation fails, we'll exit the operation now.
		if ($this->checkImageType($file))
		{
			$dropzone = $this->uploadAvatar($file);
			Session::put('dropzoneAvatar', $dropzone);
		}
	}

	public function removeDropzone(){
		if (Session::has('dropzoneAvatar')) {
			$path = public_path('avatars/').Session::get('dropzoneAvatar');
			$this->deleteAvatar($path);
			Session::forget('dropzoneAvatar');
		}
	}

	public function uploadAvatar($file){
	 	//$file  = Input::file('cover');
		$extension = $file->getClientOriginalExtension();
		$name = $file->getClientOriginalName();
		$filename        = strtolower(preg_replace("/[^\w]+/", "-",str_replace('.'.$extension,'',$name))). '_'.uniqid().'.'.$extension;
		$destinationPath = public_path('avatars/users_avatars/');
		$cover = $file->move($destinationPath, $filename );
		$cover = 'users_avatars/'.$filename;
		return $cover;
	}   

	public function deleteAvatar($paths)
	{
		$paths = is_array($paths) ? $paths : func_get_args();
		$success = true;
		foreach ($paths as $path) { if ( ! @unlink($path)) $success = false; }
		return $success;
	}
		  
	public function checkImageType($file)
	{
		$mimeallow = array('image/jpg', 'image/jpeg', 'image/png');
		$mimeupload = $file->getMimeType();
		//If validation fails, we'll exit the operation now.
		if (in_array($mimeupload, $mimeallow))
		{
			return true;
		}else {
			return false;
		}
	}	  
	public function deleteSession()
	{
		Session::forget('number');
		Session::forget('alertNotification');
		return Redirect::back();
	}

}
