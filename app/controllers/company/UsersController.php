<?php namespace Controllers\Company;

use AdminController;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Config;
use Input;
use Lang;
use Redirect;
use Sentry;
use Validator;
use View;
use Log;
use Response;
use DB;
use Session;


use \Company;


class UsersController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'first_name'       => 'required|min:3',
		'last_name'        => 'required|min:3',
		'companies_id'     => 'required',
		'email'            => 'required|email|unique:users,email',
		'password'         => 'required|between:3,32',
		'password_confirm' => 'required|between:3,32|same:password',
	);

	/**
	 * Show a list of all the users.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();

		// Grab all the users
		/*$role = Input::get('role');
		if (is_null($role)) {
			return Redirect::route('admin')->withErrors(array(\Lang::get('general.wrong_url')));
		}
		if ($role == \User::USER_SUBADMIN) {
			$typeUser = 'Company User';
		}else{
			$typeUser = 'Operator User';
		}*/

		$users = Sentry::getUserProvider()->createModel();
		// Filter user by roles
		//$users = $users->where('roles', $role);
		// Filter user by companies_id
		$companies_id = Sentry::getUser()->companies_id;
		$users = $users->where('companies_id', '=', $companies_id);
		
		if (Input::get('withTrashed')) {
			$users = $users->withTrashed();
		} else if (Input::get('onlyTrashed')) {
			$users = $users->where('activated', '!=', 1);
		} else $users = $users->where('activated', '=', 1);
		// Paginate the users
		$users = $users->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
				'role' => Input::get('role'),
			));
		// 	Get all the available groups
		$groups = Sentry::getGroupProvider()->createModel();
		$groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();
		// 	Get company that belong to logging  user
		$companies = Company::find($companies_id);
		// Show the page
		return View::make('backend/companyusers/index', compact('users', 'companies', 'groups', 'typeUser', 'role', 'profile'));
	}

	/**
	 * User create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Get the user information
		$profile = Sentry::getUser();

		$groups = Sentry::getGroupProvider()->createModel();
		$groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();

		$companies_id = Sentry::getUser()->companies_id;
		$companies = Company::find($companies_id);

		// Show the page
		return View::make('backend/companyusers/create', compact('companies', 'groups', 'profile'));
	}

	/**
	 * User create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::except('recover'), $this->validationRules);
		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			Session::flash('activated', Input::get('activated') ? true : false);
			Session::flash('groups', Input::get('groups') ? Input::get('groups') : '');
			Session::flash('companies_id', Input::get('companies_id') ? Input::get('companies_id') : '');
			// Ooops.. something went wrong
			return Redirect::route('create/companyuser')->withInput()->withErrors($validator);
		}
		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token', 'password_confirm', 'groups', 'companies','recover');
			$inputs['roles'] = Input::get('groups')[0];
			$inputs['gravatar'] = 'users_avatars/default-user-avatar.png';
            if (Input::has('activated')) {
            	$inputs['activated'] = 1;
            } else {
            	$inputs['activated'] = 0;
            }
			// Was the user created?
			if ($user = Sentry::getUserProvider()->create($inputs)) {
				// Assign the selected groups to this user
				$groupId = Input::get('groups')[0];
				$group = Sentry::getGroupProvider()->findById($groupId);
				$user->addGroup($group);
				// Setting sessions
				if (Session::has('number'))
				{
				    $number = Session::get('number') + 1;
				}else $number = 1;

				Session::put('number', $number);
				
				if (Session::has('alertNotification')) {
					$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New user created. </span></a></li>';
				} else {
					$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New user created. </span></a></li>';
				}
				
				Session::put('alertNotification', $li);
				// Prepare the success message
				$success = Lang::get('admin/users/message.success.create');

				// Redirect to the new user page
				return Redirect::route('companyusers')->with('success', $success);
			}
			// Prepare the error message
			$error = Lang::get('admin/users/message.error.create');

			// Redirect to the user creation page
			return Redirect::route('create/companyuser')->with('error', $error);
		}
		catch (LoginRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_login_required');
		}
		catch (PasswordRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_password_required');
		}
		catch (UserExistsException $e)
		{
			$error = Lang::get('admin/users/message.user_exists');
		}
		// Redirect to the user creation page
		return Redirect::route('create/companyuser')->withInput()->with('error', $error);
	}

	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();
		try
		{
			// Get the user information
			$user = Sentry::getUserProvider()->findById($id);
			// Filter user by roles
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		    	if ($user->roles==\User::USER_OPERATOR) {
		    		return Redirect::route('companyusers');
		    		exit();
		    	}
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
		    	if ($user->roles==\User::USER_ADMIN) {
		    		return Redirect::route('companyusers');
		    		exit();
		    	}
		    }

			// Get this user groups
			$userGroups = $user->groups()->lists('name', 'group_id');

			// Get this user permissions
			$userPermissions = array_merge(Input::old('permissions', array('superuser' => -1)), $user->getPermissions());
			$this->encodePermissions($userPermissions);

			// Get a list of all the available groups
			$groups = Sentry::getGroupProvider()->createModel();
			// Filter group by id
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		        $groups = $groups->where('id', '!=', \User::USER_OPERATOR)->get();
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
		        $groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();
		    }

			// Selected groups
			$selectedCompanies = Input::old('companies', array());

			//$companies = Company::all();

			$companies_id = Sentry::getUser()->companies_id;
			$companies = Company::where('id', '=', $companies_id)->lists('name' ,'id');
			//$companies = Company::lists('name' ,'id');

			// Get all the available permissions
			$permissions = Config::get('permissions');
			// Filter permissions
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
				$permissions = array_except($permissions, array('Operator'));
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
				$permissions = array_except($permissions, array('Admin'));
		    }
			$this->encodeAllPermissions($permissions);


		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));
			// Redirect to the user management page
			return Redirect::route('companyusers')->with('error', $error);
		}
		// Show the page
		return View::make('backend/companyusers/edit',
			compact(
					'user', 'groups', 'userGroups', 'permissions', 'userPermissions',
					'selectedCompanies', 'companies', 'userCompanies', 'profile'));
	}

	/**
	 * User update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		// We need to reverse the UI specific logic for our
		// permissions here before we update the user.
		$permissions = Input::get('permissions', array());
		$this->decodePermissions($permissions);
		app('request')->request->set('permissions', $permissions);

		try {
			// Get the user information
			$user = Sentry::getUserProvider()->findById($id);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));
			// Redirect to the user management page
			return Redirect::route('companyusers')->with('error', $error);
		}
		//	Update validation rule for email
		$this->validationRules['email'] = "required|email|unique:users,email,{$user->email},email";

		// Do we want to update the user password?
		if (Input::has('password_edit'))
		{
			$this->validationRules['password_edit'] = $this->validationRules['password'];
			$this->validationRules['password_confirm_edit'] = 'required|between:3,32|same:password_edit';
		}
		unset($this->validationRules['password']);
		unset($this->validationRules['password_confirm']);
		// Do we want to update the user password?
		//if ( ! $password = Input::get('password_edit'))
		//{
		//	unset($this->validationRules['password']);
		//	unset($this->validationRules['password_confirm']);
		//}

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::route('companyusers')->withInput()->withErrors($validator);
		}

		try
		{
			// Update the user
			$user->first_name  = Input::get('first_name');
			$user->last_name   = Input::get('last_name');
			$user->email       = Input::get('email');
    		if (Sentry::getId() !== $user->id) {
	            if (Input::has('activated')) {
	            	$user->activated = 1;
	            } else {
	            	$user->activated = 0;
	            }
    		}
			$user->permissions = Input::get('permissions');
			$user->companies_id = Input::get('companies_id');
			$user->roles = Input::get('groups')[0];
			// Do we want to update the user password?
			if (Input::has('password_edit'))
			{
				$user->password = Input::get('password_edit');
			}

			// Get the current user groups
			$userGroups = $user->groups()->lists('group_id', 'group_id');

			// Get the selected groups
			$selectedGroups = Input::get('groups', array());

			// Groups comparison between the groups the user currently
			// have and the groups the user wish to have.
			$groupsToAdd    = array_diff($selectedGroups, $userGroups);
			$groupsToRemove = array_diff($userGroups, $selectedGroups);

			// Assign the user to groups
			foreach ($groupsToAdd as $groupId)
			{
				$group = Sentry::getGroupProvider()->findById($groupId);

				$user->addGroup($group);
			}

			// Remove the user from groups
			foreach ($groupsToRemove as $groupId)
			{
				$group = Sentry::getGroupProvider()->findById($groupId);

				$user->removeGroup($group);
			}

			// Was the user updated?
			if ($user->save())
			{
				// Get the current user companies
				// Prepare the success message
				$success = Lang::get('admin/users/message.success.update');

				// Redirect to the user page
				return Redirect::route('companyusers')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/users/message.error.update');
		}
		catch (LoginRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_login_required');
		}

		// Redirect to the user page
		return Redirect::route('update/companyuser', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given user.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get user information
			$user = Sentry::getUserProvider()->findById($id);
			$role = $user->roles;
			// Check if we are not trying to delete ourselves
			if ($user->id === Sentry::getId())
			{
				// Prepare the error message
				$error = Lang::get('admin/users/message.error.delete');

				// Redirect to the user management page
				return Redirect::route('companyusers', array('role' => $role ))->with('error', $error);
			}

			// Do we have permission to delete this user?
			if ($user->isSuperUser() and ! Sentry::getUser()->isSuperUser())
			{
				// Redirect to the user management page
				return Redirect::route('companyusers', array('role' => $role ))->with('error', 'Insufficient permissions!');
			}
			// Delete the user
			$user->delete();

			// Prepare the success message
			$success = Lang::get('admin/users/message.success.delete');

			// Redirect to the user management page
			return Redirect::route('companyusers', array('role' => $role ))->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id' ));

			// Redirect to the user management page
			return Redirect::route('companyusers')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted user.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get user information
			$user = Sentry::getUserProvider()->createModel()->withTrashed()->find($id);

			// Restore the user
			$user->restore();

			// Prepare the success message
			$success = Lang::get('admin/users/message.success.restored');

			// Redirect to the user management page
			return Redirect::route('companyusers', array('role' => $user->roles) )->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));

			// Redirect to the user management page
			return Redirect::route('companyusers')->with('error', $error);
		}
	}



	/**
	 * Ajax, to check email
	 *
	 * @param  email
	 * @return json
	 */
	public function getEmailDuplicate()
	{

		$validationEmailRules = array(
			'email'            => 'required|email'
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $validationEmailRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			//send response
			return Response::json(array(
	                'success' 	=> 	false,
	                'email'   	=> 	Input::get('email'),
	                'msg'     	=> 	'email not correct format',
	                'code'      => -1,
	                'valid'   	=> 	false,
			        ), 200);
		}

		//check if the user exist via email
	    $result = DB::select(DB::raw('select id, deleted_at from users where email=?'), array(Input::get('email')));

	    //If count is zero there is no email then is success is true, and is valid.
	    if (count($result) == 0)
	    {
	    	//send response
			return Response::json(array(
                'success'   => true,
                'email' 	=> Input::get('email'),
		        'msg'  		=> 'email not found',
		        ), 200);

		} else {
			//Check if the email that exist is activated or not
			$row = $result[0];
			$activated = false;
			if (is_null($row->deleted_at))
			{
				$activated = true;
			}

			//send response
			return Response::json(array(
	                'success' 	=> 	false,
	                'email'   	=> 	Input::get('email'),
	                'msg'     	=> 	'email found, if activated is true, then is active account',
	                'activated'   	=> 	$activated
			        ), 200);
		}
	}
}
