<?php
namespace Controllers\Company;
use AdminController;
use Input;
use Ibeacon;
use Sentry;
use User;
use View;
use Redirect;
use Lang;
use Validator;
use Zone;
class IbeaconsController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
        'name'              => 'required',
        'peripheralID'      => 'required',
        'proximityUUID'     => 'required',
        'major'             => 'required|numeric',
        'minor'             => 'required|numeric',
	);

	/**
	 * Show a list of all the beacons.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();

		$companies_id = \Sentry::getUser()->companies_id;
		/*$ibeacons = Ibeacon::whereHas('user',  function($q) use($companies_id) {
		    $q->where('companies_id', $companies_id);
		});*/
		$ibeacons = Ibeacon::where('companies_id', $companies_id);

		$ibeacons = $ibeacons->paginate(10);
		$locals = Zone::where('companies_id', $companies_id)->get();
		// Show the page
		return \View::make('backend/companyibeacons/index', compact('ibeacons', 'profile', 'locals'));
	}
	/**
	 * Ibeacon update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();

		$companies_id = \Sentry::getUser()->companies_id;
		$ibeacon = Ibeacon::where('id', $id)->whereHas('user',  function($q) use($companies_id) {
		    $q->where('companies_id', $companies_id);
		})->first();
		if (!$ibeacon) {
			return Redirect::route('admin')->withErrors(array(Lang::get('admin/users/message.noaccess')));
		}
		$companies = \Company::get();

		$selectedCompany = $ibeacon->companies_id;

		$users = $this->getUsers();

		$selectedUser = $ibeacon->users_id;
		$locals = Zone::where('companies_id', $companies_id)->get();
		$selectedlocals = $ibeacon->zones_id;
		if (empty($ibeacon->cover))
		{
			$ibeacon->cover = '../pacman.png';
		}

		return View::make('backend/companyibeacons/edit', compact('profile', 'ibeacon', 'users', 'companies', 'selectedCompany','selectedUser', 'locals', 'selectedlocals'));
	}
	/**
	 * Get User for creating beacon.
	 * @return users lists
	 */
	public function getUsers()
	{
		$users = User::where('roles', '!=', User::USER_ADMIN)->where('companies_id', Sentry::getUser()->companies_id)->orderBy('email')->get();
		return $users;
	}

	/**
	 * Ibeacon update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		try
		{
			// Get the event information
			$ibeacon = Ibeacon::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/ibeacons/message.ibeacon_not_found', compact('id'));

			// Redirect to the ibeacon management page
			return Redirect::route('companyibeacons')->with('error', $error);
		}
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails()){
			return Redirect::back()->withInput()->withErrors($validator);
		}
		try {
			$inputs = Input::except('_token', 'company_id');
			if (array_key_exists('active', $inputs)) {
				$inputs['active'] = 1;
			} else {
				$inputs['active'] = 0;
			}
			if (array_key_exists('enter', $inputs)) {
				$inputs['enter'] = 1;
			} else {
				$inputs['enter'] = 0;
			}
			if (array_key_exists('exit', $inputs)) {
				$inputs['exit'] = 1;
			} else {
				$inputs['exit'] = 0;
			}
			$ibeacon->fill($inputs);
			$ibeacon->active = $inputs['active'];
			// Was the ibeacon updated?
			if ($ibeacon->save()) {
				$success = Lang::get('admin/ibeacons/message.success.update');
				// Redirect to the ibeacon page with success message
				return Redirect::route('companyibeacons')->with('success', $success);
			}
		}
		catch (Exeption $e) {
			$error = Lang::get('admin/ibeacons/message.error.update');
		}
		// Redirect to the ibeacon creation page
		return Redirect::route('update/companyibeacons', $id)->withInput()->with('error', $error);
	}
}