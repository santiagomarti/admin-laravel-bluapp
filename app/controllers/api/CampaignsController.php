<?php namespace Controllers\Api;

use AdminController;
use Lang;
use Campaign;
use Input;
use Response;
use Session;
use \DateTime;

class CampaignsController extends AdminController {
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
	  	// init  companies variable
		$companies_id = \Sentry::getUser()->companies_id;
		$campaigns = Campaign::whereHas('ibeacon',  function($q) use($companies_id) {
		    $q->where('companies_id', $companies_id);
		});
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $campaigns->count();
		}

		// Do we want to include the deleted companies?
		if (Input::get('show_value')=='withTrashed')
		{
			$campaigns = $campaigns->withTrashed();
		}
		else if (Input::get('show_value')=='onlyTrashed')
		{
			$campaigns = $campaigns->onlyTrashed();
		}

		//	using idd in template to avoid duplicating with id default 
		if (Input::has('title')) {
		    $campaigns->where('title', 'like', '%' . urldecode(Input::get('title')) . '%');
		}

		if (Input::has('msg_enter')) {
		    $campaigns->where('msg_enter', 'like', '%' . urldecode(Input::get('msg_enter')) . '%');
		}

		if (Input::has('type')) {
		    $campaigns->where('type', urldecode(Input::get('type')) );
		}
		//	Filter begin_date field
		if (Input::has('begin_date')) {
			$begin_date = DateTime::createFromFormat('d F Y - H:i', Input::get('begin_date'));
			$campaigns->where('begin_date', '<=', $begin_date);
		}
		//	Filter end_date field, + 1 days to 00::00::00 -> 23:59:59
		if (Input::has('end_date')) {
			$end_date = DateTime::createFromFormat('d F Y - H:i', Input::get('end_date'));
			$campaigns->where('end_date', '>=', $end_date);
		}

		if (Input::has('state')) {
		    $campaigns->where('active', urldecode(Input::get('state')));
		}
		//	Order By
		if (Input::has('order')) {
			$indexSort = Input::get('order')[0]['column'];
			$typeOrder = Campaign::$headerSort[$indexSort];
			if ($typeOrder !== '') {
				$sort = Input::get('order')[0]['dir'];
				$campaigns->orderBy($typeOrder,$sort);
			}
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $campaigns->count();
		$campaigns->skip($start)->take($iDisplayLength);

		$campaigns = $campaigns->get();
		
	  	foreach ($campaigns as $key => $campaign) {
	  		if (is_null($campaign->deleted_at)) {
	  			$actionLinks = '<a href="'.route("update/campaign", $campaign->id).'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a>
			  			<a href="' . route('deleteAjax/campaign', $campaign->id) . '" class="btn default btn-xs red confirmBefore"><i class="fa fa-trash-o"></i> '.Lang::get('button.delete').'</a>';
	  		} else {
	  			$actionLinks = '<a href="' . route('restoreAjax/campaign', $campaign->id) . '" class="btn btn-mini btn-warning restore">'.Lang::get('button.restore').'</a>';
	  		};

	  		if ($campaign->isActive()) {
	  			$state = '<span class="label label-sm label-success">'.Lang::get('general.active').'</span>';
	  		} else {
	  			$state = '<span class="label label-sm label-danger">'.Lang::get('general.inactive').'</span>';
	  		};

	  		if (!is_null($campaign->begin_date) && !is_null($campaign->end_date)) {
	  			$begin_date = DateTime::createFromFormat('Y-m-d H:i:s',$campaign->begin_date);
	  			$end_date = DateTime::createFromFormat('Y-m-d H:i:s',$campaign->end_date);
	  			$active_date = 'From '.$begin_date->format('d F Y - H:i').'<br/>To '.$end_date->format('d F Y - H:i');
	  		} else {
	  			$active_date = 'Unknow';
	  		}
	  		
	  		$results["data"][] = array(
	  			$campaign->title, 
	  			$campaign->msg_enter,
	  			$campaign->type,
	  			$active_date,
	  			$state,
	  			$actionLinks
	  		);
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	
	public function delete($id = null)
	{	
		// Get campaign information
		$campaign = Campaign::find($id);

		// Delete the campaign
		Campaign::destroy($id);
		// Setting sessions
		if (Session::has('number'))
		{
		    $number = Session::get('number') + 1;
		}else $number = 1;

		Session::put('number', $number);
		
		if (Session::has('alertNotification')) {
			$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Campaign #'.$id.' deleted. </span></a></li>';
		} else {
			$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Campaign #'.$id.' deleted. </span></a></li>';
		}
		
		Session::put('alertNotification', $li);
		$number = Session::get('number');
		$alertNotification = Session::get('alertNotification');
		// Prepare the success message
		$success = Lang::get('admin/campaigns/message.success.delete');
		return Response::json(['status' => 'OK', 'message' => $success, 'number' => $number, 'alertNotification' => $alertNotification]);
	}
	public function restore($id = null)
	{
		// Get campaign information
		$campaign = Campaign::withTrashed()->find($id);

		// Restore the campaign
		$campaign->restore();
		// Prepare the success message
		$success = Lang::get('admin/campaigns/message.success.restored');
		return Response::json(['status' => 'OK', 'message' => $success]);
	}
}
