<?php namespace Controllers\Api;

use AdminController;
use Sentry;
use Lang;
use Group;
use Input;
use Response;
use Cartalyst\Sentry\Groups\GroupExistsException;
use Cartalyst\Sentry\Groups\GroupNotFoundException;
use Cartalyst\Sentry\Groups\NameRequiredException;

class GroupsController extends AdminController {

	public function ajaxdemo()
	{
		$iTotalRecords = 178;
	  	$iDisplayLength = intval($_REQUEST['length']);
	  	$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	  	$iDisplayStart = intval($_REQUEST['start']);
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$records = array();
	  	$records["data"] = array(); 

	  	$end = $iDisplayStart + $iDisplayLength;
	  	$end = $end > $iTotalRecords ? $iTotalRecords : $end;

	  	$status_list = array(
		    array("success" => "Pending"),
		    array("info" => "Closed"),
		    array("danger" => "On Hold"),
		    array("warning" => "Fraud")
	  	);

	  	for($i = $iDisplayStart; $i < $end; $i++) {
		    $status = $status_list[rand(0, 2)];
		    $id = ($i + 1);
		    $records["data"][] = array(
		      '<input type="checkbox" name="id[]" value="'.$id.'">',
		      $id,
		      '12/09/2013',
		      'Jhon Doe',
		      'Jhon Doe',
		      '450.60$',
		      rand(1, 10),
		      '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
		      '<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',
		   );
	  	}

	  	if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
		    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
		    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
	  	}

	  	$records["draw"] = $sEcho;
	  	$records["recordsTotal"] = $iTotalRecords;
	  	$records["recordsFiltered"] = $iTotalRecords;
		  
	  	echo json_encode($records);
	}
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
	  	// init  groups variable
		$groups = Group::whereRaw('1=1');
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $groups->count();
		}

		//	using idd in template to avoid duplicating with id default 
		if (Input::has('idd')) {
		    $groups->where('id', urldecode(Input::get('idd')));
		}

		if (Input::has('namee')) {
		    $groups->where('name', 'like', '%' . urldecode(Input::get('namee')) . '%');
		}

		if (Input::has('number_of_user_from')) {
			$number_of_user_from = intval(Input::get('number_of_user_from'));
			$groups->has('users', '>=', $number_of_user_from);
		}

		if (Input::has('number_of_user_to')) {
			$number_of_user_to = intval(Input::get('number_of_user_to'));
			$groups->has('users', '<=', $number_of_user_to);
		}
		//	Filter created_at field from
		if (Input::has('created_at_from')) {
			$created_at_from = date('Y-m-d H:i:s', strtotime( Input::get('created_at_from') ) );
			$groups->where('created_at', '>=', $created_at_from);
		}
		//	Filter created_at field to, + 1 days to 00::00::00 -> 23:59:59
		if (Input::has('created_at_to')) {
			$created_at_to = date('Y-m-d H:i:s', strtotime("+1 day", strtotime(Input::get('created_at_to')))  );
			$groups->where('created_at', '<=', $created_at_to);
		}
		//	Order By 
		//	Name and Create
		if (Input::get('order')[0]['column'] !== '2') {
			$indexSort = Input::get('order')[0]['column'];
			$typeOrder = Group::$headerSort[$indexSort];
			if ($typeOrder !== '' && $typeOrder !== 'number_user') {
				$sort = Input::get('order')[0]['dir'];
				$groups->orderBy($typeOrder,$sort);
			}
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $groups->count();

		$groups->skip($start)->take($iDisplayLength);
  		$groups = $groups->get();
	  	//	Order By Number of user
	  	if (Input::get('order')[0]['column'] == '2') {
	  		if (Input::get('order')[0]['dir'] == 'desc') {
	  			$groups = $groups->sortByDesc(function($q) {
				    return $q->users->count();
				});
	  		}else{
	  			$groups = $groups->sortBy(function($q) {
				    return $q->users->count();
				});
	  		}
		}
	  	foreach ($groups as $key => $group) {
	  		$results["data"][] = array(
	  			$group->id, 
	  			$group->name,
	  			$group->users()->count(),
	  			$group->created_at->diffForHumans(),
	  			'<a href="'.route("update/group", $group->id).'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a><a href="' . route('delete/group', $group->id) . '" class="btn default btn-xs red confirmBefore"><i class="fa fa-trash-o"></i> '.Lang::get('button.delete').'</a>'
	  		);
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	
	public function delete($id = null)
	{
		// Get group information
		$group = Sentry::getGroupProvider()->findById($id);

		// Delete the group
		$group->delete();
		// Prepare the success message
		$success = Lang::get('admin/groups/message.success.delete');
		return Response::json(['status' => 'OK', 'message' => $success]);
	}
}
