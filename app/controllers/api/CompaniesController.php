<?php namespace Controllers\Api;

use AdminController;
use Lang;
use Company;
use Input;
use Response;
use Session;

class CompaniesController extends AdminController {
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
	  	// init  companies variable
		$companies = Company::whereRaw('1=1');
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $companies->count();
		}

		// Do we want to include the deleted companies?
		if (Input::get('show_value')=='withTrashed')
		{
			$companies = $companies->withTrashed();
		}
		else if (Input::get('show_value')=='onlyTrashed')
		{
			$companies = $companies->onlyTrashed();
		}

		//	using idd in template to avoid duplicating with id default 
		if (Input::has('idd')) {
		    $companies->where('id', urldecode(Input::get('idd')));
		}

		if (Input::has('namee')) {
		    $companies->where('name', 'like', '%' . urldecode(Input::get('namee')) . '%');
		}

		if (Input::has('descript')) {
		    $companies->where('description', 'like', '%' . urldecode(Input::get('descript')) . '%');
		}

		if (Input::has('state')) {
		    $companies->where('active', urldecode(Input::get('state')));
		}
		//	Order By
		if (Input::has('order')) {
			$indexSort = Input::get('order')[0]['column'];
			$typeOrder = Company::$headerSort[$indexSort];
			if ($typeOrder !== '') {
				$sort = Input::get('order')[0]['dir'];
				$companies->orderBy($typeOrder,$sort);
			}
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $companies->count();
		$companies->skip($start)->take($iDisplayLength);

		$companies = $companies->get();

	  	foreach ($companies as $key => $company) {
	  		if (is_null($company->deleted_at)) {
	  			$actionLinks = '<a href="'.route("update/company", $company->id).'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a>
			  			<a href="' . route('deleteAjax/company', $company->id) . '" class="btn default btn-xs red confirmBefore"><i class="fa fa-trash-o"></i> '.Lang::get('button.delete').'</a>';
	  		} else {
	  			$actionLinks = '<a href="' . route('restoreAjax/company', $company->id) . '" class="btn btn-mini btn-warning restore">'.Lang::get('button.restore').'</a>';
	  		}

	  		if ($company->isActive()) {
	  			$state = '<span class="label label-sm label-success">'.Lang::get('general.active').'</span>';
	  		} else {
	  			$state = '<span class="label label-sm label-danger">'.Lang::get('general.inactive').'</span>';
	  		}

	  		$results["data"][] = array(
	  			$company->id, 
	  			$company->name,
	  			$company->description,
	  			$state,
	  			$actionLinks
	  		);
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	
	public function delete($id = null)
	{
		// Get company information
		$company = Company::find($id);

		// Delete the company
		$company->delete();
		// Setting sessions
		if (Session::has('number'))
		{
		    $number = Session::get('number') + 1;
		}else $number = 1;

		Session::put('number', $number);
		
		if (Session::has('alertNotification')) {
			$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Company #'.$id.' deleted. </span></a></li>';
		} else {
			$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Company #'.$id.' deleted. </span></a></li>';
		}
		
		Session::put('alertNotification', $li);
		$number = Session::get('number');
		$alertNotification = Session::get('alertNotification');

		// Prepare the success message
		$success = Lang::get('admin/companies/message.success.delete');
		return Response::json(['status' => 'OK', 'message' => $success, 'number' => $number, 'alertNotification' => $alertNotification]);
	}
	public function restore($id = null)
	{
		// Get company information
		$company = Company::withTrashed()->find($id);

		// Restore the company
		$company->restore();
		// Prepare the success message
		$success = Lang::get('admin/companies/message.success.restored');
		return Response::json(['status' => 'OK', 'message' => $success]);
	}
}
