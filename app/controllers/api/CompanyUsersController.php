<?php namespace Controllers\Api;

use AdminController;
use Lang;
use User;
use Sentry;
use Input;
use Response;
use Session;

class CompanyUsersController extends AdminController {
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
		// Grab all the users
		$users = Sentry::getUserProvider()->createModel();
		// Filter user by companies_id
		$companies_id = Sentry::getUser()->companies_id;
		$users = $users->where('companies_id', $companies_id);
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $users->count();
		}

		// Filter user by roles
		$users = $users->where('roles','!=', \User::USER_ADMIN);
		if (Input::has('group')) {
			$users = $users->where('roles', Input::get('group'));
		}

		// Do we want to include the deleted companies?
		if (Input::get('show_value')=='withTrashed')
		{
			$users = $users->withTrashed();
		}
		else if (Input::get('show_value')=='onlyTrashed')
		{
			$users = $users->onlyTrashed();
		}

		if (Input::has('id')) {
		    $users->where('id', 'like', '%' . urldecode(Input::get('id')) . '%');
		}

		if (Input::has('first_name')) {
		    $users->where('first_name', 'like', '%' . urldecode(Input::get('first_name')) . '%');
		}

		if (Input::has('last_name')) {
		    $users->where('last_name', 'like', '%' . urldecode(Input::get('last_name')) . '%');
		}

		if (Input::has('email')) {
		    $users->where('email', 'like', '%' . urldecode(Input::get('email')) . '%');
		}

		/*if (Input::has('superAdmin')) {
			if (Input::get('superAdmin')==1) {
		    	$users->where('permissions', 'like', '%'.'"superuser":1'.'%');
			} else {
		    	$users->where('permissions', 'not like', '%'.'"superuser":1'.'%');
			}
		}*/

		if (Input::has('company')) {
		    $users->where('companies_id', 'like', '%' . urldecode(Input::get('company')) . '%');
		}

		if (Input::has('state')) {
		    $users->where('activated', urldecode(Input::get('state')));
		}
		//	Filter created_at field from
		if (Input::has('created_at_from')) {
			$created_at_from = date('Y-m-d H:i:s', strtotime( Input::get('created_at_from') ) );
			$users->where('created_at', '>=', $created_at_from);
		}
		//	Filter created_at field to, + 1 days to 00::00::00 -> 23:59:59
		if (Input::has('created_at_to')) {
			$created_at_to = date('Y-m-d H:i:s', strtotime("+1 day", strtotime(Input::get('created_at_to')))  );
			$users->where('created_at', '<=', $created_at_to);
		}
		//	Order By
		if (Input::has('order')) {
			$indexSort = Input::get('order')[0]['column'];
			$typeOrder = User::$headerSort[$indexSort];
			if ($typeOrder !== '') {
				$sort = Input::get('order')[0]['dir'];
				$users->orderBy($typeOrder,$sort);
			}
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $users->count();
		$users->skip($start)->take($iDisplayLength);

		$users = $users->get();

	  	foreach ($users as $key => $user) {
	  		if ($user->isActivated()) {
	  			$state = '<span class="label label-sm label-success">'.Lang::get('general.active').'</span>';
	  		} else {
	  			$state = '<span class="label label-sm label-danger">'.Lang::get('general.inactive').'</span>';
	  		}
	  		if (is_null($user->deleted_at)) {
	  			$actionLinks = '<a href="'.route("update/companyuser", $user->id).'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a>
			  			<a href="' . route('deleteAjax/companyuser', $user->id) . '" class="btn default btn-xs red confirmBefore"><i class="fa fa-trash-o"></i> '.Lang::get('button.delete').'</a>';
	  		} else {
	  			$actionLinks = '<a href="' . route('restoreAjax/companyuser', $user->id) . '" class="btn btn-mini btn-warning restore">'.Lang::get('button.restore').'</a>';
	  		}
	  		if ($user->roles == 2) {
	  			$group = 'Company';
	  		} else {
	  			$group = 'Operator';
	  		}
	  		if (is_null($user->company)) {
	  			$company_name = 'Unassigned';
	  		} else {
	  			$company_name = $user->company->name;
	  		}
			 $results["data"][] = array(
	  			$user->id,
	  			$user->first_name,
	  			$user->last_name,
	  			$user->email,
	  			$state,
	  			$group,//must be replace by group name laters
	  			$company_name,
	  			$user->created_at->diffForHumans(),
	  			$actionLinks
	  		);
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	
	public function delete($id = null)
	{
		// Get user information
		$user = Sentry::getUserProvider()->findById($id);
		// Check if we are not trying to delete ourselves
		if ($user->id === Sentry::getId())
		{
			return Response::json(['status' => 'error', 'message' => "Can't delete yourself"]);
		}

		// Do we have permission to delete this user?
		if ($user->isSuperUser() and ! Sentry::getUser()->isSuperUser())
		{
			return Response::json(['status' => 'error', 'message' => "Insufficient permissions!"]);
		}

		// Delete the user
		$user->delete();

		// Setting sessions
		if (Session::has('number'))
		{
		    $number = Session::get('number') + 1;
		}else $number = 1;
		Session::put('number', $number);
		if (Session::has('alertNotification')) {
			$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>User #'.$id.' deleted. </span></a></li>';
		} else {
			$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>User #'.$id.' deleted. </span></a></li>';
		}
		
		Session::put('alertNotification', $li);
		$number = Session::get('number');
		$alertNotification = Session::get('alertNotification');
		// Prepare the success message
		$success = Lang::get('admin/users/message.success.delete');
		return Response::json(['status' => 'OK', 'message' => $success, 'number' => $number, 'alertNotification' => $alertNotification]);
	}
	public function restore($id = null)
	{
		// Get user information
		$user = Sentry::getUserProvider()->createModel()->withTrashed()->find($id);

		// Restore the user
		$user->restore();
		$group = Sentry::findGroupById($user->roles);

		$user->addGroup($group);
		// Prepare the success message
		$success = Lang::get('admin/users/message.success.restored');
		return Response::json(['status' => 'OK', 'message' => $success]);
	}
}
