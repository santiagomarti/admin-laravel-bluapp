<?php namespace Controllers\Api;

use AdminController;
use Lang;
use Ibeacon;
use Input;
use Response;

class CompanyIbeaconsController extends AdminController {
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
	  	// init  ibeacons variable
		$companies_id = \Sentry::getUser()->companies_id;
		/*$ibeacons = Ibeacon::whereHas('user',  function($q) use($companies_id) {
		    $q->where('companies_id', $companies_id);
		});*/
		$ibeacons = Ibeacon::where('companies_id', $companies_id);
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $ibeacons->count();
		}

		if (Input::has('zones_value')) {
		    $ibeacons->where('zones_id', urldecode(Input::get('zones_value')));
		}

		//	using idd in template to avoid duplicating with id default 
		if (Input::has('idd')) {
		    $ibeacons->where('id', urldecode(Input::get('idd')));
		}

		if (Input::has('namee')) {
		    $ibeacons->where('name', 'like', '%' . urldecode(Input::get('namee')) . '%');
		}

		if (Input::has('descript')) {
		    $ibeacons->where('description', 'like', '%' . urldecode(Input::get('descript')) . '%');
		}

		if (Input::has('peripheralIDD')) {
		    $ibeacons->where('peripheralID', 'like', '%' . urldecode(Input::get('peripheralIDD')) . '%');
		}

		if (Input::has('proximityUUIDD')) {
		    $ibeacons->where('proximityUUID', 'like', '%' . urldecode(Input::get('proximityUUIDD')) . '%');
		}

		if (Input::has('majorr')) {
		    $ibeacons->where('major', 'like', '%' . urldecode(Input::get('majorr')) . '%');
		}

		if (Input::has('minorr')) {
		    $ibeacons->where('minor', 'like', '%' . urldecode(Input::get('minorr')) . '%');
		}

		if (Input::has('proximityy')) {
		    $ibeacons->where('proximity', 'like', '%' . urldecode(Input::get('proximityy')) . '%');
		}

		if (Input::has('enterr')) {
		    $ibeacons->where('enter', 'like', '%' . urldecode(Input::get('enterr')) . '%');
		}

		if (Input::has('exitt')) {
		    $ibeacons->where('exit', 'like', '%' . urldecode(Input::get('exitt')) . '%');
		}

		if (Input::has('state')) {
		    $ibeacons->where('active', urldecode(Input::get('state')));
		}
		//	Order By
		if (Input::has('order')) {
			$get_sort_follow = Input::get('order')[0]['column'];
			$sort_follow = $this->getColumnName($get_sort_follow);
			$sort = Input::get('order')[0]['dir'];
			$ibeacons = $ibeacons->orderBy($sort_follow,$sort);
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $ibeacons->count();
		$ibeacons->skip($start)->take($iDisplayLength);

		$ibeacons = $ibeacons->get();

	  	foreach ($ibeacons as $key => $ibeacon) {
			if ($ibeacon->isActive()) {
				$state = '<span class="label label-sm label-success">'.Lang::get('general.active').'</span>';
			}else $state = '<span class="label label-sm label-danger">'.Lang::get('general.inactive').'</span>';
			
			if ($ibeacon->isEnter()) {
				$enter = Lang::get('general.yes');
			}else $enter = Lang::get('general.no');
			
			if ($ibeacon->isExit()) {
				$exit = Lang::get('general.yes');
			}else $exit = Lang::get('general.no');

	  		$results["data"][] = array(
	  			$ibeacon->name,
	  			$ibeacon->peripheralID,
	  			$ibeacon->proximity,
	  			$enter,
	  			$exit,
	  			$state,
	  			'<a href="'.route("update/companyibeacon", $ibeacon->id).'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a>'
	  		);
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	public function getColumnName($value = null)
	{
		if ($value=='0') {
			return $value = 'name';
		}elseif ($value=='1') {
			return $value = 'peripheralID';
		}elseif ($value=='2') {
			return $value = 'proximity';
		}elseif ($value=='3') {
			return $value = 'enter';
		}elseif ($value=='4') {
			return $value = 'exit';
		}elseif ($value=='5') {
			return $value = 'active';
		}else return $value = 'name';
	}
}
