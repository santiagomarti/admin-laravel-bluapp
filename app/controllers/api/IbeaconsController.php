<?php namespace Controllers\Api;

use AdminController;
use Lang;
use Ibeacon;
use Input;
use Response;
use Session;

class IbeaconsController extends AdminController {
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
	  	// init  ibeacons variable
		$ibeacons = Ibeacon::whereRaw('1=1');
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $ibeacons->count();
		}

		// Do we want to include the deleted companies?
		if (Input::get('show_value')=='withTrashed')
		{
			$ibeacons = $ibeacons->withTrashed();
		}
		else if (Input::get('show_value')=='onlyTrashed')
		{
			$ibeacons = $ibeacons->onlyTrashed();
		}

		if (Input::has('zones_value')) {
		    $ibeacons->where('zones_id', urldecode(Input::get('zones_value')));
		}

		if (Input::has('name')) {
		    $ibeacons->where('name', 'like', '%' . urldecode(Input::get('name')) . '%');
		}

		if (Input::has('peripheralID')) {
		    $ibeacons->where('peripheralID', 'like', '%' . urldecode(Input::get('peripheralID')) . '%');
		}

		if (Input::has('proximity')) {
		    $ibeacons->where('proximity', 'like', '%' . urldecode(Input::get('proximity')) . '%');
		}

		if (Input::has('enter')) {
		    $ibeacons->where('enter', 'like', '%' . urldecode(Input::get('enter')) . '%');
		}

		if (Input::has('exit')) {
		    $ibeacons->where('exit', 'like', '%' . urldecode(Input::get('exit')) . '%');
		}

		if (Input::has('state')) {
		    $ibeacons->where('active', urldecode(Input::get('state')));
		}
		//	Order By
		if (Input::has('order')) {
			$indexSort = Input::get('order')[0]['column'];
			$typeOrder = Ibeacon::$headerSort[$indexSort];
			if ($typeOrder !== '') {
				$sort = Input::get('order')[0]['dir'];
				$ibeacons->orderBy($typeOrder,$sort);
			}
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $ibeacons->count();
		$ibeacons->skip($start)->take($iDisplayLength);

		$ibeacons = $ibeacons->get();

	  	foreach ($ibeacons as $key => $ibeacon) {
	  		if ($ibeacon->isActive()) {
	  			$state = '<span class="label label-sm label-success">'.Lang::get('general.active').'</span>';
	  		} else {
	  			$state = '<span class="label label-sm label-danger">'.Lang::get('general.inactive').'</span>';
	  		}
	  		if (is_null($ibeacon->deleted_at)) {
	  			$actionLinks = '<a href="'.route("update/ibeacon", $ibeacon->id).'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a>
			  			<a href="' . route('deleteAjax/ibeacon', $ibeacon->id) . '" class="btn default btn-xs red confirmBefore"><i class="fa fa-trash-o"></i> '.Lang::get('button.delete').'</a>';
	  		} else {
	  			$actionLinks = '<a href="' . route('restoreAjax/ibeacon', $ibeacon->id) . '" class="btn btn-mini btn-warning restore">'.Lang::get('button.restore').'</a>';
	  		}
	  		if ($ibeacon->isEnter()) {
	  			$enter = Lang::get('general.yes');
	  		} else {
	  			$enter = Lang::get('general.no');
	  		}
	  		if ($ibeacon->isExit()) {
	  			$exit = Lang::get('general.yes');
	  		} else {
	  			$exit = Lang::get('general.no');
	  		}
	  		
			 $results["data"][] = array(
	  			$ibeacon->name,
	  			$ibeacon->peripheralID,
	  			$ibeacon->proximity,
	  			$enter,
	  			$exit,
	  			$state,
	  			$actionLinks
	  		);
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	
	public function delete($id = null)
	{
		// Get ibeacon information
		$ibeacon = Ibeacon::find($id);

		// Delete the ibeacon
		$ibeacon->delete();

		// Setting sessions
		if (Session::has('number'))
		{
		    $number = Session::get('number') + 1;
		}else $number = 1;
		Session::put('number', $number);
		if (Session::has('alertNotification')) {
			$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Ibeacon #'.$id.' deleted. </span></a></li>';
		} else {
			$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Ibeacon #'.$id.' deleted. </span></a></li>';
		}
		
		Session::put('alertNotification', $li);
		$number = Session::get('number');
		$alertNotification = Session::get('alertNotification');
		// Prepare the success message
		$success = Lang::get('admin/ibeacons/message.success.delete');
		return Response::json(['status' => 'OK', 'message' => $success, 'number' => $number, 'alertNotification' => $alertNotification]);
	}
	public function restore($id = null)
	{
		// Get ibeacon information
		$ibeacon = Ibeacon::withTrashed()->find($id);

		// Restore the ibeacon
		$ibeacon->restore();
		// Prepare the success message
		$success = Lang::get('admin/ibeacons/message.success.restored');
		return Response::json(['status' => 'OK', 'message' => $success]);
	}
}
