<?php namespace Controllers\Api;

use AdminController;
use Lang;
use Zone;
use Campaign;
use Input;
use Response;
use Session;
use Sentry;

class ZonesController extends AdminController {
	/**
	*	Get group info through ajax
	*	@return text/html type
	*/
	public function ajax() {
		//	How many record to take
	  	$iDisplayLength = Input::get('length') ?  Input::get('length') : 10;
	  	//	Skip how many
	  	$start = intval($_REQUEST['start']);
	  	// How many request ajax after the first reload page
	  	$sEcho = intval($_REQUEST['draw']);
		  
	  	$results = array();
	  	$results["data"] = array(); 
	  	// init  zones variable
		$zones = Zone::where('companies_id', Sentry::getUser()->company->id);
		if ($iDisplayLength=='-1') {
			$iDisplayLength = $zones->count();
		}

		// Do we want to include the deleted companies?
		if (Input::get('show_value')=='withTrashed')
		{
			$zones = $zones->withTrashed();
		}
		else if (Input::get('show_value')=='onlyTrashed')
		{
			$zones = $zones->onlyTrashed();
		}

		//	using idd in template to avoid duplicating with id default 
		if (Input::has('idd')) {
		    $zones->where('id', urldecode(Input::get('idd')));
		}

		if (Input::has('namee')) {
		    $zones->where('name', 'like', '%' . urldecode(Input::get('namee')) . '%');
		}

		if (Input::has('descript')) {
		    $zones->where('description', 'like', '%' . urldecode(Input::get('descript')) . '%');
		}

		if (Input::has('state')) {
		    $zones->where('active', urldecode(Input::get('state')));
		}
		//	Order By
		if (Input::has('order')) {
			$indexSort = Input::get('order')[0]['column'];
			$typeOrder = Zone::$headerSort[$indexSort];
			if ($typeOrder !== '') {
				$sort = Input::get('order')[0]['dir'];
				$zones->orderBy($typeOrder,$sort);
			}
		}
		// Get total record here before skip some record for pagination
		$iTotalRecords = $zones->count();
		$zones->skip($start)->take($iDisplayLength);

		$zones = $zones->get();
	  	foreach ($zones as $key => $zone) {
			if ($zone->name=='Unassigned') {
				$disable = ' disabled';
			} else {
				$disable = '';
			}
			
  			if( is_null($zone->deleted_at)){
  				$action_links = '<a href="'.route("update/zone", $zone->id).'" class="btn default btn-xs purple'.$disable.'"'.'><i class="fa fa-edit"></i> '.Lang::get('button.edit').'</a>
			  			<a href="' . route('deleteAjax/zone', $zone->id) . '" class="btn default btn-xs red confirmBefore'.$disable.'"'.'><i class="fa fa-trash-o"></i> '.Lang::get('button.delete').'</a>';
  			}else $action_links = '<a href="' . route('restoreAjax/zone', $zone->id) . '" class="btn btn-mini btn-warning restore">'.Lang::get('button.restore').'</a>';

  			if ($zone->isActive()) {
  				$state = '<span class="label label-sm label-success">'.Lang::get('general.active').'</span>';
  			} else {
  				$state = '<span class="label label-sm label-danger">'.Lang::get('general.inactive').'</span>';
  			}
		
	  		$results["data"][] = array(
	  			$zone->id, 
	  			$zone->name,
	  			$zone->description,
	  			$state,
	  			$action_links
	  		);
	  		
	  	}

	  	$results["draw"] = $sEcho;
	  	$results["recordsTotal"] = $iTotalRecords;
	  	$results["recordsFiltered"] = $iTotalRecords;
		return Response::json($results);

	  	//echo json_encode($results);
	}
	
	public function delete($id = null)
	{
		// Get local information
		$local = Zone::find($id);

		$campaigns = Campaign::whereHas('ibeacon',  function($q) use($id) {
		    $q->where('zones_id', $id);
		});
		if (empty($campaign)) {

			// Delete the local
			$local->delete();

			// Setting sessions
			if (Session::has('number'))
			{
			    $number = Session::get('number') + 1;
			}else $number = 1;
			Session::put('number', $number);
			if (Session::has('alertNotification')) {
				$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Zone #'.$id.' deleted. </span></a></li>';
			} else {
				$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-times"></i></span>Zone #'.$id.' deleted. </span></a></li>';
			}
			
			Session::put('alertNotification', $li);
			$number = Session::get('number');
			$alertNotification = Session::get('alertNotification');

			// Prepare the success message
			$success = Lang::get('admin/locals/message.success.delete');
			return Response::json(['status' => 'OK', 'message' => $success, 'number' => $number, 'alertNotification' => $alertNotification]);
		}else {
			return Response::json(['status' => 'error', 'message' => "Can't Delete a Zone assigned to a Campaign."]);
		}
	}
	public function restore($id = null)
	{
		// Get zone information
		$zone = Zone::withTrashed()->find($id);

		// Restore the zone
		$zone->restore();
		// Prepare the success message
		$success = Lang::get('admin/locals/message.success.restored');
		return Response::json(['status' => 'OK', 'message' => $success]);
	}
}
