<?php

class AdsController extends AdminController {


	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'type'		=> 'required',
		'ibeacon'   => 'not_in:0',
        'type'      => 'required',
        'promote'   => 'required',
        'ttl_enter' => 'required|numeric',
        'ttl_exit'  => 'required|numeric'


	);

	/**
	 * Show a list of all the ads.
	 *
	 * @return View
	 */
	public function getIndex()
	{

		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$locals = User::find(Sentry::getUser()->id)->getLocals();
		}

		$selectedLocal = Input::get('selectedLocal', null);


		if (is_null($selectedLocal) || empty($selectedLocal) || $selectedLocal == 0)
		{
			$ads = Ad::all();

			//Filters all ads that belongs to the locals the user belongs too.
			$ads = $ads->filter(function($ad) use ($locals)
			{
				if (!is_null($ad->ibeacon))
				{
					//gets the ads local_id
					$adLocalId = $ad->ibeacon->local_id;

					foreach ($locals as $local)
					{
						if ($adLocalId == $local->id)
						{
							//if ads.local_id is thi this local. we add it to the ads.
							return true;
						}
					}
				}
				return false;
			});

		}
		else
		{
			$ads = Local::find($selectedLocal)->ads;
		}

		// Do we want to include the deleted ads?
		if (Input::get('withTrashed'))
		{
			$ads = Ad::withTrashed()->get();
		}
		else if (Input::get('onlyTrashed'))
		{
			$ads = Ad::onlyTrashed()->get();
		}

		// Show the page
		return View::make('backend/ads/index', compact('ads', 'locals', 'selectedLocal'));
	}

	/**
	 * User create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		$locals = User::find(Sentry::getUser()->id)->getLocals();
		$selectedLocal = Input::get('selectedLocal', null);

		if ($locals->count() == 0) {
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors(array('Operator has no locals'));
		}

		if (is_null($selectedLocal) || empty($selectedLocal) || $selectedLocal == 0)
		{
			$local = $locals->first();
			$selectedLocal = $locals->first()->id;
		}
		else
		{
			$local = Local::find($selectedLocal);
		}

		$ibeacons = [];

		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$ibeacons[] = $local->ibeacons->toArray();
		}

		if (count($ibeacons)>0) {
			$ibeacons = $ibeacons[0];
		}

		$placeHolderCover = '../pacman.png';

		//$types = ['image','html','video','sound'];
		$types = ['image','html-url','html-embeded','video'];

		return View::make('backend/ads/create', compact('placeHolderCover', 'types', 'ibeacons','proximites', 'locals', 'selectedLocal'));
	}

	/**
	 * User create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{

		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		//If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token','_token', 'type');

			// Was the ad created?
			if ($ad = Ad::create($inputs))
			{

				$typeDigest = Input::get('type', $ad->type);

				if(starts_with($typeDigest, 'html'))
				{
					$typeDigest = 'html';
				}
    	        $ad->type           = 	$typeDigest;

				$ad->ibeacon_id = Input::get('ibeacon');
				$ad->save();

				//after saving the company, store the image,
				//replacing cover input data.

				Request::merge(
					array('ad_id' => $ad->id,
						  'cover' => Input::get('cover64'),
						  'covertype' => Input::get('covertype64')
				));


				if (Input::get('cover64', '') !== '')
				{
					$this->postEditCover();
				}

				if (Input::get('covertype64', '') !== '')
				{
					$this->postEditCoverType();
				}

				// Prepare the success message
				$success = Lang::get('admin/ads/message.success.create');

				// Redirect to the new ad page
				//return Redirect::route('update/ad', $ad->id)->with('success', $success);
				return Redirect::route('ads')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/ads/message.error.create');

			// Redirect to the ad creation page
			return Redirect::route('create/ad')->with('error', $error);
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/ads/message.error.create');
		}

		// Redirect to the ad creation page
		return Redirect::route('create/ad')->withInput()->with('error', $error);
	}

	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		$ad = Ad::find($id);

		if (empty($ad->cover))
		{
			$ad->cover = '../pacman.png';
		}

		$types = ['image','html-url','html-embeded','video'];

		$realtype = $ad->type;
		if ($ad->type === 'html' && strlen($ad->body) === 0)
		{
			$realtype = 'html-url';
		}
		if ($ad->type === 'html' && strlen($ad->body) > 0)
		{
			$realtype = 'html-embeded';
		}

		// Show the page
		return View::make('backend/ads/edit', compact('ad','types', 'realtype'));
	}

	/**
	 * User update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		try
		{
			// Get the ad information
			$ad = Ad::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/ads/message.ad_not_found', compact('id'));

			// Redirect to the ad management page
			return Redirect::route('ads')->with('error', $error);
		}

		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{

			$typeDigest = Input::get('type', $ad->type);

			if ($typeDigest === 'html-url' || $typeDigest === 'video')
            {
				$ad->url = Input::get('url', $ad->url);
            }
            if ($typeDigest === 'html-embeded')
            {
				$ad->url = '';
            }

			if(starts_with($typeDigest, 'html'))
			{
				$typeDigest = 'html';
			}

			// Update the ad
			$ad->bookshelf      = 	Input::get('bookshelf', $ad->title);
            $ad->title          = 	Input::get('title', $ad->title);
            $ad->description    = 	Input::get('description', $ad->description);
            $ad->active         = 	Input::get('activated', $ad->active);
            $ad->type           = 	$typeDigest;
            $ad->body           = 	Input::get('body', $ad->body);

            $ad->ibeacon_id     = 	Input::get('ibeacon', $ad->ibeacon_id);
            $ad->promote        = 	Input::get('promote', $ad->promote);
            $ad->msg_enter      = 	Input::get('msg_enter', $ad->msg_enter);
            $ad->msg_exit       = 	Input::get('msg_exit', $ad->msg_exit);
            $ad->ttl_enter      = 	Input::get('ttl_enter', $ad->ttl_enter);
            $ad->ttl_exit       = 	Input::get('ttl_exit', $ad->ttl_exit);

            Log::info('the ad to save:' . $ad->toJson());
			//TODO save other fields here

			// Was the ad updated?
			if ($ad->save())
			{
				//Log::info('polygon save');
				// Prepare the success message
				$success = Lang::get('admin/ads/message.success.update');

				// Redirect to the ad page
				//return Redirect::route('update/ad', $id)->with('success', $success);
				return Redirect::route('ads')->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/ads/message.error.update');
		}

		// Redirect to the ad creation page
		return Redirect::route('update/ad', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given ad.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get ad information
			$ad = Ad::find($id);

			// Delete the ad
			$ad->delete();

			// Prepare the success message
			$success = Lang::get('admin/ads/message.success.delete');

			// Redirect to the ad management page
			return Redirect::route('ads')->with('success', $success);
		}
		catch (Exceptions $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/ads/message.ad_not_found', compact('id' ));

			// Redirect to the ad management page
			return Redirect::route('ads')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted ad.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	/**
	 * Restore a deleted ad.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get ad information
			$ad = Ad::withTrashed()->find($id);

			// Restore the ad
			$ad->restore();

			// Prepare the success message
			$success = Lang::get('admin/ads/message.success.restored');

			// Redirect to the ad management page
			return Redirect::route('ads')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/ads/message.ad_not_found', compact('id'));

			// Redirect to the ad management page
			return Redirect::route('ads')->with('error', $error);
		}
	}



	/**
	 * Ad update cover
	 *
	 * @return Edit
	 */
	public function postEditCoverType()
	{
		try
		{
			$mimeType = HelperInputFile::getMimeType('covertype');
			$base64Data = HelperInputFile::getDataBase64('covertype');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'type_image_' . Input::get('ad_id','noad') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/ads/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						$ad_id = Input::get('ad_id');

						// // Get the user information
						$ad = Ad::find($ad_id);
						$ad->url = 'ads/' . $finalName;
						$ad->save();
						Log::info('===> ads url is:' . $ad->url);

					}
					catch (UserNotFoundException $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing ad'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/ads/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}



	/**
	 * Ad update cover
	 *
	 * @return Edit Cover Type Image
	 */
	public function postEditCover()
	{
		try
		{
			$mimeType = HelperInputFile::getMimeType('cover');
			$base64Data = HelperInputFile::getDataBase64('cover');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'cover_' . Input::get('ad_id','noad') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/ads/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						$ad_id = Input::get('ad_id');

						// // Get the user information
						$ad = Ad::find($ad_id);
						$ad->cover = 'ads/' . $finalName;
						$ad->save();

						Log::info('$destinationPath' .  $destinationPath);
					}
					catch (UserNotFoundException $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing ad'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/ads/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}


}
