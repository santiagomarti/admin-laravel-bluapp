	<?php

class ServicesController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default API Controller
	|--------------------------------------------------------------------------
	|
	| Manages the api version 1.1, login, users
	|
	*/


	/**
	 * Declare the rules for the form validation user
	 *
	 * @var array
	 */
	protected $validationRulesUser = array(
		// 'first_name'       => 'required|min:3',
		// 'last_name'        => 'required|min:3',
        	'email'                 => 'required|email|unique:users,email',
        	'password'              => 'required|between:3,32',
        	'password_confirm'      => 'required|between:3,32|same:password',
	);


	/**************************************************************************************************************************
	* 											AUTH METHODS
	*************************************************************************************************************************/



	/**
	 * Forgot password Sends Email
	 *
	 * @return Json
	 */
	public function postForgotPassword()
	{
		// Declare the rules for the validator
		$rules = array(
			'email' => 'required|email',
		);

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Response::json(array(
                'success'   => false,
				'code' 		=> -1,
				'error'		=>	$validator->messages()->all()
				), 200);
		}

		try
		{
			// Get the user password recovery code
			$user = Sentry::getUserProvider()->findByLogin(Input::get('email'));

			// Data to be used on the email view
			$data = array(
				'user'              => $user,
				'forgotPasswordUrl' => URL::route('forgot-password-confirm', $user->getResetPasswordCode()),
			);

			// Send the activation code through email
			Mail::send('emails.forgot-password', $data, function($m) use ($user)
			{
				$m->to($user->email, $user->first_name . ' ' . $user->last_name);
				$m->subject('Account Password Recovery');
			});
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			// Even though the email was not found, we will pretend
			// we have sent the password reset code through email,
			// this is a security measure against hackers.
		}

		// OK
			return Response::json(array(
					'success' => true,
					'msg' => Lang::get('auth/message.forgot-password.success'),
					'code' => 1), 200);
	}


	/**
	 * Gets user info via token
	 *
	 * @return Json
	 */
	public function postUserInfoReset()
	{
		try
		{
			$passwordResetCode = Input::get('token','');
			// Find the user using the password reset code
			$user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);

			// OK
			return Response::json(array(
					'success' => true,
					'msg' => $user->email,
					'code' => 1), 200);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			// Even though the user was not found, we will pretend
			// successfully event.
			// this is a security measure against hackers.
		}

		// OK
		return Response::json(array(
				'success' => true,
				'msg' => '',
				'code' => 1), 200);
	}



	/**
	 * Forgot Password Confirmation, this reset account with a new password
	 *
	 * @param  string  $passwordResetCode
	 * @return Redirect
	 */
	public function postForgotPasswordConfirm($passwordResetCode = null)
	{
		// Declare the rules for the form validation
		$rules = array(
			'password'         => 'required|between:3,32',
			'password_confirm' => 'required|between:3,32|same:password',
		);

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Response::json(array(
				'success' 	=> false,
				'code' 		=> -1,
				'error'		=>	$validator->messages()->all()
				), 200);
		}

		try
		{
			$passwordResetCode = Input::get('passwordResetCode','');

			// Find the user using the password reset code
			$user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);

			// Attempt to reset the user password
			if ($user->attemptResetPassword($passwordResetCode, Input::get('password')))
			{
				// Password successfully reseted
				return Response::json(array(
				'success' 	=> true,
				'code' 		=> 1,
				'msg'		=> Lang::get('auth/message.forgot-password-confirm.success')
				), 200);
			}
			else
			{
			// Ooops.. something went wrong
			return Response::json(array(
				'success' 	=> false,
				'code' 		=> -1,
				'error'		=>	array(Lang::get('auth/message.forgot-password-confirm.error'))
				), 200);
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			// Ooops.. something went wrong
			return Response::json(array(
				'success' 	=> false,
				'code' 		=> -2,
				'error'		=>	array(Lang::get('auth/message.account_not_found'))
				), 200);
		}
	}



	/**
	 * Account sign up form processing.
	 *
	 * @return JSON
	 */
	public function postSignup()
	{
		// Declare the rules for the form validation
		$rules = array(
			'email'            => 'required|email|unique:users',
			'password'         => 'required|between:3,32'
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Response::json(array(
				'success' 	=> false,
				'code' 		=> -1,
				'error'		=>	$validator->messages()->all()
				), 200);
		}

		try
		{
			// Register the user
			$user = Sentry::register(array(
				'first_name' => '',
				'last_name'  => '',
				'email'      => Input::get('email'),
				'password'   => Input::get('password'),
			));

			// Data to be used on the email view
			$data = array(
				'user'          => $user,
				'activationUrl' => URL::route('activate', $user->getActivationCode()),
			);

			// Send the activation code through email
			Mail::send('emails.register-activate', $data, function($m) use ($user)
			{
				$m->to($user->email, $user->first_name . ' ' . $user->last_name);
				$m->subject('Welcome ' . $user->first_name);
			});

			// OK
			return Response::json(array(
					'success' => true,
					'msg' => Lang::get('auth/message.signup.success'),
					'code' => 1), 200);


		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			return Response::json(array(
				'success' 	=> false,
				'code' 		=> -2,
				'error'		=>	array(Lang::get('auth/message.account_already_exists'))
				), 200);
		}

		// Ooops.. something went wrong
		return Response::json(array(
				'success' 	=> false,
				'code' 		=> -3,
				'error'		=>	array('Ooops.. something went wrong')
				), 200);
	}

	/**
	 * User account activation.
	 *
	 * @param  string  $actvationCode
	 * @return Json
	 */
	public function getActivate($activationCode = null)
	{
		try
		{
			// Get the user we are trying to activate
			$user = Sentry::getUserProvider()->findByActivationCode($activationCode);

			// Try to activate this user account
			if ($user->attemptActivation($activationCode))
			{
				// Redirect to the login page
				//return Redirect::route('signin')->with('success', Lang::get('auth/message.activate.success'));
					return Response::json(array(
						'success' => true,
						'email' => $user->email,
						'msg' => Lang::get('auth/message.activate.success') . ' Welcome:' . $user->email . '!',
						'code' => 1), 200);
			}

			// The activation failed.
			$error = Lang::get('auth/message.activate.error') . ' User ' . $user->email;
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$error = Lang::get('auth/message.activate.error');
		}
		catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
		{
			$error = Lang::get('auth/message.activate.activated');
		}

	   // Ooops.. something went wrong
	  return Response::json(array(
      'success' => false,
      'code' => -1,
      'error' => array($error)), 200);
	}

	/**
	* This method will ping the server, must be accesstoken avaible to see success true.
	*
	* @return Json
	*/
	public function getPing()
	{
		 return Response::json(array('success' => true), 200);
	}


	/**
	* Return the csrf-token use for laravel, this is used for the post request and
	* must be send as a parameter with the name _token
	*
	* @return Json
	*/
	public function getCsrfToken()
	{
		 return Response::json(array('success' => true, 'csrf-token' => Session::token()), 200);
	}


	/**
	* This method signs the user.
	* In case the user has 3 sessions it will reomve the oldest the access token and insert the new one.
	*
	* @return Json
	*/
	public function postSignin()
	{
    $error = '';

		try
		{

     	 // Try to log the user in
			$user = Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));


			//Creates the access token
			$publicAccesToken = md5(mt_rand());
			$accessToken = $this->getPublicAccessToken($publicAccesToken);

			//Removes duplicated devices, in other words, one row for one input
	     	$affectedRows = Access::where('device_id', '=', Input::get('device_id','N/A'))->delete();
	      	//Log::info('===> Deleted Repeteade Devices Affected Rows:' . $affectedRows);


     		//This will create the access.
			$access = Access::create(array('success' => true,
											'user_id' => $user->getId(),
											'public_token' => $publicAccesToken,
											'device_id' => Input::get('device_id','N/A'),
											'access_token' => $accessToken
											));

			//Log::info('===> Access Created:' . $access);


			//Once the user is logged in, we must now remove the old extra access
			$accessOld = Access::where('user_id', '=', $access->user_id)->orderBy('created_at')->get();

			$totalDevicesPerUser = Config::get('settings.totalDevicesPerUser');

			$removeItems = $accessOld->count() - $totalDevicesPerUser;

			if ($accessOld->count() > $totalDevicesPerUser) {
					//Log::info('===> Too many devices for user:' . $user->email . ' ' .
					//$accessOld->count() . '/' . $totalDevicesPerUser . 'removing now..');

					for ($i=0; $i < $removeItems; $i++)
					{
						//Log::info('===> Removing Access For User:' . $user->email . ' deleted:' . $accessOld->get($i));
						$accessOld->get($i)->delete();
					}

			}

			// OK, login
			return Response::json(array('success' => true,
										'user' => array('id'     => $user->getId(),
			  				  				  			'first_name'   => $user->first_name,
			  				  				  			'last_name'   => $user->last_name,
			  				  				  			'activated'   => $user->activated,
			  				  				  			'email'   => $user->email,
			  				  				  			'public_token' => $publicAccesToken,
			  				  				  			'access_token' => $accessToken,
			  				  				  			'phone'   => $user->phone,
			  				  				  			'social_number'   => $user->social_number,
			  				  				  			'gravatar'   => $user->gravatar(),
			  				  				  			'cover'   => $user->cover,
			  				  				  			)
								), 202);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$error = Lang::get('auth/message.account_not_found');

		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			$error = Lang::get('auth/message.account_not_activated');
		}
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
			$error = Lang::get('auth/message.account_suspended');
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
			$error =Lang::get('auth/message.account_banned');
		}
	    catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
	    {
	      $error = Lang::get('admin/users/message.user_password_required');
	    }
	    catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
	    {
	      $error = Lang::get('admin/users/message.user_login_required');
	    }

		// Ooops.. something went wrong
		return Response::json(array(
	      'success' => false,
	      'error' => array($error)), 200);
	}


	/**
	* This method will send an a public access token, and will store in a db table.
	* The final access-token is created as
	* FinalToken = md5(pulbic access token + private access token).
	* this method is called in the user login method
	*
	* @param  string  publicAccesToken
	* @return Json
	*/
	private function getPublicAccessToken($publicAccesToken)
	{
		$privateAccessToken = Config::get('settings.privateAccessToken');

		$access_token = md5($publicAccesToken.$privateAccessToken);

		return $access_token;
	}



	/***************************************************************************************************************************
	* 											USERS METHODS
	*************************************************************************************************************************/


	/**
	 * User Create , used on addind the contact
	 *
	 * @return json
	 */
	public function postCreateUser()
	{
		$user = null;
		$group = null;

		$user_from = null;

		try
		{
		    $user_from = Sentry::find(Input::get('user_id'));
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		     // send not foung json
			return Response::json(array('success' => false, 'error' => array('Missing user_id in the parameters')), 200);
		}

		try
		{
		    $group = Sentry::findGroupByName('Client');
		    $user = Sentry::findUserByLogin(Input::get('email'));
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    // send not foung json
			return Response::json(array('success' => false, 'error' => array('Administrator Error: The Group Client Must Be Found')), 200);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    //Its Ok if it doesnt exist
		    $user = null;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//User existed Creating Contact, And Request Pending
		//if user_id is 0, then dont add contact
		if (!is_null($user) && Input::get('user_id') != 0)
		{
				///Was the conctact created?
				if ( $contact = Contact::create(array('status' => 'pending')) ) {

				$contact->contact_id = $user->id;
					$contact->user_id = Input::get('user_id');
					$contact->alias = '';
					$contact->save();

					// Prepare the success message, for user added
					$success = 'Contact Added';

					// send success json
					return Response::json(array(
                        'success'   => true,
                        'msg'       => $success,
                        'code'      => 1,
                        'warning'   => 'user existed adding to contact list'), 200);

				} else {
					// send success json
					return Response::json(array('success' => true, 'msg' => $success, 'warning' => 'crap, contact not added for new user'), 200);
				}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Create User, Then Add Contact

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRulesUser);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
				return Response::json(array('success' => false,
					'error'		=>	$validator->messages()->all()
				), 200);
		}

		try
		{

			// Was the user created?
			if ($user = Sentry::create(array(
										        'email'       => Input::get('email'),
										        'password'    => Input::get('password'),
										        'first_name'  => Input::get('first_name'),
										        'last_name'   => Input::get('last_name'),
										        'permissions' => array('superuser' => -1)
			)))
			{
				//User Added, SetUp guest, And Group to CLient

				$user->addGroup($group);
				$user->guest = true;
				$user->activated = true;
				$user->activated_at = $user->created_at;
				$user->reset_password_code = $this->generateRandomString(20);
				$user->activation_code = NULL;
				$user->save();

				// Data to be used on the email view
				$data = array(
					'user'              => $user,
					'user_from'         => $user_from,
					'forgotPasswordUrl' => URL::route('forgot-password-confirm', $user->getResetPasswordCode()),
				);

				$emailSender = View::make('emails.register-forgot-password', $data);

				if (Input::get('user_id') != 0) {
						//Was the conctact created?
					if ( $contact = Contact::create(array('status' => 'pending')) ) {

						$contact->contact_id = $user->id;
						$contact->user_id = Input::get('user_id');
						$contact->alias = '';
						$contact->save();

						// Prepare the success message, for user added
						$success = 'user created and contacts added to user';

						// send success json
						return Response::json(array(
							'success' => true,
							'code' => 2,
							'user_from' => $user_from->toArray(),
							'user_to' => $user->toArray(),
							'email' => base64_encode($emailSender),
							'msg' => $success), 200);

					} else {
						// send success json
						return Response::json(array('success' => true,
							'msg' => $success,
							'warning' => 'crap, contact not added for new user'), 200);
					}
				}

				// Prepare the success message, for user added
				$success = 'user created contacts were not added';

				// send success json
				return Response::json(array('success' => true,
								 	  		'code' => 3,
								 	  		'msg' => $success), 200);

			}

			// Prepare the error message
			$error = Lang::get('admin/users/message.error.create');

			// couldnot crate user
			return Response::json(array('success' => false, 'error' => array($error)), 200);
		}
		catch (LoginRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_login_required');
		}
		catch (PasswordRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_password_required');
		}
		catch (UserExistsException $e)
		{
			$error = Lang::get('admin/users/message.user_exists');
		}

		$error = 'thing that makes you go hmmmm';

		// Ooops.. something went wrong
		return Response::json(array('success' => false, 'error' => array($error)), 200);
	}


	protected function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
    	return $randomString;
	}
	/**
	 * User update first_name, last_name, phone, email
	 *
	 * @return json
	 */
	public function postEditUser()
	{
		$user_id  = Input::get('user_id');

		try
		{
			// Get the user information
			$user = Sentry::getUserProvider()->findById($user_id);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));

			// Redirect to the user management page
			return Response::json(array('success' => false,
		 	'error' => $error), 200);
		}


		$user->first_name = Input::get('first_name');
		$user->last_name    = Input::get('last_name');
		$user->phone    = Input::get('phone');
		$user->email    = Input::get('email');

		if ($user->save())
		{
			// Prepare the success message
			$success = Lang::get('admin/events/message.success.update');


			// Redirect to the user management page
			return Response::json(array('success' => true,
										'user' => $user->toArray(),
		 								'msg' => $success
		 								), 200);
		}

		// something wrong
		$error = Lang::get('admin/events/message.error.update');

		return Response::json(array('success' => false,
		'error' => $error), 200);
	}


	/**
	 * User update pushd_code
	 *
	 * @return json
	 */
	public function postUpdateUserPushdCode()
	{
		$user_id  = Input::get('user_id');

		try
		{
			// Get the user information
			$user = Sentry::getUserProvider()->findById($user_id);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));

			// Redirect to the user management page
			return Response::json(array('success' => false,
		 	'error' => $error), 200);
		}


		$user->pushd_code = Input::get('pushd_code');

		if ($user->save())
		{
			// Prepare the success message
			$success = Lang::get('admin/events/message.success.update');


			// Redirect to the user management page
			return Response::json(array('success' => true,
										'user' => $user->toArray(),
		 								'msg' => $success
		 								), 200);
		}

		// something wrong
		$error = Lang::get('admin/events/message.error.update');

		return Response::json(array('success' => false,
		'error' => $error), 200);
	}


	/**
	 * User change password
	 *
	 * @return json
	 */
	protected function postChangePassword()
	{
		// Declare the rules for the form validation
		$rules = array(
			'old_password'     => 'required|between:3,32',
			'password'         => 'required|between:3,32',
			'password_confirm' => 'required|same:password',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Response::json(array(
				'success'	=>	false,
				'msg'		=>	'ChangePassword´s Validator rules failed',
				'error'		=>	$validator->messages()->all()
				), 200);
		}

		// Grab the user
		$user = Sentry::getUser();

		// Check the user current password
		if ( ! $user->checkPassword(Input::get('old_password')))
		{
			// Set the error message
			$this->messageBag->add('old_password', 'Your current password is incorrect.');

			//Password error
			return Response::json(array(
				'success'	=>	false,
				'msg'		=>	'ChangePassword´s Auth Failed',
				'error'		=>	$this->messageBag
				), 200);
		}

		// Update the user password
		$user->password = Input::get('password');
		$user->save();

		// OK
		return Response::json(array(
				'success'	=>	true,
				'msg'		=>	'ChangePassword´s OK',
				), 200);
	}


	//Test
	public function testPost()
	{
		//send response
		return Response::json(array(
		        'success'  => true,
		        'msg'  => 'all ok',
		        'code'     => 1,
		        ), 200);
	}

	public function getAdByBeacon($proximityUUID, $major, $minor)
	{
		//$beaconId
		try
		{
			$ibeacon = Ibeacon::where('proximityUUID', '=', $proximityUUID)->
								where('major', '=', $major)->
								where('minor', '=', $minor)->
								where('active', '=', TRUE)->first();

			if (is_null($ibeacon))
			{
				throw new ModelNotFoundException();
			}

			//make them appear, lazy loading
			$ibeacon->local;
			$ibeacon->local->company;
			$ibeacon->ads;

			return Response::json(array(
		        'success'  => true,
		        'beacon'  => $ibeacon->toArray(),
		        'code'     => 1,
		    ), 200);


		}
		catch (ModelNotFoundException $e)
		{
			return Response::json(array(
		        'success'  => false,
		        'error' 	=> array('not found'),
		        'code'     => -1,
		        ), 200);
		}

		//send response
		return Response::json(array(
		        'success'  => true,
		        'msg'  => 'all ok',
		        'code'     => 1,
		        ), 200);
	}

	public function getPromotedAdsByBeacon($proximityUUID, $major, $minor)
	{
		//$beaconId
		try
		{
			$ibeacon = Ibeacon::where('proximityUUID', '=', $proximityUUID)->
								where('major', '=', $major)->
								where('minor', '=', $minor)->
								where('active', '=', TRUE)->first();

			if (is_null($ibeacon))
			{
				throw new ModelNotFoundException();
			}

			//make them appear, lazy loading
			$ads = Ad::where('ibeacon_id', '=', $ibeacon->id)->
					   where('promote', '=', TRUE)->
					   where('active', '=', TRUE)->get();


			return Response::json(array(
		        'success'  => true,
		        'ads'  => $ads->toArray(),
		        'code'     => 1,
		    ), 200);


		}
		catch (ModelNotFoundException $e)
		{
			return Response::json(array(
		        'success'  => false,
		        'error' 	=> array('not found'),
		        'code'     => -1,
		        ), 200);
		}

		//send response
		return Response::json(array(
		        'success'  => true,
		        'msg'  => 'all ok',
		        'code'     => 1,
		        ), 200);
	}

	//curl -X GET http://localhost:8888/index.php/apiv1.1/beacons/promoted-ads
	//This method will return all active beacons with companies, locals, ads active.
	public function getPromotedAds()
	{
		try
		{

			$ibeacons = Ibeacon::whereRaw('id in (
			select distinct(ibeacon_id) from ads where active = 1 and ibeacon_id in (
				select id from ibeacons where active = 1 and local_id in (
					select id from locals where active = 1 and company_id in (
						select id from companies where active = 1))))', array())->get();

			//Log::info('Query:' . json_encode(DB::getQueryLog()));

			$companiesArray = [];
			$ibeaconsArray = [];

			$companiesArrayAds = [];

			$position = 0;
			foreach ($ibeacons as $ibeacon)
		    {
		        $ibeacon->ads;
		        $ibeacon->local->company;

		        if(!isset($companiesArrayAds[$ibeacon->local->company->id]))
		        {
		        	$companiesArrayAds[$ibeacon->local->company->id] = [];
		        }

		        $ibeaconsAds = $ibeacon->ads->toArray();
		        $newiBeaconsAds = [];
				foreach ($ibeaconsAds as $subArray)
		    	{
		    		if ($subArray['active'] != "0"){
		    			$newiBeaconsAds[] = $subArray;
		    		}
		    	}


		        $companiesArrayAds[$ibeacon->local->company->id][] = $newiBeaconsAds;

		        $beaconArray = $ibeacon->toArray();
		        $beaconArray['timeIn']  = '0000-00-00 00:00:00';
		        $beaconArray['timeOut'] = '0000-00-00 00:00:00';
		        $beaconArray['display']  = FALSE;
		        $beaconArray['identifier']  =  $beaconArray['proximityUUID'] . '|' . $beaconArray['major'] . '|' . $beaconArray['minor'] . '|BEACON';
		        $beaconArray['position']  =  $position;
		        $beaconArray['ads'] = $newiBeaconsAds;
		        $position = $position + 1;

		        $ibeaconsArray[] = $beaconArray;
		        $companiesArray[] = $ibeacon->local->company->toArray();

		    }


			$companiesArray = $this->cleanDuplicates($companiesArray);

			$companiesArray = $this->mergeAdsIntoCompanyArray($companiesArrayAds,$companiesArray);

			$date = date_create();
			$timestamp =  date_format($date, 'Y-m-d H:i:s');

			// return Response::json(array(
   //              'success'   => true,
   //              'beaconsIDs' => Config::get('settings.beaconsIDs'),
   //              'beacons'  => $ibeaconsArray,
   //              'companies' => $companiesArray,
   //              'time'		=> $timestamp,
   //              'code'      => 1,
		 //    ), 200);

		   return Response::json( array(
                'success'   => true,
                'beaconsIDs' => Config::get('settings.beaconsIDs'),
                'beacons'  => $ibeaconsArray,
                'companies' => $companiesArray,
                'time'		=> $timestamp,
                'code'      => 1,
		    ), 200, [], JSON_NUMERIC_CHECK );

		}
		catch (ModelNotFoundException $e)
		{
			return Response::json(array(
		        'success'  => false,
		        'error' 	=> array('not found'),
		        'code'     => -1,
		        ), 200);
		}

		//send response
		return Response::json(array(
		        'success'  => false,
		        'msg'  	   => array('not found'),
		        'code'     => -2,
		        ), 200);
	}

	//Dont show more companies
	private function cleanDuplicates($companiesArray)
	{
		//Remove
	    $final  = [];
		foreach ($companiesArray as $current) {
		    if ( ! in_array($current, $final)) {
		        $final[] = $current;
		    }
		}
		return $final;
	}

	private function removeInactive($array){
		return $array;
	}

	//first we merge the multiple arrays from the ads, then send add it to the company
	private function mergeAdsIntoCompanyArray($companiesArrayAds, $companiesArray)
	{
		$newCompaniesArray = [];

		// Log::info('companiesArrayAds :' . json_encode($companiesArrayAds, JSON_PRETTY_PRINT));
		// Log::info('companiesArray :' . json_encode($companiesArray, JSON_PRETTY_PRINT));

		foreach ($companiesArrayAds as $key => $arrayTmp)
	    {
	    		// Log::info('key :' . $key);
	    		// Log::info('arrayTmp :' . json_encode($arrayTmp, JSON_PRETTY_PRINT));

	    		//Merges all companies ads into one array
	    		$mergeArray = [];
		    	foreach ($arrayTmp as $subAdsArray)
		    	{
		    		//Log::info('subAdsArray :' .  json_encode($subAdsArray, JSON_PRETTY_PRINT) );
		    		$mergeArray = array_merge($mergeArray, $subAdsArray);
		    	}

		    	//Sets display false the mergeArray
		    	$newMergeArray = [];
				foreach ($mergeArray as $subArray)
		    	{
	    			$subArray['display'] = false;
	    			//Log::info('subArray :' . json_encode($subArray, JSON_PRETTY_PRINT) );
	    			$newMergeArray[] = $subArray;
		    	}

		    	// Log::info('mergeArray :' . json_encode($mergeArray, JSON_PRETTY_PRINT) );
		    	// Log::info('newMergeArray :' . json_encode($newMergeArray, JSON_PRETTY_PRINT) );

		    	//Insert ads into the companies, and set display to false
		    	foreach ($companiesArray as $keyCompaniesArray => $arrayTmp)
	    		{
	    			$arrayTmp['display'] = false;
	    			$arrayTmp['ads'] = $newMergeArray;
	    			$newCompaniesArray[] = $arrayTmp;
	    		}
	    }

	    //Log::info('companiesArray Final :' . json_encode($newCompaniesArray, JSON_PRETTY_PRINT));

		return $newCompaniesArray;
	}

	//http://blue.local:8888/index.php/apiv1.1/ad/3/show
	public function getAd($id = 0)
	{
		$ad = Ad::find($id);

		// Show the page
		return View::make('showAd', compact('ad'));
	}

}
