<?php

class DocumentController {

    
    public static function document($name)
    {
        $rc = new \ReflectionClass($name);
        echo '<pre>'; 
        $getMethods = $rc->getMethods();
        foreach($getMethods as $method){
            if ($method->name == 'document') {
                continue;
            }
            echo '<h1 style="color:red"> '.$method->name.'</h1><br/>';
            echo  $rc->getMethod($method->name)->getDocComment();
            echo '<br>';
        }
    }
}
