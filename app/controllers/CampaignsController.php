<?php

class CampaignsController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'type'		=> 'required',
		'ibeacon'   => 'not_in:0',
        'title'     => 'required',
        'ttl_enter' => 'required|numeric',
        'ttl_exit'  => 'required|numeric',
        'url'		=> 'url',
        'video'		=> 'url' 
	);

	/**
	 * Show a list of all the campaigns.
	 *
	 * @return View
	 */
	public function getIndex()
	{

		// Get the user information
		$profile = Sentry::getUser();
		
		$companies_id = \Sentry::getUser()->companies_id;
		$campaigns = Campaign::whereHas('ibeacon',  function($q) use($companies_id) {
		    $q->where('companies_id', $companies_id);
		});

		$ibeacons = Ibeacon::whereRaw('1=1');
		// operator users can manage campaigns belong to their beacon
		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$ibeacon = Ibeacon::where('users_id', Sentry::getUser()->id)->lists('id');
			$campaigns = Campaign::whereIn('ibeacons_id', $ibeacon);
		}

		// Do you want to include the deleted campaigns?
		if (Input::get('withTrashed'))
		{
			$campaigns = $campaigns->withTrashed();
		}
		else if (Input::get('onlyTrashed'))
		{
			$campaigns = $campaigns->onlyTrashed();
		}
		
		$campaigns = $campaigns->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));
		//create
		//A Campaign belongs to a Beacon, that belongs to a User, that belongs to a Company.
		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$users = User::where('id', Sentry::getUser()->id)->lists('id');
		}elseif ((Sentry::getUser()->hasAccess('subadmin'))) {
			$company = Sentry::getUser()->companies_id;
			$users = User::where('companies_id', $company)->lists('id');
		}
		$ibeacons = Ibeacon::whereIn('users_id', $users)->get();

		$placeHolderCover = '../pacman.png';

		//$types = ['image','html','video','sound'];
		$types = Campaign::$types;
		//end create

		// Show the page
		return View::make('backend/campaigns/index', compact('profile', 'campaigns','placeHolderCover', 'types', 'ibeacons','proximites', 'users', 'ibeacons'));
	}

	/**
	 * User create.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Get the user information
		$profile = Sentry::getUser();

		//A Campaign belongs to a Beacon, that belongs to a User, that belongs to a Company.
		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$users = User::where('id', Sentry::getUser()->id)->lists('id');
		}elseif ((Sentry::getUser()->hasAccess('subadmin'))) {
			$company = Sentry::getUser()->companies_id;
			$users = User::where('companies_id', $company)->lists('id');
		}
		$ibeacons = Ibeacon::whereIn('users_id', $users)->get();

		$placeHolderCover = '../pacman.png';

		//$types = ['image','html','video','sound'];
		$types = Campaign::$types;

		return View::make('backend/campaigns/create', compact('profile', 'placeHolderCover', 'types', 'ibeacons','proximites'));
	}

	/**
	 * User create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{	
		if (Input::has('beaconid')) {
			$data = Input::get('beaconid');
			$proximity = Ibeacon::where('id',$data)->lists('proximity')[0];
			Session::put('proximity', $proximity);
		    return Response::json($proximity); 
		}
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		//If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			Session::flash('active', Input::get('active') ? true : false);
			Session::flash('promote', Input::get('promote') ? true : false);
			Session::flash('ibeacons_id', Input::get('ibeacons_id') ? Input::get('ibeacons_id') : '');
			Session::flash('begin_date', Input::get('begin_date') ? Input::get('begin_date') : '');
			Session::flash('end_date', Input::get('end_date') ? Input::get('end_date') : '');
			// Ooops.. something went wrong
			return Redirect::route('create/campaign')->withInput()->withErrors($validator);
		}

		try
		{
			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token','_token', 'cover64', 'covertype64', '_wysihtml5_mode');

			$filename = '';
		    $filename2 = '';

			if (Input::hasFile('image-input') && Input::get('type') == 'image') {
		    	$file2          = Input::file('image-input');
		        $destinationPath2 = public_path().'/contentimages/';
		        $filename2        = str_random(6) . '_' . $file2->getClientOriginalName();
		        if ($this->checkImageType($file2)){
		        	$uploadSuccess2   = $file2->move($destinationPath2, $filename2);
		        } else {
					return Redirect::route('create/campaign')->withInput()->withErrors('Invalid file type uploaded');
				}
		    } 

		    if (Input::hasFile('image-input') == false && Input::get('type') == 'image'){
		    	return Redirect::route('create/campaign')->withInput()->withErrors('You need to upload a picture.');
		    }

			if (Input::hasFile('cover-img')) {
		        $file            = Input::file('cover-img');
		        $destinationPath = public_path().'/covers/';
		        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
		        if ($this->checkImageType($file)){
		        	$uploadSuccess   = $file->move($destinationPath, $filename);
		        } else {
					return Redirect::route('create/campaign')->withInput()->withErrors('Invalid file type uploaded');
				}
		    }

		    if(Input::get('type') == 'image'){
		    	$inputs['url'] = $filename2;
		    } elseif (Input::get('type') == 'video') {
		    	$inputs['url'] = Input::get('video');
		    } else {
		    	$inputs['url'] = Input::get('url');
		    }
			/*
		    if(Input::get('type') == 'video' || Input::get('type') == 'html-url')){
		    	if(Input::has('url') == false){
		    		return Redirect::route('create/campaign')->withInput()->withErrors('You need to specify an URL');
		    	}
		    }*/

			$inputs['ibeacons_companies_id'] = Sentry::getUser()->companies_id;
			$inputs['cover'] = $filename;
			$inputs['covertype'] = 'deprecated';
			$inputs['begin_date'] = DateTime::createFromFormat('d F Y - H:i', $inputs['begin_date']);
			$inputs['end_date'] = DateTime::createFromFormat('d F Y - H:i', $inputs['end_date']);
			if($inputs['end_date'] < $inputs['begin_date']){
				return Redirect::route('create/campaign')->withInput()->withErrors('Invalid date range selected.');
			}
			/*
            if (Input::has('active')) {
            	$inputs['active']     = 1;
            } else {
            	$inputs['active']     = 0;
            }*/

            if (strtotime($inputs['end_date']->format('Y-m-d H:i:s')) > strtotime(date('Y-m-d H:i:s')) && strtotime($inputs['begin_date']->format('Y-m-d H:i:s')) < strtotime(date('Y-m-d H:i:s'))) {
            	$inputs['active']     = 1;
            } else {
            	$inputs['active']     = 0;
            }
            if (Input::has('promote')) {
            	$inputs['promote']     = 1;
            } else {
            	$inputs['promote']     = 0;
            }
			// Was the ad created?
			if ($ad = Campaign::create($inputs))
			{
				$ad->save();
				Session::forget('proximity');
				// Setting sessions
				if (Session::has('number'))
				{
				    $number = Session::get('number') + 1;
				}else $number = 1;

				Session::put('number', $number);
				
				if (Session::has('alertNotification')) {
					$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New campaign created. </span></a></li>';
				} else {
					$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New campaign created. </span></a></li>';
				}
				
				Session::put('alertNotification', $li);
				// Prepare the success message
				$success = Lang::get('admin/campaigns/message.success.create');
				// Redirect to the new ad page;
		        if(Input::get('save_create_submit')) {
					return Redirect::route('create/campaign')->with('success', $success);
	        	}else return Redirect::route('campaigns')->with('success', $success);
			}

		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/campaigns/message.error.create');
			// Redirect to the ad creation page
			return Redirect::route('create/campaign')->withInput()->with('error', $error);
		}

	}
	/**
	*	Process image on drop zone for campaign
	*/
	public function dropzone(){
		if (Session::has('dropzone')) {
			$this->removeDropzone();
		}
		$file = Input::file('file');
		//If validation fails, we'll exit the operation now.
		if ($this->checkImageType($file))
		{
			$dropzone = $this->uploadAvatar($file,'image');
			Session::put('dropzone', $dropzone);
		}
	}
	/**
	*	Process image for campaign
	*	Remove dropzone image
	*/
	public function removeDropzone(){
		if (Session::has('dropzone')) {
			$path = public_path('avatars/').Session::get('dropzone');
			$this->deleteAvatar($path);
			Session::forget('dropzone');
		}
	}
	/**
	*	Upload an image
	*/
	public function uploadAvatar($file,$cover){
	 	//$file  = Input::file('cover');
		$extension = $file->getClientOriginalExtension();
		$name = $file->getClientOriginalName();
		$filename        = $cover.'_'.strtolower(preg_replace("/[^\w]+/", "-",str_replace('.'.$extension,'',$name))). '_'.uniqid().'.'.$extension;
		$destinationPath = public_path('avatars/ads/');
		$cover = $file->move($destinationPath, $filename );
		$cover = 'ads/'.$filename;
		return $cover;
	}   
	/**
	*	Delete an image
	*/
	public function deleteAvatar($paths)
	{
		$paths = is_array($paths) ? $paths : func_get_args();
		$success = true;
		foreach ($paths as $path) { if ( ! @unlink($path)) $success = false; }
		return $success;
	}
	/**
	*	Check Image Type
	*/	  
	public function checkImageType($file)
	{
		$mimeallow = array('image/jpg', 'image/jpeg', 'image/png');
		$mimeupload = $file->getMimeType();
		//If validation fails, we'll exit the operation now.
		if (in_array($mimeupload, $mimeallow))
		{
			return true;
		}else {
			return false;
		}
	}

	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();

		$ad = Campaign::find($id);
		if ($ad->begin_date) {
			$begin_date = DateTime::createFromFormat('Y-m-d H:i:s',$ad->begin_date)->format('d F Y - H:i');
		} else {
			$begin_date = 'Unknow';
		}
		if ($ad->end_date) {
			$end_date = DateTime::createFromFormat('Y-m-d H:i:s',$ad->end_date)->format('d F Y - H:i');
		} else {
			$end_date = 'Unknow';
		}
		//A Campaign belongs to a Beacon, that belongs to a User, that belongs to a Company.
		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$users = User::where('id', Sentry::getUser()->id)->lists('id');
		}elseif ((Sentry::getUser()->hasAccess('subadmin'))) {
			$company = Sentry::getUser()->companies_id;
			$users = User::where('companies_id', $company)->lists('id');
		}
		$beacons = Ibeacon::whereIn('users_id', $users)->get();

		//proximity
		$proximity = Ibeacon::where('id',$ad->ibeacons_id)->lists('proximity')[0];
		if (Request::ajax()) {
			$data = Input::get('beaconid');
			$proximitySelected = Ibeacon::where('id',$data)->lists('proximity')[0];
		    return Response::json($proximitySelected); 
		}
		/*
		if (empty($ad->cover))
		{
			$ad->cover = '../pacman.png';
		}*/

		$types = Campaign::$types;

		
		$url  = $ad->url;
		$cover = $ad->cover;
		$type = $ad->type;

		$realtype = $ad->type;
		if ($ad->type === 'html' && strlen($ad->body) === 0)
		{
			$realtype = 'html-url';
		}
		if ($ad->type === 'html' && strlen($ad->body) > 0)
		{
			$realtype = 'html-embeded';
		}

		// Show the page
		return View::make('backend/campaigns/edit', compact('profile', 'ad', 'beacons', 'types', 'realtype', 'proximity', 'begin_date', 'end_date', 'cover', 'url', 'type'));
	}

	/**
	 * User update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		
		try
		{
			// Get the ad information
			$ad = Campaign::find($id);
		}
		catch (Exeption $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/campaigns/message.ad_not_found', compact('id'));

			// Redirect to the ad management page
			return Redirect::route('campaigns')->with('error', $error);
		}
		//Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		$old_type = $ad->type;
		$old_url = $ad->url;
		$old_cover = $ad->cover;
		$filename = '';
		$filename2 = '';

		try
		{
			// Update the ad
            $ad->title          = 	Input::get('title', $ad->title);
            $ad->type           = 	Input::get('type', $ad->type);

            if (Input::has('promote')) {
            	$ad->promote    = 1;
            } else {
            	$ad->promote    = 0;
            }

            $ad->msg_enter      = 	Input::get('msg_enter', $ad->msg_enter);
            $ad->msg_exit       = 	Input::get('msg_exit', $ad->msg_exit);
            $ad->description    = 	Input::get('description', $ad->description);
			$ad->bookshelf      = 	Input::get('bookshelf', $ad->bookshelf);
            $ad->ttl_enter      = 	Input::get('ttl_enter', $ad->ttl_enter);
            $ad->ttl_exit       = 	Input::get('ttl_exit', $ad->ttl_exit);
            $ad->ibeacons_id    = 	Input::get('ibeacons_id', $ad->ibeacon_id);
            
            if (Input::get('begin_date') !== $ad->begin_date) {
            	$ad->begin_date = DateTime::createFromFormat('d F Y - H:i', Input::get('begin_date'));
            } 
            if (Input::get('end_date') !== $ad->end_date) {
            	$ad->end_date = DateTime::createFromFormat('d F Y - H:i', Input::get('end_date'));
            }

            if($ad->end_date < $ad->begin_date){
            	return Redirect::route('update/ad')->withInput()->withErrors('Invalid date range.');
            }

            if (strtotime($ad->end_date->format('Y-m-d H:i:s')) > strtotime(date('Y-m-d H:i:s')) && strtotime($ad->begin_date->format('Y-m-d H:i:s')) < strtotime(date('Y-m-d H:i:s'))) {
            	$ad->active     = 1;
            } else {
            	$ad->active     = 0;
            }

            if( $old_type != $ad->type) //Cambió el tipo del ad
            {
            	if($old_type == 'image')//El tipo antiguo era una imagen, por lo que hay que eliminar la imagen antigua
            	{
            		if($old_url != ''){
            			unlink(public_path().'/contentimages/'.$old_url);
            		}
            		if($ad->type == 'video'){
            			$ad->url = Input::get('video', $ad->url);
            		} elseif($ad->type == 'html-url'){
            			$ad->url = Input::get('url', $ad->url);
            		}	
            	} elseif($ad->type == 'image'){//El tipo nuevo es imagen, por lo que hay que guardar la nueva imagen
            		if (Input::hasFile('image-input')) {
				    	$file2          = Input::file('image-input');
				        $destinationPath2 = public_path().'/contentimages/';
				        $filename2        = str_random(6) . '_' . $file2->getClientOriginalName();
				        if ($this->checkImageType($file2)){
				        	$uploadSuccess2   = $file2->move($destinationPath2, $filename2);
				        } else {
							return Redirect::route('update/ad')->withInput()->withErrors('Invalid file type uploaded');
						}
						$ad->url = $filename2;
				    } else {
				    	return Redirect::route('update/ad')->withInput()->withErrors('You need to upload a picture.');
				    }            	
				}
            } elseif ($old_type == $ad->type && $ad->type == 'image'){//Se mantuvo el tipo del ad en imagen, hay que revisar si cambió el contenido
            	if (Input::hasFile('image-input')) //Se cargó una nueva imagen, por lo que hay que guardarla y eliminar la anterior
        		{
        			unlink(public_path().'/contentimages/'.$old_url);
			    	$file3          = Input::file('image-input');
			        $destinationPath3 = public_path().'/contentimages/';
			        $filename3        = str_random(6) . '_' . $file3->getClientOriginalName();
			        if ($this->checkImageType($file3)){
			        	$uploadSuccess3   = $file3->move($destinationPath3, $filename3);
			        } else {
						return Redirect::route('update/ad')->withInput()->withErrors('Invalid file type uploaded');
					}
					$ad->url = $filename3;
			    }  
            } else { 
            	$ad->url = Input::get('url', $ad->url);
            }
            
            if (Input::hasFile('cover-img')) { //Si tiene cover el formulario, eliminar el antiguo y poner el nuevo
		        if($old_cover != '') {
		        	unlink(public_path().'/covers/'.$old_cover);
		        }
		        $file            = Input::file('cover-img');
		        $destinationPath = public_path().'/covers/';
		        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
		        if ($this->checkImageType($file)){
		        	$uploadSuccess   = $file->move($destinationPath, $filename);
		        } else {
					return Redirect::route('update/ad')->withInput()->withErrors('Invalid file type uploaded');
				}
				$ad->cover = $filename;
		    }

            Log::info('the ad to save:' . $ad->toJson());
			// Was the ad updated?
			if ($ad->save())
			{
				// Prepare the success message
				$success = Lang::get('admin/campaigns/message.success.update');

				// Redirect to the ad page
				return Redirect::route('campaigns')->with('success', $success);
			}
		}
		catch (Exeption $e)
		{
			$error = Lang::get('admin/campaigns/message.error.update');
		}

		// Redirect to the ad creation page
		return Redirect::route('update/ad', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given ad.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get ad information
			$ad = Campaign::find($id);

			// Delete the ad
			$ad->delete();

			// Prepare the success message
			$success = Lang::get('admin/campaigns/message.success.delete');

			// Redirect to the ad management page
			return Redirect::route('campaigns')->with('success', $success);
		}
		catch (Exceptions $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/campaigns/message.ad_not_found', compact('id' ));

			// Redirect to the ad management page
			return Redirect::route('campaigns')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted ad.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	/**
	 * Restore a deleted ad.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get ad information
			$ad = Campaign::withTrashed()->find($id);

			// Restore the ad
			$ad->restore();

			// Prepare the success message
			$success = Lang::get('admin/campaigns/message.success.restored');

			// Redirect to the ad management page
			return Redirect::route('campaigns')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/campaigns/message.ad_not_found', compact('id'));

			// Redirect to the ad management page
			return Redirect::route('campaigns')->with('error', $error);
		}
	}



	/**
	 * Campaign update cover
	 *
	 * @return Edit
	 */
	public function postEditCoverType()
	{
		try
		{
			$mimeType = HelperInputFile::getMimeType('covertype');
			$base64Data = HelperInputFile::getDataBase64('covertype');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'type_image_' . Input::get('ad_id','noad') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/campaigns/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						$ad_id = Input::get('ad_id');

						// // Get the user information
						$ad = Campaign::find($ad_id);
						$ad->url = 'campaigns/' . $finalName;
						$ad->save();
						Log::info('===> campaigns url is:' . $ad->url);

					}
					catch (UserNotFoundException $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing ad'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/campaigns/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}



	/**
	 * Campaign update cover
	 *
	 * @return Edit Cover Type Image
	 */
	public function postEditCover()
	{
		try
		{
			$mimeType = HelperInputFile::getMimeType('cover');
			$base64Data = HelperInputFile::getDataBase64('cover');

			//base 64 Image data
		    if ($base64Data !== 'none' && !empty($base64Data))
		    {

		        $finalName = 'cover_' . Input::get('ad_id','noad') . '_' . rand()  . "@2x" . $mimeType;
		        $destinationPath = base_path() . '/public/avatars/campaigns/' . $finalName;

		        if (HelperInputFile::writeFile($destinationPath, $base64Data))
		        {
					try
					{
						$ad_id = Input::get('ad_id');

						// // Get the user information
						$ad = Campaign::find($ad_id);
						$ad->cover = 'campaigns/' . $finalName;
						$ad->save();

						Log::info('$destinationPath' .  $destinationPath);
					}
					catch (UserNotFoundException $e)
					{
						return Response::json(array(
						                'success' => false,
						                'code'    => -1,
						                'msg'   => 'missing ad'), 200);
					}

					return Response::json(array(
                                                'success'  => true,
                                                'code'     => 1,
                                                'cover'    => 'avatars/campaigns/' . $finalName,
                                                //'dst'      => $destinationPath,
                                                'basename' => $finalName,
                                                'msg'    => 'cover added'), 200);
		        }
			}
			else
			{
				return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover has no file input'), 200);
			}
		}
		catch (Exceptions $e)
		{
			//Log::info('==> exception' . $e.message);
			return Response::json(array(
                'success' => false,
                'code'    => -1,
                'msg'   => 'cover exception error'), 200);
		}

		// Ooops.. something went wrong
			return Response::json(array(
                'success' => false,
                'code'    => -2,
                'msg'   => 'cover not uploaded'), 200);
	}

	public function document()
	{
		DocumentController::document('CampaignsController');
	}

}
