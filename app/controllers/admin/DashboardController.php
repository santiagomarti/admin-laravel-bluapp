<?php namespace Controllers\Admin;

use AdminController;
use View;
use Sentry;
use Ibeacon;
use Campaign;
use User;
use Carbon\Carbon;
use DB;

class DashboardController extends AdminController {

	/**
	 * Show the administration dashboard page.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Grab the user
		$profile = Sentry::getUser();
		return \View::make('backend/dashboard', compact('profile'));
	}

	/**
	 * Show the administration dashboard page.
	 *
	 * @return View
	 */
	public function report()
	{
		// Grab the user
		$profile = Sentry::getUser();
		// Get infor of Card
		$activeBeaconNumber = Ibeacon::where('active', 1)->where('companies_id', $profile->companies_id)->count();
		$activeCampaignNumber = Campaign::where('active', 1)
						->whereRaw('ibeacons_id in (select id from beacons where companies_id = ?)', array($profile->companies_id))
						->count();
		/*
		$enterPushNumber = Campaign::where('active', 1)->whereHas('trackings', function($q)
		{
		    $q->where('enter', 0)->where('showinfo', 0);

		})->count();*/
		
		$enterPushNumber = Campaign::whereRaw('active = 1 and ibeacons_id in (select id from beacons where `enter` = 1 and companies_id = ?)', array($profile->companies_id))
						->count();
		$exitPushNumber = Campaign::whereRaw('active = 1 and ibeacons_id in (select id from beacons where `exit` = 1 and companies_id = ?)', array($profile->companies_id))
						->count();
		/*
		$exitPushNumber = Campaign::where('active', 1)->whereHas('trackings', function($q)
		{
		    $q->where('enter', 1)->where('showinfo', 0);

		})->count();*/
		$cardInfo = array(
			'activebeacon' => $activeBeaconNumber,
			'activecampaign' => $activeCampaignNumber,
			'enterpush' => $enterPushNumber,
			'exitpush' => $exitPushNumber
		);
		// End Get infor of Card
		// Get list campaign and beacon
		/*$campaignList = Campaign::whereRaw('deleted_at is null and ibeacons_id in  (select id from beacons where companies_id = ?)', 
			array($profile->companies_id))->lists('title', 'id');
		$campaignList = array_merge(['' => 'All Campaigns'], $campaignList);*/

		$companies_id2 = \Sentry::getUser()->companies_id;
		$campaigns2 = Campaign::whereHas('ibeacon',  function($q) use($companies_id2) {
		    $q->where('companies_id','=', $companies_id2);
		});

		// operator users can manage campaigns belong to their beacon
		if ((Sentry::getUser()->hasAccess('operator')))
		{
			$ibeacon2 = Ibeacon::where('users_id', Sentry::getUser()->id)->lists('id');
			$campaigns2 = Campaign::whereIn('ibeacons_id', $ibeacon2);
		}
		$campaignList = $campaigns2->get(array('title', 'id'));
	
		$company = Sentry::getUser()->companies_id;
		$beaconList = Ibeacon::where('companies_id','=', $company)->get(array('name', 'major', 'minor', 'proximityUUID', 'id'));

		$today = Carbon::now();
		$one_week_ago = Carbon::now()->subWeeks(1);
		
		$topFiveBeacons = DB::select('SELECT t.minor, t.major, SUM( t.enter ) AS cont, b.name, b.peripheralID, b.proximity
									FROM  TrackingRange t, beacons b 
									WHERE t.minor = b.minor 
									AND t.major = t.major 
									AND b.companies_id = ? 
									AND t.updated_at < ? 
									AND t.updated_at > ? 
									AND b.deleted_at IS NULL
									GROUP BY t.minor, t.major 
									ORDER BY SUM( t.enter ) DESC  
									LIMIT 5', array(Sentry::getUser()->companies_id, $today, $one_week_ago));

		$topFiveCampaigns = DB::select('SELECT SUM( t.showinfo ) / COUNT( * ) AS cr, c.title, c.type, c.active
										FROM Tracking t, campaigns c
										WHERE t.idcamp = c.id
										AND c.ibeacons_id
										IN (
										SELECT b.id
										FROM beacons b
										WHERE b.companies_id = ?
										)
										AND t.updated_at < ? 
										AND t.updated_at > ? 
										AND c.deleted_at IS NULL
										GROUP BY t.idcamp
										ORDER BY cr DESC 
										LIMIT 5', array(Sentry::getUser()->companies_id, $today, $one_week_ago));
		
		return \View::make('backend/report/index', compact('profile', 'cardInfo', 'campaignList', 'beaconList', 'topFiveBeacons','topFiveCampaigns'));
	}

}
