<?php namespace Controllers\Admin;

use AdminController;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Config;
use Input;
use Lang;
use Redirect;
use Sentry;
use Validator;
use View;
use Log;
use Response;
use DB;
use Session;
use User;


use \Company;

/**
 * Declare the rules for the form validation
 *
 * @var array
 */
class UsersController extends AdminController {

	/**
	 * Declare the rules for the form validation
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'first_name'       => 'required|min:3',
		'last_name'        => 'required|min:3',
		'email'            => 'required|email|unique:users,email',
		'password'         => 'required|between:3,32',
		'password_confirm' => 'required|between:3,32|same:password',
	);

	/**
	 *
	 * Show a list of all the users.
	 *
	 * @return View
	 */

	public function getIndex()
	{
		// Get the user information
		$profile = Sentry::getUser();
		// Grab all the users
		//$users = Sentry::getUserProvider()->createModel();
		//$users = Sentry::getUserProvider()->createModel();
		//$users = \User::whereRaw('1=1')->hasP('');

		$users = Sentry::getUserProvider()->createModel();
		// Filter user by roles
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
	        $users = $users->where('roles', '!=', \User::USER_OPERATOR);
	    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
	    {
	        $users = $users->where('roles', '!=', \User::USER_ADMIN);
	    }

		if (Input::get('withTrashed'))
		{
			$users = $users->withTrashed();
		}
		else if (Input::get('onlyTrashed'))
		{
			$users = $users->where('activated', '!=', 1);
		} else $users = $users->where('activated', '=', 1);
		// Paginate the users
		
		//$users = \Paginator::make($users, count($users), 10);

		$users = $users->paginate(10)
			->appends(array(
				'withTrashed' => Input::get('withTrashed'),
				'onlyTrashed' => Input::get('onlyTrashed'),
			));
		
		//	Get Create
		// Get all the available groups
		$groups = Sentry::getGroupProvider()->createModel();
		// Filter group by id
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
	        $groups = $groups->where('id', '!=', \User::USER_OPERATOR)->get();
	    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
	    {
	        $groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();
	    }


		// Selected groups
		$selectedGroups = Input::old('groups', array());

		// Get all the available permissions
		$permissions = Config::get('permissions');
		// Filter permissions
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
			$permissions = array_except($permissions, array('Operator'));
	    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
	    {
			$permissions = array_except($permissions, array('Admin'));
	    }

		$this->encodeAllPermissions($permissions);

		// Selected permissions
		$selectedPermissions = Input::old('permissions', array('superuser' => -1));
		$this->encodePermissions($selectedPermissions);

		$companies = Company::where('active', Company::ACTIVE)->get();
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
	        $user_company = Company::find($profile->companies_id);
	    }
		//	End Create
		// Show the page
		return View::make('backend/users/index', compact('profile', 'users','companies', 'groups', 'selectedGroups', 'permissions', 'selectedPermissions', 'user_company'));
	}

	/**
	 * Create new a user
	 *
	 * @return View
	 */
	public function getCreate()
	{
		// Get the user information
		$profile = Sentry::getUser();
		// Get all the available groups
		$groups = Sentry::getGroupProvider()->createModel();

		// Filter group by roles
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
	        $groups = $groups->where('id', '!=', \User::USER_OPERATOR)->get();
	    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
	    {
	        $groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();
	    }

		// Selected groups
		$selectedGroups = Input::old('groups', array());

		// Get all the available permissions
		$permissions = Config::get('permissions');
		// Filter permissions
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
			$permissions = array_except($permissions, array('Operator'));
	    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
	    {
			$permissions = array_except($permissions, array('Admin'));
	    }

		$this->encodeAllPermissions($permissions);

		// Selected permissions
		$selectedPermissions = Input::old('permissions', array('superuser' => -1));
		$this->encodePermissions($selectedPermissions);

		$companies = Company::where('active', Company::ACTIVE)->get();
		if (Sentry::getUser()->hasAnyAccess(array('admin')))
	    {
	        $user_company = Company::find(Sentry::getUser()->companies_id);
	    }
		// Show the page
		return View::make('backend/users/create', compact('profile', 'companies', 'groups', 'selectedGroups', 'permissions', 'selectedPermissions' ,'user_company'));
	}

	/**
	 * User create form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		//Checks if the user want to recove the user

		if (Input::get('recover','0') === '1')
		{
			try
			{
				$email = Input::get('email');

				$result = DB::select(DB::raw('select id, deleted_at from users where email=?'), array(Input::get('email')));

				$id = $result[0]->id;

				$user = Sentry::getUserProvider()->createModel()->withTrashed()->find($id);
				$user->restore();

				return Redirect::route('update/user',$id);
			}
			catch (UserNotFoundException $e)
			{
				// Ooops.. something went wrong
				return Redirect::route('create/user')->withInput()->withErrors(array('user not found'));
			}
			catch (Exception $e)
			{
				// Ooops.. something went wrong
				return Redirect::route('create/user')->withInput()->withErrors(array('exeption'));
			}
		}
		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::except('recover'), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			Session::flash('activated', Input::get('activated') ? true : false);
			Session::flash('groups', Input::get('groups') ? Input::get('groups') : '');
			Session::flash('companies_id', Input::get('companies_id') ? Input::get('companies_id') : '');
			// Ooops.. something went wrong
			return Redirect::route('create/user')->withInput()->withErrors($validator);
		}

		try
		{
			// We need to reverse the UI specific logic for our
			// permissions here before we create the user.
			$permissions = Input::get('permissions', array());
			$this->decodePermissions($permissions);
			app('request')->request->set('permissions', $permissions);

			// Get the inputs, with some exceptions
			$inputs = Input::except('csrf_token', 'password_confirm', 'groups', 'companies','recover','hide');
			$inputs['roles'] = Input::get('groups')[0];
			// If select group is admin, companies = blueapp
			if (Input::get('groups')[0]==\User::USER_ADMIN) {
				$inputs['companies_id'] = Sentry::getUser()->companies_id;
			}
			$inputs['gravatar'] = 'users_avatars/default-user-avatar.png';
            if (Input::has('activated')) {
            	$inputs['activated'] = 1;
            } else {
            	$inputs['activated'] = 0;
            }
			// Was the user created?
			if ($user = Sentry::getUserProvider()->create($inputs))
			{
				// Assign the selected groups to this user
				foreach (Input::get('groups', array()) as $groupId)
				{
					$group = Sentry::findGroupById($groupId);

					$user->addGroup($group);
				}
				$companiesToAdd    = Input::get('companies', array());

				// Assign the user to groups
				foreach ($companiesToAdd as $companyId)
				{
					$company = Company::find($companyId);

					$added = $user->addCompany($company);
				}
				// Setting sessions
				if (Session::has('number'))
				{
				    $number = Session::get('number') + 1;
				}else $number = 1;

				Session::put('number', $number);
				
				if (Session::has('alertNotification')) {
					$li = Session::get('alertNotification').'<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New user created. </span></a></li>';
				} else {
					$li = '<li><a href="javascript:;"><span class="details"><span class="label label-sm label-icon label-success"><i class="fa fa-plus"></i></span>New user created. </span></a></li>';
				}
				
				Session::put('alertNotification', $li);

				// Prepare the success message
				$success = Lang::get('admin/users/message.success.create');

				// Redirect to the new user page
				//return Redirect::route('update/user', $user->id)->with('success', $success);
				return Redirect::route('users')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/users/message.error.create');

			// Redirect to the user creation page
			return Redirect::route('create/user')->with('error', $error);
		}
		catch (LoginRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_login_required');
		}
		catch (PasswordRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_password_required');
		}
		catch (UserExistsException $e)
		{
			$error = Lang::get('admin/users/message.user_exists');
		}

		// Redirect to the user creation page
		return Redirect::route('create/user')->withInput()->with('error', $error);
	}

	/**
	 * User update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function getEdit($id = null)
	{
		// Get the user information
		$profile = Sentry::getUser();
		try
		{
			// Get the user information
			$user = Sentry::getUserProvider()->findById($id);
			// Filter user by roles
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		    	if ($user->roles==\User::USER_OPERATOR) {
		    		return Redirect::route('users');
		    		exit();
		    	}
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
		    	if ($user->roles==\User::USER_ADMIN) {
		    		return Redirect::route('users');
		    		exit();
		    	}
		    }

			// Get this user groups
			$userGroups = $user->groups()->lists('name', 'group_id');

			// Get this user permissions
			$userPermissions = array_merge(Input::old('permissions', array('superuser' => -1)), $user->getPermissions());
			$this->encodePermissions($userPermissions);

			// Get all the available groups
			$groups = Sentry::getGroupProvider()->createModel();
			
			// Filter group by id
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		        $groups = $groups->where('id', '!=', \User::USER_OPERATOR)->get();
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
		        $groups = $groups->where('id', '!=', \User::USER_ADMIN)->get();
		    }

			// Selected groups
			$selectedCompanies = Input::old('companies', array());

			//$companies = Company::all();

			$companies = Company::lists('name' ,'id');
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		        $company = Company::find(Sentry::getUser()->companies_id);
		    }
		    $company_id = Company::find($user->companies_id)->id;
			// Get all the available permissions
			$permissions = Config::get('permissions');
			// Filter permissions
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
				$permissions = array_except($permissions, array('Operator'));
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
				$permissions = array_except($permissions, array('Admin'));
		    }
			$this->encodeAllPermissions($permissions);


		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));

			// Redirect to the user management page
			return Redirect::route('users')->with('error', $error);
		}

		// Show the page
		return View::make('backend/users/edit',
			compact(
					'user', 'groups', 'userGroups', 'permissions', 'userPermissions',
					'selectedCompanies', 'companies', 'userCompanies', 'profile' ,'company' ,'company_id'));
	}

	/**
	 * User update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		// We need to reverse the UI specific logic for our
		// permissions here before we update the user.
		$permissions = Input::get('permissions', array());
		$this->decodePermissions($permissions);
		app('request')->request->set('permissions', $permissions);

		try
		{
			// Get the user information
			$user = Sentry::getUserProvider()->findById($id);
			// Filter user by roles
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		    	if ($user->roles==\User::USER_OPERATOR) {
		    		return Redirect::route('users');
		    		exit();
		    	}
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
		    	if ($user->roles==\User::USER_ADMIN) {
		    		return Redirect::route('users');
		    		exit();
		    	}
		    }
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));

			// Redirect to the user management page
			return Redirect::route('users')->with('error', $error);
		}

		//
		$this->validationRules['email'] = "required|email|unique:users,email,{$user->email},email";

		// Do we want to update the user password?
		if (Input::has('password_edit'))
		{
			$this->validationRules['password_edit'] = $this->validationRules['password'];
			$this->validationRules['password_confirm_edit'] = 'required|between:3,32|same:password_edit';
		}
		unset($this->validationRules['password']);
		unset($this->validationRules['password_confirm']);
		
		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		try
		{
			// Update the user
			$user->first_name  = Input::get('first_name');
			$user->last_name   = Input::get('last_name');
			$user->email       = Input::get('email');
    		if (Sentry::getId() !== $user->id) {
	            if (Input::has('activated')) {
	            	$user->activated = 1;
	            } else {
	            	$user->activated = 0;
	            }
    		}
			$user->permissions = Input::get('permissions');
			// If select group is admin, companies = blueapp else companies = option choose
			if (Input::get('groups')[0]=='1') {
				$user->companies_id = '1';
			}else $user->companies_id = Input::get('companies_id');
			$user->roles = Input::get('groups')[0];
			// Do we want to update the user password?
			if (Input::has('password_edit'))
			{
				$user->password = Input::get('password_edit');
			}

			// Get the current user groups
			$userGroups = $user->groups()->lists('group_id', 'group_id');

			// Get the selected groups
			$selectedGroups = Input::get('groups', array());

			// Groups comparison between the groups the user currently
			// have and the groups the user wish to have.
			$groupsToAdd    = array_diff($selectedGroups, $userGroups);
			$groupsToRemove = array_diff($userGroups, $selectedGroups);

			// Assign the user to groups
			foreach ($groupsToAdd as $groupId)
			{
				$group = Sentry::getGroupProvider()->findById($groupId);

				$user->addGroup($group);
			}

			// Remove the user from groups
			foreach ($groupsToRemove as $groupId)
			{
				$group = Sentry::getGroupProvider()->findById($groupId);

				$user->removeGroup($group);
			}

			// Was the user updated?
			if ($user->save())
			{

				// Get the current user companies


				// Prepare the success message
				$success = Lang::get('admin/users/message.success.update');

				// Redirect to the user page
				//return Redirect::route('update/user', $id)->with('success', $success);
				return Redirect::route('users')->with('success', $success);
			}

			// Prepare the error message
			$error = Lang::get('admin/users/message.error.update');
		}
		catch (LoginRequiredException $e)
		{
			$error = Lang::get('admin/users/message.user_login_required');
		}

		// Redirect to the user page
		return Redirect::route('update/user', $id)->withInput()->with('error', $error);
	}

	/**
	 * Delete the given user.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Get user information
			$user = Sentry::getUserProvider()->findById($id);
			// Filter user by roles
			if (Sentry::getUser()->hasAnyAccess(array('admin')))
		    {
		    	if ($user->roles==\User::USER_OPERATOR) {
		    		return Redirect::route('users');
		    		exit();
		    	}
		    }else if (Sentry::getUser()->hasAnyAccess(array('subadmin')))
		    {
		    	if ($user->roles==\User::USER_ADMIN) {
		    		return Redirect::route('users');
		    		exit();
		    	}
		    }
			
			// Check if we are not trying to delete ourselves
			if ($user->id === Sentry::getId())
			{
				// Prepare the error message
				$error = Lang::get('admin/users/message.error.delete');

				// Redirect to the user management page
				return Redirect::route('users')->with('error', $error);
			}

			// Do we have permission to delete this user?
			if ($user->isSuperUser() and ! Sentry::getUser()->isSuperUser())
			{
				// Redirect to the user management page
				return Redirect::route('users')->with('error', 'Insufficient permissions!');
			}

			// Delete the user
			$user->delete();

			// Prepare the success message
			$success = Lang::get('admin/users/message.success.delete');

			// Redirect to the user management page
			return Redirect::route('users')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id' ));

			// Redirect to the user management page
			return Redirect::route('users')->with('error', $error);
		}
	}

	/**
	 * Restore a deleted user.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function getRestore($id = null)
	{
		try
		{
			// Get user information
			$user = Sentry::getUserProvider()->createModel()->withTrashed()->find($id);

			// Restore the user
			$user->restore();

			// Prepare the success message
			$success = Lang::get('admin/users/message.success.restored');

			// Redirect to the user management page
			return Redirect::route('users')->with('success', $success);
		}
		catch (UserNotFoundException $e)
		{
			// Prepare the error message
			$error = Lang::get('admin/users/message.user_not_found', compact('id'));

			// Redirect to the user management page
			return Redirect::route('users')->with('error', $error);
		}
	}



	/**
	 * Ajax, to check email
	 *
	 * @param  email
	 * @return json
	 */
	public function getEmailDuplicate()
	{

		$validationEmailRules = array(
			'email'            => 'required|email'
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $validationEmailRules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			//send response
			return Response::json(array(
	                'success' 	=> 	false,
	                'email'   	=> 	Input::get('email'),
	                'msg'     	=> 	'email not correct format',
	                'code'      => -1,
	                'valid'   	=> 	false,
			        ), 200);
		}

		//check if the user exist via email
	    $result = DB::select(DB::raw('select id, deleted_at from users where email=?'), array(Input::get('email')));

	    //If count is zero there is no email then is success is true, and is valid.
	    if (count($result) == 0)
	    {
	    	//send response
			return Response::json(array(
                'success'   => true,
                'email' 	=> Input::get('email'),
		        'msg'  		=> 'email not found',
		        ), 200);

		} else {
			//Check if the email that exist is activated or not
			$row = $result[0];
			$activated = false;
			if (is_null($row->deleted_at))
			{
				$activated = true;
			}

			//send response
			return Response::json(array(
	                'success' 	=> 	false,
	                'email'   	=> 	Input::get('email'),
	                'msg'     	=> 	'email found, if activated is true, then is active account',
	                'activated'   	=> 	$activated
			        ), 200);
		}
	}

	public function document()
	{
		\DocumentController::document('Controllers\Admin\UsersController');
	}
}
