<?php

return array(

	'edit'    	=> 'Editar',
	'delete'  	=> 'Borrar',
	'assign'    => 'Asignar',
	'delegate'  => 'Delegar',
	'restore' 	=> 'Restaurar',
	'take'		=> 'Tomar',
	'view'		=> 'Ver'

);
