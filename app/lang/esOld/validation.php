<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => ":attribute debe ser aceptado.",
	"active_url"       => ":attribute no es una URL válida.",
	"after"            => ":attribute debe ser una fecha posterior a :date.",
	"alpha"            => ":attribute debe contener solo letras.",
	"alpha_dash"       => ":attribute debe contener solo letras, números, y guiones.",
	"alpha_num"        => ":attribute debe contener solo letras y números.",
	"before"           => ":attribute debe ser una fecha anterior a :date.",
	"between"          => array(
		"numeric" => ":attribute debe ser entre :min - :max.",
		"file"    => ":attribute debe ser entre :min - :max kilobytes.",
		"string"  => ":attribute debe ser entre :min - :max characters.",
	),
	"confirmed"        => "Confirmación de :attribute no coincide.",
	"date"             => ":attribute no es una fecha válida.",
	"date_format"      => ":attribute no coincide con el formato :format.",
	"different"        => ":attribute y :other deben ser diferentes.",
	"digits"           => ":attribute debe ser de :digits digitos.",
	"digits_between"   => ":attribute debe ser entre :min y :max digitos.",
	"email"            => "El formato de :attribute es inválido.",
	"exists"           => ":attribute seleccionado es inválido.",
	"image"            => ":attribute debe ser una imagen.",
	"in"               => "El :attribute seleccionado es inválido.",
	"integer"          => ":attribute debe ser un entero.",
	"ip"               => ":attribute debe ser una dirección IP válida.",
	"max"              => array(
		"numeric" => ":attribute no debe ser mayor que :max.",
		"file"    => ":attribute no debe ser mayor que :max kilobytes.",
		"string"  => ":attribute no debe ser mayor que :max caracteres.",
	),
	"mimes"            => ":attribute debe ser un archivo de tipo: :values.",
	"min"              => array(
		"numeric" => ":attribute debe ser al menos :min.",
		"file"    => ":attribute debe ser al menos :min kilobytes.",
		"string"  => ":attribute debe ser de al menos :min caracteres.",
	),
	"not_in"           => ":attribute seleccionado inválido.",
	"numeric"          => ":attribute debe ser un número.",
	"regex"            => "El formato de :attribute es inválido.",
	"required"         => "El campo :attribute es requerido.",
	"required_if"      => "El campo :attribute es requerido cuando :other tiene el valor :value.",
	"required_with"    => "El campo :attribute es requerido cuando :values está presente.",
	"required_without" => "El campo :attribute es requerido cuando :values no está presente.",
	"same"             => ":attribute y :other deben coincidir.",
	"size"             => array(
		"numeric" => ":attribute debe ser de :size.",
		"file"    => ":attribute debe ser de :size kilobytes.",
		"string"  => ":attribute debe ser de :size caracteres.",
	),
	"unique"           => ":attribute ya está tomado.",
	"url"              => "El formato de :attribute es inválido.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
