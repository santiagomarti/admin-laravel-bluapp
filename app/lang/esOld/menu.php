<?php


return array(
	"events" 	=> "Alertas",
	"companies" => "Companies",
	"locals" 	=> "Locals",
	"beacons" 	=> "Beacons",
	"ads" 		=> "Ads",
	"beacons" 	=> "Beacons",
	"welcome"	=> "Bienvenido",
	"users"		=> "Usuarios",
	"operators"	=> "Operators",
	"groups"	=> "Groups",
	"service"	=> "Service",

	//Usermenu
	"administration" 	=> "Administración",
	"logout" 			=> "Cerrar sesión",
	"your-profile" 		=> "Tu perfil",
	"signin" 			=> "Iniciar sesión"
);