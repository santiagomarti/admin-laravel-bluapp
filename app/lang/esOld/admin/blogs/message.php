<?php

return array(

	'does_not_exist' => 'Publicación no existe.',

	'create' => array(
		'error'   => 'Publicación no creada. Por favor intenta de nuevo.',
		'success' => 'Publicación creada exitosamente.'
	),

	'update' => array(
		'error'   => 'Publicación no actualizada. Por favor intenta de nuevo.',
		'success' => 'Publicación actualizada exitosamente.'
	),

	'delete' => array(
		'error'   => 'Hubo un problema al eliminar la publicación. Por favor intenta de nuevo.',
		'success' => 'Publicación eliminada exitosamente.'
	)

);
