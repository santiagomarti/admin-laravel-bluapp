<?php

return array(

	'ad_exists'              => '¡Ad ya existe!',
	'ad_not_found'           => 'Ad [:id] no existe.',
	'ad_login_required'      => 'El campo de ad es requerido.',
	'ad_password_required'   => 'La contraseña es requerida.',
	'insufficient_permissions' => 'Permisos permisos insuficientes.',
	'noaccess' => 'Ad no tiene acceso a este recurso.',
	'notfound' => 'Ad no encontrado.',

	'success' => array(
		'create'    => 'Ad creado exitosamente.',
		'update'    => 'Ad actualizado exitosamente.',
		'delete'    => 'Ad eliminado exitosamente.',
		'ban'       => 'Ad bloqueado exitosamente.',
		'unban'     => 'Ad desbloqueado exitosamente.',
		'suspend'   => 'Ad suspendido exitosamente.',
		'unsuspend' => 'Ad restablecido exitosamente.',
		'restored'  => 'Ad restaurado exitosamente.'
	),

	'error' => array(
		'create' => 'Hubo un problema al crear el compania. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el compania. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el compania. Por favor intenta de nuevo.',
	),

);
