<?php

return array(

	'local_exists'              => '¡Local ya existe!',
	'local_not_found'           => 'Local [:id] no existe.',
	'local_login_required'      => 'El campo de local es requerido.',
	'local_password_required'   => 'La contraseña es requerida.',
	'insufficient_permissions' => 'Permisos permisos insuficientes.',
	'noaccess' => 'Local no tiene acceso a este recurso.',
	'notfound' => 'Local no encontrado.',

	'success' => array(
		'create'    => 'Local creado exitosamente.',
		'update'    => 'Local actualizado exitosamente.',
		'delete'    => 'Local eliminado exitosamente.',
		'ban'       => 'Local bloqueado exitosamente.',
		'unban'     => 'Local desbloqueado exitosamente.',
		'suspend'   => 'Local suspendido exitosamente.',
		'unsuspend' => 'Local restablecido exitosamente.',
		'restored'  => 'Local restaurado exitosamente.'
	),

	'error' => array(
		'create' => 'Hubo un problema al crear el compania. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el compania. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el compania. Por favor intenta de nuevo.',
	),

);
