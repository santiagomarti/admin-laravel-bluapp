<?php

return array(

	'user_exists'              => '¡Usuario ya existe!',
	'user_not_found'           => 'Usuario [:id] no existe.',
	'user_login_required'      => 'El campo de Log In es requerido.',
	'user_password_required'   => 'La contraseña es requerida.',
	'insufficient_permissions' => 'Permisos permisos insuficientes.',
	'noaccess' 				   => 'Permisos permisos insuficientes.',


	'success' => array(
		'create'    => 'Usuario creado exitosamente.',
		'update'    => 'Usuario actualizado exitosamente.',
		'delete'    => 'Usuario eliminado exitosamente.',
		'ban'       => 'Usuario bloqueado exitosamente.',
		'unban'     => 'Usuario desbloqueado exitosamente.',
		'suspend'   => 'Usuario suspendido exitosamente.',
		'unsuspend' => 'Usuario restablecido exitosamente.',
		'restored'  => 'Usuario restaurado exitosamente.'
	),

	'error' => array(
		'create' => 'Hubo un problema al crear el usuario. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el usuario. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el usuario. Por favor intenta de nuevo.',
	),

);
