<?php

return array(

    'id'         	=> 	'Id',
    'first_name' 	=> 	'Nombre',
    'last_name'  	=> 	'Apellido',
    'email'      	=> 	'Email',
    'company'    	=> 	'Company',
    'local'      	=> 	'Local',
    'activated'  	=> 	'Activada',
    'guest'      	=> 	'Visita',
    'superadmin' 	=> 	'Super Admin',
    'group'      	=> 	'Grupo',
    'created_at' 	=> 	'Creado a las',
    'phone'      	=> 	'Teléfono',

);
