<?php

return array(

	'company_exists'              => '¡Compania ya existe!',
	'company_not_found'           => 'Compania [:id] no existe.',
	'company_login_required'      => 'El campo de company es requerido.',
	'company_password_required'   => 'La contraseña es requerida.',
	'insufficient_permissions' => 'Permisos permisos insuficientes.',
	'noaccess' => 'Compania no tiene acceso a este recurso.',
	'notfound' => 'Compania no encontrado.',

	'success' => array(
		'create'    => 'Compania creado exitosamente.',
		'update'    => 'Compania actualizado exitosamente.',
		'delete'    => 'Compania eliminado exitosamente.',
		'ban'       => 'Compania bloqueado exitosamente.',
		'unban'     => 'Compania desbloqueado exitosamente.',
		'suspend'   => 'Compania suspendido exitosamente.',
		'unsuspend' => 'Compania restablecido exitosamente.',
		'restored'  => 'Compania restaurado exitosamente.'
	),

	'error' => array(
		'create' => 'Hubo un problema al crear el compania. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el compania. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el compania. Por favor intenta de nuevo.',
	),

);
