<?php

return array(

	'id'         => 'Id',
	'name' => 'Nombre',
	'description'  => 'Descripción',
	'email'      => 'Email',
	'company'     	=> 	'Company',
	'activated'  => 'Activada',
	'guest' 	 => 'Visita',
	'superadmin'  => 'Super Admin',
	'group'  => 'Grupo',
	'created_at' => 'Creado a las'
);
