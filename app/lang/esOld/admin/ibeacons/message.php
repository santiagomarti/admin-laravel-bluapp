<?php

return array(

	'local_exists'              => 'Local already exists!',
	'local_not_found'           => 'Local [:id] does not exist.',
	'local_login_required'      => 'The login field is required',
	'local_password_required'   => 'The password is required.',
	'insufficient_permissions' => 'Insufficient Permissions.',
	'noaccess' => 'Local has no access to this resource.',
	'notfound' => 'Local not found.',


	'success' => array(
		'create'    => 'Local was successfully created.',
		'update'    => 'Local was successfully updated.',
		'delete'    => 'Local was successfully deleted.',
		'ban'       => 'Local was successfully banned.',
		'unban'     => 'Local was successfully unbanned.',
		'suspend'   => 'Local was successfully suspended.',
		'unsuspend' => 'Local was successfully unsuspended.',
		'restored'  => 'Local was successfully restored.'
	),

	'error' => array(
		'create' => 'There was an issue creating the local. Please try again.',
		'update' => 'There was an issue updating the local. Please try again.',
		'delete' => 'There was an issue deleting the local. Please try again.',
	),

);
