<?php

return array(

	'account_already_exists' => 'Ya existe una cuenta asociada a este email.',
	'account_not_found'      => 'No se encontró esta cuenta de usuario.',
	'account_not_activated'  => 'Esta cuenta de usuario no está activada.',
	'account_suspended'      => 'Esta cuenta de usuario está suspendida.',
	'account_banned'         => 'Esta cuenta de usuario está bloqueada.',

	'signin' => array(
		'error'   => 'Hubo un problema al iniciar sesión, por favor intenta de nuevo.',
		'success' => 'Sesión iniciada exitosamente.',
	),

	'signup' => array(
		'error'   => 'Hubo un problema al crear tu cuenta, por favor intenta de nuevo.',
		'success' => 'Cuenta creada exitosamente.',
	),

        'forgot-password' => array(
            'error'   => 'Hubo un problema al intentar obtener un código de reseteo de contraseña, por favor intenta de nuevo.',
            'success' => 'Email de recuperación de contraseña enviado exitosamente.',
        ),

        'forgot-password-confirm' => array(
            'error'   => 'Hubo un problema al intentar resetear tu contraseña, por favor intenta de nuevo.',
            'success' => 'Tu contraseña ha sido reseteada exitosamente.',
        ),
    
	'activate' => array(
		'error'   => 'Hubo un problema al intentar activar tu cuenta, por favor intenta de nuevo.',
		'success' => 'Tu cuenta se ha activado exitosamente.',
	),

);
