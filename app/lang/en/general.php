<?php

return array(

	'active' => 'Active',
	'inactive'  => 'Inactive',
	'yes' => 'Yes',
	'no'  => 'No',
	
	'errors' => 'Please review the form below for errors',
    'wrong_url' => 'We can not find the url that you provided'

);
