<?php


return array(
	"events" 	=> "Events",
	"companies" => "Companies",
	"locals" 	=> "Locals",
	"beacons" 	=> "Beacons",
	"ads" 		=> "Ads",
	"campaigns" => "Campaigns",
	"welcome"	=> "Welcome",
	"users"		=> "Users",
	"operators"	=> "Operators",
	"groups"	=> "Groups",
	"service"	=> "Service",

	//Usermenu
	"administration" 	=> "Administration",
	"logout" 			=> "Logout",
	"your-profile" 		=> "Your Profile",
	"signin" 			=> "Log in"
);