<?php

return array(

	'edit'    	=> 'Edit',
	'delete'  	=> 'Delete',
	'assign'    => 'Assign',
	'delegate'  => 'Delegate',
	'restore' 	=> 'Restore',
	'take'		=> 'Take',
	'view'		=> 'View'

);
