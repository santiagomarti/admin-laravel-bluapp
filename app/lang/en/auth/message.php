<?php

return array(

	'account_already_exists' => 'There is already an account associated with this email.',
	'account_not_found'      => 'We did not find this user account.',
	'account_not_activated'  => 'This user account is not activated.',
	'account_suspended'      => 'This account is suspended.',
	'account_banned'         => 'This user account is locked.',
    'wrong_password'         => 'Wrong password, try again.',
	'signin' => array(
		'error'   => 'There was a problem logging in, please try again.',
		'success' => 'Logged in successfully.',
	),

	'signup' => array(
		'error'   => 'There was a problem creating your account, please try again.',
		'success' => 'Account created successfully.',
	),

        'forgot-password' => array(
            'error'   => 'There was a problem trying to get a code to reset password, please try again.',
            'success' => 'Email password recovery sent successfully.',
        ),

        'forgot-password-confirm' => array(
            'error'   => 'There was a problem trying to reset your password, please try again.',
            'success' => 'Your password has been reset successfully.',
        ),
    
	'activate' => array(
		'error'   => 'There was a problem when trying to activate your account, please try again.',
		'success' => 'Your account has been activated successfully.',
	),

);
