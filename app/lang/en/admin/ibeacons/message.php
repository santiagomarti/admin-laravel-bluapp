<?php

return array(

	'local_exists'              => 'Beacon already exists!',
	'local_not_found'           => 'Beacon [:id] does not exist.',
	'local_login_required'      => 'The login field is required',
	'local_password_required'   => 'The password is required.',
	'insufficient_permissions' => 'Insufficient Permissions.',
	'noaccess' => 'Beacon has no access to this resource.',
	'notfound' => 'Beacon not found.',


	'success' => array(
		'create'    => 'Beacon was successfully created.',
		'update'    => 'Beacon was successfully updated.',
		'delete'    => 'Beacon was successfully deleted.',
		'ban'       => 'Beacon was successfully banned.',
		'unban'     => 'Beacon was successfully unbanned.',
		'suspend'   => 'Beacon was successfully suspended.',
		'unsuspend' => 'Beacon was successfully unsuspended.',
		'restored'  => 'Beacon was successfully restored.'
	),

	'error' => array(
		'create' => 'There was an issue creating the local. Please try again.',
		'update' => 'There was an issue updating the local. Please try again.',
		'delete' => 'There was an issue deleting the local. Please try again.',
	),

);
