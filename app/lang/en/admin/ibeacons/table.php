<?php

return array(
    'id'           	=> 	'Id',
    'name'         	=> 	'Name',
    'description'  	=> 	'Description',
    'email'        	=> 	'Email',
    'activated'    	=> 	'State',
    'guest'        	=> 	'Guest',
    'superadmin'   	=> 	'Super Admin',
    'peripheralID'  =>  'Beacon Name',
    'proximityUUID' => 	'ProximityUUID',
    'major'        	=> 	'Major',
    'minor'        	=> 	'Minor',
    'proximity'     => 	'Proximity',
    'cover'        	=> 	'Cover',
    'enter'        	=> 	'Enter',
    'exit'        	=> 	'Exit',
    'company'       => 	'Company',
    'local'         =>  'Local',
    'created_at'   	=> 	'Created at'
);

