<?php

return array(

	'id'           => 'Id',
	'name'         => 'Name',
	'description'  => 'Description',
	'email'        => 'Email',
	'activated'    => 'State'
);
