<?php

return array(

    'id'          	=> 	'Id',
    'name'        	=> 	'Nombre',
    'description' 	=> 	'Description',
    'title'       	=> 	'Title',
    'body'       	=> 	'body',
    'cover'       	=> 	'Cover',
    'url'       	=> 	'Url',
    'type'          =>  'Type',
    'ibeacon'    	=> 	'ibeacon',
    'promote'       => 	'Promote',
    'msg_enter'     => 	'Msg Enter',
    'msg_exit'      => 	'Mag Exit',
    'local'         =>  'Local',
    'activated'   	=> 	'Activada',
    'guest'       	=> 	'Visita',
    'display'       =>  'Mostrar',
    'group'       	=> 	'Grupo',
    'created_at'  	=> 	'Creado a las'
);
