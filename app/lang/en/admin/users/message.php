<?php

return array(

	'user_exists'              => 'User already exists!',
	'user_not_found'           => 'User [: id] not exist.',
	'user_login_required'      => 'Log In field is required.',
	'user_password_required'   => 'Password is required.',
	'insufficient_permissions' => 'Insufficient permissions permissions.',
	'noaccess' 				   => 'Insufficient permissions',


	'success' => array(
		'create'    => 'User created successfully.',
		'update'    => 'User successfully updated.',
		'delete'    => 'User successfully deleted.',
		'ban'       => 'User successfully blocked.',
		'unban'     => 'User successfully unlocked.',
		'suspend'   => 'User successfully suspended.',
		'unsuspend' => 'User successfully restored.',
		'restored'  => 'User successfully restored.'
	),

	'error' => array(
		'create' => 'There was a problem creating the user. Please try again.',
		'update' => 'There was a problem updating the user. Please try again.',
		'delete' => 'There was a problem deleting the user. Please try again.',
	),

);
