<?php

return array(

    'id'         	=> 	'Id',
    'first_name' 	=> 	'First  Name',
    'last_name'  	=> 	'Last Name',
    'email'      	=> 	'Email',
    'company'    	=> 	'Company',
    'local'      	=> 	'Local',
    'activated'  	=> 	'State',
    'superadmin' 	=> 	'Super Admin',
    'created_at'    =>  'Created at',
    'group' 	    => 	'Group',

);
