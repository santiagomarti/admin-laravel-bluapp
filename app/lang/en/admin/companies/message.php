<?php

return array(

	'company_exists'              => '¡Compania ya existe!',
	'company_not_found'           => 'Compania [:id] no existe.',
	'company_login_required'      => 'El campo de company es requerido.',
	'company_password_required'   => 'La contraseña es requerida.',
	'insufficient_permissions' => 'Permisos permisos insuficientes.',
	'noaccess' => 'Compania no tiene acceso a este recurso.',
	'notfound' => 'Compania no encontrado.',

	'success' => array(
		'create'    => 'Company successfully created.',
		'update'    => 'Company successfully updated.',
		'delete'    => 'Company successfully removed.',
		'ban'       => 'Company successfully locked.',
		'unban'     => 'Company successfully unlocked.',
		'suspend'   => 'Company successfully suspended.',
		'unsuspend' => 'Company successfully re-established.',
		'restored'  => 'Company successfully restored.'
	),

	'error' => array(
		'create' => 'Hubo un problema al crear el compania. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el compania. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el compania. Por favor intenta de nuevo.',
	),

);
