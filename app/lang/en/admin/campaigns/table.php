<?php

return array(

    'id'          	=> 	'Id',
    'name'        	=> 	'Name',
    'description' 	=> 	'Description',
    'title'       	=> 	'Title',
    'body'       	=> 	'body',
    'cover'       	=> 	'Cover',
    'url'       	=> 	'Url',
    'type'          =>  'Type',
    'ibeacon'    	=> 	'ibeacon',
    'promote'       => 	'Promote',
    'msg_enter'     => 	'Msg Enter',
    'msg_exit'      => 	'Msg Exit',
    'local'         =>  'Zone',
    'activated'   	=> 	'State',
    'guest'       	=> 	'Visita',
    'display'       =>  'Mostrar',
    'group'       	=> 	'Grupo',
    'created_at'    =>  'Creado a las',
    'active_date'  	=> 	'Active Date',
);
