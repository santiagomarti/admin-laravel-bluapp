<?php

return array(

	'ad_exists'              => 'Campaign ya existe!',
	'ad_not_found'           => 'Campaign [:id] no existe.',
	'ad_login_required'      => 'El campo de ad es requerido.',
	'ad_password_required'   => 'La contraseña es requerida.',
	'insufficient_permissions' => 'Permisos permisos insuficientes.',
	'noaccess' => 'Campaign no tiene acceso a este recurso.',
	'notfound' => 'Campaign no encontrado.',

	'success' => array(
		'create'    => 'Campaign successfully created.',
		'update'    => 'Campaign successfully updated.',
		'delete'    => 'Campaign deleted successfully.',
		'ban'       => 'Campaign successfully blocked.',
		'unban'     => 'I successfully unlocked campaign.',
		'suspend'   => 'Campaign suspended successfully.',
		'unsuspend' => 'Campaign restored successfully.',
		'restored'  => 'Campaign restored successfully.'
	),

	'error' => array(
		'create' => 'Hubo un problema al crear el compania. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el compania. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el compania. Por favor intenta de nuevo.',
	),

);
