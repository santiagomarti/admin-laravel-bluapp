<?php

return array(

	'group_exists'        => '¡Grupo ya existe!',
	'group_not_found'     => 'Grupo [:id] no existe.',
	'group_name_required' => 'El campo de Nombre es requerido',

	'success' => array(
		'create' => 'Grupo creado exitosamente.',
		'update' => 'Grupo actualizado exitosamente.',
		'delete' => 'Grupo eliminado exitosamente.',
	),

	'delete' => array(
		'create' => 'Hubo un problema al crear el grupo. Por favor intenta de nuevo.',
		'update' => 'Hubo un problema al actualizar el grupo. Por favor intenta de nuevo.',
		'delete' => 'Hubo un problema al eliminar el grupo. Por favor intenta de nuevo.',
	),

);
