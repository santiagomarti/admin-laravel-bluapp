@extends('frontend/layouts/signin')

{{-- Page title --}}
@section('title')
Account Sign up ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>Sign up</h3>
</div>
<div class="row">
	<form method="post" action="{{ route('signup') }}" class="form-horizontal" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />

		<!-- First Name -->
		<div class="control-group{{ $errors->first('first_name', ' error') }}">
		<label class="control-label" for="first_name">Nombre *</label>
			<div class="controls">
				<input type="text" class="form-control" name="first_name" id="first_name" value="{{ Input::old('first_name') }}" />
				{{ $errors->first('first_name', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Last Name -->
		<div class="control-group{{ $errors->first('last_name', ' error') }}">
			<label class="control-label" for="last_name">Apellido *</label>
			<div class="controls">
				<input type="text" class="form-control" name="last_name" id="last_name" value="{{ Input::old('last_name') }}" />
				{{ $errors->first('last_name', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Email -->
		<div class="control-group{{ $errors->first('email', ' error') }}">
			<label class="control-label" for="email">Correo *</label>
			<div class="controls">
				<input type="text" name="email" id="email" value="{{ Input::old('email') }}" class="form-control" />
				{{ $errors->first('email', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Email Confirm -->
		<div class="control-group{{ $errors->first('email_confirm', ' error') }}">
			<label class="control-label" for="email_confirm">Confirmar Correo *</label>
			<div class="controls">
				<input type="text" name="email_confirm" id="email_confirm" value="{{ Input::old('email_confirm') }}" class="form-control" />
				{{ $errors->first('email_confirm', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Password -->
		<div class="control-group{{ $errors->first('password', ' error') }}">
			<label class="control-label" for="password">Clave *</label>
			<div class="controls">
				<input type="password" name="password" id="password" value="" class="form-control" />
				{{ $errors->first('password', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Password Confirm -->
		<div class="control-group{{ $errors->first('password_confirm', ' error') }}">
			<label class="control-label" for="password_confirm">Confirmar Clave *</label>
			<div class="controls">
				<input type="password" name="password_confirm" id="password_confirm" value="" class="form-control" />
				{{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- Sex -->
		<div class="control-group{{ $errors->first('sex', ' error') }}">
			<label class="control-label" for="sex">Sexo</label>
			<div class="controls form-control">
			    {{Form::select('sex', array('M' => 'Masculino', 'F' => 'Femenino'))}}				
				{{ $errors->first('sex', '<span class="help-block">:message</span>') }}
			</div>
		</div>


		<!-- HealtCare -->
		<div class="control-group{{ $errors->first('healt_care', ' error') }}">
			<label class="control-label" for="healt_care">Isapre</label>
			<div class="controls">
			    <input type="healt_care" name="healt_care" id="healt_care" value="" class="form-control" />
				{{ $errors->first('healt_care', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<!-- BloodGroup -->
		<div class="control-group{{ $errors->first('blood_group', ' error') }}">
			<label class="control-label" for="sex">Tipo Sangre</label>
			<div class="controls form-control">
			    {{Form::select('blood_group', array('O+' => 'O+', 'O-' => 'O-', 'A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-' ))}}				
				{{ $errors->first('blood_group', '<span class="help-block">:message</span>') }}
			</div>
		</div>


		<!-- Birthday -->
		<!-- <input class="datepicker" data-date-format="mm/dd/yyyy"> -->
		<div class="control-group{{ $errors->first('birthday', ' error') }}">
			<label class="control-label" for="birthday">Fecha Nacimiento</label>
			<div class="controls">
			    <input type="date" name="birthday" id="birthday" value="" class="form-control" />
				{{ $errors->first('birthday', '<span class="help-block">:message</span>') }}
			</div>
		</div>

		<hr>

		<!-- Form actions -->
		<div class="control-group">
			<div class="controls">
				<a class="btn btn-default" href="{{ route('home') }}">Cancel</a>
				<button type="submit" class="btn btn-success uppercase">Sign up</button>
			</div>
		</div>
	</form>
</div>
@stop

@section("footer") 
	<p>&copy; iBeacons {{ date('Y') }}</p>
@stop