@extends("frontend.layouts.signin")
@section("title")
Bluapp | Login
@stop
@section("content") 
    <!-- BEGIN LOGIN FORM -->
    <!-- Notifications -->
    @include('frontend/partials/notifications')
    
        {{ Form::open(array(
          'url' => route('signin'), 
          'method' => 'post',
          'autocomplete' => 'on',
          'class' => 'login-form'
        )) }}

        {{ Form::token() }}
        <h3 class="form-title">Log In</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
            Enter any username and password. </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->

            {{ Form::label('email', 'Email', array('class' => 'control-label visible-ie8 visible-ie9')) }}
            {{ Form::text('email', '', array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder'=>'Email')) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Password', array('class' => 'control-label visible-ie8 visible-ie9')) }}
            {{ Form::password('password', array('class' => 'form-control form-control-solid placeholder-no-fix',  'placeholder'=>'Password')) }}

        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase" style="width:100%;">Login</button>
           <!-- <label class="rememberme check">
                {{ Form::checkbox('remember-me', '1') }} Remember me
            </label>-->
            <!--a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a-->
        </div>
    {{ Form::close() }}
@stop
@section("footer") 
    Bluapp Login Page
@stop
