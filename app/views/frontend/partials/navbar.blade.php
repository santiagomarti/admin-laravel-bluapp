<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

	<div class="container">
	  <!-- Brand and toggle get grouped for better mobile display -->
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="#">{{ Config::get('settings.siteName') }}</a>
	  </div>

	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    <!--<ul class="nav navbar-nav">
	      <li class="active"><a href="#">Link</a></li>
	    </ul>-->

		<ul class="nav navbar-nav navbar-right">
		@if (Sentry::check())
			<li class="dropdown{{ (Request::is('account*') ? ' active' : '') }}">
				<a class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="{{ route('account') }}">
					Welcome, {{ Sentry::getUser()->first_name }}
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				@if(Sentry::getUser()->hasAnyAccess(array('admin','company','operator')))
					<li><a href="{{ route('admin') }}"><i class="icon-cog"></i> Administration</a></li>
				@endif
					<li{{ (Request::is('account/profile') ? ' class="active"' : '') }}><a href="{{ route('profile') }}"><i class="icon-user"></i> Your profile</a></li>
					<li class="divider"></li>
					<li><a href="{{ route('logout') }}"><i class="icon-off"></i> Logout</a></li>
				</ul>
			</li>
			@else
				<li {{ (Request::is('auth/signin') ? 'class="active"' : '') }}><a href="{{ route('signin') }}">Sign in</a></li>
			@endif
		</ul>
<!--
	    <ul class="nav navbar-nav navbar-right">
	      <li><a href="#">Link</a></li>
	      <li class="dropdown">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
	        <ul class="dropdown-menu">
	          <li><a href="#">Action</a></li>
	          <li><a href="#">Another action</a></li>
	          <li><a href="#">Something else here</a></li>
	          <li class="divider"></li>
	          <li><a href="#">Separated link</a></li>
	        </ul>
	      </li>
	    </ul>-->
	  </div><!-- /.navbar-collapse -->
	</div>
</nav>

