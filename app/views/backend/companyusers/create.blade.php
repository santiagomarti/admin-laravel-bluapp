@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Create a User ::
@parent
@stop

{{-- Page content --}}
@section('content')
	<div class='tab-pane' id='tab_1'>
<div class='portlet-body tabbable-custom'>
<div class='tab-content'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>
		Create a New User
		<div class="pull-right">
			<a href="{{ route('companyusers') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>
	@include('backend/companyusers/_form')
</div>
</div>
</div>
	</div>
@stop

@section('javascript')
<script>

<!--Paint all the selects from the bootstrap-select -->
$('select').selectpicker();




var handlerCheckerEmail = function(doSubmit)
{
	var url = "{{route('validate/user')}}";

	$.post(url, { email: $('#email').val() }, function(response) {

	  console.log ('response:', response);

	  //'success'  => true, email not found, is ok to create a new account
	  if (response.success)
	  {
	  	if (doSubmit)
	  	{
	  		$('form').submit();
	  	};
	  }
	  else //Success false
	  {
	  	if (response.code === -1)
		{
			bootbox.alert("Formato de email incorrecto");
			return;
		}

		if (response.activated)
		{
			bootbox.alert("Cuenta exite y esta activa, usar otro email");
		}
		else
		{
			bootbox.confirm("Email existe y esta eliminado, desea recuperar la cuenta?", function(result) {
			  	if (result)
			  	{
			  		//send email, and recover
			    	$('#recover').val(1);
			    	$('form').submit();
				}
			});
		}
	  }

	})
	.fail(function() {

	  })
	.always(function() {
		$('#recover').val(0);
	});
}

$('#email').change(function()
{
	handlerCheckerEmail(false);
})

$('button.btn-success').click(function(e)
{
	e.preventDefault();
	handlerCheckerEmail(true);
});

</script>
@stop
