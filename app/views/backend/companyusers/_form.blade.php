<form class="form-horizontal form" method="post" action="{{route('create/companyuser')}}" autocomplete="off" id='form_sample_2' novalidate="novalidate">
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="recover" id="recover" value="0" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-6">

					<!-- Email -->
					<div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="email">Email</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="email" id="email" value="{{ Input::old('email') }}" />
							{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- First Name -->
					<div class="form-group {{ $errors->has('first_name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="first_name">First Name</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="first_name" id="first_name" value="{{ Input::old('first_name') }}" />
							{{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Last Name -->
					<div class="form-group {{ $errors->has('last_name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="last_name">Last Name</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="last_name" id="last_name" value="{{ Input::old('last_name') }}" />
							{{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password -->
					<div class="form-group {{ $errors->has('password') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="password">Password</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="password" name="password" id="password" value="" />
							
							{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password Confirm -->
					<div class="form-group {{ $errors->has('password_confirm') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="password_confirm">Confirm Password</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="password" name="password_confirm" id="password_confirm" value="" />
							{{ $errors->first('password_confirm', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<!-- Activation Status -->
					<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="activated">User Activated</label>
						<div class="controls col-md-8">
							<input id="activated" name='activated' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('activated'))?'checked':''}}>
							<!-- <select class='form-control' name="activated" id="activated">
								<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select> -->
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Groups -->
					<div class="form-group {{ $errors->has('groups') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="groups">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Company = show all campaigns information and ca create, edit and delete admin information | Operator = show only specific campaigns">
								<i class="fa fa-info-circle"></i>
								Groups
							</span>
						</label>
						<div class="controls col-md-8">
							<select class='form-control' name="groups[]" id="groups[]" >
								@foreach ($groups as $key => $group)
									<option value="{{ $group->id }}" {{(Session::get('groups')['0']==$group->id)?'selected':''}}>{{ $group->name }}</option>
									
								@endforeach
							</select>
							{{ $errors->first('groups', '<span class="help-inline">:message</span>') }}

							<!-- <span class="help-block">
								Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
							</span> -->
						</div>
					</div>

					<!-- Companies -->
					<div class="form-group {{ $errors->has('companies_id') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="companies_id">Company</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input type='text' value='{{ ( !is_null($companies) ? $companies->name : "Unassigned" ) }}' class='form-control' readonly/>
							<input type='hidden' value='{{(!is_null($companies)?$companies->id:"")}}' name="companies_id" />
							{{ $errors->first('companies_id', '<span class="help-inline">:message</span>') }}
							<!-- <span class="help-block">
								Select companies the user belongs too.
							</span> -->
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Form Actions -->
	<div class="control-group">
		<div class="controls form-actions">
			<!-- <a class="btn default" href="{{ route('users') }}">Cancel</a> -->
			<button type="reset" class="btn default">Reset</button>
			<button type="submit" class="btn blue">Create User</button>
		</div>
	</div>
</form>