<html lang="en"><!--<![endif]--><!-- BEGIN HEAD --><head>
<meta charset="utf-8">
<title>Bluapp</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description">
<meta content="" name="author">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="/assetsnew/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/admin/layout/css/layout.css" rel="stylesheet" type="text/css">
<link id="style_color" href="/assetsnew/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css">
<link href="/assetsnew/admin/layout/css/custom.css" rel="stylesheet" type="text/css">

<link href="/assetsnew/css/custombackend.css" rel="stylesheet" type="text/css">

<link href="/assetsnew/css/cssForCustommer.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css">
<link rel="shortcut icon" href="http://54.233.70.75/favicon.ico">
</head>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<h3 style="color:white; text-align: left; padding-left: 20px;">BLUAPP SDK</h3>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<style>

</style>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1317px">
			<!-- BEGIN STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			SDK Integration steps <small>for both Android and iOS platforms</small>
			</h3>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 blog-page">
					<div class="row">
						<div class="col-md-12 col-sm-11 article-block">
							<div class="caption">
			                    <h1><i class="fa fa-android" style="font-size:45px;"></i> Android</h1>
			                </div>
							<div class="row" style="padding-left:60px;">					
								<div class="col-md-8 blog-article">
									<p> Our Android SDK is built over the AltBeacon specification and SDK. It works as a background sticky service, wich automatically detects Bluapp's iBeacons and shows their related content and registers important analitics. If you have any questions please write at <a href="mailto:santiago@bluapp.cl?Subject=Bluapp%20Android%20SDK" target="_top">santiago@bluapp.cl</a> or leave a message at our <a href="https://github.com/Seelkus/bluapp-android-sdk">GitHub</a> page.
									<h3>
									Step 1:
									</h3>
									<p>Include the JCenter repository in your project's <code>build.gradle</code> file:</p>
									<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #996633">repositories</span> {
        <span style="color: #0066BB; font-weight: bold">jcenter</span>()
}
</pre></div>

									<h3>
									Step 2:
									</h3>
									<p>
										Insert the following dependency in your app's <code>build.gradle</code> file:</p>
										<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #996633">dependencies</span> {
    <span style="color: #996633">compile '</span><span style="color: #996633">com</span><span style="color: #333333">.</span><span style="color: #0066BB; font-weight: bold">seelkus</span>:<span style="color: #996633">android</span><span style="color: #333333">-</span><span style="color: #996633">sdk</span>:<span style="color: #6600EE; font-weight: bold">1.1'</span>
}
</pre></div>

									<h3>
									Step 3:
									</h3>
									<p>
										Add the following metadata to your app's <code>Manifest.xml</code> file:</p>
										<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #333333">&lt;</span>meta<span style="color: #333333">-</span>data <span style="color: #997700; font-weight: bold">android:</span>name<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;bluapp-client&quot;</span> <span style="color: #997700; font-weight: bold">android:</span>value<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;client@mail.com&quot;</span> <span style="color: #333333">/&gt;</span>
<span style="color: #333333">&lt;</span>meta<span style="color: #333333">-</span>data <span style="color: #997700; font-weight: bold">android:</span>name<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;bluapp-company&quot;</span> <span style="color: #997700; font-weight: bold">android:</span>value<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;YourCompany&quot;</span> <span style="color: #333333">/&gt;</span>
<span style="color: #333333">&lt;</span>meta<span style="color: #333333">-</span>data <span style="color: #997700; font-weight: bold">android:</span>name<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;bluapp-smallicon&quot;</span> <span style="color: #997700; font-weight: bold">android:</span>value<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;StatusBarIcon&quot;</span> <span style="color: #333333">/&gt;</span>
<span style="color: #333333">&lt;</span>meta<span style="color: #333333">-</span>data <span style="color: #997700; font-weight: bold">android:</span>name<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;bluapp-largeicon&quot;</span> <span style="color: #997700; font-weight: bold">android:</span>value<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;LargeIcon&quot;</span> <span style="color: #333333">/&gt;</span>
</pre></div>
								<p>Please note that you must enter your client email as our team provides it to you. The icon fields must be filled with pictures wich MUST be inside the drawable folder. Otherwise, notifications won't appear. Large icon is the main notification icon and the small one is the status bar icon.</p>
								<h3>
									Step 4:
									</h3>
									<p>Finally, initialize our service anywhere in your code:</p>
									<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">BluManager manager <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> BluManager<span style="color: #333333">(</span>getApplication<span style="color: #333333">());</span>
manager<span style="color: #333333">.</span><span style="color: #0000CC">start</span><span style="color: #333333">();</span>
</pre></div>

								</div>
							</div>
							<hr>
							<div class="caption">
			                    <h1><i class="fa fa-apple" style="font-size:45px;"></i> iOS</h1>
			                </div>
							<div class="row" style="padding-left:60px;">					
								<div class="col-md-8 blog-article">
									<h3>
									Coming soon...
									</h3>
								</div>
							</div>
							<hr>
							
						</div>
						<!--end col-md-9-->
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2015 © Bluapp
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<script async="" src="//www.google-analytics.com/analytics.js"></script><script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/assetsnew/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/select2/select2.min.js"></script>

<script src="/assetsnew/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/fuelux/js/spinner.min.js"></script>
<script src="/assetsnew/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="/assetsnew/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/assetsnew/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/assetsnew/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="/assetsnew/admin/layout/scripts/demo.js" type="text/javascript"></script>

<script src="/assetsnew/global/plugins/bootstrap-confirmation/bootstrap-confirmation.js" type="text/javascript"></script>
<script src="/assetsnew/admin/pages/scripts/components-form-tools.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-validation.js"></script>
<!-- Date picker, only use when having datepicker-->
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datepicker/css/datepicker.css">

<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- End Date picker-->

<!-- common table for all pages-->
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assetsnew/global/scripts/datatable.js"></script>
<!-- table-ajax file for each page-->
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/ajax/table-ajax-group.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66877929-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
});
</script>
<!-- END JAVASCRIPTS -->


<!-- END BODY -->
</body></html>