<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            @if (Sentry::getUser()->hasAccess('admin'))
                <a href="{{ URL::to('admin/groups') }}">
            @else
                <a href="{{ URL::to('admin/report') }}">
            @endif

            <img src="/assetsnew/image/logo_small.png" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-bell"></i>
                    <span class="{{ (Session::has('alertNotification') ? 'badge' : '') }} badge-default"> {{Session::get('number')}} </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external external1 {{ (!Session::has('alertNotification') ? 'hide' : '') }}">
                            <h3><span class="bold">{{Session::get('number')}} pending</span> notifications</h3>
                            <a href="{{route('sessionFlush')}}">delete all</a>
                        </li>
                        <li class="external external2 {{ (Session::has('alertNotification') ? 'hide' : '') }}">
                            <h3><span class="bold">No pending</span> notifications</h3>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                {{ Session::get('alertNotification') }}
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END NOTIFICATION DROPDOWN -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img alt="" class="img-circle" src="/avatars/{{$profile->gravatar}}"/>
                    <span class="username username-hide-on-mobile">
                    {{ Sentry::getUser()->first_name }} {{ Sentry::getUser()->last_name }}
                    (
                        @if (Sentry::getUser()->groups->count()>0) {{-- Some Users doesnt have associated a group --}}
                            <?php $groupClean = array(); ?>
                            @foreach (Sentry::getUser()->groups as $group)
                                <?php array_push($groupClean, $group->name); ?>
                            @endforeach
                            {{ implode(",", $groupClean) }}
                        @else
                            N/A
                        @endif
                    )</span>
                    <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('profile') }}">
                            <i class="icon-user"></i> My Profile </a>
                        </li>
                        <!--<li>
                            <a href="page_calendar.html">
                            <i class="icon-calendar"></i> My Calendar </a>
                        </li>
                        <li>
                            <a href="inbox.html">
                            <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
                            3 </span>
                            </a>
                        </li>
                        <li>
                            <a href="page_todo.html">
                            <i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
                            7 </span>
                            </a>
                        </li>
                        <li class="divider">
                        </li>
                        <li>
                            <a href="extra_lock.html">
                            <i class="icon-lock"></i> Lock Screen </a>
                        </li-->
                        <li>
                            <a href="{{ route('logout') }}">
                            <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <!--li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="javascript:;" class="dropdown-toggle">
                    <i class="icon-logout"></i>
                    </a>
                </li-->
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>