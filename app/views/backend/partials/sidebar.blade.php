<div class="page-sidebar navbar-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->
	<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
		<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
		<li class="sidebar-toggler-wrapper mb15" >
			<div class="sidebar-toggler"></div>
		</li>
		
		@if (Sentry::getUser()->hasAccess('admin'))
			<li class="start {{ (Request::is('admin/groups*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/groups') }}">
				<i class="icon-users"></i>
				<span class="title">{{ Lang::get('menu.groups') }}</span>
				<span class="arrow {{ (Request::is('admin/groups*') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif
		
		@if(Sentry::getUser()->hasAnyAccess(array('manage_campaigns')) && (!Sentry::getUser()->isSuperUser()) )
			<li class="btn red start {{ (Request::is('admin/campaigns/create') ? 'open' : '') }}" >
				<a href="{{ URL::to('admin/campaigns/create') }}" style="color: #fff; font-weight: 600; font-size: 16px; text-decoration: none;">
				<i class="fa fa-plus" style="color: #fff;"></i>
				<span class="title">CREATE A CAMPAIGN</span>
				</a>
			</li>
		@endif

		@if (Sentry::getUser()->inGroup(Sentry::findGroupByName('Company')) || Sentry::getUser()->inGroup(Sentry::findGroupByName('Operator')) )
			<li class="start {{ (Request::is('admin/report') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/report') }}">
					<i class="fa fa-bar-chart-o"></i>
					<span class="title">Analitics</span>
					<span class="arrow {{ (Request::is('admin/report') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif
		
		@if (Sentry::getUser()->inGroup(Sentry::findGroupByName('Company')) || Sentry::getUser()->inGroup(Sentry::findGroupByName('Operator')) )
			<li class="start {{ (Request::is('admin/panels') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/panels') }}">
					<i class="fa fa-dashboard"></i>
					<span class="title">Dashboard</span>
					<span class="arrow {{ (Request::is('admin/panels') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif
		
		@if (Sentry::getUser()->inGroup(Sentry::findGroupByName('Admin')))
			<li class="start {{ (Request::is('admin/users*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/users') }}">
				<i class="fa fa-users"></i>
				<span class="title">Users</span>
				<span class="arrow {{ (Request::is('admin/users*') ? 'open' : '') }}"></span>
				</a>
				<!--ul class="sub-menu" style="{{ (Request::is('admin/users*') ? 'display : block' : '') }}" >
					<li class="{{ Request::is( 'admin/users') ? 'active' : '' }}">
						<a href="{{ URL::to('admin/users') }}">
						Users</a>
					</li>
				</ul-->
			</li>
		@endif
		@if (Sentry::getUser()->hasAnyAccess(array('manage_users_for_company')) && (!Sentry::getUser()->isSuperUser()) )
			<li class="companyusers start {{ (Request::is('admin/companyusers*') ? 'open' : '') }}">
				<a href="{{ route('companyusers') }}">
				<i class="fa fa-users"></i>
				<span class="title">Users</span>
				<span class="arrow {{ (Request::is('admin/companyusers*') ? 'open' : '') }}"></span>
				</a>
				<!-- <ul class="sub-menu" style="{{ (Request::is('admin/companyusers*') ? 'display : block' : '') }}" >
					<li>
						<a href="{{ route('companyusers', array('role' =>'2')); }}" class='company_users_links' data-role='2'>
						Company Users</a>
					</li>
					<li>
						<a href="{{ route('companyusers', array('role' =>'3')); }}" class='company_users_links' data-role='3'>
						Operator Users</a>
					</li>
				</ul> -->
			</li>

			
		@endif

		@if (Sentry::getUser()->hasAnyAccess(array('manage_zones')) && (!Sentry::getUser()->isSuperUser()) )
			<li class="start {{ (Request::is('admin/zones*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/zones') }}">
				<i class="fa fa-map-marker"></i>
				<span class="title">Zones</span>
				<span class="arrow {{ (Request::is('admin/zones*') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif
		@if(Sentry::getUser()->hasAnyAccess(array('only_modify_beacon')) && (!Sentry::getUser()->isSuperUser()) )
			<li class="start {{ (Request::is('admin/companyibeacons*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/companyibeacons') }}">
				<i class="icon-feed"></i>
				<span class="title"> Beacons</span>
				<span class="arrow {{ (Request::is('admin/companyibeacons*') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif
		@if(Sentry::getUser()->hasAnyAccess(array('manage_companies')))
			<li class="start {{ (Request::is('admin/companies*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/companies') }}">
				<i class="fa fa-building-o"></i>
				<span class="title">{{ Lang::get('menu.companies') }}</span>
				<span class="arrow {{ (Request::is('admin/companies*') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif

		@if(Sentry::getUser()->hasAnyAccess(array('manage_beacons')))
			<li class="start {{ (Request::is('admin/ibeacons*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/ibeacons') }}">
				<i class="icon-feed"></i>
				<span class="title">{{ Lang::get('menu.beacons') }}</span>
				<span class="arrow {{ (Request::is('admin/ibeacons*') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif

		@if(Sentry::getUser()->hasAnyAccess(array('manage_campaigns')) && (!Sentry::getUser()->isSuperUser()) )
			<li class="start {{ (Request::is('admin/campaigns*') ? 'open' : '') }}">
				<a href="{{ URL::to('admin/campaigns') }}">
				<i class="fa fa-bullhorn"></i>
				<span class="title">{{ Lang::get('menu.campaigns') }}</span>
				<span class="arrow {{ (Request::is('admin/campaigns*') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif

		@if(Sentry::getUser()->hasAnyAccess(array('manage_campaigns')) && (!Sentry::getUser()->isSuperUser()) )
			<li class="start {{ (Request::is('sdk') ? 'open' : '') }}">
				<a href="{{ URL::to('sdk') }}" target="_blank">
				<i class="fa fa-code"></i>
				<span class="title">SDK</span>
				<span class="arrow {{ (Request::is('sdk') ? 'open' : '') }}"></span>
				</a>
			</li>
		@endif

		@if(Sentry::getUser()->hasAnyAccess(array('manage_companies')))
			<!--li{{ (Request::is('admin/companies/service') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/companies/service') }}"><i class="icon-map"></i> {{ Lang::get('menu.service') }}</a></li-->
		@endif

		@if(Sentry::getUser()->hasAnyAccess(array('operator')))
			<!--li{{ (Request::is('admin/operators') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/ads') }}"><i class="icon-user"></i> {{ Lang::get('menu.ads') }}</a></li-->
		@endif
	</ul>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66877929-1', 'auto');
  ga('send', 'pageview');

</script>