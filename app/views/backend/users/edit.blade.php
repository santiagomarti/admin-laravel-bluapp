@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
User Update ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class=' portlet box blue'>
<div class="page-header portlet-title">
	<h3>
		User Update

		<div class="pull-right">
			<a href="{{ route('users') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>
<div class='portlet-body '>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>
<form class="form-horizontal" method="post" action="" autocomplete="off" id='form_sample_2' novalidate="novalidate">
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-6">

					<!-- Email -->
					<div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="email">Email</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="email" id="email" value="{{ Input::old('email', $user->email) }}" />
							{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- First Name -->
					<div class="form-group {{ $errors->has('first_name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="first_name">First Name</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="first_name" id="first_name" value="{{ Input::old('first_name', $user->first_name) }}" />
							{{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Last Name -->
					<div class="form-group {{ $errors->has('last_name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="last_name">Last Name</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="last_name" id="last_name" value="{{ Input::old('last_name', $user->last_name) }}" />
							{{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password -->
					<div class="form-group {{ $errors->has('password') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="password">Password</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="password" name="password_edit" id="password" value="" />
							{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password Confirm -->
					<div class="form-group {{ $errors->has('password_confirm') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="password_confirm">Confirm Password</label>
						<div class="controls col-md-8">
							<input class='form-control' type="password" name="password_confirm_edit" id="password_confirm" value="" />
							{{ $errors->first('password_confirm', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<!-- Activation Status -->
					<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="activated">User Activated</label>
						<div class="controls col-md-8">
							<input id="activated" name='activated' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{ ($user->id === Sentry::getId() ? ' disabled="disabled"' : '') }} {{ ($user->isActivated() ? ' checked' : '') }}>
							<!-- <select class='form-control'{{ ($user->id === Sentry::getId() ? ' disabled="disabled"' : '') }} name="activated" id="activated">
								<option value="1"{{ ($user->isActivated() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ ( ! $user->isActivated() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select> -->
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Groups -->
					<div class="form-group {{ $errors->has('groups') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="groups">Groups</label>
						<div class="controls col-md-8">
							<select class='form-control groups' name="groups[]" id="groups[]">
								@foreach ($groups as $group)
								<option value="{{ $group->id }}"{{ (array_key_exists($group->id, $userGroups) ? ' selected="selected"' : '') }}>{{ $group->name }}</option>
								@endforeach
							</select>

							<!-- <span class="help-block">
								Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
							</span> -->
						</div>
					</div>

					<!-- Companies -->
					<div class="form-group {{ $errors->has('companies') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="companies">Companies</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control companies_id1' value='{{ (!is_null($company)?$company->name:"Unassigned") }}' class='form-control' readonly/>
							<input value='{{ (!is_null($company)?$company->id:"") }}' name="" id="companies_id1" type='hidden'/>
							<select class='form-control companies_id2' name="" id="companies[]">
								@foreach ($companies as $companyid=>$companyname)
									<option value="{{ $companyid }}" {{($company_id==$companyid?'selected':'')}}>{{ $companyname }}</option>
								@endforeach
							</select>
							<!-- <span class="help-block">
								Select companies the user belongs too.
							</span> -->
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- Form Actions -->
	<div class="control-group form">
		<div class="controls form-actions">
			<a class="btn default" href="{{ route('users') }}">Cancel</a>

			<button type="reset" class="btn btn-default">Reset</button>

			<button type="submit" class="btn blue">Update User</button>
		</div>
	</div>
</form>
</div>
</div>
@stop

@section('javascript')
<script>
	$( ".groups" )
	  .change(function () {
	    $( ".groups option:selected" ).each(function() {
	      if ($( ".groups option:selected" ).val()=='1') {
	      	$('.companies_id2').hide().attr('name','hide');
	      	$('.companies_id1').show();
	      	$('#companies_id1').attr('name','companies_id');
	      }else {
	      		$('.companies_id2').show().attr('name','companies_id');
	      		$('.companies_id1').hide();
	      		$('#companies_id1').attr('name','hide');
	  		}
	      ;
	    });
	  })
	  .change();
</script>
@stop
