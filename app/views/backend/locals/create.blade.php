@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Create a Local ::
@parent
@stop

{{-- Page content --}}
@section('content')

	<div class='tab-pane' id='tab_1'>
<div class='portlet-body tabbable-custom'>
<div class='tab-content'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Zone
			<a class="btn default pull-right" href="{{ route('zones') }}"style="margin-top: 4px;">Back</a>
		</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>
	@include('backend/locals/_form')
</div>
</div>
</div>
	</div>

@stop

@section('javascript')
<script type="text/javascript">

	<!--Paint all the selects from the bootstrap-select -->
	//$('select').selectpicker();

	$('#company').change(function() {
		var url = "{{ URL::to('admin/zones?selectedCompany=')}}" + $('select').val();
		window.location = url;
	});
</script>
@stop

