
<form id='form_sample_2' class="form-horizontal form" method="post" action="{{route('create/zone')}}" autocomplete="off" novalidate="novalidate">
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="company_id" value="{{$selectedCompany}}">

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class='col-md-6'>
			<!-- Name -->
			<div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="name">Name</label>
				<div class="controls col-md-8 input-icon right">
					<i class="icon fa"></i>
					<input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name') }}" />
					<!-- <input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name') }}" /> -->
					{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
				</div>
			</div>

			<!-- Description -->
			<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="description">Description</label>
				<div class="controls col-md-8">
					<input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description') }}" />
					{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
			</div>

			<div class='col-md-6'>
			<!-- Activation Status -->
			<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="activated">
					<span class='tooltips' data-container="body" data-placement="top" data-original-title="Yes = Activate all beacons in Zone | No = Desactivate all beacons in Zone">
						<i class="fa fa-info-circle"></i>
						Zone Activated
					</span>
				</label>
				<div class="controls col-md-8">
					<input id="activated" name='active' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('active'))?'checked':''}}>
					<!-- <select class='form-control' name="active" id="activated">
						<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
						<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
					</select> -->
					{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
			</div>


		</div>

	</div>

	<br>

	<!-- Form Actions -->
	<div class='row'>
		<div class="control-group col-xs-12">
			<div class="controls form-actions">

				<button type="reset" class="btn default">Reset</button>

				<button type="submit" class="btn blue">Create Zone</button>
			</div>
		</div>
	</div>
</form>