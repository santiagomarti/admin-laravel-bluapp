@extends('backend/layouts/defaultnew')
{{-- Page title --}}
@section('title')
Dashboard
@parent
@stop

{{-- Page content --}}
@section('content')
    @include('backend.report.card', array('cardInfo' => $cardInfo))

<div class="row">
    <div class="col-md-6">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Unique Beacon Visits Last 10 weeks
                </div>
                <div class="tools">
                    <form>
                        <select class="form-control" id="beaconSelect" name="beacon">
                            <option selected="selected">Choose a beacon</option>
                            @foreach($beaconList as $beacon)
                             <option value data-minor="{{ $beacon->minor}}" data-major="{{ $beacon->major }}"> {{ $beacon->name }} </option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
                <div class="portlet-body">
                    <div id="pie_chart_1" class="chart" style="padding: 0px; position: relative; height=100%; width=100;">
                         <canvas id="canvasb" ></canvas>                       
                    </div>
                </div>
            </div>
        <div class="portlet box purple">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Top 5 Most Popular Beacons Last Week
                </div>
            </div>
            <div class="portlet-body">
                <div >
                     <table class="table table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th># of visits</th>
                                    <th>Name</th>
                                    <th>Beacon Name</th>
                                    <th>Proximity</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $topFiveBeacons as $topBeacon )
                                <tr>
                                    <td>{{ $topBeacon->cont }}</td>
                                    <td>{{ $topBeacon->name }}</td>
                                    <td>{{ $topBeacon->peripheralID }}</td>
                                    <td>{{ $topBeacon->proximity }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                                </table>                         
                </div>
            </div>
        </div>
        <div class="portlet box red-thunderbird">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Top 5 Most Popular Campaigns Last Week
                </div>
            </div>
            <div class="portlet-body">
                <div >
                     <table class="table table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th>Conversion Rate</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>State</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $topFiveCampaigns as $topCampaign )
                                <tr>
                                    <td>{{ $topCampaign->cr }}</td>
                                    <td>{{ $topCampaign->title }}</td>
                                    <td>{{ $topCampaign->type }}</td>
                                    @if( $topCampaign->active == 0)
                                    <td> <span class="label label-sm label-danger">Inactive</span> </td>
                                    @else
                                    <td><span class="label label-sm label-success">Active</span></td>
                                    @endif
                                </tr>
                                @endforeach
                                </tbody>
                                </table>                         
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Users Average Time on Beacon Range
                </div>
                <div class="tools">
                    <select class="form-control" id="beaconSelecta" name="beacon" >
                        <option selected="selected">Choose a beacon</option>
                        @foreach($beaconList as $beacon)
                         <option value data-minor="{{ $beacon->minor}}" data-major="{{ $beacon->major }}"> {{ $beacon->name }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="portlet-body">
               <form id="submit_form" class="form-horizontal form campaigns" enctype="multipart/form-data" method="post" >
                    <div class="form-wizard">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-2">Date Range</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="date form_datetime input-icon right">
                                            <i class="icon fa"></i>
                                            <input type="text" size="16" readonly class="form-control" id="begin_datea"  style='cursor: pointer;'>
                                            <span class="input-group-btn">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <span class="input-group-addon">
                                        to </span>
                                        <div class="date form_datetime input-icon right">
                                            <i class="icon fa"></i>
                                            <input type="text" size="16" readonly class="form-control" id="end_datea" style='cursor: pointer;'>
                                            <span class="input-group-btn">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div id="pie_chart_1" class="chart" style="padding: 0px; position: relative; height=100%; width=100;">
                      <div id="metricaainfo" style="font-size:200px; text-align: center; ">0m</div>                   
                </div>
            </div>
        </div>
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Number of Times a Campaign was Sent/Opened
                </div>
                <div class="tools">
                    <select class="form-control" id="campaignSelecte"  >
                        <option selected="selected">Choose a campaign</option>
                        @foreach($campaignList as $campaign)
                         <option value data-id="{{ $campaign->id}}"> {{ $campaign->title }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="portlet-body">
               <form id="submit_form" class="form-horizontal form campaigns" enctype="multipart/form-data" method="post" >
                    <div class="form-wizard">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-2">Date Range</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="date form_datetime input-icon right">
                                            <i class="icon fa"></i>
                                            <input type="text" size="16" readonly class="form-control" id="begin_datee"  style='cursor: pointer;'>
                                            <span class="input-group-btn">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <span class="input-group-addon">
                                        to </span>
                                        <div class="date form_datetime input-icon right">
                                            <i class="icon fa"></i>
                                            <input type="text" size="16" readonly class="form-control" id="end_datee" style='cursor: pointer;'>
                                            <span class="input-group-btn">
                                            <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div id="pie_chart_1" class="chart" style="padding: 0px; position: relative; height=100%; width=100;">
                      <div id="metricaainfoe" style="font-size:70px; text-align: center; ">0 times sent</div>
                      <div id="metricaainfoe2" style="font-size:70px; text-align: center; ">0 times opened</div> 
                      <div id="metricaainfoe3" style="font-size:50px; text-align: center; ">Conversion Rate: 0</div> 

                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
@stop

@section('js')
<script type="text/javascript" src="/chartjs/src/Chart.Core.js"></script>
<script type="text/javascript" src="/chartjs/src/Chart.Bar.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/components-pickers.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/form-wizard.js"></script>
@stop

@section('tableadvanced')
    FormWizard.init();
    ComponentsPickers.init();
@stop

@section('javascript')

<script>
    //  Ajax when change beacon
    String.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var days    = Math.floor(sec_num / (3600 * 24));

        var hoursR  = Math.round( ((sec_num- days * 3600 * 24) / 3600) * 10) / 10;
        var minutesR = Math.round( ((sec_num - (hours * 3600)) / 60) * 10) / 10;
        var daysR   = Math.round( (sec_num / (3600 * 24)) * 10 ) / 10;

        var time = '';
        if(daysR > 1){
            return daysR + 'd';
        }
        if(hoursR > 1){
            return hoursR + 'h';
        }
        if(minutesR > 1){
            return minutesR + 'm';
        }
        if(sec_num > 0){
            return sec_num + 's';
        }
        return '0s';
    }

    function getmetricaa(major, minor, start, end) {
        $.ajax({
            url: "/api/getmetricaa",
            type: "POST",
            data : {
                minor : minor,
                major : major,
                start : start, 
                end   : end
            },
            dataType: "json",
            success : function(res) {
                var time = res[0] + '';
                if(time == 'nodata'){
                    $('#metricaainfo').text('No data');
                    $('#metricaainfo').css({ fontSize:75})
                }else {
                    $('#metricaainfo').text(time.toHHMMSS());
                    $('#metricaainfo').css({ fontSize:200 })
                }
            },
            error : function(res) {
                $('#metricaainfo').text('Error');
            }
        })
    }

    function getmetricab(major, minor) {
        $.ajax({
            url: "/api/getmetricab",
            type: "POST",
            data : {
                minor : minor,
                major : major
            },
            dataType: "json",
            success : function(res) {
                var barChartData = {
                    labels : ["1 week", "2 weeks", "3 weeks", "4 weeks", "5 weeks", "6 weeks", "7 weeks", "8 weeks", "9 weeks", "10 weeks"],
                    datasets : [
                        {
                            fillColor : "rgba(151,187,205,0.5)",
                            strokeColor : "rgba(151,187,205,0.8)",
                            highlightFill : "rgba(151,187,205,0.75)",
                            highlightStroke : "rgba(151,187,205,1)",
                            data : res[0]
                        }
                    ]
                }
                var ctx = document.getElementById("canvasb").getContext("2d");
                window.myBar = new Chart(ctx).Bar(barChartData, {
                    responsive : true, maintainAspectRatio: false
                });
                
                
            },
            error : function(res) {
            }
        })
    }

    function getmetricae(id, start, end) {
        $.ajax({
            url: "/api/getmetricae",
            type: "POST",
            data : {
                id    : id, 
                start : start,
                end : end
            },
            dataType: "json",
            success : function(res) {
                var sent = res[0] + '';
                var opened = res[1] + '';
                if(sent == 'error'){
                    $('#metricaainfoe').text('error');
                    $('#metricaainfoe2').text('');
                    $('#metricaainfoe3').text('');
                }else {
                    $('#metricaainfoe').text(sent + '  times sent');
                    $('#metricaainfoe2').text(opened + '  times opened');
                    $('#metricaainfoe3').text('Conversion Rate: ' + (res[1]/res[0]).toFixed(3) );
                }
            },
            error : function(res) {
                $('#metricaainfoe').text('Error');
            }
        })
    }
    //Metrica b
    $('#beaconSelecta').on('change', function() {
        var minor = $('#beaconSelecta option:selected').data("minor");
        var major = $('#beaconSelecta option:selected').data("major");
        var start = $('#begin_datea').val();
        var end = $('#end_datea').val();
        if (typeof minor !== "undefined" && typeof major !== "undefined" && start != '' && end != ''){
             getmetricaa(major, minor, start, end);
        }
    })

    $('#end_datea').on('change', function() {
        var minor = $('#beaconSelecta option:selected').data("minor");
        var major = $('#beaconSelecta option:selected').data("major");
        var start = $('#begin_datea').val();
        var end = $('#end_datea').val();
        if (typeof minor !== "undefined" && typeof major !== "undefined" && start != '' && end != ''){
             getmetricaa(major, minor, start, end);
        }
    })

    $('#begin_datea').on('change', function() {
        var minor = $('#beaconSelecta option:selected').data("minor");
        var major = $('#beaconSelecta option:selected').data("major");
        var start = $('#begin_datea').val();
        var end = $('#end_datea').val();
        if (typeof minor !== "undefined" && typeof major !== "undefined" && start != '' && end != ''){
             getmetricaa(major, minor, start, end);
        }
    })

    // Metrica a
    $('#beaconSelect').on('change', function() {
        var minor = $('#beaconSelect option:selected').data("minor");
        var major = $('#beaconSelect option:selected').data("major");
        getmetricab(major, minor);
    })
    
    //Metrica e

    $('#campaignSelecte').on('change', function() {
        var id = $('#campaignSelecte option:selected').data("id");
        var start = $('#begin_datee').val();
        var end = $('#end_datee').val();
        if (typeof id !== "undefined" && start != '' && end != ''){
             getmetricae(id, start, end);
        }
    })

    $('#end_datee').on('change', function() {
        var id = $('#campaignSelecte option:selected').data("id");
        var start = $('#begin_datee').val();
        var end = $('#end_datee').val();
        if (typeof id !== "undefined" && start != '' && end != ''){
             getmetricae(id, start, end);
        }
    })

    $('#begin_datee').on('change', function() {
        var id = $('#campaignSelecte option:selected').data("id");
        var start = $('#begin_datee').val();
        var end = $('#end_datee').val();
        if (typeof id !== "undefined" && start != '' && end != ''){
             getmetricae(id, start, end);
        }
    })

    var data = {
        labels: ["1 week", "2 weeks", "3 weeks", "4 weeks", "5 weeks", "6 weeks", "7 weeks", "8 weeks", "9 weeks", "10 weeks"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: [0,0,0,0,0,0,0,0,0,0]
            }
        ]
    };
    var ctx = document.getElementById("canvasb").getContext("2d");
                window.myBar = new Chart(ctx).Bar(data, {
                    responsive : true, maintainAspectRatio: false
                });
</script>
@stop
