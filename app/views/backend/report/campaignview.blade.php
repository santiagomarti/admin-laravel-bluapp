@if(Sentry::getUser()->id == 2)
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Campaign View
        </div>
        <div class="tools">
            <!--select class="form-control" data-placeholder="Select..." tabindex="-1" title="">
                <option value=""></option>
                <option value="AL">Alabama</option>
                <o ption value="WY">Wyoming</option>
            </select-->
            <form>
                {{ Form::select('campaign', $campaignList, '', array('class' => 'form-control', 'id' => 'campaignSelect')) }}
            </form>
        </div>
    </div>
    <div class="portlet-body">
        <div id="campaign_chart" class="chart">
        </div>
    </div>
</div>
@else
<h1>Analytics under maintenance</h1>
@endif