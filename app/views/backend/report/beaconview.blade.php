@if(Sentry::getUser()->id == 2)
<div class="portlet box purple">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Beacon View
        </div>
        <div class="tools">
            <form>
                <select class="form-control" id="beaconSelect" name="beacon">
                    <option selected="selected">Choose a beacon</option>
                    @foreach($beaconList as $beacon)
                     <option value data-minor="{{ $beacon->minor}}" data-major="{{ $beacon->major }}"> {{ $beacon->name }} </option>
                    @endforeach
                </select>
            </form>
        </div>
    </div>
    <div class="portlet-body">

        <div style="width: 50%">
            <canvas id="canvas" height="450" width="600"></canvas>
        </div>
    </div>
</div>
@endif