<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa"></i>
            </div>
            <div class="details">
                <div class="number"> <?= $cardInfo['activebeacon'] ?></div>
                <div class="desc"> Active Beacons </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
            <div class="visual"> <i class="fa"></i>
            </div>
            <div class="details">
                <div class="number"> <?= $cardInfo['activecampaign'] ?> </div>
                <div class="desc"> Active Campaigns </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual"> <i class="fa"></i>
            </div>
            <div class="details">
                <div class="number"> <?= $cardInfo['enterpush'] ?> </div>
                <div class="desc"> Active Enter Push </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
            <div class="visual">
                <i class="fa"></i>
            </div>
            <div class="details">
                <div class="number"> <?= $cardInfo['exitpush'] ?> </div>
                <div class="desc"> Active Exit Push </div>
            </div>
        </div>
    </div>
</div>