<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Bluapp</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="/assetsnew/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="/assetsnew/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

<link href="/assetsnew/css/custombackend.css" rel="stylesheet" type="text/css"/>

<link href="/assetsnew/css/cssForCustommer.css" rel="stylesheet" type="text/css"/>
@yield('css')
@yield('wysihtml5css')
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content">
<!-- BEGIN HEADER -->
	@include('backend/partials/page-header-top')

<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		@include('backend/partials/sidebar')
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			</h3><!--
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					@yield('breadcrumb')
				</ul>
			</div>-->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row ">
				@include('backend/partials/messages-alert')
				<div class="col-md-12">
					 @yield('content')
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	<a href="javascript:;" class="page-quick-sidebar-toggler"><i class="icon-close"></i></a>
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		Bluapp 2015
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assetsnew/global/plugins/respond.min.js"></script>
<script src="../../assetsnew/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/assetsnew/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/assetsnew/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/assetsnew/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/select2/select2.min.js"></script>

<script src="/assetsnew/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/fuelux/js/spinner.min.js"></script>
<script src="/assetsnew/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="/assetsnew/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/assetsnew/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/assetsnew/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="/assetsnew/admin/layout/scripts/demo.js" type="text/javascript"></script>

<script src="/assetsnew/global/plugins/bootstrap-confirmation/bootstrap-confirmation.js" type="text/javascript"></script>
<script src="/assetsnew/admin/pages/scripts/components-form-tools.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-validation.js"></script>
@yield('js')
@yield('wysihtml5js')
<script>
      jQuery(document).ready(function() {    
         Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		QuickSidebar.init(); // init quick sidebar
		Demo.init(); // init demo features
		FormValidation.init();
	    //TableAdvanced.init();
	    @yield('tableadvanced')
   		ComponentsFormTools.init();
   		$('.page-content .ajax_alert').on('click','.close',function(){
   			$(this).parent().addClass('hide');
   		});
	    $('#datatable_ajax').on('mouseenter','a.confirmBefore',function(e) {
	        e.preventDefault();
	        var urlHref = $(this).attr('href');
	        $(this).attr('href','');
	        $(this).confirmation({
	        	popout:true,
	        	singleton:true,
	        	btnOkClass:'btn-xs btn-success',
	        	btnCancelClass:'btn-xs btn-danger',
	        	onConfirm: function(e){
		            $.ajax({
		                method: 'delete',
		                url: urlHref,
		                success: function(response) {
		    				if (response.status=='error') {
		    					$('.page-content').find('.error_ajax').removeClass('hide');
		    					$('.page-content .error_ajax').find('.message').text(response.message);
		    				} else if (response.status=='OK'){
		    					$('.page-content').find('.success_ajax').removeClass('hide');
		    					$('.page-content .success_ajax').find('.message').text(response.message);
		    				};
		    				if (response.number) {
		    					$('.top-menu').find('#header_notification_bar').find('.badge-default').text(response.number);
		    					$('.top-menu').find('#header_notification_bar').find('.external1').find('span.bold').text(response.number + ' pending');
		    				};
		    				if (response.alertNotification) {
		    					var badge = $('.top-menu').find('#header_notification_bar').find('.badge-default');
		    					var external = $('.top-menu').find('#header_notification_bar');
		    					if (!badge.hasClass("badge")) {
								    badge.addClass('badge');
								}
		    					if (!external.find('.external2').hasClass("hide")) {
								    external.find('.external2').addClass('hide');
								}
							    external.find('.external1').removeClass('hide');
		    					$('.top-menu').find('#header_notification_bar').find('.external2').addClass('hide');
		    					$('.top-menu').find('#header_notification_bar').find('.dropdown-menu-list.scroller').html(response.alertNotification);
		    				};
		                    $(".filter-submit").trigger("click");
		                },
		                error: function(e) {
		                    alert('Error' + e);
		                }
		            });
	        	},
	        	onCancel: function(e){
	        		console.log('Cancel delete');
	        	}
	        });
	    });
	    $('#datatable_ajax').on('mouseenter','a.restore',function(e) {
	        e.preventDefault();
	        var urlHref = $(this).attr('href');
	        $(this).attr('href','');
	        $(this).confirmation({
	        	popout:true,
	        	singleton:true,
	        	btnOkClass:'btn-xs btn-success',
	        	btnCancelClass:'btn-xs btn-danger',
	        	onConfirm: function(e){
		            $.ajax({
		                method: 'post',
		                url: urlHref,
		                success: function(response) {
		    				if (response.status=='error') {
		    					$('.page-content').find('.error_ajax').removeClass('hide');
		    					$('.page-content .error_ajax').find('.message').text(response.message);
		    				} else if (response.status=='OK'){
		    					$('.page-content').find('.success_ajax').removeClass('hide');
		    					$('.page-content .success_ajax').find('.message').text(response.message);
		    				};
		                    $(".filter-submit").trigger("click");
		                },
		                error: function(e) {
		                    alert('Error' + e);
		                }
		            });
	        	},
	        	onCancel: function(e){
	        		console.log('Cancel restore');
	        	}
	        });
	    });
      });
   </script>
 @yield('footer-script')
 @yield('javascript')
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>