<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			Administration
			@show
		</title>
		<meta name="keywords" content="" />
		<meta name="author" content="Magno" />
		<meta name="description" content="" />

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- CSS
		================================================== -->
		<link href="/assets/libs/font-awesome.min.css" rel="stylesheet">
		<link href="/assets/libs/bootstrap.min.css" rel="stylesheet">

		<link rel="stylesheet" href="/assets/libs/jquery-ui.css">
		<link href="/assets/fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

		<link href="/assets/bootstrap-select/bootstrap-select.min.css" rel="stylesheet"/>

	    <link rel="stylesheet" type="text/css" href="/assets/bootstrap3-wysihtml5/src/bootstrap-wysihtml5.css" />

		@section('links')
		@show

		<style>
		@section('styles')
		body {
			padding: 10px 0;
			padding-top: 60px;
		}
		@show
		</style>

		@section('scripts')
		@show

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="/assets/libs/html5.js"></script>
		<![endif]-->

		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
		<link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png') }}">
	</head>

	<body>

		<!-- Main menu -->
		@include('backend/partials/navbar')

		<!-- Container -->
		<div class="container">

			<!-- Notifications -->
			@include('frontend/partials/notifications')

			<!-- Content -->
			@yield('content')
		</div>

		<!-- Javascripts
		================================================== -->
		<script src="{{ asset('assets/js/jquery.1.11.0.min.js') }}"></script>
		<script src="/assets/libs/bootstrap.min.js"></script>
		<script src="{{ asset('assets/bootstrap-select/bootstrap-select.min.js') }}"></script>
		<script src="{{ asset('assets/fileinput/js/fileinput.min.js') }}"></script>


	    <script src="/assets/bootstrap3-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
	    <script src="/assets/bootstrap3-wysihtml5/src/bootstrap3-wysihtml5.js"></script>

	    <!-- View Javascripts-->
		@yield('javascript')
	</body>
</html>
