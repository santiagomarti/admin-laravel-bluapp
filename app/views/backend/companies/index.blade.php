@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Company Management ::
@parent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

{{-- Page content --}}
@section('content')
<div class='portlet-body tabbable-custom companies_test'>
<ul class="nav nav-tabs">
<li class="active">
	<a href="#tab_0" data-toggle="tab" aria-expanded="true">Companies Management</a>
</li>
<li class="">
	<a href="#tab_1" data-toggle="tab" aria-expanded="false" class='btn green'>Create a New Company<i class="fa fa-plus"></i></a>
</li>
</ul>
<div class='tab-content'>
	<div class='tab-pane active' id='tab_0'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>
		Company Management
	</h3>
</div>

<div class="text-center show_links">
	<a class="btn btn-primary btn-danger" data-show=''>Show Companies</a>
	<a class="btn btn-primary" data-show='withTrashed'>Include Deleted Companies</a>
	<a class="btn btn-primary" data-show='onlyTrashed'>Deleted Companies</a>
</div>
<div class="portlet box red">
<div class="portlet-body ">
<div class='table-container'>
<table class="table table-striped table-bordered table-hover" id="datatable_ajax">
    <thead>
        <tr role="row" class="heading">
            <th width="20%" name="id">@lang('admin/companies/table.id')</th>
            <th width="20%" name="name">@lang('admin/companies/table.name')</th>
            <th width="20%" name='description'>@lang('admin/companies/table.description')</th>
            <th width="20%" name='activated'>@lang('admin/users/table.activated')</th>
            <th width="20%" class="sorting_disabled">@lang('table.actions')</th>
        </tr>
        <tr role="row" class="filter">
            <td>
                <input type="text" class="form-control form-filter input-sm" name="idd">
                <input type="hidden" class="form-control form-filter input-sm" name="show_value" value='' id='showValue'>
            </td>
            <td>
                <input type="text" class="form-control form-filter input-sm" name="namee">
            </td>
            <td>
                <input type="text" class="form-control form-filter input-sm" name="descript">
            </td>
            <td>
				<select name="state" class="form-control form-filter input-sm">
					<option value="">Select...</option>
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				</select>
            </td>
            <td>
                <div class="margin-bottom-5">
                    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                </div>
                <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
            </td>
        </tr>
    </thead>
    <tbody></tbody>
</table>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>

	</div>
	<div class='tab-pane' id='tab_1'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Company</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>

	@include('backend/companies/_form')
</div>
	</div>
</div>
</div>

@stop

@section('js')

<!-- common table for all pages-->
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assetsnew/global/scripts/datatable.js"></script>
<!-- table-ajax file for each page-->
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/ajax/table-ajax-companies.js"></script>

@stop

@section('tableadvanced') 
    TableAjax.init();
@stop

@section('javascript') 
    <script type="text/javascript">
    	$('.show_links').on('click','a', function (e) {
    		var data_show = $(this).data('show');
    		$(this).closest('.show_links').find('a').removeClass('btn-danger');
    		$(this).addClass('btn-danger');
            $('#showValue').val(data_show).trigger("change");
    	});
        $('#showValue').on('change', function () {
            $(".filter-submit").trigger("click");
        });
        $('.filter-cancel').on('click', function () {
            $('.show_links').find('a').removeClass('btn-danger');
            $('.show_links a').first().addClass('btn-danger');
        });
    </script>
@stop
