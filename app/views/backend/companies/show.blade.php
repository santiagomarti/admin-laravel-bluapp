@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
	Services Display
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>
		Service Display
	</h3>
</div>

<a href="http://jsonformatter.curiousconcept.com/#http://ib.dev.vs.cl/index.php/apiv1.1/beacons/promoted-ads" target="_blank">Click Hero to Open JSON Formatter</a>

<textarea style="width:100%; height:500px;">
{{$json}}
</textarea>

@stop
