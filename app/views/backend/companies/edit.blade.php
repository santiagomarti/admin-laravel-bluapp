@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Company Update ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class=' portlet box blue'>
<div class="page-header portlet-title">
	<h3>
		Company Update: {{ $company->name }}

		<div class="pull-right">
			<a href="{{ route('companies') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<div class='portlet-body tabbable-custom'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>
<form class="form-horizontal" enctype="multipart/form-data" method="post" action="" autocomplete="off" id='form_sample_2' novalidate="novalidate">
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class='col-md-6'>
				<!-- Name -->
				<div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="name">Name</label>
					<div class="controls col-md-8 input-icon right">
						<i class="icon fa"></i>
						<input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name', $company->name) }}" />
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Description -->
				<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="description">Description</label>
					<div class="controls col-md-8">
						<input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description', $company->description) }}" />
						{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Company Activated -->
				<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="activated">Company Activated</label>
					<div class="controls col-md-8">
						<input id="activated" name='activated' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{ ($company->isActive() ? ' checked' : '') }}>
						<!-- <select class='form-control' name="activated" id="activated">
							<option value="1"{{ ($company->isActive() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
							<option value="0"{{ ( ! $company->isActive() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
						</select> -->
						{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
			</div>

			<div class='col-md-6'>
				<!-- Cover -->
				<div class="form-group {{ $errors->has('cover') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="cover">Cover</label>
					<div class='col-md-8'><img id="holder" src="/avatars/{{$company->cover}}" style="height: auto;width: auto;"></div>
					<br>
					<div class="controls col-md-8 col-md-offset-4">
						<input class='form-control' id="cover"  name="cover" type="file" accept="image/*" onchange="PreviewImage();">
						{{ $errors->first('cover', '<span class="help-inline">:message</span>') }}
					</div>
					<script type="text/javascript">

					    function PreviewImage() {
					        var oFReader = new FileReader();
					        oFReader.readAsDataURL(document.getElementById("cover").files[0]);

					        oFReader.onload = function (oFREvent) {
					            document.getElementById("holder").src = oFREvent.target.result;
					        };
					    };

					</script>
					
				</div>
			</div>


		</div>
	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group form col-xs-12">
		<div class="controls form-actions">
			<a class="btn default" href="{{ route('companies') }}">Cancel</a>

			<button type="reset" class="btn btn-default">Reset</button>

			<button type="submit" class="btn blue">Update Company</button>
		</div>
	</div>
</form>
</div>
</div>

<br>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Done</h4>
      </div>
      <div class="modal-body" id="response">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop



@section('javascript')
<script>

<!--Paint all the selects from the bootstrap-select -->
//$('select').selectpicker();


/*$("#cover").fileinput({
	previewFileType: "image",
	browseClass: "btn btn-success",
	browseLabel: "Pick Image",
	browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
	removeClass: "btn btn-danger",
	removeLabel: "Delete",
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	uploadClass: "btn btn-info",
	uploadLabel: "Upload",
	uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
	uploadUrl: "{{route('cover-company')}}"
});*/

$('.input-group-btn a').click(function(e) {
    e.preventDefault();

    var urlAjax = $(this).attr('href');

    $.post(urlAjax,
	{
		company_id: '{{$company->id}}',
		cover: $("#cover").attr('base64')
	}).done(function(response)
	{
	  $('#holder').attr('src', $('.file-preview-image').attr('src'));
	  $('#response').html(response.msg);
	  $('#myModal').modal('show')
	})
	.fail(function(response) {
	  $('#response').html('failed to uplaod image');
	  $('#myModal').modal('show')
	})
	.always(function() {

	});

});

function readImage(evt, component) {
	var files = evt.target.files;
    var file = files[0];

    if (files && file) {

        var FR= new FileReader();

        FR.onload = function(readerEvt)
        {
              var binaryString = readerEvt.target.result;
              component.attr('base64',binaryString);
        };

        FR.readAsDataURL( file );
    }
}

$("#cover").change(function (e)
{
    readImage(e, $("#cover"));
});

</script>
@stop
