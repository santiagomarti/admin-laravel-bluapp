
<form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('create/company')}}" autocomplete="off" id='form_sample_2' novalidate="novalidate">
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="cover64" id= "cover64" value="" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class='col-md-6'>
			<!-- Name -->
			<div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="name">Name</label>
				<div class="controls col-md-8 input-icon right">
					<i class="icon fa"></i>
					<input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name') }}" />
					{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
				</div>
			</div>

			<!-- Description -->
			<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="description">Description</label>
				<div class="controls col-md-8">
					<input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description') }}" />
					{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
				</div>
			</div>


			<!-- Activation Status -->
			<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="activated">Company Activated</label>
				<div class="controls col-md-8">
					<input id="activated" name='activated' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('activated'))?'checked':''}}>
					<!-- <select class='form-control' name="activated" id="activated">
						<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
						<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
					</select> -->
					{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
			</div>
			<div class='col-md-6'>
			<!-- Cover -->
			<div class="form-group {{ $errors->has('cover') ? 'error' : '' }}">
				<label class="control-label col-md-4" for="cover">Cover</label>
				<div class='col-md-8'><img id="holder" alt="cover" style="height: auto;width: auto;"></div>
				<br>
				<div class="controls col-md-8 col-md-offset-4">
					<input class='form-control' id="cover"  name="cover" type="file" accept="image/*" onchange="PreviewImage();">
					{{ $errors->first('cover', '<span class="help-inline">:message</span>') }}
				</div>
				<script type="text/javascript">

				    function PreviewImage() {
				        var oFReader = new FileReader();
				        oFReader.readAsDataURL(document.getElementById("cover").files[0]);

				        oFReader.onload = function (oFREvent) {
				            document.getElementById("holder").src = oFREvent.target.result;
				        };
				    };

				</script>
			</div>
			</div>
		</div>

	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group form col-xs-12">
		<div class="controls form-actions">

			<button type="reset" class="btn default">Reset</button>

			<button type="submit" class="btn blue">Create Company</button>
		</div>
	</div>
</form>