@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Create a Company ::
@parent
@stop

{{-- Page content --}}
@section('content')
	<div class='tab-pane' id='tab_1'>
<div class='portlet-body tabbable-custom'>
<div class='tab-content'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Company
		<div class="pull-right">
			<a href="{{ route('companies') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>

	@include('backend/companies/_form')
</div>
</div>
</div>
	</div>

@stop




@section('javascript')
<script>

<!--Paint all the selects from the bootstrap-select -->
$('select').selectpicker();


$("#cover").fileinput({
	previewFileType: "image",
	browseClass: "btn btn-success",
	browseLabel: "Pick Image",
	browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
	removeClass: "btn btn-danger",
	removeLabel: "Delete",
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	uploadClass: "btn btn-info",
	uploadLabel: "Upload",
	uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
	uploadUrl: "{{route('cover-company')}}",
	showUpload: false
});

$('.input-group-btn a').click(function(e) {
    e.preventDefault();
    $('#cover64').val($("#cover").attr('base64'));
    console.log('data:', $('#cover64').val() );
});

function readImage(evt, component, callback) {
	var files = evt.target.files;
    var file = files[0];

    if (files && file) {

        var FR= new FileReader();

        FR.onload = function(readerEvt)
        {
              var binaryString = readerEvt.target.result;
              component.attr('base64',binaryString);
              callback();
        };

        FR.readAsDataURL( file );
    }
}

$("#cover").change(function (e)
{
    readImage(e, $("#cover"),function(){
	    	$('#cover64').val($("#cover").attr('base64'));
	    	console.log('data:', $('#cover64').val() );
		});
});


</script>
@stop

