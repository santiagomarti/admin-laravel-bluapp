@extends('backend/layouts/defaultnew')

{{-- Web site Title --}}
@section('title')
Group Management ::
@parent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

{{-- Content --}}
@section('content')
<div class='portlet-body tabbable-custom'>
<ul class="nav nav-tabs">
<li class="active">
    <a href="#tab_0" data-toggle="tab" aria-expanded="true">Group Management</a>
</li>
<li class="">
    <a href="#tab_1" data-toggle="tab" aria-expanded="false">Create a New Group</a>
</li>
</ul>
<div class='tab-content'>
    <div class='tab-pane active' id='tab_0'>
<div class="page-header portlet box blue">
    <h3 class='portlet-title'>
        Group Management
    </h3>
</div>

<div class='text-center'>
{{-- $groups->links() --}}
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i>Groups Table
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config">
            </a>
            <a href="javascript:;" class="reload">
            </a>
            <a href="javascript:;" class="remove">
            </a>
        </div>
    </div>
<div class="portlet-body ">
    <div class="table-container">
                                <div class="table-actions-wrapper">
                                    <span>
                                    </span>
                                    <select class="table-group-action-input form-control input-inline input-small input-sm">
                                        <option value="">Select...</option>
                                        <option value="Cancel">Cancel</option>
                                        <option value="Cancel">Hold</option>
                                        <option value="Cancel">On Hold</option>
                                        <option value="Close">Close</option>
                                    </select>
                                    <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
                                </div>
                                <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">
                                        <input type="checkbox" class="group-checkable">
                                    </th>
                                    <th width="5%">
                                         Record&nbsp;#
                                    </th>
                                    <th width="15%">
                                         Date
                                    </th>
                                    <th width="15%">
                                         Customer
                                    </th>
                                    <th width="10%">
                                         Ship&nbsp;To
                                    </th>
                                    <th width="10%">
                                         Price
                                    </th>
                                    <th width="10%">
                                         Amount
                                    </th>
                                    <th width="10%">
                                         Status
                                    </th>
                                    <th width="10%">
                                         Actions
                                    </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="order_id">
                                    </td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                                            <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                                            <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="order_customer_name">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="order_ship_to">
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <input type="text" class="form-control form-filter input-sm" name="order_price_from" placeholder="From"/>
                                        </div>
                                        <input type="text" class="form-control form-filter input-sm" name="order_price_to" placeholder="To"/>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <input type="text" class="form-control form-filter input-sm margin-bottom-5 clearfix" name="order_quantity_from" placeholder="From"/>
                                        </div>
                                        <input type="text" class="form-control form-filter input-sm" name="order_quantity_to" placeholder="To"/>
                                    </td>
                                    <td>
                                        <select name="order_status" class="form-control form-filter input-sm">
                                            <option value="">Select...</option>
                                            <option value="pending">Pending</option>
                                            <option value="closed">Closed</option>
                                            <option value="hold">On Hold</option>
                                            <option value="fraud">Fraud</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                                        </div>
                                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                </table>
                            </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>


<div class='text-center'>
{{-- $groups->links() --}}
</div>

    </div>
    <div class='tab-pane' id='tab_1'>
<div class="page-header portlet box blue">
    <h3 class='portlet-title'>Create a New Group</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
    <li><a href="#tab-permissions" data-toggle="tab">Permissions</a></li>
</ul>
</div>
    @include('backend/groups/_form')
</div>
    </div>
</div>
</div>
@stop

@section('js')
<!--script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="/assetsnew/admin/pages/scripts/table-advanced.js"></script-->
@stop

@section('javascript')
<!-- Date picker, only use when having datepicker-->
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datepicker/css/datepicker.css"/>

<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- End Date picker-->

<!-- common table for all pages-->
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assetsnew/global/scripts/datatable.js"></script>

<!-- table-ajax file for each page-->
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/ajax/table-ajax-group-demo.js"></script>

<script>
    jQuery(document).ready(function() {
        TableAjax.init();
    });
</script>


@stop

@section('tableadvanced')
    
@stop

