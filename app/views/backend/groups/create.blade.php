@extends('backend/layouts/defaultnew')

{{-- Web site Title --}}
@section('title')
Create a Group ::
@parent
@stop

{{-- Content --}}
@section('content')
	<div class='tab-pane' id='tab_1'>
<div class='portlet-body tabbable-custom'>
<div class='tab-content'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Group
		<div class="pull-right">
			<a href="{{ route('groups') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	<li><a href="#tab-permissions" data-toggle="tab">Permissions</a></li>
</ul>
</div>
	@include('backend/groups/_form')
</div>
</div>
</div>
	</div>
@stop
