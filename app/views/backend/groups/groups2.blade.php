@extends('backend/layouts/defaultnew')

{{-- Web site Title --}}
@section('title')
Group Management ::
@parent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

{{-- Content --}}
@section('content')
<div class='portlet-body tabbable-custom'>
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab_0" data-toggle="tab" aria-expanded="true">Group Management</a>
        </li>
        <li class="">
            <a href="#tab_1" data-toggle="tab" aria-expanded="false" class='btn green'>Create a New Group<i class="fa fa-plus"></i></a>
        </li>
    </ul>
<div class='tab-content'>
    <div class='tab-pane active' id='tab_0'>
        <div class="page-header portlet box blue">
            <h3 class='portlet-title'>Group Management</h3>
        </div>
<div class="portlet box red">
<div class="portlet-body ">
    <div class="table-container">
        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
            <thead>
                <tr role="row" class="heading">
                    <th width="20%" name="id">Id</th>
                    <th width="20%" name="name">Name&nbsp;#</th>
                    <th width="20%" name='number_user'>Number of users</th>
                    <th width="20%" name='created'>Created</th>
                    <th width="20%" class="sorting_disabled">Actions</th>
                </tr>
                <tr role="row" class="filter">
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="idd">
                    </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="namee">
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <input type="text" class="form-control form-filter input-sm" name="number_of_user_from" placeholder="From"/>
                        </div>
                        <input type="text" class="form-control form-filter input-sm" name="number_of_user_to" placeholder="To"/>
                    </td>
                    <td>
                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control form-filter input-sm" readonly name="created_at_from" placeholder="From">
                            <span class="input-group-btn">
                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control form-filter input-sm" readonly name="created_at_to" placeholder="To">
                            <span class="input-group-btn">
                            <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                        </div>
                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
                    </td>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>



    </div>
    <div class='tab-pane' id='tab_1'>
<div class="page-header portlet box blue">
    <h3 class='portlet-title'>Create a New Group</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
    <li><a href="#tab-permissions" data-toggle="tab">Permissions</a></li>
</ul>
</div>
    @include('backend/groups/_form')
</div>
    </div>
</div>
</div>
@stop

@section('js')
<!-- Date picker, only use when having datepicker-->
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datepicker/css/datepicker.css"/>

<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- End Date picker-->

<!-- common table for all pages-->
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assetsnew/global/scripts/datatable.js"></script>
<!-- table-ajax file for each page-->
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/ajax/table-ajax-group.js"></script>

@stop

@section('tableadvanced') 
    TableAjax.init();
@stop

