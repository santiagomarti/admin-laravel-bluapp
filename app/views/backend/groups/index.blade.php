@extends('backend/layouts/defaultnew')

{{-- Web site Title --}}
@section('title')
Group Management ::
@parent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

{{-- Content --}}
@section('content')
<div class='portlet-body tabbable-custom'>
<ul class="nav nav-tabs">
<li class="active">
	<a href="#tab_0" data-toggle="tab" aria-expanded="true">Group Management</a>
</li>
<li class="">
	<a href="#tab_1" data-toggle="tab" aria-expanded="false" class='btn green'>Create a New Group<i class="fa fa-plus"></i></a>
</li>
</ul>
<div class='tab-content'>
	<div class='tab-pane active' id='tab_0'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>
		Group Management
	</h3>
</div>

<div class='text-center'>
{{ $groups->links() }}
</div>
<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-cogs"></i>Groups Table
		</div>
		<div class="tools">
			<a href="javascript:;" class="collapse">
			</a>
			<a href="#portlet-config" data-toggle="modal" class="config">
			</a>
			<a href="javascript:;" class="reload">
			</a>
			<a href="javascript:;" class="remove">
			</a>
		</div>
	</div>
<div class="portlet-body ">
<table class="table table-bordered table-striped table-hover" id="sample_1">
	<thead>
		<tr>
			<th class="span1">@lang('admin/groups/table.id')</th>
			<th class="span6">@lang('admin/groups/table.name')</th>
			<th class="span2">@lang('admin/groups/table.users')</th>
			<th class="span2">@lang('admin/groups/table.created_at')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@if ($groups->count() >= 1)
		@foreach ($groups as $group)
		<tr>
			<td>{{ $group->id }}</td>
			<td>{{ $group->name }}</td>
			<td>{{ $group->users()->count() }}</td>
			<td>{{ $group->created_at->diffForHumans() }}</td>
			<td>
				<a href="{{ route('update/group', $group->id) }}" class="btn btn-xs purple"><i class="fa fa-edit"></i>@lang('button.edit')</a>
				<a href="{{ route('delete/group', $group->id) }}" class="btn btn-xs red"><i class="fa fa-trash-o"></i>@lang('button.delete')</a>
			</td>
		</tr>
		@endforeach
		@else
		<tr>
			<td colspan="5">No results</td>
		</tr>
		@endif
	</tbody>
</table>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>


<div class='text-center'>
{{ $groups->links() }}
</div>

	</div>
	<div class='tab-pane' id='tab_1'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Group</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	<li><a href="#tab-permissions" data-toggle="tab">Permissions</a></li>
</ul>
</div>
	@include('backend/groups/_form')
</div>
	</div>
</div>
</div>
@stop

@section('js')
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="/assetsnew/admin/pages/scripts/table-advanced.js"></script>
@stop

@section('tableadvanced')
	TableAdvanced.init();
@stop

