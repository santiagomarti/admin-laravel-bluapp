@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
IBeacons Management ::
@parent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
@stop

{{-- Page content --}}
@section('content')
<div class='portlet-body tabbable-custom companyibeacons'>
<ul class="nav nav-tabs">
<li class="active">
	<a href="#tab_0" data-toggle="tab" aria-expanded="true">IBeacons Management</a>
</li>
@if ((Sentry::getUser()->hasAccess('admin')))
<li class="">
	<a href="#tab_1" data-toggle="tab" aria-expanded="false">Create a New IBeacon</a>
</li>
@endif
</ul>
<div class='tab-content'>
	<div class='tab-pane active' id='tab_0'>
<div class="page-header portlet box blue">
	<div class='portlet-title'>
		<h3 class='pull-left'>IBeacons Management</h3>
		<div class="zones_dropdown pull-left">
			<select class="bs-select form-control input-small" data-style="blue" name='zones_dropdown' id='zones_dropdown'>
    				<option value="">All</option>
	    		@foreach ($locals as $local)
    				<option value="{{ $local->id }}">{{ $local->name }}</option>
				@endforeach
	  		</select>
		</div>
	</div>
</div>

<div class="text-center show_links">
<a class="btn btn-primary @if(!Input::get('withTrashed') and !Input::get('onlyTrashed'))btn-danger@endif" href="{{ URL::to('admin/companyibeacons') }}">Show IBeacons</a>
<!--a class="btn btn-primary @if(Input::get('withTrashed'))btn-danger@endif" href="{{ URL::to('admin/companyibeacons?withTrashed=true') }}">Include Deleted IBeacons</a>
<a class="btn btn-primary @if(Input::get('onlyTrashed'))btn-danger@endif" href="{{ URL::to('admin/companyibeacons?onlyTrashed=true') }}">Include Only Deleted IBeacons</a-->

</div>

<div class="portlet box red">
<div class="portlet-body ">
<div class='table-container'>
<table class="table table-striped table-bordered table-hover" id="datatable_ajax">
    <thead>
        <tr role="row" class="heading">
            <th data-name="name">@lang('admin/ibeacons/table.name')</th>
			<th data-name='peripheralID'>@lang('admin/ibeacons/table.peripheralID')</th>
			<th data-name='proximity'>@lang('admin/ibeacons/table.proximity')</th>
			<th data-name='enter'>@lang('admin/ibeacons/table.enter')</th>
			<th data-name='exit'>@lang('admin/ibeacons/table.exit')</th>
            <th data-name='active'>@lang('admin/users/table.activated')</th>
            <th class="sorting_disabled">@lang('table.actions')</th>
        </tr>
        <tr role="row" class="filter">
            <td>
                <input type="text" class="form-control form-filter input-sm" name="namee">
                <input type="hidden" class="form-control form-filter input-sm" name="zones_value" value='' id='zones_value'>
            </td>
            <td>
                <input type="text" class="form-control form-filter input-sm" name="peripheralIDD">
            </td>
            <td>
				<select name="proximityy" class="form-control form-filter input-sm">
					<option value="">Select...</option>
					<option value="immediate">Immediate</option>
					<option value="far">Far</option>
					<option value="near">Near</option>
				</select>
            </td>
            <td>
				<select name="enterr" class="form-control form-filter input-sm">
					<option value="">Select...</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
            </td>
            <td>
				<select name="exitt" class="form-control form-filter input-sm">
					<option value="">Select...</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
            </td>
            <td>
				<select name="state" class="form-control form-filter input-sm">
					<option value="">Select...</option>
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				</select>
            </td>
            <td>
                <div class="margin-bottom-5">
                    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                </div>
                <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
            </td>
        </tr>
    </thead>
    <tbody></tbody>
</table>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>


	</div>
	
</div>
</div>
@stop

<!-- @section('javascript')
<script type="text/javascript">

	$('#company').change(function() {
		var url = "{{ URL::to('admin/ibeacons?selectedCompany=')}}" + $('#company').val();
		window.location = url;
	});
</script>
@stop -->

@section('js')

<!-- common table for all pages-->
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assetsnew/global/scripts/datatable.js"></script>
<!-- table-ajax file for each page-->
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/ajax/table-ajax-companyibeacons.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="/assetsnew/admin/pages/scripts/components-dropdowns.js"></script>

@stop

@section('tableadvanced') 
    TableAjax.init();
    ComponentsDropdowns.init();
@stop
@section('footer-script') 
    <script type="text/javascript">
	    //Show record with zone
	    $('#zones_dropdown').on('change', function () {
	    	var zone_id = $(this).find("option:selected").val();
	        $('#zones_value').val(zone_id).trigger("change");
	    });
	    $('#zones_value').on('change', function () {
	        $(".filter-submit").trigger("click");
	    });
	    $('.filter-cancel').on('click', function () {
	        $("#zones_dropdown").val($("#zones_dropdown option:first").val()).change();
	    });
    </script>
@stop
