<form class="form-horizontal" method="post" action="{{route('create/ibeacon')}}" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="company_id" value="" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-6">
					<!-- Name -->
					<div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="name">Name</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name') }}" />
							{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Description -->
					<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="description">Description</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description') }}" />
							{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- Activation Status -->
					<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="activated">Activated</label>
						<div class="controls col-md-8">
							<select class='form-control' name="activated" id="activated">
								<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Enter Status -->
					<div class="form-group {{ $errors->has('enter') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="enter">enter</label>
						<div class="controls col-md-8">
							<select class='form-control' name="enter" id="enter">
								<option value="1"{{ (Input::old('enter', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('enter', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('enter', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Exit Status -->
					<div class="form-group {{ $errors->has('exit') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="exit">exit</label>
						<div class="controls col-md-8">
							<select class='form-control' name="exit" id="exit">
								<option value="1"{{ (Input::old('exit', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('exit', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('exit', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<!-- peripheralID -->
					<div class="form-group {{ $errors->has('peripheralID') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="peripheralID">Beacon Name</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="peripheralID" id="peripheralID" value="{{ Input::old('peripheralID') }}" />
							{{ $errors->first('peripheralID', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- proximityUUID -->
					<div class="form-group {{ $errors->has('proximityUUID') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="proximityUUID">ProximityUUID</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="proximityUUID" id="proximityUUID" value="{{ Input::old('proximityUUID') }}" />
							{{ $errors->first('proximityUUID', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- major -->
					<div class="form-group {{ $errors->has('major') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="major">Major</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="major" id="major" value="{{ Input::old('major') }}" />
							{{ $errors->first('major', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- minor -->
					<div class="form-group {{ $errors->has('minor') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="minor">Minor</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="minor" id="minor" value="{{ Input::old('minor') }}" />
							{{ $errors->first('minor', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- proximity -->
					<div class="form-group {{ $errors->has('proximity') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="proximity">Proximity</label>
						<div class="controls col-md-8">
							<select class='form-control' name="proximity" id="proximity">
								<option value="0"> immediate </option>
								<option value="1"> near </option>
								<option value="2"> far </option>
								<option value="3"> unknown </option>
							</select>
							{{ $errors->first('proximity', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				@if(Sentry::getUser()->hasAccess('subadmin'))
				<div class="col-md-6">
					<!-- Locals -->
					<div class="form-group {{ $errors->has('locals') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="locals">Local</label>
						<div class="controls col-md-8">
							<select class='form-control' name="local" id="local" style="width:80%">
								@foreach ($locals as $local)
									<option value="{{ $local->id }}">{{ $local->name }}</option>
								@endforeach
							</select>
							<span class="help-block">
								Select local the beacon belongs too.
							</span>
						</div>
					</div>
				</div>
				@endif

			</div>
		</div>
	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group form">
		<div class="controls form-actions">

			<button type="reset" class="btn default">Reset</button>

			<button type="submit" class="btn blue">Create Ibeacon</button>
		</div>
	</div>
</form>