@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Ibeacon Update ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class=' portlet box blue'>
<div class="page-header portlet-title">
	<h3>
		Ibeacon Update: {{ $ibeacon->name }}

		<div class="pull-right">
			<a href="{{ route('companyibeacons') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<div class='portlet-body tabbable-custom'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>
<form class="form-horizontal" method="post" action="" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="company_id" value="{{ $ibeacon->company_id }}" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-6">
					<!-- Name -->
					<div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="name">Name</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name', $ibeacon->name) }}"  />
							{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Description -->
					<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="description">Description</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description', $ibeacon->description) }}"   />
							{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Activation Status -->
					<div class="form-group {{ $errors->has('active') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="active">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Activate/Desactivate beacon">
								<i class="fa fa-info-circle"></i>
								Activated
							</span>
						</label>
						<div class="controls col-md-8">
							<input id="active" name='active' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{ ($ibeacon->isActive() ? ' checked' : '') }}>
							{{ $errors->first('active', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
					<!-- Enter Status -->
					<div class="form-group {{ $errors->has('enter') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="enter">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Send push message when detect beacon at entry">
								<i class="fa fa-info-circle"></i>
								Send notifications
							</span>
						</label>
						<div class="controls col-md-8">
							<input id="enter" name='enter' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{ ($ibeacon->isEnter() ? ' checked' : '') }}>
							{{ $errors->first('enter', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- Exit Status -->
					<!--
					<div class="form-group {{ $errors->has('exit') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="exit">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Send push message when detect beacon at exit">
								<i class="fa fa-info-circle"></i>
								Exit
							</span>
						</label>
						<div class="controls col-md-8">
							<input id="exit" name='exit' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{ ($ibeacon->isExit() ? ' checked' : '') }}>
							{{ $errors->first('exit', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
					-->
					<!-- Zone -->
					<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="zones_id">Zone</label>
						<div class="controls col-md-8">
							<select class='form-control' name='zones_id' id='zone'>
					    		@foreach ($locals as $local)
					    			@if($local->id == $selectedlocals)
					    				<option value="{{ $local->id }}" selected="selected">{{ $local->name }}</option>
					    			@else
					    				<option value="{{ $local->id }}">{{ $local->name }}</option>
					    			@endif
								@endforeach
					  		</select>
							{{ $errors->first('local', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>


				<div class="col-md-6">

					<!-- peripheralID -->
					<div class="form-group {{ $errors->has('peripheralID') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="peripheralID">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Phisical Beacon's Name">
								<i class="fa fa-info-circle"></i>
								PeripheralID
							</span>
						</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="peripheralID" id="peripheralID" value="{{ Input::old('peripheralID', $ibeacon->peripheralID) }}" />
							{{ $errors->first('peripheralID', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- proximityUUID -->
					<div class="form-group {{ $errors->has('proximityUUID') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="proximityUUID">ProximityUUID</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="proximityUUID" id="proximityUUID" value="{{ Input::old('proximityUUID', $ibeacon->proximityUUID) }}" {{(Sentry::getUser()->hasAccess('only_modify_beacon')) ? 'readonly="readonly" style="background-color:#cccccc"' : ''}}/>
							{{ $errors->first('proximityUUID', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- major -->
					<div class="form-group {{ $errors->has('major') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="major">Major</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="major" id="major" value="{{ Input::old('major', $ibeacon->major) }}" {{(Sentry::getUser()->hasAccess('only_modify_beacon')) ? 'readonly="readonly" style="background-color:#cccccc"' : ''}}/>
							{{ $errors->first('major', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- minor -->
					<div class="form-group {{ $errors->has('minor') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="minor">Minor</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="minor" id="minor" value="{{ Input::old('minor', $ibeacon->minor) }}" {{(Sentry::getUser()->hasAccess('only_modify_beacon')) ? 'readonly="readonly" style="background-color:#cccccc"' : ''}}/>
							{{ $errors->first('minor', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- proximity -->
					<div class="form-group {{ $errors->has('proximity') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="proximity">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Range detection for beacon (inmediate<=5, near=<10, far<=20) metres">
								<i class="fa fa-info-circle"></i>
								Proximity
							</span>
						</label>
						<div class="controls col-md-8">
							<select class='form-control' name="proximity" id="proximity" {{(Sentry::getUser()->hasAccess('only_modify_beacon')) ? '' : ''}} >
								<option value="immediate" {{ ($ibeacon->proximity === 'immediate' ? ' selected="selected"' : '') }}> immediate </option>
								<option value="near" {{ ($ibeacon->proximity === 'near' ? ' selected="selected"' : '') }}> near </option>
								<option value="far" {{ ($ibeacon->proximity === 'far' ? ' selected="selected"' : '') }}> far </option>
								<option value="unknown" {{ ($ibeacon->proximity === 'unknown' ? ' selected="selected"' : '') }}> unknown </option>
							</select>
							{{ $errors->first('proximity', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


				</div>

				@if(Sentry::getUser()->hasAccess('only_modify_beacon'))
				<div class="col-md-6">
					<!-- User -->
					<div class="form-group {{ $errors->has('users_id') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="locals">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Manager Operator Beacon for beacon">
								<i class="fa fa-info-circle"></i>
								User
							</span>
						</label>
						<div class="controls col-md-8">
							<select class='form-control' name="users_id" id="users_is">
								@foreach ($users as $user)
					    			@if($user->id == $ibeacon->users_id)
					    				<option value="{{ $user->id }}" selected="selected">{{ $user->email }}</option>
					    			@else
					    				<option value="{{ $user->id }}">{{ $user->email }}</option>
					    			@endif
								@endforeach
							</select>
							<!-- <span class="help-block">
								Select user the beacon belongs too.
							</span> -->
						</div>
					</div>
				</div>
				@endif

			</div>
		</div>
	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group form">
		<div class="controls form-actions">
			<a class="btn default" href="{{ route('companyibeacons') }}">Cancel</a>

			<button type="reset" class="btn btn-default">Reset</button>

			<button type="submit" class="btn blue">Update Ibeacon</button>
		</div>
	</div>
</form>
</div>
</div>

<br>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Done</h4>
      </div>
      <div class="modal-body" id="response">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script>

<!--Paint all the selects from the bootstrap-select -->
$('select').selectpicker();
//set hidden
$('#company').change(function() {
	console.log('changed');
	$("input[name='company_id']").val($('select').val());
});

$("#cover").fileinput({
	previewFileType: "image",
	browseClass: "btn btn-success",
	browseLabel: "Pick Image",
	browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
	removeClass: "btn btn-danger",
	removeLabel: "Delete",
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	uploadClass: "btn btn-info",
	uploadLabel: "Upload",
	uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
	uploadUrl: "{{route('cover-ibeacon')}}"
});

$('.input-group-btn a').click(function(e) {
    e.preventDefault();

    var urlAjax = $(this).attr('href');

    $.post(urlAjax,
	{
		ibeacon_id: '{{$ibeacon->id}}',
		cover: $("#cover").attr('base64')
	}).done(function(response)
	{
	  $('#holder').attr('src', $('.file-preview-image').attr('src'));
	  $('#response').html(response.msg);
	  $('#myModal').modal('show')
	})
	.fail(function(response) {
	  $('#response').html('failed to uplaod image');
	  $('#myModal').modal('show')
	})
	.always(function() {

	});

});

function readImage(evt, component) {
	var files = evt.target.files;
    var file = files[0];

    if (files && file) {

        var FR= new FileReader();

        FR.onload = function(readerEvt)
        {
              var binaryString = readerEvt.target.result;
              component.attr('base64',binaryString);
        };

        FR.readAsDataURL( file );
    }
}

$("#cover").change(function (e)
{
    readImage(e, $("#cover"));
});
</script>
@stop
