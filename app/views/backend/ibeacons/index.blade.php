@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
IBeacons Management ::
@parent
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/jquery-multi-select/css/multi-select.css"/>
@stop

{{-- Page content --}}
@section('content')
<div class='portlet-body tabbable-custom'>
<ul class="nav nav-tabs">
<li class="active">
	<a href="#tab_0" data-toggle="tab" aria-expanded="true">IBeacons Management</a>
</li>
@if ((Sentry::getUser()->hasAccess('admin')))
<li class="">
	<a href="#tab_1" data-toggle="tab" aria-expanded="false" class='btn green'>Create a New IBeacon<i class="fa fa-plus"></i></a>
</li>
@endif
</ul>
<div class='tab-content'>
	<div class='tab-pane active' id='tab_0'>
<div class="page-header portlet box blue">
	<div class='portlet-title'>
		<h3 class='pull-left'>IBeacons Management</h3>
		<div class="zones_dropdown pull-left">
			<select class="bs-select form-control input-small" data-style="blue" name='zones_dropdown' id='zones_dropdown'>
    				<option value="">All</option>
	    		@foreach ($locals as $local)
    				<option value="{{ $local->id }}">{{ $local->name }}</option>
				@endforeach
	  		</select>
		</div>
	</div>
</div>

<div class="text-center show_links">
	<a class="btn btn-primary btn-danger" data-show=''>Show IBeacons</a>
	<a class="btn btn-primary" data-show='withTrashed'>Include Deleted IBeacons</a>
	<a class="btn btn-primary" data-show='onlyTrashed'>Deleted IBeacons</a>
</div>

<div class="portlet box red">
<div class="portlet-body ">
<div class='table-container'>
	<table class="table table-striped table-bordered table-hover" id="datatable_ajax">
	    <thead>
	        <tr role="row" class="heading">
	            <th>@lang('admin/ibeacons/table.name')</th>
	            <th>@lang('admin/ibeacons/table.peripheralID')</th>
	            <th>@lang('admin/ibeacons/table.proximity')</th>
	            <th>@lang('admin/ibeacons/table.enter')</th>
	            <th>@lang('admin/ibeacons/table.exit')</th>
	            <th>@lang('admin/users/table.activated')</th>
	            <th class="sorting_disabled">@lang('table.actions')</th>
	        </tr>
	        <tr role="row" class="filter">
	            <td>
	                <input type="text" class="form-control form-filter input-sm" name="name">
	                <input type="hidden" class="form-control form-filter input-sm" name="show_value" value='' id='showValue'>
	                <input type="hidden" class="form-control form-filter input-sm" name="zones_value" value='' id='zones_value'>
	            </td>
	            <td>
	                <input type="text" class="form-control form-filter input-sm" name="peripheralID">
	            </td>
	            <td>
					<select name="proximity" class="form-control form-filter input-sm">
						<option value="">Select...</option>
						<option value="immediate">Immediate</option>
						<option value="far">Far</option>
						<option value="near">Near</option>
					</select>
	            </td>
	            <td>
					<select name="enter" class="form-control form-filter input-sm">
						<option value="">Select...</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
	            </td>
	            <td>
					<select name="exit" class="form-control form-filter input-sm">
						<option value="">Select...</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
	            </td>
	            <td>
					<select name="state" class="form-control form-filter input-sm">
						<option value="">Select...</option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
	            </td>
	            <td>
	                <div class="margin-bottom-5">
	                    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
	                </div>
	                <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
	            </td>
	        </tr>
	    </thead>
	    <tbody></tbody>
	</table>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>

	</div>
	<div class='tab-pane' id='tab_1'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Ibeacon</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>

	@include('backend/ibeacons/_form')
</div>
	</div>
</div>
</div>
@stop

@section('js')

<!-- common table for all pages-->
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assetsnew/global/scripts/datatable.js"></script>
<!-- table-ajax file for each page-->
<script type="text/javascript" src="/assetsnew/admin/pages/scripts/ajax/table-ajax-ibeacons.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="/assetsnew/admin/pages/scripts/components-dropdowns.js"></script>

@stop

@section('tableadvanced') 
    TableAjax.init();
    ComponentsDropdowns.init();
@stop

@section('javascript') 
    <script type="text/javascript">
	    //Show records
		$('.show_links').on('click','a', function (e) {
			var data_show = $(this).data('show');
			$(this).closest('.show_links').find('a').removeClass('btn-danger');
			$(this).addClass('btn-danger');
	        $('#showValue').val(data_show).trigger("change");
		});
	    $('#showValue').on('change', function () {
	        $(".filter-submit").trigger("click");
	    });
	    $('.filter-cancel').on('click', function () {
	        $('.show_links').find('a').removeClass('btn-danger');
	        $('.show_links a').first().addClass('btn-danger');
	        $("#zones_dropdown").val($("#zones_dropdown option:first").val()).change();
	    });
	    //Show record with zone
	    $('#zones_dropdown').on('change', function () {
	    	var zone_id = $(this).find("option:selected").val();
	        $('#zones_value').val(zone_id).trigger("change");
	    });
	    $('#zones_value').on('change', function () {
	        $(".filter-submit").trigger("click");
	    });
    </script>
@stop

