<form class="form-horizontal" method="post" action="{{route('create/ibeacon')}}" autocomplete="off" id='form_sample_2' novalidate="novalidate">
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-6">


					<!-- Name -->
					<div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="name">Name</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="name" id="name" value="{{ Input::old('name') }}" />
							{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Description -->
					<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="description">Description</label>
						<div class="controls col-md-8">
							<input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description') }}" />
							{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- Activation Status -->
					<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="activated">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Activate/Desactivate beacon">
								<i class="fa fa-info-circle"></i>
								Activated
							</span>
						</label>
						<div class="controls col-md-8">
							<input id="activated" name='activated' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('activated'))?'checked':''}}>
							<!-- <select class='form-control' name="activated" id="activated">
								<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select> -->
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Enter Status -->
					<div class="form-group {{ $errors->has('enter') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="enter">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Send push message when detect beacon at entry">
								<i class="fa fa-info-circle"></i>
								Send notifications
							</span>
						</label>
						<div class="controls col-md-8">
							<input id="enter" name='enter' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('enter'))?'checked':''}}>
							<!-- <select class='form-control' name="enter" id="enter">
								<option value="1"{{ (Input::old('enter', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('enter', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select> -->
							{{ $errors->first('enter', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Exit Status -->
					<!--
					<div class="form-group {{ $errors->has('exit') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="exit">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Send push message when detect beacon at exit">
								<i class="fa fa-info-circle"></i>
								Exit
							</span>
						</label>
						<div class="controls col-md-8">
							<input id="exit" name='exit' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('exit'))?'checked':''}}>
							{{ $errors->first('exit', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
					-->
					<!-- Company -->
					<div class="form-group {{ $errors->has('companies_id') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="companies_id">Company</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<select class='form-control' name='companies_id' id='companies_id' onChange='showUser();'>
								<option value="">Select One</option>
					    		@foreach ($companies as $company)
				    				<option value="{{ $company->id }}">{{ $company->name }}</option>
								@endforeach
					  		</select>
							{{ $errors->first('companies_id', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
					<!-- Zone -->
					<div class="form-group user-closest {{ $errors->has('zones_id') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="zones_id">Zone</label>
						<div class="controls col-md-8">
							<select class='form-control' name='zones_id' id='zone'>
					  		</select>
							{{ $errors->first('zones_id', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
					<!-- User -->
					<div class="form-group user-closest {{ $errors->has('users_id') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="users_id">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Manager Operator Beacon for beacon">
								<i class="fa fa-info-circle"></i>
								User
							</span>
						</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<select class='form-control' name="users_id" id="user">

							</select>
							{{ $errors->first('users_id', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<!-- peripheralID -->
					<div class="form-group {{ $errors->has('peripheralID') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="peripheralID">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Phisical Beacon's Name">
								<i class="fa fa-info-circle"></i>
								Beacon Name
							</span>
						</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="peripheralID" id="peripheralID" value="{{ Input::old('peripheralID') }}" />
							{{ $errors->first('peripheralID', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- proximityUUID -->
					<div class="form-group {{ $errors->has('proximityUUID') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="proximityUUID">ProximityUUID</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="proximityUUID" id="proximityUUID" value="{{ Input::old('proximityUUID') }}" />
							{{ $errors->first('proximityUUID', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- major -->
					<div class="form-group {{ $errors->has('major') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="major">Major</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="major" id="major" value="{{ Input::old('major') }}" />
							{{ $errors->first('major', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- minor -->
					<div class="form-group {{ $errors->has('minor') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="minor">Minor</label>
						<div class="controls col-md-8 input-icon right">
							<i class="icon fa"></i>
							<input class='form-control' type="text" name="minor" id="minor" value="{{ Input::old('minor') }}" />
							{{ $errors->first('minor', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- proximity -->
					<div class="form-group {{ $errors->has('proximity') ? 'error' : '' }}">
						<label class="control-label col-md-4" for="proximity">
							<span class='tooltips' data-container="body" data-placement="top" data-original-title="Range detection for beacon (inmediate<=5, near=<10, far<=20) metres">
								<i class="fa fa-info-circle"></i>
								Proximity
							</span>
						</label>
						<div class="controls col-md-8">
							<select class='form-control' name="proximity" id="proximity">
								<option value="1" {{(Session::get('proximity')==1)?'selected':''}}> immediate </option>
								<option value="2" {{(Session::get('proximity')==2)?'selected':''}}> near </option>
								<option value="3" {{(Session::get('proximity')==3)?'selected':''}}> far </option>
								<option value="4" {{(Session::get('proximity')==4)?'selected':''}}> unknown </option>
							</select>
							{{ $errors->first('proximity', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group form">
		<div class="controls form-actions">

			<button type="reset" class="btn default">Reset</button>

			<button type="submit" class="btn blue">Create Ibeacon</button>
		</div>
	</div>
</form>
@section('footer-script')
<script type="text/javascript">
	//Ajax load user and zone
	function showUser() {
		var optionSelected = $('#companies_id').find("option:selected");
    	var companyid = optionSelected.val();
    	if (companyid !=='') {
	    	$.ajax({
		        method: "post",
		        url: $('#form_sample_2').attr('action'),
		        data: {companyid: companyid},
			    success :function(zones_users){
			    	var useropts = "";
			    	var zoneopts = "";
			    	var users = zones_users.users;
			    	var zones = zones_users.zones;
					for (var i = 0; i < users.length; i++) {
					    useropts += "<option value='" + users[i].id + "'>" + users[i].first_name + ' ' + users[i].last_name + "</option>";
					}
					$(".user-closest #user").empty().append(useropts);
					for (var i = 0; i < zones.length; i++) {
					    if (zones[i].name=='Unassigned') {
					    	zoneopts += "<option value='" + zones[i].id + "' selected='selected'>" + zones[i].name + "</option>";
					    } else{
					    	zoneopts += "<option value='" + zones[i].id + "'>" + zones[i].name + "</option>";
					    };
					}
					$(".user-closest #zone").empty().append(zoneopts);
				}
			});
		}else $('.user-closest').find('select').empty();
	}
</script>
@stop