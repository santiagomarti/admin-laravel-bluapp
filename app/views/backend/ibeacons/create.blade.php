@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Create a Ibeacon ::
@parent
@stop

{{-- Page content --}}
@section('content')
	<div class='tab-pane' id='tab_1'>
<div class='portlet-body tabbable-custom'>
<div class='tab-content'>
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>Create a New Ibeacon
		<div class="pull-right">
			<a href="{{ route('ibeacons') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>
<div class='portlet-body'>
<!-- Tabs -->
<div class='tabbable-custom'>
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>
</div>

	@include('backend/ibeacons/_form')
</div>
</div>
</div>
	</div>
@stop


@section('javascript')
<script>

//set hidden
$('#company').change(function() {
	console.log('changed');
	$("input[name='company_id']").val($('select').val());
});

</script>
@stop


