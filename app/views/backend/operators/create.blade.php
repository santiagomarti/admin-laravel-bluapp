@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Create a Operator ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>
		Create a New Operator : Company

		<select   name='company' id='company'>
    		@foreach ($companies as $company)    			@if($company->id == $selectedCompany)
    				<option value="{{ $company->id }}" selected="selected">{{ $company->name }}</option>
    			@else
    				<option value="{{ $company->id }}">{{ $company->name }}</option>
    			@endif
			@endforeach
  		</select>

		<div class="pull-right">
			<a href="{{ route('operators') }}" class="btn btn-small btn-inverse"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>

<form class="form-horizontal" method="post" action="" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="company_id" value="{{$selectedCompany}}">

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-4">
					<!-- First Name -->
					<div class="control-group {{ $errors->has('first_name') ? 'error' : '' }}">
						<label class="control-label" for="first_name">First Name</label>
						<div class="controls">
							<input type="text" name="first_name" id="first_name" value="{{ Input::old('first_name') }}" />
							{{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Last Name -->
					<div class="control-group {{ $errors->has('last_name') ? 'error' : '' }}">
						<label class="control-label" for="last_name">Last Name</label>
						<div class="controls">
							<input type="text" name="last_name" id="last_name" value="{{ Input::old('last_name') }}" />
							{{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Email -->
					<div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
						<label class="control-label" for="email">Email</label>
						<div class="controls">
							<input type="text" name="email" id="email" value="{{ Input::old('email') }}" />
							{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password -->
					<div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							<input type="password" name="password" id="password" value="" />
							{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password Confirm -->
					<div class="control-group {{ $errors->has('password_confirm') ? 'error' : '' }}">
						<label class="control-label" for="password_confirm">Confirm Password</label>
						<div class="controls">
							<input type="password" name="password_confirm" id="password_confirm" value="" />
							{{ $errors->first('password_confirm', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Activation Status -->
					<div class="control-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label" for="activated">User Activated</label>
						<div class="controls">
							<select name="activated" id="activated">
								<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<!-- Locals -->
					<div class="control-group {{ $errors->has('locals') ? 'error' : '' }}">
						<label class="control-label" for="locals">Locals</label>
						<div class="controls">
							<select name="locals[]" id="locals[]" style="width:80%" multiple>
								@foreach ($locals as $local)
										<option value="{{ $local->id }}">{{ $local->name }}</option>
								@endforeach
							</select>
							<span class="help-block">
								Select locals the user belongs too.
							</span>
						</div>
					</div>
				</div>

		</div>

	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group">
		<div class="controls">
			<a class="btn btn-link" href="{{ route('operators') }}">Cancel</a>

			<button type="reset" class="btn">Reset</button>

			<button type="submit" class="btn btn-success">Create Operator</button>
		</div>
	</div>
</form>
@stop


@section('javascript')
<script type="text/javascript">

	<!--Paint all the selects from the bootstrap-select -->
	$('select').selectpicker();
	$('#company').change(function() {
		var url = "{{ URL::to('admin/operators?selectedCompany=')}}" + $('select').val();
		window.location = url;
	});
</script>
@stop
