@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Operator Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>
		Operator Management : Company

		<select   name='company' >
    		@foreach ($companies as $company)    			@if($company->id == $selectedCompany)
    				<option value="{{ $company->id }}" selected="selected">{{ $company->name }}</option>
    			@else
    				<option value="{{ $company->id }}">{{ $company->name }}</option>
    			@endif
			@endforeach
  		</select>

		<div class="pull-right">
			<a href="{{ route('create/operator') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Create</a>
		</div>
	</h3>
</div>

<!-- <a class="btn btn-default navbar-btn" href="{{ URL::to('admin/operators') }}">Show Operators</a>
<a class="btn btn-default navbar-btn" href="{{ URL::to('admin/operators?withTrashed=true') }}">Include Deleted Operators</a>
<a class="btn btn-default navbar-btn" href="{{ URL::to('admin/operators?onlyTrashed=true') }}">Include Only Deleted Operators</a>
 -->
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="span1">@lang('admin/users/table.id')</th>
			<th class="span2">@lang('admin/users/table.email')</th>
			<th class="span2">@lang('admin/users/table.first_name')</th>
			<th class="span2">@lang('admin/users/table.last_name')</th>
			<th class="span2">@lang('admin/users/table.activated')</th>
			<th class="span2">local</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($operators as $operator)
		<tr>
			<td>{{ $operator->id }}</td>
			<td>{{ $operator->email }}</td>
			<td>{{ $operator->first_name }}</td>
			<td>{{ $operator->last_name }}</td>

			<td>@lang('general.' . ($operator->isActivated() ? 'yes' : 'no'))</td>
			<!-- <td>
				@if ($operator->locals->count()>0) {{-- Some Users doesnt have associated a group --}}
					<?php $localClean = array(); ?>
					@foreach ($operator->locals as $local)
						<?php array_push($localClean, $local->name); ?>
					@endforeach
					{{ implode(",", $localClean) }}
				@else
					N/A
				@endif
			</td> -->
			<td>
				@if ($operator->groups->count()>0) {{-- Some Users doesnt have associated a group --}}
					<?php $groupClean = array(); ?>
					@foreach ($operator->groups as $group)
						<?php array_push($groupClean, $group->name); ?>
					@endforeach
					{{ implode(",", $groupClean) }}
				@else
					N/A
				@endif
			</td>
			<td>
				<a href="{{ route('update/operator', $operator->id) }}" class="btn btn-mini">@lang('button.edit')</a>

				@if ( ! is_null($operator->deleted_at))
					<a href="{{ route('restore/operator', $operator->id) }}" class="btn btn-mini btn-warning">@lang('button.restore')</a>
				@else
					<a href="{{ route('delete/operator', $operator->id) }}" class="btn btn-mini btn-danger">@lang('button.delete')</a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

@stop


@section('javascript')
<script type="text/javascript">

	<!--Paint all the selects from the bootstrap-select -->
	$('select').selectpicker();
	$('select').change(function() {
		var url = "{{ URL::to('admin/operators?selectedCompany=')}}" + $('select').val();
		window.location = url;
	});
</script>
@stop
