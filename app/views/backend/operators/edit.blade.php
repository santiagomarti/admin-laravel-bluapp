@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Operator Update ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="page-header">
	<h3>
		Operator Update: : Company

		<select name='company'>
    		@foreach ($companies as $company)
    			@if($company->id == $selectedCompany)
    				<option value="{{ $company->id }}" selected="selected">{{ $company->name }}</option>
    			@else
    				<option value="{{ $company->id }}">{{ $company->name }}</option>
    			@endif
			@endforeach
  		</select>

		<div class="pull-right">
			<a href="{{ route('operators') }}" class="btn btn-small btn-inverse"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
</ul>

<form class="form-horizontal" method="post" action="" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">

				<div class="col-md-4">

					<!-- First Name -->
					<div class="control-group {{ $errors->has('first_name') ? 'error' : '' }}">
						<label class="control-label" for="first_name">First Name</label>
						<div class="controls">
							<input type="text" name="first_name" id="first_name" value="{{ Input::old('first_name', $operator->first_name) }}" />
							{{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Last Name -->
					<div class="control-group {{ $errors->has('last_name') ? 'error' : '' }}">
						<label class="control-label" for="last_name">Last Name</label>
						<div class="controls">
							<input type="text" name="last_name" id="last_name" value="{{ Input::old('last_name', $operator->last_name) }}" />
							{{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Email -->
					<div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
						<label class="control-label" for="email">Email</label>
						<div class="controls">
							<input type="text" name="email" id="email" value="{{ Input::old('email', $operator->email) }}" />
							{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password -->
					<div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							<input type="password" name="password" id="password" value="" />
							{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Password Confirm -->
					<div class="control-group {{ $errors->has('password_confirm') ? 'error' : '' }}">
						<label class="control-label" for="password_confirm">Confirm Password</label>
						<div class="controls">
							<input type="password" name="password_confirm" id="password_confirm" value="" />
							{{ $errors->first('password_confirm', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Activation Status -->
					<div class="control-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label" for="activated">Operator Activated</label>
						<div class="controls">
							<select name="activated" id="activated">
								<option value="1"{{ ($operator->isActivated() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ ( ! $operator->isActivated() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>

				<div class="col-md-4">

					<!-- Locals -->
					<div class="control-group {{ $errors->has('locals') ? 'error' : '' }}">
						<label class="control-label" for="locals">Locals</label>
						<div class="controls">
							<select name="locals[]" id="locals[]" style="width:80%" multiple>
								@foreach ($locals as $local)
									@if ($userLocals->contains($local->id))
										<option value="{{ $local->id }}" selected="selected">{{ $local->name }}</option>
									@else
										<option value="{{ $local->id }}">{{ $local->name }}</option>
									@endif
								@endforeach
							</select>
							<span class="help-block">
								Select locals the user belongs too.
							</span>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group">
		<div class="controls">
			<a class="btn btn-link" href="{{ route('operators') }}">Cancel</a>

			<button type="reset" class="btn">Reset</button>

			<button type="submit" class="btn btn-success">Update Operator</button>
		</div>
	</div>
</form>

<br>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Done</h4>
      </div>
      <div class="modal-body" id="response">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script>

<!--Paint all the selects from the bootstrap-select -->
$('select').selectpicker();
//set hidden
$('select').change(function() {
	console.log('changed');
	$("input[name='company_id']").val($('select').val());
	location.href = '{{$urlChange}}' + $('select').val();
});


</script>
@stop
