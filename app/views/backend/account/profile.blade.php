@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Profile Management ::
@parent
@stop
@section('css')
<link href="/assetsnew/global/plugins/dropzone/css/dropzone.css" rel="stylesheet">
@stop

{{-- Page content --}}
@section('content')
<div class='portlet-body tabbable-custom'>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab_0" data-toggle="tab" aria-expanded="true">View Profile</a>
		</li>
		<li class="">
			<a href="#tab_1" data-toggle="tab" aria-expanded="false">Change Avatar</a>
		</li>
	</ul>
	<div class='tab-content'>
		<div class='tab-pane active' id='tab_0'>
			<div class="page-header portlet box blue">
				<h3 class='portlet-title'>
					Your Profile
				</h3>
			</div>
			<form class="form-horizontal form" method="post" autocomplete="off" action="{{route('profile')}}" id='form_sample_2' novalidate="novalidate">
				<div class="alert alert-danger display-hide">
					<button class="close" data-close="alert"></button>
					You have some form errors. Please check below.
				</div>
				<div class="alert alert-success display-hide">
					<button class="close" data-close="alert"></button>
					Your form validation is successful!
				</div>
				<!-- CSRF Token -->
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />

				<!-- First Name -->
				<div class="col-md-6 form-group {{ $errors->has('first_name') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="first_name">First Name</label>
					<div class="controls col-md-8 input-icon right">
						<i class="icon fa"></i>
						<input class='form-control' type="text" name="first_name" id="first_name" value="{{ Input::old('first_name', $profile->first_name) }}" />
						{{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Last Name -->
				<div class="col-md-6 form-group {{ $errors->has('last_name') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="last_name">Last Name</label>
					<div class="controls col-md-8 input-icon right">
						<i class="icon fa"></i>
						<input class='form-control' type="text" name="last_name" id="last_name" value="{{ Input::old('last_name', $profile->last_name) }}" />
						{{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- Groups -->
				<div class="col-md-6 form-group {{ $errors->has('groups') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="groups">Groups</label>
					<div class="controls col-md-8">
						<select class='form-control' name="groups[]" id="groups[]" disabled="">
							@foreach ($groups as $group)
								@if ($userGroups[0]==$group->name)
								<option value="{{ $group->id }}" selected>{{ $group->name }}</option>
								@else
								<option value="{{ $group->id }}">{{ $group->name }}</option>
								@endif
							@endforeach
						</select>
						<!--
						<span class="help-block">
							Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
						</span> -->
					</div>
				</div>

				<!-- Companies -->
				<div class="col-md-6 form-group {{ $errors->has('companies') ? 'error' : '' }}">
					<label class="control-label col-md-4" for="companies">Companies</label>
					<div class="controls col-md-8">
						@if (Sentry::getUser()->inGroup(Sentry::findGroupByName('Admin')))
						<select class='form-control' name="companies_id" id="companies[]" disabled>
							<option value="{{$usercompany[0]}}">{{$usercompany[0]}}</option>
						</select>
						@else
						<select class='form-control' name="companies_id" id="companies[]" disabled>
							@foreach ($companies as $companyid=>$companyname)
								@if ($usercompany[0]==$companyname)
								<option value="{{ $companyid }}" selected>{{ $companyname }}</option>
								@else
								<option value="{{ $companyid }}">{{ $companyname }}</option>
								@endif
							@endforeach
						</select>
						@endif <!--
						<span class="help-block">
							Select companies the user belongs too.
						</span>-->
					</div>
				</div>
				<!-- Form Actions -->
				<div class="col-xs-12 control-group" style="padding: 0;">
					<div class="controls form-actions">
						<button type="submit" class="btn blue">Save profile</button>
					</div>
				</div>

			</form>
		</div>
		<div class='tab-pane' id='tab_1'>
			<div class="page-header portlet box blue">
				<h3 class='portlet-title'>Change Avatar</h3>
			</div>
			<!-- Tabs Content -->
			<div class="tab-content">
				<!-- General tab -->
				<div class="tab-pane active" id="tab-general">
					<form action="{{route('avatar')}}" class='form form-horizontal' method='post'>
						<div class="row">
							<div class='col-md-6'>
								<!-- Upload new Avatar -->
								<label class="control-label col-md-4" for="avatar">Upload new avatar</label>
								<div class="controls col-md-8">
									<div class="dropzone" id="my-dropzone"></div>
								</div>
							</div>
							<div class='col-md-6'>
								<!-- Avatar -->
								<label class="control-label col-md-4" for="avatar">Your old avatar</label>
								<div class="controls col-md-8">
									<img src="/avatars/{{$profile->gravatar}}" alt='avatar'>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn blue button-submit">
									Save Avatar
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('js')
<script src="/assetsnew/global/plugins/dropzone/dropzone.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-dropzone.js"></script>
@stop
@section('tableadvanced')
FormDropzone.init();
@stop