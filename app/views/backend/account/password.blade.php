@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Profile Management ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="page-header">
	<h3>
		<div class="btn-group">
			 <a href="profile" class="btn btn-default {{Request::is('account/profile') ? 'active' : '' }}">
                    Change Profile
             </a>
			 <a href="change-password" class="btn btn-default {{Request::is('account/change-password') ? 'active' : '' }}">
                    Change Password
             </a>
			 <a href="change-email" class="btn btn-default {{Request::is('account/change-email') ? 'active' : '' }}">
                    Change Email
             </a>
		</div>
	</h3>
</div>


<form method="post" action="" class="form-horizontal" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Old Password -->
	<div class="control-group{{ $errors->first('old_password', ' error') }}">
		<label class="control-label" for="old_password">Old Password</label>
		<div class="controls">
			<input type="password" name="old_password" id="old_password" value="" />
			{{ $errors->first('old_password', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<!-- New Password -->
	<div class="control-group{{ $errors->first('password', ' error') }}">
		<label class="control-label" for="password">New Password</label>
		<div class="controls">
			<input type="password" name="password" id="password" value="" />
			{{ $errors->first('password', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<!-- Confirm New Password  -->
	<div class="control-group{{ $errors->first('password_confirm', ' error') }}">
		<label class="control-label" for="password_confirm">Confirm New Password</label>
		<div class="controls">
			<input type="password" name="password_confirm" id="password_confirm" value="" />
			{{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
		</div>
	</div>

	<hr>

	<!-- Form actions -->
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn">Update Password</button>

			<a href="{{ route('forgot-password') }}" class="btn btn-link">I forgot my password</a>
		</div>
	</div>
</form>

@stop
