@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Avatar Management ::
@parent
@stop
@section('css')
<link href="/assetsnew/global/plugins/dropzone/css/dropzone.css" rel="stylesheet">
@stop

{{-- Page content --}}
@section('content')
<div class='portlet-body tabbable-custom'>
	<div class='tab-content'>
		<div class="page-header portlet box blue">
			<h3 class='portlet-title'>Change Avatar</h3>
		</div>
		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<form action="{{route('avatar')}}" class='form form-horizontal' method='post'>
					<div class="row">
						<div class='col-md-6'>
							<!-- Upload new Avatar -->
							<label class="control-label col-md-4" for="avatar">Upload new avatar</label>
							<div class="controls col-md-8">
								<div class="dropzone" id="my-dropzone"></div>
							</div>
						</div>
						<div class='col-md-6'>
							<!-- Avatar -->
							<label class="control-label col-md-4" for="avatar">Your old avatar</label>
							<div class="controls col-md-8">
								<img src="/avatars/{{$profile->gravatar}}" alt='avatar'>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn blue button-submit">
								Save Avatar
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@stop

@section('js')
<script src="/assetsnew/global/plugins/dropzone/dropzone.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-dropzone.js"></script>
@stop
@section('tableadvanced')
FormDropzone.init();
@stop