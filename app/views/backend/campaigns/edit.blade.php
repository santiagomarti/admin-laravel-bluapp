@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Ad Update
@parent
@stop

{{-- Page content --}}
@section('content')
<div class=' portlet box blue'>
<div class="page-header portlet-title">
	<h3>
		Campaign Update

		<div class="pull-right">
			<a href="{{ route('campaigns') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>


<div class='portlet-body' id="form_wizard_1">

<form id="submit_form" class="form-horizontal form campaigns" enctype="multipart/form-data" method="post" action="{{route('update/campaign', $ad->id)}}" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<div class="form-wizard">
		<div class="form-body">
			<ul class="nav nav-pills nav-justified steps">
				<li>
					<a href="#tab1" data-toggle="tab" class="step">
					<span class="number">
					1 </span>
					<span class="desc">
					<i class="fa fa-check"></i> Basic data </span>
					</a>
				</li>
				<li>
					<a href="#tab2" data-toggle="tab" class="step">
					<span class="number">
					2 </span>
					<span class="desc">
					<i class="fa fa-check"></i> Media settings </span>
					</a>
				</li>
			</ul>
			<div id="bar" class="progress progress-striped" role="progressbar">
				<div class="progress-bar progress-bar-success">
				</div>
			</div>
			<!-- <div id="bar" class="progress progress-striped" role="progressbar">
				<div class="progress-bar progress-bar-success">
				</div>
			</div> -->
			<div class="tab-content">
				<div class="alert alert-danger display-none">
					<button class="close" data-dismiss="alert"></button>
					You have some form errors. Please check below.
				</div>
				<div class="alert alert-success display-none">
					<button class="close" data-dismiss="alert"></button>
					Your form validation is successful!
				</div>
				<div class="tab-pane active" id="tab1">
					<div class="col-md-6">
						<!-- Operator -->
						<div class="form-group {{ $errors->has('operator') ? 'error' : '' }}" style='display:none;'>
							<label class="control-label col-md-4" for="operator">Company/Operator</label>
							<div class="controls col-md-8" style='padding-top:8px;'>{{$profile->first_name}} {{$profile->last_name}}</div>
						</div>
						<!-- Ibeacon -->
						<div class="form-group {{ $errors->has('ibeacon') ? 'error' : '' }}">
							<label class="control-label col-md-4" for="ibeacon">Ibeacon</label>
							<div class="controls col-md-8 input-icon right" >
								<i class="icon fa"></i>
								<select class='form-control' name="ibeacons_id" id="ibeacon" onchange='showProximity();'>
									@foreach ($beacons as $ibeacon)
						    			@if($ibeacon->id == $ad->ibeacons_id)
						    				<option value="{{ $ibeacon->id }}" selected="selected" data-proximityUUID="{{ $ibeacon->proximityUUID . ' Major:' . $ibeacon->major . ' Minor' . $ibeacon->minor }}">{{ $ibeacon->peripheralID }}</option>
						    			@else
						    				<option value="{{ $ibeacon->id }}" data-proximityUUID="{{ $ibeacon->proximityUUID . ' Major:' . $ibeacon->major . ' Minor:' . $ibeacon->minor }}">{{ $ibeacon->peripheralID }}</option>
						    			@endif
									@endforeach
								</select>

								{{ $errors->first('ibeacon', '<span class="help-inline">:message</span>') }}
							</div>
						</div>
						<!-- Tittle -->
						<div class="form-group {{ $errors->has('title') ? 'error' : '' }}">
							<label class="control-label col-md-4" for="title">Title</label>
							<div class="controls col-md-8 input-icon right">
								<i class="icon fa"></i>
								<input class='form-control' type="text" name="title" id="title" value="{{ Input::old('title', $ad->title) }}" />
								{{ $errors->first('title', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						


						<!-- Description -->
						<div class="form-group {{ $errors->has('description') ? 'error' : '' }}">
							<a class="btn btn-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">More</a>
							<div class="collapse" id="collapseExample">
								<label class="control-label col-md-4" for="description">Description</label>
								<div class="controlsv col-md-8">
									<textarea class='form-control' type="text" name="description" id="description" rows="3"/>{{ Input::old('description', $ad->description) }}</textarea>
									{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
								</div>
								<!-- Cover -->
								<div class="form-group {{ $errors->has('cover') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="cover" style="padding-top: 21px">
										<span class='tooltips' data-container="body" data-placement="top" data-original-title="Cover url (for some apps only)">
											<i class="fa fa-info-circle"></i>
											Cover
										</span>
									</label>
									<div class="controls col-md-6 input-icon" style="padding-top: 21px">
										<div class="fileinput fileinput-new col-md-8" data-provides="fileinput" style="padding:5px ! important;">
										  <div class="fileinput-preview thumbnail " data-trigger="fileinput" style="width: 300px; height: 200px;">
										  	@if($cover != '')
										  		<img src="{{ URL::to('/') }}/covers/{{ $cover }}" alt="{{$type}}">
										  	@endif
										  </div>
										  <div <span class="fileinput-filename"></span></div>
										  <div>
										    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="cover-img"></span>
										    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
										  </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					
					</div>
					<div class="col-md-6">
						<!-- Display Status -->
						<div class="form-group {{ $errors->has('bookshelf') ? 'error' : '' }}" style='display:none;'>
							<label class="control-label col-md-4" for="bookshelf">Display Bookshelf</label>
							<div class="controls col-md-8">
								<select class='form-control' name="bookshelf" id="bookshelf">
									<option value="1"{{ ($ad->isBookshelf() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
									<option value="0"{{ ( ! $ad->isBookshelf() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
								</select>
								{{ $errors->first('bookshelf', '<span class="help-inline">:message</span>') }}
							</div>
						</div>
						<!-- Proximity -->
						<div class="proximity form-group {{ $errors->has('proximity') ? 'error' : '' }}">
							<label class="control-label col-md-4" for="proximity">Proximity</label>
							<span class="col-md-8 control-label radio-list">
								<label class="radio-inline">
								  <input type="radio" name="proximity" id="inlineRadio1" value="1" disabled {{ ($proximity==='immediate' ? ' checked="checked"' : '') }}> Immediate
								</label>
								<label class="radio-inline">
								  <input type="radio" name="proximity" id="inlineRadio2" value="2" disabled {{ ($proximity==='near' ? ' checked="checked"' : '') }}> Near
								</label>
								<label class="radio-inline">
								  <input type="radio" name="proximity" id="inlineRadio3" value="3" disabled ($proximity==='far' ? ' checked="checked"' : '') }}> Far
								</label>
							</span>
						</div>
						<!-- Activation Status -->
					<!--	<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
							<label class="control-label col-md-4" for="activated">Campaign Activated</label>
							<div class="controls col-md-8">
								<input name='activated' type="checkbox" class="make-switch" data-size="normal" data-on-text="{{Lang::get('general.yes')}}" data-off-text="{{Lang::get('general.no')}}"{{ ($ad->isActive() ? ' checked="checked"' : '') }}>
								{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
							</div>
						</div>-->
						<!-- Date Range -->
						<div class="form-group">
							<label class="control-label col-md-4">Date Range</label>
							<div class="col-md-8">
								<div class="input-group">
									<div class="date form_datetime input-icon right">
										<i class="icon fa"></i>
										<input type="text" size="16" readonly class="form-control" name="begin_date" value='{{$begin_date}}' style='cursor: pointer;'>
										<span class="input-group-btn">
										<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<span class="input-group-addon">
									to </span>
									<div class="date form_datetime input-icon right">
										<i class="icon fa"></i>
										<input type="text" size="16" readonly class="form-control" name="end_date"value='{{$end_date}}' style='cursor: pointer;'>
										<span class="input-group-btn">
										<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane row" id="tab2">
						<div class="col-md-6">
							<!-- Type 'image','video','sound','html'  -->
							<div class="form-group {{ $errors->has('type') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="type">Media Type</label>
								<div class="controls col-md-8">
									<select class='form-control' name="type" id="type">
										@foreach ($types as $type)
							    			@if($type == $realtype)
							    				<option value="{{ $type }}" selected="selected">{{ $type }}</option>
							    			@else
							    				<option value="{{ $type }}">{{ $type }}</option>
							    			@endif
										@endforeach
									</select>
									{{ $errors->first('type', '<span class="help-inline">:message</span>') }}
								</div>
							</div>
							<div class="form-group" id="placeHolderMediaControl">
							</div>
						</div>

						<div class="col-md-6" style='padding-bottom: 18px;'>
							<!-- Msg. Enter Preview-->
							<div class="form-group {{ $errors->has('msg_enter') ? 'error' : '' }}" style='display:none;'>
								<label class="control-label col-md-4" for="msg_enter" style="padding-top: 24px;">Msg. Enter</label>
								<div class="controls col-md-8">
									<div class='form-control' type="text" name="msg_enter" id="msg_enter" value="" style="height: 68px;" >
										<div class='col-md-3'>
											<img src="/avatars/{{$ad->cover}}" id='img_preview'>
										</div>
										<div class='col-md-9' id='msg_enter_preview'>{{$ad->msg_enter}}</div>
									</div>
								</div>
							</div>
							<!-- Cover -->
							<div class="form-group {{ $errors->has('cover') ? 'error' : '' }}" style='display:none;'>
								<label class="control-label col-md-4" for="cover">Cover</label>
								<div class="controls col-md-8">
									<input class='form-control' id="cover"  name="cover" type="file" accept="image/*" onchange="PreviewImage();">
									{{ $errors->first('cover', '<span class="help-inline">:message</span>') }}
								</div>
							</div>
							
							<!-- Promote Status -->
							<div class="form-group {{ $errors->has('promote') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="promote">Promote Activated</label>
								<div class="controls col-md-8 radio-list control-label">
									<input name='promote' type="checkbox" class="make-switch" data-size="normal" data-on-text="{{Lang::get('general.yes')}}" data-off-text="{{Lang::get('general.no')}}"
									{{ ($ad->isPromote() ? ' checked="checked"' : '') }}>
									{{ $errors->first('promote', '<span class="help-inline">:message</span>') }}
								</div>
							</div>

							<!-- Msg. Enter -->
							<div class="form-group {{ $errors->has('msg_enter') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="msg_enter">Push Message</label>
								<div class="controls col-md-8 input-icon right">
									<i class="icon fa"></i>
									<input class='form-control' type="text" name="msg_enter" id="maxlength_thresholdconfig" value="{{ Input::old('msg_enter', $ad->msg_enter) }}" onkeyup="document.getElementById('msg_enter_preview').innerHTML = this.value" maxlength="30" />
									{{ $errors->first('msg_enter', '<span class="help-inline">:message</span>') }}
								</div>
							</div>

							<!-- More -->
							<a class="btn btn-info" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">More</a>
							
							<div class="collapse" id="collapseExample2">

								<div class="collapse2" id="collapseExample3">

								
								<!-- TTL. Enter -->
								<div class="form-group {{ $errors->has('ttl_enter') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="ttl_enter">Repeat Push Each</label>
									<div class="controls col-md-8">
										<div class='input-group' id='spinner1'>
											<input readonly class='spinner-input form-control' type="number" name="ttl_enter" id="ttl_enter" value="{{ Input::old('ttl_enter', $ad->ttl_enter) }}" />
											<span class="input-group-addon">
											<i class="fa">hrs</i>
											</span>
											<div class="spinner-buttons input-group-btn btn-group-vertical">
												<button type="button" class="btn spinner-up btn-xs blue">
												<i class="fa fa-angle-up"></i>
												</button>
												<button type="button" class="btn spinner-down btn-xs blue">
												<i class="fa fa-angle-down"></i>
												</button>
											</div>
										</div>
										{{ $errors->first('ttl_enter', '<span class="help-inline">:message</span>') }}
									</div>
								</div>
								<!-- Msg. Exit -->
								<!--
								<div class="form-group {{ $errors->has('msg_exit') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="msg_exit">Message Exit</label>
									<div class="controls col-md-8">
										<input class='form-control' type="text" name="msg_exit" id="maxlength_thresholdconfig2" value="{{ Input::old('msg_exit', $ad->msg_exit) }}" maxlength="30"  />
										{{ $errors->first('msg_exit', '<span class="help-inline">:message</span>') }}
									</div>
								</div>
								-->
								<!-- TTL. Exit -->
								<!--
								<div class="form-group {{ $errors->has('ttl_exit') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="ttl_exit">Repeat Message Exit Each</label>
									<div class="controls col-md-8">
										<div class='input-group' id='spinner11'>
											<input readonly class='spinner-input form-control' type="number" name="ttl_exit" id="ttl_exit" value="{{ Input::old('ttl_exit', $ad->ttl_exit) }}" />
											<span class="input-group-addon">
											<i class="fa">hrs</i>
											</span>
											<div class="spinner-buttons input-group-btn btn-group-vertical">
												<button type="button" class="btn spinner-up btn-xs blue">
												<i class="fa fa-angle-up"></i>
												</button>
												<button type="button" class="btn spinner-down btn-xs blue">
												<i class="fa fa-angle-down"></i>
												</button>
											</div>
										</div>
										{{ $errors->first('ttl_exit', '<span class="help-inline">:message</span>') }}
									</div>
								</div>
								-->
							</div>
						</div>
						
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-12">
					<a class="btn default" href="{{ route('campaigns') }}">Cancel</a>
					<!--<button type="reset" class="btn btn-default">Reset</button>-->
					<a href="javascript:;" class="btn default button-previous">
					<i class="m-icon-swapleft"></i> Back </a>
					<a href="javascript:;" class="btn blue button-next">
					Next <i class="m-icon-swapright m-icon-white"></i>
					</a>
					<button type="submit" class="btn blue button-submit">
					Save Campaign
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
</div>

<br>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Done</h4>
      </div>
      <div class="modal-body" id="response">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="templates-page">

	<div class="form-group {{ $errors->has('covertype') ? 'error' : '' }}" style="display:none" id="templateImageTarget">
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label col-md-4" for="url">
				<span data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="URL for Image">
					<i class="fa fa-info-circle" ></i>
					Image
				</span>
			</label>
			<div class="fileinput fileinput-new col-md-8" data-provides="fileinput" >
			  <div class="fileinput-preview thumbnail col-md-8" data-trigger="fileinput" style="width: 100%; height: 250px;">
			  	@if($ad->type == 'image')
			  		<img src="{{ URL::to('/') }}/contentimages/{{ $url }}" alt="Not available.">
			  	@endif
			  </div>
			  <div <span class="fileinput-filename"></span></div>
			  <div>
			    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="image-input"></span>
			    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
			  </div>
			</div>
		</div>
	</div>

	<div class="form-group" style="display:none" id="templateURLTarget">
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label col-md-4" for="url">
				<span data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="URL for Image">
					<i class="fa fa-info-circle" ></i>
					Web address
				</span>
			</label>
			<div class="controls col-md-8">
			@if($ad->type == 'html-url')
				<input class='form-control' type="text" name="url" id="url" value="{{ $url }}" />
			@else 
				<input class='form-control' type="text" name="url" id="url" placeholder="htt://www.bluapp.cl" />
			@endif
				{{ $errors->first('url', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
	</div>

	<div class="form-group" style="display:none" id="templateVideoTarget">
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label col-md-4" for="url">
				<span data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="URL for Image">
					<i class="fa fa-info-circle" ></i>
					Video address
				</span>
			</label>
			<div class="controls col-md-8">
				@if($ad->type == 'video')
					<input class='form-control' type="text" name="video" id="video" value="{{ $url }}" />
				@else 
					<input class='form-control' type="text" name="video" id="video" placeholder="htt://www.youtube.com" />
				@endif
					{{ $errors->first('url', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
	</div>

</div>

@stop

@section('wysihtml5css')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->
<link href="/assetsnew/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="/assetsnew/global/plugins/dropzone/css/dropzone.css" rel="stylesheet">
@stop
@section('wysihtml5js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/select2/select2.min.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-wizard.js"></script>

<script src="/assetsnew/admin/pages/scripts/components-pickers.js"></script>

<!-- Scripts de Santiago -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<!-- END -->

<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="/assetsnew/global/plugins/dropzone/dropzone.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-dropzone2.js"></script>
@stop
@section('tableadvanced')
   	FormWizard.init();
   	ComponentsPickers.init();
   	FormDropzone.init();
@stop
@section('javascript')
<script>

	//Ajax load proximity
	function showProximity() {
		var optionSelected = $('#ibeacon').find("option:selected");
    	var beaconid = optionSelected.val();
    	if (beaconid !=='') {
	    	$.ajax({
	        type: "get",
	        url: $('#submit_form').attr('action'),
	        dataType:"html",
	        data: {beaconid: beaconid},
		    success :function(proximitySelected){
		    	var value = JSON.parse(proximitySelected);
		    	if (value==='far') {
		    		$('.proximity .radio-inline input').parent().removeClass('checked');
		    		$('.proximity #inlineRadio3').parent().addClass('checked');
		    	}else if (value==='near') {
		    			$('.proximity .radio-inline input').parent().removeClass('checked');
			    		$('.proximity #inlineRadio2').parent().addClass('checked');
		    		}else if (value==='immediate') {
		    				$('.proximity .radio-inline input').parent().removeClass('checked');
				    		$('.proximity #inlineRadio1').parent().addClass('checked');
	    				}
	    		}
			});
		}else $('.proximity .radio-inline input').parent().removeClass('checked');
	}

	//Reset
	$("button[type='reset']").click(function() {
		$('#img_preview').attr('src','/avatars/{{$ad->cover}}');
		$('#frameurl').attr('src','{{$ad->url}}');
		$('#msg_enter_preview').text('{{$ad->msg_enter}}');
		changeControl('{{$ad->type}}');
	})

	//Preview htmlpreview
	function htmlpreview() {
		$('.media_preview > div').show();
		$('.htmlcontent').html($('.textarea').val());
	}

	//Preview url
	function urlpreview() {
		$('.media_preview > div').show();
		$('#frameurl').attr('src',$('#url').val());
	}

	//Preview image
    function PreviewCover() {
    	var src = $('.dropzone .dz-preview .dz-details img').attr('src');
    	$('#holder-covertype').attr('src', src);
    };

var changeControl = function(controlValue)
	{
		//Change the control on the fly, for images, html

		if (controlValue === 'html-url')
		{
			$('#placeHolderMediaControl').html($('#templateURLTarget').html());
		}

		if (controlValue == 'video')
		{
			$('#placeHolderMediaControl').html($('#templateVideoTarget').html());
		}
		/*
		if (controlValue === 'html-embeded')
		{
			$('.htmlcontent').empty();
			$('#placeHolderMediaControl').html($('#templateHtmlTarget').html());
			$('#placeHolderMediaControl .textarea').wysihtml5();
		}*/

		/*
		if (controlValue === 'sound' )
		{
			$('#placeHolderMediaControl').html('<br><br> comming soon ... <br><br>');
		}*/

		if (controlValue === 'image')
		{
			$('#placeHolderMediaControl').html($('#templateImageTarget').html());
		}
	}

	//force changeControl
	changeControl($('#type').val());

	//on chage select type, change control
	$('#type').change(function()
	{
		changeControl($('#type').val());
	});

//============================================== Cover
//utils for upload component
function readImage(evt, component) {
	var files = evt.target.files;
    var file = files[0];

    if (files && file) {

        var FR= new FileReader();

        FR.onload = function(readerEvt)
        {
              var binaryString = readerEvt.target.result;
              component.attr('base64',binaryString);
        };

        FR.readAsDataURL( file );
    }
}

$("#ibeacon").change(function (e)
{
	var proximityUUID = $("#ibeacon option:selected").attr('data-proximityUUID');
    $('#labelProximityUUID').text(proximityUUID);
});

</script>
@stop
