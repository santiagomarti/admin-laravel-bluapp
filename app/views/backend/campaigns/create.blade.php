@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Create Ad
@parent
@stop

{{-- Page content --}}
@section('content')
	<div class='tab-pane' id='tab_1'>
<div class='portlet-body tabbable-custom'>
<div class='tab-content' id="form_wizard_1">
<div class="page-header portlet box blue">
	<h3 class='portlet-title'>
		Create a New Campaign

		<div class="pull-right">
			<a href="{{ route('campaigns') }}" class="btn btn-small btn-inverse default"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>
	@include('backend/campaigns/_form')

</div>
</div>
	</div>


@stop
@section('tableadvanced')
   	FormWizard.init();
   	ComponentsPickers.init();
   	FormDropzone.init();
@stop


