
<div class="portlet-body form campaigns">
	<form class="form-horizontal" id="submit_form" enctype="multipart/form-data" method="post" action="{{route('create/campaign')}}" autocomplete="off">
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
						<span class="number">
						1 </span>
						<span class="desc" id="basicdata">
						<i class="fa fa-check"></i> Basic data </span>
						</a>
					</li>
					<li>
						<a href="#tab2" data-toggle="tab" class="step">
						<span class="number">
						2 </span>
						<span class="desc" id="mediasettings">
						<i class="fa fa-check"></i> Media settings </span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success">
					</div>
				</div>
				<!-- <div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success">
					</div>
				</div> -->
				<div class="tab-content">
					<div class="alert alert-danger display-none">
						<button class="close" data-dismiss="alert"></button>
						You have some form errors. Please check below.
					</div>
					<div class="alert alert-success display-none">
						<button class="close" data-dismiss="alert"></button>
						Your form validation is successful!
					</div>
					<div class="tab-pane active" id="tab1">
						<div class="col-md-6">
							<!-- Operator -->
							<div class="form-group {{ $errors->has('operator') ? 'error' : '' }}" style='display:none;'>
								<label class="control-label col-md-4" for="operator">Company/Operator</label>
								<div class="controls col-md-8" style='padding-top:8px;'>{{$profile->first_name}} {{$profile->last_name}}</div>
							</div>
							<!-- Ibeacon -->
							<div class="form-group {{ $errors->has('ibeacon') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="ibeacon">Ibeacon</label>
								<div class="controls col-md-8 input-icon right" >
									<i class="icon fa"></i>
									<select class='form-control' name="ibeacons_id" id="ibeacon" onchange="showProximity();">
										<option value="">--Select One--</option>
										@foreach ($ibeacons as $ibeacon)
						    			<option value="{{ $ibeacon['id'] }}" data-proximityUUID="{{ $ibeacon['proximityUUID'] . ' Major:' . $ibeacon['major'] . ' Minor:' . $ibeacon['minor'] }}" 
						    			{{(Session::get('ibeacons_id')==$ibeacon['id'])?'selected="selected"':''}}>{{ $ibeacon['peripheralID'] }}</option>
										@endforeach
									</select>
									{{ $errors->first('ibeacon', '<span class="help-inline">:message</span>') }}
								</div>
							</div>
							<!-- Tittle -->
							<div class="form-group {{ $errors->has('title') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="Title">
									<span class='tooltips' data-container="body" data-placement="top" data-original-title="Campaign Name">
										<i class="fa fa-info-circle"></i>
										Title
									</span>
								</label>
								<div class="controls col-md-8 input-icon right">
									<i class="icon fa"></i>
									<input class='form-control' type="text" name="title" id="title" value="{{ Input::old('title') }}" />
									{{ $errors->first('title', '<span class="help-inline">:message</span>') }}
								</div>
							</div>
							<!-- Description -->
							<div class="more form-group {{ $errors->has('description') ? 'error' : '' }}">
								<a class="btn btn-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">More</a>
								<div class="collapse" id="collapseExample">
									<label class="control-label col-md-4" for="description">Description</label>
									<div class="controls col-md-8">
										<textarea class='form-control' name="description" id="description" rows="3" >{{ Input::old('description') }}</textarea>
										<!-- <input class='form-control' type="text" name="description" id="description" value="{{ Input::old('description') }}" /> -->
										{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- Display Status -->
							<div class="form-group {{ $errors->has('bookshelf') ? 'error' : '' }}" style='display:none;'>
								<label class="control-label col-md-4" for="bookshelf">Display Bookshelf</label>
								<div class="controls col-md-8">
									<select class='form-control' name="bookshelf" id="bookshelf">
										<option value="1" selected="selected">@lang('general.yes')</option>
										<option value="0">@lang('general.no')</option>
									</select>
									{{ $errors->first('bookshelf', '<span class="help-inline">:message</span>') }}
								</div>
							</div>
							<!-- Proximity -->
							<div class="proximity form-group {{ $errors->has('proximity') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="proximity">
									<span class='tooltips' data-container="body" data-placement="top" data-original-title="Detection distance for beacon">
										<i class="fa fa-info-circle"></i>
										Proximity
									</span>
								</label>
								<span class="col-md-8 control-label radio-list">
									<label class="radio-inline">
									  <input type="radio" name="proximity" id="inlineRadio1" value="immediate" disabled {{(Session::get('proximity')=='immediate')?'checked':''}}> Immediate
									</label>
									<label class="radio-inline">
									  <input type="radio" name="proximity" id="inlineRadio2" value="near" disabled {{(Session::get('proximity')=='near')?'checked':''}}> Near
									</label>
									<label class="radio-inline">
									  <input type="radio" name="proximity" id="inlineRadio3" value="far" disabled {{(Session::get('proximity')=='far')?'checked':''}}> Far
									</label>
								</span>
							</div>
							<!-- Activation Status -->
						<!--	<div class="form-group {{ $errors->has('activated') ? 'error' : '' }}">
								<label class="control-label col-md-4" for="active">
									<span class='tooltips' data-container="body" data-placement="top" data-original-title="Activate/Desactivate Campaign">
										<i class="fa fa-info-circle"></i>
										Campaign Activated
									</span>
								</label>
								<div class="controls col-md-8">
									<input name='active' type="checkbox" class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('active'))?'checked':''}}>
									{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
								</div>
							</div> -->
							<!-- Date Range -->
							<div class="form-group">
								<label class="control-label col-md-4">
									<span class='tooltips' data-container="body" data-placement="top" data-original-title="Activate Period time for activate campaign">
										<i class="fa fa-info-circle"></i>
										Date Range
									</span>
								</label>
								<div class="col-md-8">
									<div class="input-group">
										<div class="date form_datetime input-icon right">
											<i class="icon fa"></i>
											<input type="text" size="16" readonly class="form-control" name="begin_date" value='{{Session::get('begin_date')}}' style='cursor: pointer;'>
											<span class="input-group-btn">
											<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
										<span class="input-group-addon">
										to </span>
										<div class="date form_datetime input-icon right">
											<i class="icon fa"></i>
											<input type="text" size="16" readonly class="form-control" name="end_date" value='{{Session::get('end_date')}}' style='cursor: pointer;'>
											<span class="input-group-btn">
											<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
									<!-- <div class="input-group input-large date-picker input-daterange" data-date-format="mm/dd/yyyy">	
										<div class='form-group'>
										<div class='input-icon right'>
											<i class="icon fa"></i>
											<input type="text" class="form-control" name="begin_date">
										</div>
										</div>
										<span class="input-group-addon">
										to </span>
										<div class='form-group'>
										<div class='input-icon right'>
											<i class="icon fa"></i>
											<input type="text" class="form-control" name="end_date">
										</div>
										</div>
									</div> -->
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane row" id="tab2">
							<div class="col-md-6">
								<!-- Msg. Enter Preview-->
								<div class="form-group {{ $errors->has('msg_enter') ? 'error' : '' }}" style='display:none;'>
									<label class="control-label col-md-4" for="msg_enter" style="padding-top: 24px;">Msg. Enter</label>
									<div class="controls col-md-8">
										<div class='form-control' type="text" name="msg_enter" id="msg_enter" value="" style="height: 68px;" >
											<div class='col-md-3'>
												<img src="" id='img_preview'>
											</div>
											<div class='col-md-9' id='msg_enter_preview'></div>
										</div>
									</div>
								</div>
								
								<!-- Promote Status -->
								<div class="form-group {{ $errors->has('promote') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="promote">Activate Push</label>
									<div class="controls col-md-8">
										<input name='promote' type="checkbox" checked class="make-switch" data-size="normal" data-on-text='YES' data-off-text='NO' {{(Session::get('promote'))?'checked':''}}>
										{{ $errors->first('promote', '<span class="help-inline">:message</span>') }}
									</div>
								</div>

								<!-- Msg. Enter -->
								<div class="form-group {{ $errors->has('msg_enter') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="msg_enter">
										<span class='tooltips' data-container="body" data-placement="top" data-original-title="Push notification message for Enter from beacon">
											<i class="fa fa-info-circle"></i>
											Push Message
										</span>
									</label>
									<div class="controls col-md-8 input-icon right">
										<i class="icon fa"></i>
										<input class='form-control' type="text" name="msg_enter" value="{{ Input::old('msg_enter') }}" onkeyup="document.getElementById('msg_enter_preview').innerHTML = this.value" 
										id="maxlength_thresholdconfig" maxlength="30"/>
										{{ $errors->first('msg_enter', '<span class="help-inline">:message</span>') }}
									
										<!-- <span class="help-block">
											'default = device default message', 'empty= no msg'.
										</span> -->
									</div>
								</div>

								<!-- More -->
								<div class="more">
									<a class="btn btn-info" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">More</a>
									<div class="collapse" id="collapseExample2">

										<!-- Cover -->
										<div class="form-group {{ $errors->has('cover') ? 'error' : '' }}">
											<label class="control-label col-md-4" for="cover">
												<span class='tooltips' data-container="body" data-placement="top" data-original-title="Cover url (for some apps only)">
													<i class="fa fa-info-circle"></i>
													Cover
												</span>
											</label>
											<div class="controls col-md-8 input-icon right">
												<div class="fileinput fileinput-new col-md-8" data-provides="fileinput" style="padding:0px ! important;">
												  <div class="fileinput-preview thumbnail " data-trigger="fileinput" style="width: 300px; height: 200px;"></div>
												  <div <span class="fileinput-filename"></span></div>
												  <div>
												    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="cover-img"></span>
												    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
												  </div>
												</div>
											</div>
										</div>

										<!-- TTL. Enter -->
										<div class="form-group {{ $errors->has('ttl_enter') ? 'error' : '' }}">
											<label class="control-label col-md-4" for="ttl_enter">
												<span class='tooltips' data-container="body" data-placement="top" data-original-title="Repeating campaign message on a beacon at enter">
													<i class="fa fa-info-circle"></i>
													Repeat Push Each
												</span>
											</label>
											<div class="controls col-md-8">
												<div class="input-group" id='spinner1'>
													<input readonly class='spinner-input form-control' type="number" name="ttl_enter" id="ttl_enter" value="{{ Input::old('ttl_enter','24') }}" />
													<span class="input-group-addon">
													<i class="fa">hrs</i>
													</span>
													<div class="spinner-buttons input-group-btn btn-group-vertical">
														<button type="button" class="btn spinner-up btn-xs blue">
														<i class="fa fa-angle-up"></i>
														</button>
														<button type="button" class="btn spinner-down btn-xs blue">
														<i class="fa fa-angle-down"></i>
														</button>
													</div>
												</div>
												{{ $errors->first('ttl_enter', '<span class="help-inline">:message</span>') }}
											
												<!-- <span class="help-block">
													TTL in minutes for the push notification to appear.
												</span> -->
											</div>
										</div>
										<!-- Msg. Exit -->
										<!--
										<div class="form-group {{ $errors->has('msg_exit') ? 'error' : '' }}">
											<label class="control-label col-md-4" for="msg_exit">
												<span class='tooltips' data-container="body" data-placement="top" data-original-title="Push notification message for Exit from beacon">
													<i class="fa fa-info-circle"></i>
													Message Exit
												</span>
											</label>
											<div class="controls col-md-8">
												<input class='form-control' type="text" name="msg_exit" value="{{ Input::old('msg_exit') }}"  id="maxlength_thresholdconfig2" maxlength="30"/>
												{{ $errors->first('msg_exit', '<span class="help-inline">:message</span>') }}
											
											</div>
										</div>-->
										<!-- TTL. Exit -->
										<!--
										<div class="form-group {{ $errors->has('ttl_exit') ? 'error' : '' }}">
											<label class="control-label col-md-4" for="ttl_exit">
												<span class='tooltips' data-container="body" data-placement="top" data-original-title="Repeating campaign message on a beacon at exit">
													<i class="fa fa-info-circle"></i>
													Repeat Message Exit Each
												</span>
											</label>
											<div class="controls col-md-8">
												<div class="input-group" id='spinner11'>
													<input readonly class='spinner-input form-control' type="number" name="ttl_exit" id="ttl_exit" value="{{ Input::old('ttl_exit','24') }}" />
													<span class="input-group-addon">
													<i class="fa">hrs</i>
													</span>
													<div class="spinner-buttons input-group-btn btn-group-vertical">
														<button type="button" class="btn spinner-up btn-xs blue">
														<i class="fa fa-angle-up"></i>
														</button>
														<button type="button" class="btn spinner-down btn-xs blue">
														<i class="fa fa-angle-down"></i>
														</button>
													</div>
												</div>
												{{ $errors->first('ttl_exit', '<span class="help-inline">:message</span>') }}
											</div>
										</div>
										-->
									</div>
								</div>
								<!-- <label class="col-md-8 col-md-offset-4"><button type="button" onclick="PreviewNotification();" class="btn btn-info">Preview</button></label> -->
							</div>
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('type') ? 'error' : '' }}">
									<label class="control-label col-md-4" for="type">
										<span class='tooltips' data-container="body" data-placement="top" data-original-title="Type of content for campaign">
											<i class="fa fa-info-circle"></i>
											Media Type
										</span>
									</label>
									<div class="controls col-md-8">
										<select class='form-control' name="type" id="type">
											@foreach ($types as $type)
								    			@if($type == 'image')
								    				<option value="{{ $type }}" selected="selected">{{ $type }}</option>
								    			@else
								    				<option value="{{ $type }}">{{ $type }}</option>
								    			@endif
											@endforeach
										</select>
										{{ $errors->first('type', '<span class="help-inline">:message</span>') }}
									</div>
								</div>
								<div class="form-group" id="placeHolderMediaControl">
								</div>
							</div>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-12">
						<a class="btn default" href="{{ route('campaigns') }}">Cancel</a>
						<!--<button type="reset" class="btn btn-default">Reset</button>-->
						<a href="javascript:;" class="btn default button-previous">
						<i class="m-icon-swapleft" id="back"></i> Back </a>
						<a href="javascript:;" class="btn blue button-next" id="next">
						Next <i class="m-icon-swapright m-icon-white"></i>
						</a>
						<button type="submit" class="btn blue button-submit" name='save_submit'>
						Save Campaign
						</button>
						<input type="submit" class="btn blue button-submit" name='save_create_submit' value='Save and Create New'>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="templates-page">
	<div class="form-group {{ $errors->has('covertype') ? 'error' : '' }}" style="display:none" id="templateImageTarget">
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label col-md-4" for="url">
				<span data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="URL for Image">
					<i class="fa fa-info-circle" ></i>
					Image
				</span>
			</label>
			<div class="fileinput fileinput-new col-md-8" data-provides="fileinput" >
			  <div class="fileinput-preview thumbnail col-md-8" data-trigger="fileinput" style="width: 100%; height: 250px;"></div>
			  <div <span class="fileinput-filename"></span></div>
			  <div>
			    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="image-input"></span>
			    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
			  </div>
			</div>
		</div>
	</div>

	<div class="form-group" style="display:none" id="templateURLTarget">
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label col-md-4" for="url">
				<span data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="URL for Image">
					<i class="fa fa-info-circle" ></i>
					Web address
				</span>
			</label>
			<div class="controls col-md-8">
				<input class='form-control' type="text" name="url" id="url" value="{{ Input::old('url') }}" placeholder="http://www.bluapp.cl"/>
				{{ $errors->first('url', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
	</div>

	<div class="form-group" style="display:none" id="templateVideoTarget">
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label col-md-4" for="url">
				<span data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="URL for Image">
					<i class="fa fa-info-circle" ></i>
					Video address
				</span>
			</label>
			<div class="controls col-md-8">
				<input class='form-control' type="text" name="video" id="url" value="{{ Input::old('url') }}" placeholder="YouTube.com url"/>
				{{ $errors->first('url', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
	</div>

</div>
@section('tableadvanced2')
   	FormWizard.init();
   	ComponentsPickers.init();
   	FormDropzone.init();
@stop
<!-- @section('css')
<link href="/assetsnew/global/plugins/dropzone/css/dropzone.css" rel="stylesheet">
@stop -->
@section('wysihtml5css')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<!-- END PAGE LEVEL STYLES -->
@stop
@section('wysihtml5js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link rel="stylesheet" type="text/css" href="/assetsnew/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assetsnew/global/plugins/jquery-validation/js/additional-methods.min.js"></script>

<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="/assetsnew/global/plugins/select2/select2.min.js"></script>

<!-- Scripts de Santiago -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<!-- END -->

<script src="/assetsnew/admin/pages/scripts/form-wizard.js"></script>

<script type="text/javascript" src="/assetsnew/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assetsnew/admin/pages/scripts/components-pickers.js"></script>

<!-- <script src="/assetsnew/global/plugins/dropzone/dropzone.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-dropzone.js"></script> -->
<!-- END PAGE LEVEL SCRIPTS -->
<script src="/assetsnew/global/plugins/dropzone/dropzone.js"></script>
<script src="/assetsnew/admin/pages/scripts/form-dropzone2.js"></script>
@stop
@section('javascript')
<script type="text/javascript">
	//Ajax load proximity
	function showProximity() {
		var optionSelected = $('#ibeacon').find("option:selected");
    	var beaconid = optionSelected.val();
    	if (beaconid !=='') {
	    	$.ajax({
	        type: "post",
	        url: $('#submit_form').attr('action'),
	        dataType:"html",
	        data: {beaconid: beaconid},
		    success :function(proximity){
		    	var value = JSON.parse(proximity);
		    	if (value==='far') {
		    		$('.proximity .radio-inline input').parent().removeClass('checked');
		    		$('.proximity #inlineRadio3').parent().addClass('checked');
		    	}else if (value==='near') {
		    			$('.proximity .radio-inline input').parent().removeClass('checked');
			    		$('.proximity #inlineRadio2').parent().addClass('checked');
		    		}else if (value==='immediate') {
		    				$('.proximity .radio-inline input').parent().removeClass('checked');
				    		$('.proximity #inlineRadio1').parent().addClass('checked');
	    				}
	    		}
			});
		}else $('.proximity .radio-inline input').parent().removeClass('checked');
	}

	//Reset
	$("button[type='reset']").click(function() {
		$('#img_preview').attr('src','');
		$('#msg_enter_preview').empty();

		$('#placeHolderMediaControl').html($('#templateImageTarget').html());

		$('#holder-covertype').attr('src','');
		$('.htmlcontent').empty();
		$('#frameurl').attr('src','');
	})

	//Preview htmlpreview
	function htmlpreview() {
		$('.htmlcontent').html($('.textarea').val());
	}

	//Preview url
	function urlpreview() {
		$('#frameurl').attr('src',$('#url').val());
	}

	//Preview image
    function PreviewCover() {
    	var src = $('.dropzone .dz-preview .dz-details img').attr('src');
    	$('#holder-covertype').attr('src', src);
    };

	var changeControl = function(controlValue)
	{
		//Change the control on the fly, for images, html

		if (controlValue === 'html-url')
		{
			$('#placeHolderMediaControl').html($('#templateURLTarget').html());
		}

		if (controlValue == 'video')
		{
			$('#placeHolderMediaControl').html($('#templateVideoTarget').html());
		}
		/*
		if (controlValue === 'html-embeded')
		{
			$('.htmlcontent').empty();
			$('#placeHolderMediaControl').html($('#templateHtmlTarget').html());
			$('#placeHolderMediaControl .textarea').wysihtml5();
		}*/

		/*
		if (controlValue === 'sound' )
		{
			$('#placeHolderMediaControl').html('<br><br> comming soon ... <br><br>');
		}*/

		if (controlValue === 'image')
		{
			$('#placeHolderMediaControl').html($('#templateImageTarget').html());
		}
		$('[data-toggle="tooltip"]').on('hover', function() {
			$(this).tooltip('toggle');
		});
	}

	//forces to change type component
	changeControl('image');

	//on chage select type, change control
	$('#type').change(function()
	{
		changeControl($('#type').val());
	});

	$("#ibeacon").change(function (e)
	{
		var proximityUUID = $("#ibeacon option:selected").attr('data-proximityUUID');
	    $('#labelProximityUUID').text(proximityUUID);
	});

	$('#local').change(function() {
		var url = "{{ URL::to('admin/campaigns/create?selectedLocal=')}}" + $('#local').val();
		window.location = url;
	});

	var inBasicData = true;

	$('#next').click(function(){
		inBasicData = false;
	});

	$('#back').click(function(){
		inBasicData = true;
	});

	$('#basicdata').click(function(){
		if(inBasicData == false){
			$('#back').trigger('click');
			inBasicData = true;
		}
	});

	$('#mediasettings').click(function(){
		if(inBasicData == true){
			$('#next').trigger('click');
			inBasicData = false;
		}
	});
		
</script>

@stop