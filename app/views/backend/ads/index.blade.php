@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Ad Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>
		Ad Management : Local

		<select name='local' id='local'>
			@if(is_null($selectedLocal))
				<option value="0">All</option>
			@endif
    		@foreach ($locals as $local)
    			@if($local->id == $selectedLocal)
    				<option value="{{ $local->id }}" selected="selected">{{ $local->name }}</option>
    			@else
    				<option value="{{ $local->id }}">{{ $local->name }}</option>
    			@endif
			@endforeach
  		</select>
		<div class="pull-right">
			<a href="{{ route('create/ad') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Create</a>
		</div>
	</h3>
</div>

<a class="btn btn-default navbar-btn" href="{{ URL::to('admin/ads') }}">Show Ads</a>
<a class="btn btn-default navbar-btn" href="{{ URL::to('admin/ads?withTrashed=true') }}">Include Deleted Ads</a>
<a class="btn btn-default navbar-btn" href="{{ URL::to('admin/ads?onlyTrashed=true') }}">Include Only Deleted Ads</a>

<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="span1">@lang('admin/ads/table.id')</th>
			<th class="span2">@lang('admin/ads/table.type')</th>
			<th class="span2">@lang('admin/ads/table.title')</th>
			<th class="span2">@lang('admin/ads/table.ibeacon')</th>
			<th class="span2">@lang('admin/ads/table.promote')</th>
			<th class="span2">@lang('admin/ads/table.msg_enter')</th>
			<th class="span2">@lang('admin/ads/table.msg_exit')</th>
			<th class="span2">@lang('admin/ads/table.local')</th>
			<th class="span2">@lang('admin/ads/table.display')</th>
			<th class="span2">@lang('admin/users/table.activated')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($ads as $ad)
		<tr>
			<td>{{ $ad->id }}</td>
			<td>{{ $ad->type }}</td>
			<td>{{ $ad->title }}</td>
			<td>{{ isset($ad->ibeacon) ? $ad->ibeacon->proximityUUID : '' }}</td>
			<td>{{ $ad->promote }}</td>
			<td>{{ $ad->msg_enter }}</td>
			<td>{{ $ad->msg_exit }}</td>
			<td>{{ isset($ad->ibeacon->local) ? $ad->ibeacon->local->name : '' }}</td>
			<td>@lang('general.' . ($ad->isBookshelf() ? 'yes' : 'no'))</td>
			<td>@lang('general.' . ($ad->isActive() ? 'yes' : 'no'))</td>
			<td>
				<a href="{{ route('update/ad', $ad->id) }}" class="btn btn-mini">@lang('button.edit')</a>

				@if ( ! is_null($ad->deleted_at))
					<a href="{{ route('restore/ad', $ad->id) }}" class="btn btn-mini btn-warning">@lang('button.restore')</a>
				@else
					<a href="{{ route('delete/ad', $ad->id) }}" class="btn btn-mini btn-danger">@lang('button.delete')</a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

@stop


@section('javascript')
<script type="text/javascript">

	<!--Paint all the selects from the bootstrap-select -->
	$('#local').selectpicker();
	$('#local').change(function() {
		var url = "{{ URL::to('admin/ads?selectedLocal=')}}" + $('#local').val();
		window.location = url;
	});
</script>
@stop
