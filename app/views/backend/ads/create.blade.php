@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Create Ad
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-header">
	<h3>
		Create a New Ad : Local

		<select name='local' id='local'>
    		@foreach ($locals as $local)
    			@if($local->id == $selectedLocal)
    				<option value="{{ $local->id }}" selected="selected">{{ $local->name }}</option>
    			@else
    				<option value="{{ $local->id }}">{{ $local->name }}</option>
    			@endif
			@endforeach
  		</select>

		<div class="pull-right">
			<a href="{{ route('ads') }}" class="btn btn-small btn-inverse"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	<li><a href="#tab-media" data-toggle="tab">Media</a></li>
	<li><a href="#tab-notification" data-toggle="tab">Notification</a></li>
</ul>

<form class="form-horizontal" method="post" action="" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="cover64" id= "cover64" value="" />
	<input type="hidden" name="covertype64" id= "covertype64" value="" />

	<!-- Tabs Content -->
	<div class="tab-content">

		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">
				<div class="col-md-4">

					<!-- Ibeacon -->
					<div class="control-group {{ $errors->has('ibeacon') ? 'error' : '' }}" style="width:800px">
						<label class="control-label" for="ibeacon">Ibeacon</label>
						<div class="controls" >
							<select name="ibeacon" id="ibeacon" >
								<option value="0" data-proximityUUID="N/A">Select One</option>
								@foreach ($ibeacons as $ibeacon)
					    			<option value="{{ $ibeacon['id'] }}" data-proximityUUID="{{ $ibeacon['proximityUUID'] . ' Major:' . $ibeacon['major'] . ' Minor:' . $ibeacon['minor'] }}">{{ $ibeacon['peripheralID'] }}</option>
								@endforeach
							</select>

							<span> ProximityUUID : </span>
							<span id="labelProximityUUID">N/A</span>

							{{ $errors->first('ibeacon', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- Display Status -->
					<div class="control-group {{ $errors->has('bookshelf') ? 'error' : '' }}">
						<label class="control-label" for="bookshelf">Display Bookshelf</label>
						<div class="controls">
							<select name="bookshelf" id="bookshelf">
								<option value="1"{{ (Input::old('bookshelf', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('bookshelf', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('bookshelf', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Tittle -->
					<div class="control-group {{ $errors->has('title') ? 'error' : '' }}">
						<label class="control-label" for="Title">Title</label>
						<div class="controls">
							<input type="text" name="title" id="title" value="{{ Input::old('title') }}" />
							{{ $errors->first('title', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- Cover -->
					<div class="control-group {{ $errors->has('cover') ? 'error' : '' }}">
						<label class="control-label" for="cover">Cover</label>
						<div><img id="holder-cover" src="/avatars/pacman.png"></div>
						<br>
						<div class="controls" style="width:700px">
							<input id="cover"  name="Cover" type="file" accept="image/*" >
							{{ $errors->first('cover', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Description -->
					<div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
						<label class="control-label" for="description">Description</label>
						<div class="controls">
							<input type="text" name="description" id="description" value="{{ Input::old('description') }}" />
							{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
						</div>
					</div>


					<!-- Activation Status -->
					<div class="control-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label" for="activated">Ad Activated</label>
						<div class="controls">
							<select name="activated" id="activated">
								<option value="1"{{ (Input::old('activated', 0) === 1 ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ (Input::old('activated', 0) === 0 ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Tabs Media -->
		<div class="tab-pane" id="tab-media">
			<div class="row">
				<div class="col-md-4">

					<!-- Type 'image','video','sound','html'  -->
					<div class="control-group {{ $errors->has('type') ? 'error' : '' }}">
						<label class="control-label" for="type">Media Type</label>
						<div class="controls">
							<select name="type" id="type">
								@foreach ($types as $type)
					    			@if($type == 'image')
					    				<option value="{{ $type }}" selected="selected">{{ $type }}</option>
					    			@else
					    				<option value="{{ $type }}">{{ $type }}</option>
					    			@endif
								@endforeach
							</select>
							{{ $errors->first('type', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<div id="placeHolderMediaControl" style="width:860px;">
					</div>

				</div>
			</div>
		</div>

		<!-- Tabs Notification -->
		<div class="tab-pane" id="tab-notification">
			<div class="row">
				<div class="col-md-4">

					<!-- Promote Status -->
					<div class="control-group {{ $errors->has('promote') ? 'error' : '' }}">
						<label class="control-label" for="promote">Promote Activated</label>
						<div class="controls">
							<select name="promote" id="promote">
								<option value="1" selected="selected">@lang('general.yes')</option>
								<option value="0">@lang('general.no')</option>
							</select>
							{{ $errors->first('promote', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Msg. Exit -->
					<div class="control-group {{ $errors->has('msg_exit') ? 'error' : '' }}">
						<label class="control-label" for="msg_exit">Msg. Exit</label>
						<div class="controls">
							<input type="text" name="msg_exit" id="msg_exit" value="{{ Input::old('msg_exit') }}" />
							{{ $errors->first('msg_exit', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								'default = device default message', 'empty= no msg'.
						</span>
					</div>

					<!-- TTL. Exit -->
					<div class="control-group {{ $errors->has('ttl_exit') ? 'error' : '' }}">
						<label class="control-label" for="ttl_exit">Time To Live For Exit</label>
						<div class="controls">
							<input type="text" name="ttl_exit" id="ttl_exit" value="{{ Input::old('ttl_exit','0') }}" />
							{{ $errors->first('ttl_exit', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								TTL in minutes for the push notification to appear.
						</span>
					</div>


					<!-- Msg. Enter -->
					<div class="control-group {{ $errors->has('msg_enter') ? 'error' : '' }}">
						<label class="control-label" for="msg_enter">Msg. Enter</label>
						<div class="controls">
							<input type="text" name="msg_enter" id="msg_enter" value="{{ Input::old('msg_enter') }}" />
							{{ $errors->first('msg_enter', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								'default = device default message', 'empty= no msg'.
						</span>
					</div>

					<!-- TTL. Enter -->
					<div class="control-group {{ $errors->has('ttl_enter') ? 'error' : '' }}">
						<label class="control-label" for="ttl_enter">Time To Live For Enter</label>
						<div class="controls">
							<input type="text" name="ttl_enter" id="ttl_enter" value="{{ Input::old('ttl_enter','0') }}" />
							{{ $errors->first('ttl_enter', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								TTL in minutes for the push notification to appear.
						</span>
					</div>
				</div>
			</div>
		</div>

	</div>

	<br>

	<!-- Form Actions -->
	<div class="control-group">
		<div class="controls">
			<a class="btn btn-link" href="{{ route('locals') }}">Cancel</a>

			<button type="reset" class="btn">Reset</button>

			<button type="submit" class="btn btn-success">Create Ad</button>
		</div>
	</div>
</form>



<div class="templates-page">

	<!-- HTML -->
	<div style="display:none" id="templateHtmlTarget">

		<input type="hidden" name="url" value=""/>

		<!-- Body -->
		<div class="control-group {{ $errors->has('body') ? 'error' : '' }}">
			<label class="control-label" for="body">HTML</label>
			<div class="controls">
				<textarea name="body" id="body" cols="30" rows="5" class="form-control" placeholder="embeded">{{ Input::old('body') }}</textarea>
				{{ $errors->first('body', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

	</div>



	<div style="display:none" id="templateUrlTarget">

		<!-- send body empty value as hidden -->
		<input type="hidden" name="body" value=""/>

		<!-- URL -->
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label" for="url">URL</label>
			<div class="controls">
				<input type="text" name="url" id="url" value="{{ Input::old('url') }}" placeholder="http://example.com"/>
				{{ $errors->first('url', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

	</div>




	<!-- Cover Type -->
	<div class="control-group {{ $errors->has('covertype') ? 'error' : '' }}" style="display:none" id="templateImageTarget">

		<!-- send body empty value as hidden -->
		<input type="hidden" name="body" value=""/>

		<label class="control-label" for="covertype">Cover Image</label>
		<div><img id="holder-covertype" src="/avatars/pacman.png"></div>
		<br>
		<div class="controls" style="width:700px">

			<!-- send url value as hidden -->
			<input type="hidden" name="url" id="url" value="{{ Input::old('url') }}"/>

			<input id="covertype"  name="covertype" type="file" accept="image/*" >

			{{ $errors->first('covertype', '<span class="help-inline">:message</span>') }}
		</div>
	</div>

</div>

@stop

@section('javascript')
<script type="text/javascript">

	<!--Paint all the selects from the bootstrap-select -->
	$('select').selectpicker();

	var changeControl = function(controlValue)
	{
		//Change the control on the fly, for images, html

		if (controlValue === 'html-url' || controlValue === 'video')
		{
			$('#placeHolderMediaControl').html($('#templateUrlTarget').html());
		}

		if (controlValue === 'html-embeded')
		{
			$('#placeHolderMediaControl').html($('#templateHtmlTarget').html());
			$('#body').wysihtml5({
				"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
				"emphasis": true, //Italics, bold, etc. Default true
				"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
				"html": true, //Button which allows you to edit the generated HTML. Default false
				"link": true, //Button to insert a link. Default true
				"image": true, //Button to insert an image. Default true,
				"color": true //Button to change color of font
			});

			$('.wysihtml5-toolbar').each(function(index){
				if (index>0) {
					$(this).remove();
				}
			});
		}

		if (controlValue === 'sound' )
		{
			$('#placeHolderMediaControl').html('<br><br> comming soon ... <br><br>');
		}

		if (controlValue === 'image' )
		{
			$('#placeHolderMediaControl').html($('#templateImageTarget').html());

			//Setup
			$("#covertype").fileinput({
				previewFileType: "image",
				browseClass: "btn btn-success",
				browseLabel: "Pick Image",
				browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
				removeClass: "btn btn-danger",
				removeLabel: "Delete",
				removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
				uploadClass: "btn btn-info",
				uploadLabel: "Upload",
				uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
				uploadUrl: "{{route('cover-ad-type')}}",
				showUpload: false
			});

			$("#covertype").change(function (e)
			{
			    readImage(e, $("#covertype"), function(){
			    	$('#covertype64').val($("#covertype").attr('base64'));
			    	console.log('covertype64data:', $('#covertype64').val() );
			    });
			});

		}
	}

	//forces to change type component
	changeControl('image');

	//on chage select type, change control
	$('#type').change(function()
	{
		changeControl($('#type').val());
	});


	$("#ibeacon").change(function (e)
	{
		var proximityUUID = $("#ibeacon option:selected").attr('data-proximityUUID');
	    $('#labelProximityUUID').text(proximityUUID);
	});


	$('#local').change(function() {
		var url = "{{ URL::to('admin/ads/create?selectedLocal=')}}" + $('#local').val();
		window.location = url;
	});


	$("#cover").fileinput({
		previewFileType: "image",
		browseClass: "btn btn-success",
		browseLabel: "Pick Image",
		browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
		removeClass: "btn btn-danger",
		removeLabel: "Delete",
		removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
		uploadClass: "btn btn-info",
		uploadLabel: "Upload",
		uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
		uploadUrl: "{{route('cover-company')}}",
		showUpload: false
	});


	$("#cover").change(function (e)
	{
	    readImage(e, $("#cover"), function(){
	    	$('#cover64').val($("#cover").attr('base64'));
	    	console.log('data:', $('#cover64').val() );
		});

	});

	$('.input-group-btn a').click(function(e) {
	    e.preventDefault();
	    var coverInput = $(this).parent().find('input').attr('id');
		$('#' + coverInput +'64').val($("#"+coverInput).attr('base64'));
	});

	//============================================== Cover
	//utils for upload component
	function readImage(evt, component, callback) {
		var files = evt.target.files;
	    var file = files[0];

	    if (files && file) {

	        var FR= new FileReader();

	        FR.onload = function(readerEvt)
	        {
	              var binaryString = readerEvt.target.result;
	              component.attr('base64',binaryString);
	              callback();
	        };

	        FR.readAsDataURL( file );
	    }
	}


</script>

@stop

