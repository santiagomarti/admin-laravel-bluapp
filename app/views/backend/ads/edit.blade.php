@extends('backend/layouts/defaultnew')

{{-- Page title --}}
@section('title')
Ad Update
@parent
@stop

{{-- Page content --}}
@section('content')


<div class="page-header">
	<h3>
		Ad Update

		<div class="pull-right">
			<a href="{{ route('ads') }}" class="btn btn-small btn-inverse"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	<li><a href="#tab-media" data-toggle="tab">Media</a></li>
	<li><a href="#tab-notification" data-toggle="tab">Notification</a></li>
</ul>

<form class="form-horizontal" method="post" action="" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Tabs Content -->
	<div class="tab-content">

		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="row">
				<div class="col-md-4">

					<div class="control-group {{ $errors->has('ibeacon') ? 'error' : '' }}" style="width:800px">
						<label class="control-label" for="ibeacon">Ibeacon</label>
						<div class="controls" >
							<select name="ibeacon" id="ibeacon" >
								@foreach ($ad->ibeacon->local->ibeacons as $ibeacon)
					    			@if($ibeacon->id == $ad->ibeacon->id)
					    				<option value="{{ $ibeacon->id }}" selected="selected" data-proximityUUID="{{ $ibeacon->proximityUUID . ' Major:' . $ibeacon->major . ' Minor' . $ibeacon->minor }}">{{ $ibeacon->peripheralID }}</option>
					    			@else
					    				<option value="{{ $ibeacon->id }}" data-proximityUUID="{{ $ibeacon->proximityUUID . ' Major:' . $ibeacon->major . ' Minor:' . $ibeacon->minor }}">{{ $ibeacon->peripheralID }}</option>
					    			@endif
								@endforeach
							</select>

							<span> ProximityUUID : </span>
							<span id="labelProximityUUID">
								@foreach ($ad->ibeacon->local->ibeacons as $ibeacon)
					    			@if($ibeacon->id == $ad->ibeacon->id)
					    				{{ $ibeacon->proximityUUID . ' Major:' . $ibeacon->major . ' Minor:' . $ibeacon->minor }}
					    			@endif
								@endforeach
							</span>

							{{ $errors->first('ibeacon', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Display Status -->
					<div class="control-group {{ $errors->has('bookshelf') ? 'error' : '' }}">
						<label class="control-label" for="bookshelf">Display Bookshelf</label>
						<div class="controls">
							<select name="bookshelf" id="bookshelf">
								<option value="1"{{ ($ad->isBookshelf() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ ( ! $ad->isBookshelf() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('bookshelf', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Title -->
					<div class="control-group {{ $errors->has('title') ? 'error' : '' }}">
						<label class="control-label" for="title">Title</label>
						<div class="controls">
							<input type="text" name="title" id="title" value="{{ Input::old('title', $ad->title) }}" />
							{{ $errors->first('title', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Cover -->
					<div class="control-group {{ $errors->has('cover') ? 'error' : '' }}">
						<label class="control-label" for="cover">Cover</label>
						<div><img id="holder-cover" src="/avatars/{{$ad->cover}}"></div>
						<br>
						<div class="controls" style="width:700px">
							<input id="cover"  name="Cover" type="file" accept="image/*" >
							{{ $errors->first('cover', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Description -->
					<div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
						<label class="control-label" for="description">Description</label>
						<div class="controls">
							<input type="text" name="description" id="description" value="{{ Input::old('description', $ad->description) }}" />
							{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Activation Status -->
					<div class="control-group {{ $errors->has('activated') ? 'error' : '' }}">
						<label class="control-label" for="activated">Ad Activated</label>
						<div class="controls">
							<select name="activated" id="activated">
								<option value="1"{{ ($ad->isActive() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ ( ! $ad->isActive() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('activated', '<span class="help-inline">:message</span>') }}
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- media tab -->
		<div class="tab-pane" id="tab-media">
			<div class="row">
				<div class="col-md-4">

					<!-- Type 'image','video','sound','html'  -->
					<div class="control-group {{ $errors->has('type') ? 'error' : '' }}">
						<label class="control-label" for="type">Media Type</label>
						<div class="controls">
							<select name="type" id="type">
								@foreach ($types as $type)
									@if($type == $realtype)
					    				<option value="{{ $type }}" selected="selected">{{ $type }}</option>
					    			@else
					    				<option value="{{ $type }}">{{ $type }}</option>
					    			@endif
								@endforeach
							</select>
							{{ $errors->first('type', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<div id="placeHolderMediaControl" style="width:860px;">
					</div>

				</div>
			</div>
		</div>


		<!-- notification tab -->
		<div class="tab-pane" id="tab-notification">
			<div class="row">
				<div class="col-md-4">


					<!-- Promote Status -->
					<div class="control-group {{ $errors->has('promote') ? 'error' : '' }}">
						<label class="control-label" for="promote">Promote Activated</label>
						<div class="controls">
							<select name="promote" id="promote">
								<option value="1"{{ ($ad->isPromote() ? ' selected="selected"' : '') }}>@lang('general.yes')</option>
								<option value="0"{{ ( ! $ad->isPromote() ? ' selected="selected"' : '') }}>@lang('general.no')</option>
							</select>
							{{ $errors->first('promote', '<span class="help-inline">:message</span>') }}
						</div>
					</div>

					<!-- Msg. Exit -->
					<div class="control-group {{ $errors->has('msg_exit') ? 'error' : '' }}">
						<label class="control-label" for="msg_exit">Msg. Exit</label>
						<div class="controls">
							<input type="text" name="msg_exit" id="msg_exit" value="{{ Input::old('msg_exit', $ad->msg_exit) }}" />
							{{ $errors->first('msg_exit', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								'default = device default message', 'empty= no msg'.
						</span>
					</div>

					<!-- TTL. Exit -->
					<div class="control-group {{ $errors->has('ttl_exit') ? 'error' : '' }}">
						<label class="control-label" for="ttl_exit">Time To Live For Exit</label>
						<div class="controls">
							<input type="text" name="ttl_exit" id="ttl_exit" value="{{ Input::old('ttl_exit', $ad->ttl_exit) }}" />
							{{ $errors->first('ttl_exit', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								TTL in minutes for the push notification to appear.
						</span>
					</div>

					<!-- Msg. Enter -->
					<div class="control-group {{ $errors->has('msg_enter') ? 'error' : '' }}">
						<label class="control-label" for="msg_enter">Msg. Enter</label>
						<div class="controls">
							<input type="text" name="msg_enter" id="msg_enter" value="{{ Input::old('msg_enter', $ad->msg_enter) }}" />
							{{ $errors->first('msg_enter', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								'default = device default message', 'empty= no msg'.
						</span>
					</div>

					<!-- TTL. Enter -->
					<div class="control-group {{ $errors->has('ttl_enter') ? 'error' : '' }}">
						<label class="control-label" for="ttl_enter">Time To Live For Enter</label>
						<div class="controls">
							<input type="text" name="ttl_enter" id="ttl_enter" value="{{ Input::old('ttl_enter', $ad->ttl_enter) }}" />
							{{ $errors->first('ttl_enter', '<span class="help-inline">:message</span>') }}
						</div>
						<span class="help-block">
								TTL in minutes for the push notification to appear.
						</span>
					</div>
				</div>
			</div>
		</div>

	</div>


	<br>

	<!-- Form Actions -->
	<div class="control-group">
		<div class="controls">
			<a class="btn btn-link" href="{{ route('ads') }}">Cancel</a>

			<button type="reset" class="btn">Reset</button>

			<button type="submit" class="btn btn-success">Update Ad</button>
		</div>
	</div>
</form>

<br>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Done</h4>
      </div>
      <div class="modal-body" id="response">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="templates-page">

	<!-- HTML -->
	<div style="display:none" id="templateHtmlTarget">

		<input type="hidden" name="url" value=""/>

		<!-- Body -->
		<div class="control-group {{ $errors->has('body') ? 'error' : '' }}">
			<label class="control-label" for="body">HTML</label>
			<div class="controls">
				<textarea name="body" id="body" cols="30" rows="10" class="form-control" placeholder="embeded">{{ Input::old('body', $ad->body) }}</textarea>
				{{ $errors->first('body', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

	</div>


	<div style="display:none" id="templateUrlTarget">

		<!-- send body empty value as hidden -->
		<input type="hidden" name="body" value=""/>


		<!-- URL -->
		<div class="control-group {{ $errors->has('url') ? 'error' : '' }}">
			<label class="control-label" for="url">URL</label>
			<div class="controls">
				<input type="text" name="url" id="url" value="{{ Input::old('url', $ad->url) }}" />
				{{ $errors->first('url', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

	</div>

	<!-- Cover Type -->
	<div class="control-group {{ $errors->has('covertype') ? 'error' : '' }}" style="display:none" id="templateImageTarget">

		<!-- send body empty value as hidden -->
		<input type="hidden" name="body" value=""/>

		<label class="control-label" for="covertype">Cover Image</label>
		<div><img id="holder-covertype" src="/avatars/{{$ad->url}}"></div>
		<br>
		<div class="controls" style="width:700px">

			<!-- send url value as hidden -->
			<input type="hidden" name="url" id="url" value="{{ Input::old('url', $ad->url) }}"/>

			<input id="covertype"  name="covertype" type="file" accept="image/*" >

			{{ $errors->first('covertype', '<span class="help-inline">:message</span>') }}
		</div>
	</div>
</div>

@stop

@section('javascript')
<script>

<!--Paint all the selects from the bootstrap-select -->
$('select').selectpicker();


var changeControl = function(controlValue)
{


	//Change the control on the fly, for images, html
	if (controlValue === 'image')
	{
		$('#placeHolderMediaControl').html($('#templateImageTarget').html());

		//Setup
		$("#covertype").fileinput({
			previewFileType: "image",
			browseClass: "btn btn-success",
			browseLabel: "Pick Image",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: "Delete",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
			uploadLabel: "Upload",
			uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
			uploadUrl: "{{route('cover-ad-type')}}"
		});

		$("#covertype").change(function (e)
		{
		    readImage(e, $("#covertype"));
		});

	}

	if (controlValue === 'html-url' || controlValue === 'video')
	{
		$('#placeHolderMediaControl').html($('#templateUrlTarget').html());
	}


	if (controlValue === 'html-embeded')
	{
		$('#placeHolderMediaControl').html($('#templateHtmlTarget').html());
		$('#body').wysihtml5({
			"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
			"emphasis": true, //Italics, bold, etc. Default true
			"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
			"html": true, //Button which allows you to edit the generated HTML. Default false
			"link": true, //Button to insert a link. Default true
			"image": true, //Button to insert an image. Default true,
			"color": true //Button to change color of font
		});

		$('.wysihtml5-toolbar').each(function(index){
			if (index>0) {
				$(this).remove();
			}
		});

	}

	if (controlValue === 'sound' )
	{
		$('#body').html('');
		$('#placeHolderMediaControl').html('<br><br> comming soon ... <br><br>');
	}
}

var changeTypeControlInit = function()
{
	var type = $('#type').val();
	console.log('type is:', type, ' but is real:', '{{$ad->type}}');
	changeControl(type);
}

changeTypeControlInit();

//onn chage select type, change control
$('#type').change(function()
{
	changeControl($('#type').val());
});


//============================================== Cover
$("#cover").fileinput({
	previewFileType: "image",
	browseClass: "btn btn-success",
	browseLabel: "Pick Image",
	browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
	removeClass: "btn btn-danger",
	removeLabel: "Delete",
	removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
	uploadClass: "btn btn-info",
	uploadLabel: "Upload",
	uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
	uploadUrl: "{{route('cover-ad')}}"
});

$("#cover").change(function (e)
{
    readImage(e, $("#cover"));
});


//============================================== Cover
//utils for upload component
function readImage(evt, component) {
	var files = evt.target.files;
    var file = files[0];

    if (files && file) {

        var FR= new FileReader();

        FR.onload = function(readerEvt)
        {
              var binaryString = readerEvt.target.result;
              component.attr('base64',binaryString);
        };

        FR.readAsDataURL( file );
    }
}


//Component upload, to make it for multiple images, must
//setup holder-name of the compenent
//example #holder-covertype or #holder-cover
$('.input-group-btn a').click(function(e) {
    e.preventDefault();

    var coverInput = $(this).parent().find('input').attr('id');

    var urlAjax = $(this).attr('href');

    console.log('urlAjax Full 2:' , urlAjax);

    $.post(urlAjax,
	{
		ad_id: '{{$ad->id}}',
		cover: $("#" + coverInput).attr('base64'),
		covertype: $("#" + coverInput).attr('base64')
	}).done(function(response)
	{
	  $('#holder-' + coverInput).attr('src', $('.file-preview-image').attr('src'));
	  $('#response').html(response.msg);
	  $('#myModal').modal('show')
	})
	.fail(function(response) {
	  $('#response').html('failed to uplaod image');
	  $('#myModal').modal('show')
	})
	.always(function() {

	});
});


$("#ibeacon").change(function (e)
{
	var proximityUUID = $("#ibeacon option:selected").attr('data-proximityUUID');
    $('#labelProximityUUID').text(proximityUUID);
});

</script>
@stop
