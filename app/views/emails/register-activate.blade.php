@extends('emails/layouts/default')

@section('content')
<p>Hello {{ $user->first_name }},</p>

<p>Welcome to {{ Config::get('settings.siteName') }} Panic Button! Please click on the following link to confirm your {{ Config::get('settings.siteName') }} account:</p>

<p>For Web: <a href="{{ $activationUrl }}">{{ $activationUrl }}</a></p>

<p>For iPhone/Android: ibeacons://activate/{{$user->activation_code}}</p>

<p>Best regards,</p>

<p>{{ Config::get('settings.siteName') }} Team</p>
@stop
