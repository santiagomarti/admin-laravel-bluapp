@extends('emails/layouts/default')

@section('content')
<p>Hello {{ $user->email }},</p>

<p>{{ $user_from->first_name }} {{ $user_from->last_name }}  
	invited you to join iBeacons and be part of his emergency contacts list.
</p>

<br>

<p> To do so just follow these simple steps:</p>

<p>1) Download iBeacons From (APPSTORE) or (ANDROID)</p>

<p>2) Open this email in your device and click on the following link to set up your password:</p>

<p>For Web: <a href="{{ $forgotPasswordUrl }}">{{ $forgotPasswordUrl }}</a></p>

<p>For iPhone: ibeacons://forgot/{{$user->reset_password_code}}</p>


<p>Best regards,</p>

<p>{{ Config::get('settings.siteName') }} Team</p>

@stop
