@extends('emails/layouts/default')

@section('content')
<p>Hello {{ $user->first_name }},</p>

<p>Please click on the following link to updated your password:</p>

<p>For Web: <a href="{{ $forgotPasswordUrl }}">{{ $forgotPasswordUrl }}</a></p>

<p>For iPhone: ibeacons://forgot/{{$user->reset_password_code}}</p>

<p>Best regards,</p>

<p>{{ Config::get('settings.siteName') }} Team</p>
@stop
